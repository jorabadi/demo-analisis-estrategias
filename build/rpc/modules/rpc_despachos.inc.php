<?php

class Despachos {

    public static function loadPagePuntosDespacho($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if (!empty($p["column_filters"])) {
            foreach ($p["column_filters"] as $colum => $valorCulumn) {
                if ($valorCulumn != "") {
                    $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
                }
            }
        }
        $sql = " select  
                             codigo_ubicacion,
                             nombre,
                             direccion,
                             telefono,
                             ciudad,
                             estado,
                             case when es_por_defecto = 't' then 'SI'
                             else 'No' end as por_defecto
                        from (
                            select distinct a.codigo_ubicacion,a.nombre,a.direccion,a.telefono,b.nombre as ciudad,a.estado, a.es_por_defecto
                            from cu_proveedores_ubicaciones a 
                            left join cu_ciudades b on (a.idciudad=b.idciudad)
                            where a.codigo_proveedor=:codigo_proveedor and a.punto_despacho=true
                        ) tl
                        where ($where)
                ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $result = $ca->execPage($p);

        return $result;
    }

    public static function loadOnePuntoDespacho($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_proveedores_ubicaciones a join cu_ciudades b on (a.idciudad=b.idciudad)", "a.*,b.nombre as nombre_ciudad", "codigo_ubicacion=:codigo_ubicacion");
        $ca->bindValue(":codigo_ubicacion", $p["codigo_ubicacion"]);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("punto de despacho del proveedor no localizado");
        }

        $result = $ca->fetch();

        $result["ciudad"] = array(array("data" => $result["idciudad"], "label" => $result["nombre_ciudad"]));

        return $result;
    }

    public static function savePuntoDespacho($p) {
        $si = Session::info();

        if (!in_array("FPuntosDespachoNe_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }

        if (empty($p["nombre"])) {
            throw new JPublicException("Nombre inválido");
        }

        if (empty($p["telefono"])) {
            throw new JPublicException("Teléfono inválido");
        }

        if (empty($p["direccion"])) {
            throw new JPublicException("Dirección inválida");
        }

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        //throw new JPublicException(print_r($p,1));

        if (!isset($p["idciudad"][0]) || !is_numeric($p["idciudad"][0])) {
            throw new JPublicException("Ciudad invalida, elija una existente");
        }

        $p["codigo_ubicacion"] = isset($p["codigo_ubicacion"]) ? $p["codigo_ubicacion"] : "";

        $campos = "codigo_proveedor,codigo_ubicacion,nombre,idciudad,direccion,telefono,descripcion,punto_despacho";



        if ($p["codigo_ubicacion"] === "") {
            $ca->prepareInsert("cu_proveedores_ubicaciones", $campos);
            $codigoUbicacion = $db->nextVal("cu_proveedores_ubicaciones_codigo");
        } else {
            $ca->prepareUpdate("cu_proveedores_ubicaciones", $campos, "codigo_ubicacion=:codigo_ubicacion");
            $codigoUbicacion = $p["codigo_ubicacion"];
        }
        $ca->bindValue(":codigo_ubicacion", $codigoUbicacion, false);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":direccion", $p["direccion"], true);
        $ca->bindValue(":telefono", $p["telefono"], true);
        $ca->bindValue(":idciudad", $p["idciudad"][0]); //$si["proveedor"]["idciudad"]);
        $ca->bindValue(":descripcion", $p["descripcion"], true);
        $ca->bindValue(":punto_despacho", "true", false);

        $db->transaction();
        $ca->exec();
        $db->commit();

        return;
    }

    public static function loadRsRestriciones($p) {
        $si = session::info();
        $restricciones = array();
        $puntosDespachos = self::loadPagePuntosDespacho($p);
        $restricciones = $si['restricciones_paquetes'][get_class()];

        return array("restricciones" => $restricciones, "data" => array("cantidad_puntos_despachos" => $puntosDespachos['recordCount']));
    }

    public static function inactivarPuntoDespacho($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareUpdate("cu_proveedores_ubicaciones", "estado='inactivo'", "codigo_ubicacion=:codigo_ubicacion and codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_ubicacion", $p["codigo_ubicacion"]);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();

        return;
    }

    public static function marcarComoPuntoDespachoPorDefecto($p) {
        $si = session::info();
        $ca = new JDbQuery(JDatabase::database());

        $ca->prepareUpdate("cu_proveedores_ubicaciones", "es_por_defecto = FALSE", "codigo_proveedor = :codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();

        $ca->prepareUpdate("cu_proveedores_ubicaciones", "es_por_defecto = TRUE", "codigo_ubicacion = :codigo_ubicacion and codigo_proveedor = :codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->bindValue(":codigo_ubicacion", $p["codigo_ubicacion"]);
        $ca->exec();

        return true;
	}
        
        public static function anularSolicitudRecogida($p) {
            $si = session::info();
            return modDespachos::anularSolicitudRecogida($si["codigo_proveedor"], $p["codigo_pedido"]);
	}
	
	public static function cotizadorDespacho($p) {
	    $si = session::info();
	    
	    return modDespachos::calcularFleteDespacho($p);
	    
	}
	
	public static function loadCotizadorRs() {
	    $si = Session::info();
	    
	    
	    $result = array();
	    
	    $result["campos"] = array(
	        array("label" => "Gramo", "data" => "g"),
	        array("label" => "Kilogramo", "data" => "kg"),
	    );
	    
	    
	    return $result;
	}
        
        
}