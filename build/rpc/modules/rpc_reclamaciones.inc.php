<?php
require_once $cfg["rutaModeloLogico"]."mod_reclamaciones.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_mails.inc.php";

class Reclamaciones {
    
   public static function loadReclamacionesPage($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modReclamaciones::loadReclamacionesPage($p);
   }
   
   public static function loadEditRs() {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();

        //Estados
        $result["agente"] = array(
            array("label" => "Coordiutil", "data" => modAgentes::coordiutil),
            array("label" => "Tienda", "data" => modAgentes::tienda)
        );
        
        $result["asunto"] = array(
            array("label" => "Petición", "data" => "peticion"),
            array("label" => "Quejas", "data" => "quejas"),
            array("label" => "Reclamos", "data" => "reclamos"),
            array("label" => "Felicitaciones", "data" => "felicitaciones"),
            array("label" => "Garantías", "data" => "garantias"),
            array("label" => "Seleccione", "data" => "")
        );
        
        $result["info_cliente"]= array(
            array("campo"=>"nombre_cliente" , "label"=> "Nombre cliente", "valor"=> ""),
            array("campo"=>"apellido_cliente" , "label"=> "Apellido cliente", "valor"=> ""),
            array("campo"=>"email_cliente" , "label"=> "Email cliente", "valor"=> ""),
            array("campo"=>"direccion_notificacion_cliente" , "label"=> "Dirección notificación cliente", "valor"=> ""),
            array("campo"=>"telefono_cliente" , "label"=> "Telefono de contacto", "valor"=>""),
            array("campo"=>"identificacion_cliente" , "label"=> "Identificación cliente", "valor"=>"")
        );
        
        $result["estado"] = array(
            array("label" => "Pendiente", "data" => "pendiente"),
            array("label" => "Cerrado", "data" => "cerrado")
        );
        
        $result["procede_garantia"] = array(
            array("label" => "Si", "data" => "1"),
            array("label" => "No", "data" => "0")
        );
        
        $result["respuesta_garantia"] = array(
            array("label" => "Asignación de bono", "data" => "1"),
            array("label" => "Devolución de dinero", "data" => "2"),
            array("label" => "Cambio de producto", "data" => "3"),
            array("label" => "Seleccione", "data" => "0")
        );
        
        $result["medio_origen"] = array(
            array("label" => "Portal", "data" => "portal"),
            array("label" => "Chat", "data" => "chat"),
            array("label" => "Carta", "data" => "carta"),
            array("label" => "Correo", "data" => "correo"),
            array("label" => "Oficina", "data" => "oficina"),
            array("label" => "Telefono", "data" => "telefono")
        );
        
        $result["agente_seguimiento"] = array(
            array("label" => "Tienda", "data" => "tienda"),
            array("label" => "Cliente", "data" => "cliente")
        );

        $ca->prepareSelect("cu_proveedores","codigo_proveedor as data,nombre as label","estado_tienda='activo' ");
        $ca->exec();
        $result["proveedores"] = $ca->fetchAll();
        $result["proveedores"][] = array("label"=>"Coordiutil","data"=>"0");
        
        $ca->prepareSelect("cu_radicaciones_plantillas","codigo_plantilla as data,nombre as label","codigo_proveedor_pp=:codigo_proveedor_pp");
        $ca->bindValue(":codigo_proveedor_pp",$si["codigo_proveedor"],false);
        $ca->exec();
        $result["plantillas"] = $ca->fetchAll();
        $result["plantillas"][] = array("label"=>"Seleccione","data"=>"0");
        
        
        $ca->prepareSelect("view_cu_proveedores","flags,flags_pp","codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor",$si["codigo_proveedor"],false);
        $ca->exec();
        $rProv = $ca->fetch();
        
        $flagsProv = JUtils::pgsqlHStoreToArray($rProv["flags_pp"]);
        $result["flag_pqr_coordiutil"] = $flagsProv["flag_pqr_coordiutil"];
        
        
        $result["defaults"] = array(
            "agente" => modAgentes::coordiutil,
            "proveedores" => "0",
            "asunto" => "",
            "estado"=>"pendiente",
            "plantillas"=>"0",
            "medio_origen"=>"portal",
            "procede_garantia"=>"0",
            "respuesta_garantia"=>""
        );

        return $result;
    }
    
    public static function loadInfoPedido($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
         
        $result["info_pin"]= array();
        
        if(!empty($p["pin"])) {
            $ca->prepareSelect("view_cu_pedidos_det p","p.codigo_producto,p.referencia,p.nombre,p.nombre_proveedor,sum(p.unidades) as unidades,sum(p.precio_venta_coniva) as subtotal",
            "pin=:pin group by p.codigo_producto,p.referencia,p.nombre,p.nombre_proveedor");
            $ca->bindValue(":pin",$p["pin"],true);
            $ca->exec();
            $result["info_pin"]= $ca->fetchAll();
        }
        
        $ca->prepareSelect("cr_validaciones v","v.notas,v.fechahora",
        "pin=:pin");
        $ca->bindValue(":pin",$p["pin"],true);
        $ca->exec();
        $result["validaciones"]= $ca->fetchAll();
        
        return $result;
    }
    
    public static function loadOne($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $ca->prepareSelect("cu_radicaciones","cast(fechahora as date) as fechahora,cast(fechahora_radicacion as date) as fechahora_radicacion,asunto,agente,asunto,codigo_proveedor,
        nombres,apellidos,email_cliente,direccion_notificacion,telefono_contacto,identificacion,codigo_radicado,pin,estado,medio_origen,procede_garantia,respuesta_garantia"
        ,"codigo_radicado=:codigo_radicado");
        $ca->bindValue(":codigo_radicado", $p["codigo_radicado"],true);
        $ca->exec();
        $result = $ca->fetch();
        
        $result["info_cliente"] = array(
            array("campo"=>"nombre_cliente" , "label"=> "Nombre cliente", "valor"=> $result["nombres"]),
            array("campo"=>"apellido_cliente" , "label"=> "Apellido cliente", "valor"=> $result["apellidos"]),
            array("campo"=>"email_cliente" , "label"=> "Email cliente", "valor"=> $result["email_cliente"]),
            array("campo"=>"direccion_notificacion_cliente" , "label"=> "Dirección notificación cliente", "valor"=> $result["direccion_notificacion"]),
            array("campo"=>"telefono_cliente" , "label"=> "Telefono de contacto", "valor"=>$result["telefono_contacto"]),
            array("campo"=>"identificacion_cliente" , "label"=> "Identificación cliente", "valor"=>$result["identificacion"])
        );
        
        
        
        return $result;
    }

    /**
     * Carga los mensajes de seguimientos
     */
    public static function loadSeguimientos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $sql="select cuerpo,usuario,fechahora,agente
        from cu_mensajes
        where codigo_referencia=:codigo_referencia and tipo='seguimiento' ";
        
        //si el pin ya ha sido ingresado busca las notas de auditoria
        if(!empty($p["pin"])) {
            $sql.="union all
            select notas,'auditoria' as usuario,fechahora,'coordiutil' as agente
            from cr_validaciones
            where pin=:pin";
        }
        
        $sql="select cuerpo,usuario,fechahora,agente
        from ({$sql}) t order by fechahora asc ";
        
        $ca->prepare($sql);
        $ca->bindValue(":codigo_referencia", $p["codigo_radicado"],true);
        
        //si el pin ya ha sido ingresado busca las notas de auditoria
        if(!empty($p["pin"])) {
            $ca->bindValue(":pin", $p["pin"],true);    
        }
        
        $ca->exec();
        return $ca->fetchAll();
    }
    /*
     * Carga los correos que han sido enviados como respuesta al cliente
     */
    public static function loadCorreos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $sql="select cuerpo,usuario,fechahora,asunto
        from cu_mensajes
        where codigo_referencia=:codigo_referencia and tipo='pqr' order by  fechahora";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_referencia", $p["codigo_radicado"],true);
        $ca->exec();
        
        return $ca->fetchAll();
    }
    /***
     * En caso de seleccionar la plantilla trae los datos del asunt
     */
    public static function loadPlantilla($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $sql="select asunto,cuerpo
        from cu_radicaciones_plantillas
        where codigo_plantilla=:codigo_plantilla";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_plantilla", $p["codigo_plantilla"],false);
        $ca->exec();
        
        return $ca->fetch();
    }
    
    public static function exportarPqr($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $campos="pin,identificacion,codigo_radicado";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        
        if ($p["filters"]["estado"] != "todos") {
            $where .= " and r.estado='{$p["filters"]["estado"]}' ";
        }
        
        $filter="";
        if ( !empty($p["filters"]["pin"]) ) {
            $filter .= " r.pin='{$p["filters"]["pin"]}' and"; 
        }
        
        if ( !empty($p["filters"]["fecha_desde"]) && !empty($p["filters"]["fecha_hasta"]) ) {
            $where .= " and r.fechahora_radicacion between '{$p["filters"]["fecha_desde"]}'::date and '{$p["filters"]["fecha_hasta"]}'::date ";
        }
        
        if(!empty($si["codigo_proveedor"])) {
            $where .= " and r.agente='tienda' and r.codigo_proveedor='{$si["codigo_proveedor"]}' ";
        }

        $sql = "select r.codigo_radicado,r.pin,r.fechahora,r.fechahora_radicacion,r.nombres || ' '||r.apellidos as nombre_cliente, r.asunto,r.agente,p.nombre as nombre_tienda,r.estado,
         case when (current_timestamp - r.fechahora_radicacion  between INTERVAL '5 days' and INTERVAL '9 days') and r.estado <> 'cerrado' then 'yellow'
         when (current_timestamp - r.fechahora_radicacion > INTERVAL '10 days') and r.estado <> 'cerrado' then 'red' end as estado_color,usuario_agente
        from cu_radicaciones r left join cu_proveedores p on r.codigo_proveedor = p.codigo_proveedor
        where 1=1 {$filter} and ({$where})";

        $ca->prepare($sql);
        $ca->exec();

        $path = JApp::privateTempPath() . "/pqr_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws =& $wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {
                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }
    
    /**
 * Guardo la info del mensaje de seguimiento, las notas de los asesores, pero antes debo guardar el pqr o actualizarlo
 */
    public static function saveSeguimiento($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $p["usuario"]=$si["usuario"];
        $p["usuario_agente"]=$si["usuario"];
        $p["tipoAsunto"] = $p["asunto"];
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        
        foreach ($p["info_cliente"] as $infoCliente) {
            switch ($infoCliente["campo"]) {
                case "nombre_cliente":
                    $p["nombre"]=$infoCliente["valor"];
                    break;
                case "apellido_cliente":
                    $p["apellido"]=$infoCliente["valor"];
                    break;
                case "email_cliente":
                    $p["email"]=$infoCliente["valor"];
                    break;
                case "direccion_notificacion_cliente":
                    $p["direccionNotificacion"]=$infoCliente["valor"];
                    break;
                case "telefono_cliente":
                    $p["telefonoContacto"]=$infoCliente["valor"];
                    break;
                case "identificacion_cliente":
                    $p["identificacion"]=$infoCliente["valor"];
                    break;
            }
        }
        
        $db->transaction();
        $radicado =  modReclamaciones::saveReclamacion($p); //guardo la info de la reclamacion en caso de no estarlo
        $p["codigo_radicado"]= $radicado;
        modReclamaciones::saveSeguimiento($p); //guardo el mensaje de seguimiento
        $db->commit();
        return $radicado;
    }
/**
 * Guarda en base de datos los correos enviados por el asesor al cliente
 */
    public static function saveCorreo($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        //valido que en la plantilla no haya xx
        if(!empty($p["contenido_correo"]) && strpos(strtoupper($p["contenido_correo"]), strtoupper("xx"))!=false ) {
            throw new JPublicException("Debe llenar todos los datos del correo, y quitar las XX");
        }
        
        $p["usuario"]=$si["usuario"];
        $p["tipoAsunto"] = $p["asunto"];
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        
        foreach ($p["info_cliente"] as $infoCliente) {
            switch ($infoCliente["campo"]) {
                case "nombre_cliente":
                    $p["nombre"]=$infoCliente["valor"];
                    break;
                case "apellido_cliente":
                    $p["apellido"]=$infoCliente["valor"];
                    break;
                case "email_cliente":
                    
                    $p["email"]=$infoCliente["valor"];
                    break;
                case "direccion_notificacion_cliente":
                    $p["direccionNotificacion"]=$infoCliente["valor"];
                    break;
                case "telefono_cliente":
                    $p["telefonoContacto"]=$infoCliente["valor"];
                    break;
                case "identificacion_cliente":
                    $p["identificacion"]=$infoCliente["valor"];
                    break;
            }
        }
        
        $db->transaction();
        $radicado =  modReclamaciones::saveReclamacion($p); //guardo la info de la reclamacion en caso de no estarlo
        $p["codigo_radicado"]= $radicado;
        $codigoMensaje= modReclamaciones::saveCorreo($p); //guardo el correo
        
        //verifico que solo se pueda enviar esta respuesta si el agente es coordiutil
        if($p["agente"]=="coordiutil"){
            modMails::enviarRespuestaPqr(array("codigo_mensaje"=>$codigoMensaje));
        }
        else {
            modMails::enviarRespuestaPqrTienda(array("codigo_mensaje"=>$codigoMensaje));
        }
        
        $db->commit();
        
        return $radicado;
    }

 /**
     * Guarda la PQR completa
     */
   public static function saveReclamacion($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $p["usuario"]=$si["usuario"];
        $p["tipoAsunto"] = $p["asunto"];
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        
        foreach ($p["info_cliente"] as $infoCliente) {
            switch ($infoCliente["campo"]) {
                case "nombre_cliente":
                    $p["nombre"]=$infoCliente["valor"];
                    break;
                case "apellido_cliente":
                    $p["apellido"]=$infoCliente["valor"];
                    break;
                case "email_cliente":
                    $p["email"]=$infoCliente["valor"];
                    break;
                case "direccion_notificacion_cliente":
                    $p["direccionNotificacion"]=$infoCliente["valor"];
                    break;
                case "telefono_cliente":
                    $p["telefonoContacto"]=$infoCliente["valor"];
                    break;
                case "identificacion_cliente":
                    $p["identificacion"]=$infoCliente["valor"];
                    break;
            }
        }
        
        $db->transaction();
            $radicado= modReclamaciones::saveReclamacion($p); //guardo la info de la reclamacion
            $p["codigo_radicado"]= $radicado;
            if(!empty($p["seguimiento"])){
                modReclamaciones::saveSeguimiento($p); //guardo el mensaje de seguimiento
            }
        $db->commit();
        return $radicado;
   }
   /*
    * Carga todas las plantillas de correos de reclamaciones asociadas al proveedor
   */
   public static function loadPlantillasCorreos($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $campos="r.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        
        foreach ($p["column_filters"] as $colum  => $valorCulumn){
        	if($valorCulumn != ""){
        		$where .= " and ". $ca->sqlFieldsFilters($colum,$valorCulumn);
        	}
        }
        
        
        $sql = "select r.asunto,r.cuerpo,r.nombre,r.codigo_plantilla
        from cu_radicaciones_plantillas r
        where 1=1 and ({$where}) and r.codigo_proveedor_pp=:codigo_proveedor_pp ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
   }
   
   public static function loadPlantillaCorreoOne($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
         $sql = "select r.asunto,r.cuerpo,r.nombre,r.codigo_plantilla
         from cu_radicaciones_plantillas r
         where r.codigo_plantilla=:codigo_plantilla";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_plantilla", $p["codigo_plantilla"], false);
        $ca->exec();

        return $ca->fetch();
   }
   
   public static function savePlantillaCorreo($p) {
       $si = session::info();
       $db = JDatabase::database();
       $ca = new JDbQuery($db);
       
       $campos = "asunto,nombre,cuerpo,codigo_plantilla,codigo_proveedor_pp";
       $db->transaction();
       if ($p["codigo_plantilla"] === "") {
           $ca->prepareInsert("cu_radicaciones_plantillas", $campos);
           $codigoPlantilla = $db->nextVal("cu_radicaciones_plantillas_codigo_plantilla");
       }else {
           $ca->prepareUpdate("cu_radicaciones_plantillas", $campos, "codigo_plantilla=:codigo_plantilla");
           $codigoPlantilla = $p["codigo_plantilla"];
       }
       $ca->bindValue(":asunto", $p["asunto"], true);
       $ca->bindValue(":nombre", $p["nombre"], true);
       $ca->bindValue(":cuerpo", $p["cuerpo"], true);
       $ca->bindValue(":codigo_plantilla", $codigoPlantilla, false);
       $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
       $ca->exec();
       $db->commit();
   }
}