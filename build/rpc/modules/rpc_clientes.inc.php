<?php
JLib::requireOnceModule("fileformats/excel-2.0/jexcel2.0.inc.php");
date_default_timezone_set('America/Bogota');

class Clientes {

    public static function exportarClientes($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $sql = "
		select 
			distinct a.email_cliente as email,
			a.nombre_cliente,
			a.fecha_pago::date as fecha_compra,
			a.direccion_cliente as direccion,
			a.nombre_pais_cliente as nombre_pais,
			b.nombre_estado,
			a.nombre_ciudad_cliente as nombre_ciudad,
			a.telefono_fijo_cliente as telefono_fijo,
			a.telefono_celular_cliente as telefono_celular
		from 
			view_cu_pedidos_det a 
			left join cu_ciudades b on (a.idciudad_cliente=b.idciudad)
		where 
			a.agente='tienda' 
			and a.codigo_proveedor=:codigo_proveedor
			and a.estado_pin in ('pagado','pce') 
			and a.estado_autorizacion='aprobado'
		";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/proveedores_clientes_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = & $wb->addWorksheet('Hoja1');

        $bold = & $wb->addFormat(array(//'Size' => 10,
                    //'Align' => 'center',
                    'bold' => 1));
        $titulo = & $wb->addFormat(array('Size' => 10,
                    'Align' => 'center',
                    'bold' => 1));
        $clientes = $ca->fetchAll();

        $ws->writeString(0, 0, "EMAIL", $bold);
        $ws->writeString(0, 1, "NOMBRE", $bold);
        $ws->writeString(0, 2, "FECHA COMPRA", $bold);
        $ws->writeString(0, 3, utf8_decode("DIRECCIÓN"), $bold);
        $ws->writeString(0, 4, "PAIS", $bold);
        $ws->writeString(0, 5, "DEPARTAMENTO", $bold);
        $ws->writeString(0, 6, "CIUDAD", $bold);
        $ws->writeString(0, 7, utf8_decode("TELÉFONO FIJO"), $bold);
        $ws->writeString(0, 8, utf8_decode("TELÉFONO CELULAR"), $bold);

        $row = 1;
        foreach ($clientes as $rCliente) {
            $col = 0;
            foreach ($rCliente as $k => $v) {
                $ws->writeString($row, $col, utf8_decode($v));
                $col++;
            }
            $row++;
        }

        $wb->close();

        return basename($path);
    }

    public static function exportarUsuariosRegistrados($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $sql = "
        select 
		    cl.usuario as email,
		    cl.nombres||' '||cl.apellidos as nombre_completo,
		    cl.edad,
		    cl.sexo,
		    cl.fechahora_registro,
		    cl.direccion,
		    cl.nombre_pais,
		    cl.nombre_estado,
		    cl.nombre_ciudad,
		    cl.telefono_fijo,
		    cl.telefono_celular,
		    cl.utmz_campaign,
		    cl.utmz_medium,
		    cl.utmz_source,
		    case when cl.envio_correos='1' and r.correo_electronico is null then 'si' else 'no' end as autoriza_envio_correo
		from 
		    view_cu_clientes cl
		    left join bm_retirados r on (cl.usuario=r.correo_electronico and cl.codigo_proveedor=r.codigo_proveedor_pp)
		where 
		    cl.agente = 'tienda' 
		    and cl.codigo_proveedor=:codigo_proveedor
        ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/proveedores_usuarios_registrados.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = & $wb->addWorksheet('Hoja1');

        $bold = & $wb->addFormat(array(//'Size' => 10,
                    //'Align' => 'center',
                    'bold' => 1));
        $titulo = & $wb->addFormat(array('Size' => 10,
                    'Align' => 'center',
                    'bold' => 1));
        $clientes = $ca->fetchAll();

        $ws->writeString(0, 0, "EMAIL", $bold);
        $ws->writeString(0, 1, "NOMBRE", $bold);
        $ws->writeString(0, 2, "EDAD", $bold);
        $ws->writeString(0, 3, "SEXO", $bold);
        $ws->writeString(0, 4, "FECHA REGISTRO", $bold);
        $ws->writeString(0, 5, utf8_decode("DIRECCIÓN"), $bold);
        $ws->writeString(0, 6, "PAIS", $bold);
        $ws->writeString(0, 7, "DEPARTAMENTO", $bold);
        $ws->writeString(0, 8, "CIUDAD", $bold);
        $ws->writeString(0, 9, utf8_decode("TELÉFONO FIJO"), $bold);
        $ws->writeString(0, 10, utf8_decode("TELÉFONO CELULAR"), $bold);
        $ws->writeString(0, 11, utf8_decode("CAMPANA"), $bold);
        $ws->writeString(0, 12, utf8_decode("MEDIO"), $bold);
        $ws->writeString(0, 13, utf8_decode("FUENTE"), $bold);
        $ws->writeString(0, 13, utf8_decode("AUTORIZA ENVIO CORREO"), $bold);

        $row = 1;
        foreach ($clientes as $rCliente) {
            $col = 0;
            foreach ($rCliente as $k => $v) {
                $ws->writeString($row, $col, utf8_decode($v));
                $col++;
            }
            $row++;
        }

        $wb->close();

        return basename($path);
    }

}