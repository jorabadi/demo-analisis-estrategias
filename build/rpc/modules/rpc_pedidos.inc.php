<?php
require_once $cfg["rutaModeloLogico"] . "mod_despachos.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_translog.inc.php";

class Pedidos {

    public static function loadPage($p) {
        $si = Session::info();
        $p["session"] = $si;

        return modDespachos::consultarPedidosPorDespachar($p);
    }

    public static function loadPageRs($p) {
        $si = session::info();
        return $si;
    }

    public static function loadOne($p) {
        $si = Session::info();
        $p["session"] = $si;
        $result = modDespachos::consultarPedidoPorDespachar($p);
        $result["enc"]["despacho_manual_valido"] = modDespachos::validarCondicionesDespachoManual($p);
        $result["flag_facturacion_tienda"] = Pedidos::consultaSession(null);
        return $result;
    }

    public static function imprimirDocumentos($p) {
        try {
            $db = JDatabase::database();
            $db->transaction();
            $p["session"] = Session::info();
            modDespachos::imprimirDocumentos($p);
            $db->commit();
        } 
	catch (Exception $ex) {
            $db->rollback();
            throw new JpublicException(print_r("Error procesando al imprimir documentos: " . $ex->getMessage(), 1));
        }
    }

    public static function solicitarRecogida($p) {
        $db = JDatabase::database();
        try {
            $db->transaction();
            $p["session"] = Session::info();
            $respuesta = modDespachos::solicitarRecogida($p);
            $db->commit();
//$db->rollback(); //pruebas
            return $respuesta;
        } 
	catch (Exception $ex) {
            $db->rollback();
            throw new JpublicException(print_r("Error procesando al solicitar recogida: " . $ex->getMessage(), 1));
        }
    }

    public static function solicitarRecogidasMasivo($p) {
        $codigoPedidoProcesando = "";
        try {
            $db = JDatabase::database();
            $db->transaction();
            $si = Session::info();

            $codigosPedidos = explode(",", $p["codigos_pedidos"]);
            foreach ($codigosPedidos as $codigosPedido) {
                $params["session"] = $si;
                $params["codigo_pedido"] = $codigosPedido;
                $codigoPedidoProcesando = $codigosPedido;
                modDespachos::solicitarRecogida($params);
            }
            $db->commit();
        } 
	catch (Exception $ex) {
            $db->rollback();
            throw new JpublicException(print_r("Error procesando el pedido: $codigoPedidoProcesando - " . $ex->getMessage(), 1));
        }
    }

    public static function loadAllHistoricoPedidos($p) {
        $si = Session::info();
        $p["session"] = $si;
        return modDespachos::consultarPedidosDespachados($p);
    }

    public static function consultaSession($p) {
        $si = Session::info();
        $flagsPp = JUtils::pgsqlHStoreToArray($si["flags_pp"]);
        if (isset($flagsPp["flag_facturacion_tienda"]) && $flagsPp["flag_facturacion_tienda"]) {
            $facturacionTienda = 1;
        } 
	else {
            $facturacionTienda = 0;
        }

        return $facturacionTienda;
    }

    /* anulacion de pedido PCE por parte del proveedor  cambia  a un  estado anulado 
     * parametro : codigo_pedido 
     */
    public static function anularPinPce($p) {
        $db = JDatabase::database();
        $si = Session::info();
        try {
            $db->transaction();
            modDespachos::anularPedidoPce($p);
            $db->commit();
        } 
	catch (ErrorException $e) {
            $db->rollback();
            modTransLog::logInput(__CLASS__, "Error" . __METHOD__, "Error: {$e->getMessage()}, Params: " . print_r($p, 1), '', 1);
            throw new JPublicException("Error:en anular pedido PCE por favor comuniquese con Coordiutil Vendesfacil");
        }
        return;
    }

    public static function reactivarUltimaSolicitudRecogida($p) {
        $db = JDatabase::database();
        $si = Session::info();
        try {
            modDespachos::reactivarUltimaSolicitudRecogida($p);
        } catch (ErrorException $e) {
            $db->rollback();
            modTransLog::logInput(__CLASS__, "Error" . __METHOD__, "Error: {$e->getMessage()}, Params: " . print_r($p, 1), '', 1);
            throw new JPublicException("Error:en la  reactivacion de la recogida por favor comuniquese con Coordiutil Vendesfacil");
        }
        return;
    }

    public static function anularRotulo($p) {
        $db = JDatabase::database();
        $si = Session::info();
        try {
            $db->transaction();
            modDespachos::anularRotulo($p);
            $db->commit();
        } 
	catch (ErrorException $e) {
            $db->rollback();
            modTransLog::logInput(__CLASS__, "Error" . __METHOD__, "Error: {$e->getMessage()}, Params: " . print_r($p, 1), '', 1);
            throw new JPublicException("Error: en la  anulacion  del rotulo por favor comuniquese con Coordiutil Vendesfacil");
        }
        return;
    }

    public static function registrarDespachoManual($p) {
        $db = JDatabase::database();
        $db->transaction();
        $p["session"] = Session::info();
        try {
            modDespachos::registrarDespachoManual($p);
        } catch (JPublicException $e) {
            $db->rollback();
            throw new JPublicException("Error: {$e->getMessage()}");
        } 
	catch (ErrorException $e) {
            $db->rollback();
            modTransLog::logInput(__CLASS__, "Error" . __METHOD__, "Error: {$e->getMessage()}, Params: " . print_r($p, 1), '', 1);
            throw new JPublicException("Error: no se pudo realizar Despacho manual, por favor comuniquese con Coordiutil Vendesfacil");
        }
        $db->commit();
        return;
    }

    public static function loadTransportePedidosEditRs() {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();

        //Estados
        $result["estados"] = array(
            array("label" => "", "data" => ""),
            array("label" => "Recogida", "data" => "3"),
            array("label" => "En transporte", "data" => "5"),
            array("label" => "En proceso de entrega", "data" => "7"),
            array("label" => "Con novedad en la entrega", "data" => "8"),
            array("label" => "Entregado", "data" => "9"),
        );

        //Transportadores
        $sql = "
    			select 
    				codigo_transportador as data,
    				nombre as label
    			from 
    				cu_transportadores
		";
        $ca->exec($sql);
        $result["transportadores"] = $ca->fetchAll();
        array_unshift($result["transportadores"], array("data" => "", "label" => "Seleccione transportador"));

        return $result;
    }

    public static function loadPageTransportePedidos($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.pin";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        if ($p["filters"]["codigo_estado_transporte"] != "todos") {
            $where .= " and a.codigo_estado_transporte='{$p["filters"]["codigo_estado_transporte"]}' ";
        }

        $filtroFecha = " (a.fechahora_pago::date between current_date - 30 and current_date or fechahora_despacho::date between current_date - 30 and current_date)";
        if (isset($p["filters"]["fecha_desde"]) && $p["filters"]["fecha_desde"] != "" && isset($p["filters"]["fecha_hasta"]) && $p["filters"]["fecha_hasta"] != "") {
            $filtroFecha = " (a.fechahora_pago::date between '{$p["filters"]["fecha_desde"]}' and '{$p["filters"]["fecha_hasta"]}' or a.fechahora_despacho::date between '{$p["filters"]["fecha_desde"]}' and '{$p["filters"]["fecha_hasta"]}')";
        }

        $where .= " and " . $filtroFecha;

        $sql = "
		select 
			a.pin,
			a.codigo_pedido,
			a.fechahora_pedido::date as fecha_pedido,
			a.nombre_estado_transporte||'<br/>'||t.nombre||'<br/>Guía # '||a.guia as estado_transporte,
			sum(a.unidades) as unidades_pedido,
			array_to_string(array_accum(a.nombre),'<br/>') as nombres,
			array_to_string(array_accum(a.referencia),'<br/>') as referencias,
			array_to_string(array_accum(a.talla),'<br/>') as tallas,
			array_to_string(array_accum(a.color),'<br/>') as colores,
			array_to_string(array_accum(a.refinventario),'<br/>') as refinventario,
			array_to_string(array_accum(a.unidades),'<br/>') as unidades
		from 
			view_cu_pedidos_det a
			join cu_transportadores t on (a.codigo_transportador=t.codigo_transportador)
		where 
			a.estado_pin='pagado' 
			and a.estado_autorizacion='aprobado' 
			and a.codigo_despacho>0
			and a.codigo_estado_transporte<9
			and a.flags_despacho->'despacho_manual'='1'
			and ({$where})
			and a.codigo_proveedor=:codigo_proveedor
		group by 
			a.pin,
			a.codigo_pedido,
			a.fechahora_pedido::date,
			a.nombre_estado_transporte,
			t.nombre,
			a.guia
		";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si['codigo_proveedor'], false);
        return $ca->execPage($p);
    }

    public static function loadTransportePedidos($p) {
        $si = Session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "
				select 
					b.pin as pin,
					b.codigo_pedido,
					a.nombre_estado_transporte as estado_actual,
					a.codigo_estado_transporte as estado_transporte,
					a.codigo_remision,
					a.codigo_transportador
				from 
					cu_despachos_enc a 
					join cu_pedidos_enc b on (a.codigo_pedido=b.codigo_pedido)
				where 
					a.codigo_pedido=:codigo_pedido 
					and a.codigo_proveedor=:codigo_proveedor
					and a.estado='activo'
		";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_pedido", $p["codigo_pedido"], false);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Orden de compra no localizada.");
        }

        $result = $ca->fetch();

        return $result;
    }

    public static function saveTransportePedidos($p) {
        $si = Session::info();

        if (!in_array("FTransportePedidos_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }

        $p["codigo_pedido"] = isset($p["codigo_pedido"]) ? $p["codigo_pedido"] : "";

        if ($p["codigo_pedido"] === "") {
            throw new JPublicException("Error, falta Orden de Compra.");
        }

        if ($p["estado_transporte"] === "") {
            throw new JPublicException("Error, falta nuevo estado de transporte.");
        }

        if ($p["estado_transporte"] == "3" && empty($p["codigo_transportador"])) {
            throw new JPublicException("Error, falta Transportador.");
        }

        if ($p["estado_transporte"] == "3" && empty($p["codigo_remision"])) {
            throw new JPublicException("Error, falta Guia.");
        }

        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        $sql = "
				select
					a.codigo_estado_transporte
				from
					cu_despachos_enc a
				where 
					a.codigo_pedido=:codigo_pedido 
					and a.codigo_proveedor=:codigo_proveedor
					and a.estado='activo'
		";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_pedido", $p["codigo_pedido"], false);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $rDespacho = $ca->fetch();

        if ($p["estado_transporte"] <= $rDespacho["codigo_estado_transporte"]) {
            throw new JPublicException("Error, no se puede registrar el pedido a un estado de transporte anterior.");
        }

        if ($p["estado_transporte"] == "9" && $rDespacho["codigo_estado_transporte"] < "3") {
            throw new JPublicException("Error, no se puede registrar el pedido como Entregado sin haberse registrado como Recogido.");
        }


        $campos = "codigo_estado_transporte,nombre_estado_transporte,fecha_recogida,fecha_entrega,codigo_transportador,codigo_remision,recogida";

        $aEstados = array(
            "3" => "Recogida",
            "5" => "En transporte",
            "7" => "En proceso de entrega",
            "8" => "Con novedad en la entrega",
            "9" => "Entregado"
        );

        $ca->prepareUpdate("cu_despachos_enc", $campos, "codigo_pedido=:codigo_pedido and codigo_proveedor=:codigo_proveedor and flags->'despacho_manual'='1' and codigo_estado_transporte<9 and estado='activo'");
        $ca->bindValue(":codigo_pedido", $p["codigo_pedido"], false);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":nombre_estado_transporte", $aEstados[$p["estado_transporte"]], true);
        $ca->bindValue(":codigo_estado_transporte", $p["estado_transporte"], false);
        $ca->bindValue(":codigo_transportador", $p["estado_transporte"] == "3" ? $p["codigo_transportador"] : "codigo_transportador", false);
        $ca->bindValue(":codigo_remision", $p["estado_transporte"] == "3" ? $p["codigo_remision"] : "codigo_remision", $p["estado_transporte"] == "3" ? true : false);
        $ca->bindValue(":fecha_recogida", $p["estado_transporte"] == "3" ? "current_date" : "fecha_recogida", false);
        $ca->bindValue(":recogida", $p["estado_transporte"] == "3" ? "true" : "recogida", false);
        $ca->bindValue(":fecha_entrega", $p["estado_transporte"] == "9" ? "current_date" : "fecha_entrega", false);

        $db->transaction();
        $ca->exec();
        $db->commit();

        return;
    }

    public static function loadPageComprasImportados($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.pin";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        $campos = " a.codigo_inventario,
					a.pin,
					a.codigo_pedido,
					a.codigo_producto,
					a.fechahora_pedido::DATE || '<br/>' || a.fechahora_pedido::TIME AS fecha_pedido,
					a.nombre || '<br/>' || a.referencia AS nombre_referencia,
					a.talla || '<br/>' || a.color AS talla_color,
					a.refinventario,
					(
						SELECT pp.precio_compra
						FROM cu_productos_precios pp
						WHERE pp.codigo_producto = a.codigo_producto AND pp.codigo_inventario = a.codigo_inventario AND pp.codigo_tipo = 1
					) AS precio_compra,
					sum(a.unidades) AS unidades_pedidas,
					'http://www.amazon.com/dp/' || a.referencia AS url_producto,
					(
						SELECT coalesce(sum(cantidad), 0)
						FROM cu_importados_compras
						WHERE codigo_pedido = a.codigo_pedido AND codigo_inventario = a.codigo_inventario AND estado = 'activo'
					) AS unidades_compradas
        ";

        $sql = "SELECT a.*
				FROM (
					SELECT {$campos}
					FROM view_cu_pedidos_det a
					WHERE a.codigo_proveedor = :codigo_proveedor AND a.estado_pin = 'pagado' AND a.estado_autorizacion = 'aprobado'
					GROUP BY a.codigo_pedido,
						a.pin,
						a.codigo_inventario,
						a.codigo_producto,
						a.nombre,
						a.referencia,
						a.talla,
						a.color,
						a.refinventario,
						a.fechahora_pedido
					HAVING sum(unidades) > (
							SELECT coalesce(sum(cantidad), 0)
							FROM cu_importados_compras
							WHERE codigo_pedido = a.codigo_pedido AND codigo_inventario = a.codigo_inventario AND estado = 'activo'
							)
					) a
				WHERE 1 = 1 AND {$where}
        ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $result = $ca->execPage($p);

        for ($i = 0; $i < count($result["records"]); $i++) {
            $tmp = modRs::load(rs::cuProductos, $result["records"][$i]["codigo_producto"], array(rs::cuProductosImagen1));
            if (isset($tmp["imagen_1"])) {
                $result["records"][$i]["imagen"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }

        return $result;
    }

    public static function loadComprasImportados($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select sum(unidades) as unidades_pedidas 
                from view_cu_pedidos_det 
                where codigo_proveedor=:codigo_proveedor and codigo_pedido=:codigo_pedido and codigo_inventario=:codigo_inventario";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_pedido", $p["codigo_pedido"], false);
        $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
        $ca->exec();

        $result = array();
        $result["pedido"] = $ca->fetch();

        $sql = "select * from cu_importados_compras where codigo_pedido=:codigo_pedido and codigo_inventario=:codigo_inventario and estado='activo'";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_pedido", $p["codigo_pedido"], false);
        $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
        $ca->exec();

        $result["compras"] = $ca->fetchAll();
        $i = 0;
        foreach ($result["compras"] as $r) {
            $result["compras"][$i]["archivo"] = json_encode(modRs::load(rs::cuImportadosCompras, $r["codigo"], array(rs::cuImportadosComprasArchivo)));
            $i++;
        }

        return $result;
    }

    public static function saveComprasImportados($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $db->transaction();

        $campos = "codigo_pedido,codigo_inventario,orden_compra,cantidad,total_producto,total_flete,total_producto_usd,total_flete_usd,fecha_compra";
        foreach ($p["compras"] as $r) {
            if (empty($r["orden_compra"])) {
                continue;
            }

            $ca->prepareSelect("cu_importados_compras", "orden_compra", "codigo_pedido=:codigo_pedido and codigo_inventario=:codigo_inventario and orden_compra=:orden_compra and estado='activo'");
            $ca->bindValue(":codigo_pedido", $p["codigo_pedido"], false);
            $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
            $ca->bindValue(":orden_compra", $r["orden_compra"], true);
            $ca->exec();

            if ($ca->size() == 0) {
                if (empty($r["cantidad"])) {
                    throw new JPublicException("Falta Cantidad para la orden de compra {$r["orden_compra"]}");
                }

                if (empty($r["total_producto_usd"])) {
                    throw new JPublicException("Falta Total producto para la orden de compra {$r["orden_compra"]}");
                }

                if (empty($r["total_flete_usd"]) && $r["total_flete_usd"] != "0") {
                    throw new JPublicException("Falta Total flete para la orden de compra {$r["orden_compra"]}");
                }

                if (empty($r["archivo"])) {
                    throw new JPublicException("Falta Archivo para la orden de compra {$r["orden_compra"]}");
                }

                if (empty($r["fecha_compra"])) {
                    throw new JPublicException("Falta selecionar la fecha de comprar{$r["fecha_compra"]}");
                }

                $sql = "select sum(unidades) as unidades 
                        from view_cu_pedidos_det 
                        where codigo_proveedor=:codigo_proveedor and codigo_pedido=:codigo_pedido and codigo_inventario=:codigo_inventario";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_pedido", $p["codigo_pedido"], false);
                $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
                $ca->exec();
                $rPedido = $ca->fetch();

                $sql = "select coalesce(sum(cantidad),0) as cantidad 
						from cu_importados_compras 
						where codigo_pedido=:codigo_pedido and codigo_inventario=:codigo_inventario and estado='activo'";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_pedido", $p["codigo_pedido"], false);
                $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
                $ca->exec();
                $rCompras = $ca->fetch();

                if ($rPedido["unidades"] < $r["cantidad"] + $rCompras["cantidad"]) {
                    throw new JPublicException("Unidades compradas es mayor a las unidades pedidas.<br/>Ya compradas {$rCompras["cantidad"]}.<br/>Nuevas {$r["cantidad"]}.<br/>Pedidas {$rPedido["unidades"]}.");
                }
                // aqui  se colsulta la trm del dia de la compra  para opterner el valor en pesos colombianos

                $sql = "select valor_real from cu_trm where fecha=:fecha_compra";
                $ca->prepare($sql);
                $ca->bindValue(":fecha_compra", $r["fecha_compra"], true);
                $ca->exec();
                $rTrm = $ca->fetch();

                $totalProducto = $rTrm["valor_real"] * $r["total_producto_usd"];
                $totalFlete = $rTrm["valor_real"] * $r["total_flete_usd"];
                $ca->prepareInsert("cu_importados_compras", $campos, "returning codigo");
                $ca->bindValue(":codigo_pedido", $p["codigo_pedido"], false);
                $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
                $ca->bindValue(":orden_compra", $r["orden_compra"], true);
                $ca->bindValue(":cantidad", $r["cantidad"], false);
                $ca->bindValue(":total_producto_usd", $r["total_producto_usd"], false);
                $ca->bindValue(":total_producto", $totalProducto, false);
                $ca->bindValue(":total_flete_usd", $r["total_flete_usd"], false);
                $ca->bindValue(":total_flete", $totalFlete, false);
                $ca->bindValue(":fecha_compra", $r["fecha_compra"], true);
                $ca->exec();


                $rImportadosCompra = $ca->fetch();

                if (!empty($r["archivo"])) {
                    $r["archivo"] = json_decode($r["archivo"], true);
                    modRs::save(rs::cuImportadosCompras, $rImportadosCompra["codigo"], rs::cuImportadosComprasArchivo, $r["archivo"]);
                }
            }
        }
        $db->commit();
        return;
    }

    public static function exportar($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.pin,
                    initcap( case when a.agente='tienda' then 'P/Personalizado' else a.agente end) as origen,
                    a.fecha_pedido,
                    sum(a.unidades_pedido) as unidades_pedido,
                    array_to_string(array_accum(a.nombre),'\n\r') as nombres,
                    array_to_string(array_accum(a.referencia),'\n\r') as referencias,
                    array_to_string(array_accum(a.talla_color),'\n\r') as tallas_colores,
                    array_to_string(array_accum(a.precio_venta),'\n\r') as precios_venta,
                    array_to_string(array_accum(a.refinventario),'\n\r') as refinventario,
                    array_to_string(array_accum(a.unidades),'\n\r') as unidades";
        if ($si["flag_tienda"] == "1" && $si["flag_factura_proveedor"] == "1") {
            $campos .= ",a.total_bonos";
        }

        $sql = "select {$campos}
                from
                    (select a.pin,
                        a.fechahora_pedido::date as fecha_pedido,
                        sum(a.unidades) as unidades_pedido,
                        a.nombre,
                        a.referencia,
                        a.talla||'/'||a.color as talla_color,
                        func_number_format(a.precio_venta_coniva,0) as precio_venta,
						a.total_bonos,
                        a.refinventario,
                        sum(a.unidades) as unidades,
                        a.agente
                    from view_cu_pedidos_det a left join cu_despachos_enc b on (a.codigo_despacho=b.codigo_despacho)
                    where 1=1 and a.estado_pin IN ('pagado', 'pce') and a.estado_autorizacion='aprobado' and
                        (a.codigo_despacho=0 or a.codigo_despacho is null or b.estado='documentos') 
                        and a.codigo_proveedor=:codigo_proveedor
                    group by a.pin,a.fechahora_pedido::date,a.agente,
                        a.nombre,a.referencia,a.talla,a.color,a.refinventario,a.precio_venta_coniva,a.total_bonos
					) a
                group by a.pin,a.fecha_pedido,a.agente,a.total_bonos
                order by fecha_pedido
            ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/pedidos_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = & $wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {
                                    if ( in_array($k,array("total_bonos")) ) {
                        $ws->writeNumber($row, $col, $v);
                        $col++;
                        continue;
                    }
                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }

    public static function exportarFormato($p) {
        $si = session::info();
        $ca = new JDbQuery(JDatabase::database());

        $sql = "select codigo_ubicacion
		from cu_proveedores_ubicaciones 
		where codigo_proveedor = :codigo_proveedor and punto_despacho = '1' and estado = 'activo'
	";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        if ($ca->size() == 1) {
            $r = $ca->fetch();
            $puntoDespacho = $r["codigo_ubicacion"];
        } 
	else {
            $puntoDespacho = "";
        }

        $sql = "select a.pin, a.codigo_pedido, '$puntoDespacho' as codigo_punto_despacho, '' as contenido, '' as unidades_de_empaque, '' as factura_proveedor
                from
                    (
		    select a.pin, a.codigo_pedido
                    from view_cu_pedidos_det a 
		    left join cu_despachos_enc b on a.codigo_despacho = b.codigo_despacho
                    where a.estado_pin IN ('pagado', 'pce') 
			and a.estado_autorizacion = 'aprobado' 
			and (a.codigo_despacho = 0 or a.codigo_despacho is null or b.estado is null)
                        and a.codigo_proveedor = :codigo_proveedor
                    group by a.pin, a.codigo_pedido
		    ) a
                group by a.pin, a.codigo_pedido
	";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/formato_pedidos_{$si["codigo_proveedor"]}_" . date('Y-m-d_H-i') . ".xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = & $wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {
                    if($col < 3){
                        $ws->writeNumber($row, $col, $v);
                    }
                    else{
                        $ws->writeString($row, $col, $v);
                    }
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }

    public static function exportarHistorico($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.referencias,a.nombres,a.agente,a.pin,a.nombre_estado_transporte,a.email_cliente,a.identificacion_cliente,a.nombre_ciudad_destinatario";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        if ($p["filters"]["codigo_estado_transporte"] != "todos") {
            $where .= " and a.codigo_estado_transporte='{$p["filters"]["codigo_estado_transporte"]}' ";
        }

        $filtro_fecha = " a.fecha_pedido::date between current_date - 30 and current_date";
        if (isset($p["filters"]["fecha_desde"]) && $p["filters"]["fecha_desde"] != "" && isset($p["filters"]["fecha_hasta"]) && $p["filters"]["fecha_hasta"] != "") {
            $filtro_fecha = " a.fecha_pedido::date between '{$p["filters"]["fecha_desde"]}' and '{$p["filters"]["fecha_hasta"]}' ";
        }

        if (isset($p["column_filters"])) {
            foreach ($p["column_filters"] as $colum => $valorCulumn) {
                if ($valorCulumn != "") {
                    $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
                }
            }
        }

        $sql = "SELECT *
						FROM (
							SELECT a.pin, 
        						'Origen:<br/>' || initcap(CASE WHEN a.agente = 'tienda' THEN 'P/Personalizado<br/>' || a.nombre_url ELSE a.agente END) AS Origen,
								a.codigo_pedido,
								a.fecha_pedido,
								a.codigo_proveedor,
								sum(a.unidades_pedido) AS unidades_pedido,
								array_to_string(array_accum(a.nombre), '<br/>') AS nombres,
								array_to_string(array_accum(a.referencia), '<br/>') AS referencias,
								array_to_string(array_accum(a.talla_color), '<br/>') AS tallas_colores,
								array_to_string(array_accum(a.refinventario), '<br/>') AS refinventario,
								array_to_string(array_accum(a.unidades), '<br/>') AS unidades,
								nombre_estado_transporte,
        						unidades_empaque,
							fecha_recogida,
							fecha_entrega,
        						CASE WHEN a.flags -> 'novedad_entrega' = '1' THEN 'Si' ELSE 'No' END AS tuvo_novedad,
        						CASE WHEN a.flags -> 'solucion_devolucion' = '1' THEN 'Si' ELSE 'No' END AS devolucion,
        						nombre_ciudad_destinatario,
								email_cliente,
								agente,
								identificacion_cliente,
								codigo_estado_transporte
        		
							--,initcap( case when a.agente='tienda' then 'P/Personalizado' else a.agente end) as agente
							FROM (
								SELECT b.pin,
									a.codigo_pedido,
									b.fechahora_pedido::DATE AS fecha_pedido,
									sum(a.unidades) AS unidades_pedido,
									a.nombre,
									i.refinventario,
									a.talla || ' <b>/</b> ' || a.color AS talla_color,
									a.referencia,
									sum(a.unidades) AS unidades,
									d.nombre_estado_transporte || '<br/>' || (CASE WHEN d.codigo_estado_transporte >= 3 THEN 'Guia: ' || d.codigo_remision ELSE '' END) AS nombre_estado_transporte,
									b.agente,
									b.email_cliente,
									b.identificacion_cliente,
									b.nombre_ciudad_destinatario,
									c.nombre_url,
									g.codigo_proveedor,
									d.codigo_estado_transporte,
        							d.flags,
        							d.unidades_empaque,
								d.fecha_recogida,
								d.fecha_entrega
								FROM  cu_pedidos_det a
								INNER JOIN cu_pedidos_enc b ON (b.codigo_pedido = a.codigo_pedido)
						        LEft join cu_productos_inventario e on (e.codigo_inventario=a.codigo_inventario)
						        Left Join cu_productos g on (g.codigo_producto= e.codigo_producto)
								LEFT JOIN  cu_proveedores c ON (g.codigo_proveedor = c.codigo_proveedor)
								LEFT JOIN cu_despachos_enc d ON a.codigo_pedido = d.codigo_pedido AND c.codigo_proveedor = d.codigo_proveedor AND (d.estado = ANY (ARRAY ['activo'::text, 'documentos'::text]))
								LEFT JOIN cu_productos_inventario i ON a.codigo_inventario = i.codigo_inventario
								WHERE 1 = 1 AND b.estado IN ('pagado', 'pce') AND b.estado_autorizacion = 'aprobado' AND a.codigo_despacho > 0 AND c.codigo_proveedor =:codigo_proveedor 
								GROUP BY b.pin,
									a.codigo_pedido,
									b.fechahora_pedido::DATE,
									b.agente,
									a.nombre,
									a.referencia,
									a.talla,
									a.color,
									d.nombre_estado_transporte,
									i.refinventario,
									b.email_cliente,
									b.identificacion_cliente,
									b.nombre_ciudad_destinatario,
									d.codigo_estado_transporte,
									d.codigo_remision,
									c.nombre_url,
									c.codigo_proveedor,
									d.codigo_estado_transporte,
						            g.codigo_proveedor,
        							d.flags,
        							d.unidades_empaque,
								d.fecha_recogida,
								d.fecha_entrega
								) a
							GROUP BY a.pin,
								a.codigo_pedido,
								a.fecha_pedido,
								a.agente,
								nombre_estado_transporte,
								a.email_cliente,
								a.identificacion_cliente,
								a.nombre_ciudad_destinatario,
								a.nombre_url,
								a.codigo_proveedor,
								a.codigo_estado_transporte,
        						a.flags,
        						a.unidades_empaque,
							a.fecha_recogida,
							a.fecha_entrega
        		
							) a
						WHERE 1 = 1 and{$where}
        and {$filtro_fecha}";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/historico_pedidos_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = & $wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {
                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }

    public static function loadPageMasivo($p) {
        $p["session"] = Session::info();
        return modDespachos::consultarPedidosMasivosPorDespachar($p);
    }

}
