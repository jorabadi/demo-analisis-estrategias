<?php
JLib::requireOnceModule("Facebook/autoload.php");

class Facebook1 {
    // ######## Facebook API Configuration ##########
    // Facebook App ID
    const appId = '495586743957457';
    // Facebook App Secret
    const appSecret = '99ea0a91c0df27d77b71b4442b871811';

    public static function loginFacebook() {
        $id = self::getId();
        $userInfo = Session::signin(array(
                    "username" => $id,
                    "password" => ""
        ));

        return $userInfo;
    }

    public static function getId() {
        $fbPermissions = [
            'email',
            'user_likes'
        ]; // Required facebook permissions
        $fb = new Facebook\Facebook([
            'app_id' => self::appId,
            'app_secret' => self::appSecret,
            'default_graph_version' => 'v2.5'
        ]);

        $helper = $fb->getJavaScriptHelper();
        $accessToken = $helper->getAccessToken();
        if (isset($accessToken)) {
            $signedRequest = $helper->getSignedRequest();
            $userId = $signedRequest->getUserId();
            return $userId;
        } else {
            return false;
        }
    }

}