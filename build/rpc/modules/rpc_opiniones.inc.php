<?php

class Opiniones {

    public static function loadPageCompras($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "nombres,pin,calificacion_servicio,codigo_pedido";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $where .= " and calificacion_servicio between '{$p["filters"]["calificacion_de"]}' and '{$p["filters"]["calificacion_a"]}'";
        $where .= " and opinion_publicada  = '{$p["filters"]["publicado"]}'";

        $sql = "select * 
				 from( SELECT a.id_pedido_opinion as id_opinion,
					a.codigo_pedido,
					a.pin,
					b.nombres_cliente || ' ' || b.apellidos_cliente AS nombres,
					b.email_cliente,
					CASE 
						WHEN a.calificacion_servicio::text > '3.50' 
						THEN  '<font style=color' || ':' || '#33AF1F > '|| a.calificacion_servicio || '</font>'     ELSE  '<font style=color' || ':' || 'red > '|| a.calificacion_servicio || '</font>' 
					END 
					AS calificacion,
					a.comentario,
					a.publicado,
					CASE 
						WHEN a.publicado = 't' 
						THEN 'Si' ELSE 'No' 
					END 
					AS opinion_publicada,
					a.token,
					a.calificacion_servicio
				FROM cu_pedidos_opiniones a
				INNER JOIN cu_pedidos_enc b ON (b.codigo_pedido = a.codigo_pedido)
					WHERE 1 = 1 AND a.codigo_proveedor_pp =:codigo_proveedor AND a.tipo = 'opinion') tbl where 1=1  and {$where} ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $result = $ca->execPage($p);

        return $result;
    }

    public static function loadOneCompras($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select nombres_cliente,email_cliente,telefono_celular_cliente,direccion_destinatario,pin,codigo_pedido,fechahora_pedido
				from cu_pedidos_enc where 1=1
					and pin =:pin::text ";
        $ca->prepare($sql);
        $ca->bindValue(":pin", $p["pin"]);
        $ca->exec();
        $result = $ca->fetch();


        $sql = "SELECT a.id_pedido_opinion,
				      a.codigo_pedido,
				      a.pin,
				      b.nombres_cliente || ' ' || b.apellidos_cliente AS nombres,
				      b.email_cliente,
				      CASE 
				          WHEN a.calificacion_servicio::text > '3.50' 
				          THEN  '<font style=color' || ':' || '#33AF1F > '|| a.calificacion_servicio || '</font>'     ELSE  '<font style=color' || ':' || 'red > '|| a.calificacion_servicio || '</font>' 
				      END 
				      AS calificacion,
				      a.comentario,
				      a.publicado,
				      CASE 
				          WHEN a.publicado = 't' 
				          THEN 'Si' ELSE 'No' 
				      END 
				      AS opinion_publicada,
				      a.calificacion_servicio,
				      coalesce (
								(select a.calificacion_servicio 
										from cu_pedidos_opiniones a 
										where 1=1
										and a.codigo_pedido=:codigo_pedido
										and a.tipo='mediacion' )::text ,''
								) 
				 as valoracion_mediada,
				(select a.comentario 
						from cu_pedidos_opiniones a 
						where 1=1
						and a.codigo_pedido=:codigo_pedido and a.tipo='mediacion' 
				) 
				as opinion_mediacion_ciente,
				 (select a.respuesta_asesor 
						from cu_pedidos_opiniones a 
						where 1=1 
						 and a.codigo_pedido=:codigo_pedido and a.tipo='mediacion' 
				) 
				as respuesta_asesor
				  
				
				FROM cu_pedidos_opiniones a
				  INNER JOIN cu_pedidos_enc b ON (b.codigo_pedido = a.codigo_pedido)
				      WHERE 1 = 1
				       AND a.codigo_proveedor_pp =:codigo_proveedor 
				       AND a.tipo = 'opinion'
				       and a.codigo_pedido=:codigo_pedido ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_pedido", $p["codigo_pedido"]);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();
        $result["pedido"] = $ca->fetch();


        $result["rs"] = modRs::load(rs::cuProveedores, $si["codigo_proveedor"], array(rs::cuProveedoresLogo));

        foreach ($result["rs"] as $k => $v) {
            $result["rs"][$k]["preview"] = JDbFile::url($v["id"], array("w" => 260, "h" => 120, "frm" => 1));
        }

        $ca->prepareSelect("cu_mensajes", "cuerpo", "codigo_referencia=:codigo_referencia and asunto='nota asesor mediacion (opiniones)'");
        $ca->bindValue(":codigo_referencia", $result["pedido"]["id_pedido_opinion"], true);
        $ca->exec();

        $result["pedido"]["nota_asesor"] = "";

        if ($ca->size() > 0) {
            $data = $ca->fetch();
            $result["pedido"]["nota_asesor"] = $data["cuerpo"];
        }



        return $result;
    }

    public static function loadPageProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "id_producto,nombres,ref_venta,calificacion";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $where .= " and calificacion between '{$p["filters"]["calificacion_de"]}' and '{$p["filters"]["calificacion_a"]}'";
        $where .= " and opinion_publicada  = '{$p["filters"]["publicado"]}'";



        $sql = " select * from(select 
									  po.id_opinion,
									  po.id_producto,
							          po.ref_venta, 
							          po.nombre ||'  '|| po.apellidos as nombres,
							          po.email,po.valoracion as calificacion,
							          opinion,
							          po.nombre,
							          po.apellidos,
							          CASE
				          				  WHEN po.publicado = 't' THEN 'Si'
							         	  ELSE 'No'
							          END
							          as opinion_publicada,
							          (	select a.calificacion 
							           	from cu_productos_calificaciones a 
							           	where  a.codigo_producto= po.id_producto::integer
									  ) 
									  as calificacion_global  
						 
						 from  cu_pedidos_productos_opiniones po
						 join cu_productos p on (po.id_producto::integer=p.codigo_producto and p.codigo_proveedor=:codigo_proveedor_pp)
				 where 1=1 and po.tipo = 'opinion') tbl where 1=1  and {$where}";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"]);
        $result = $ca->execPage($p);
        $records = array();

        for ($i = 0; $i < count($result["records"]); $i++) {
            $tmp = modRs::load(rs::cuProductos, $result["records"][$i]["id_producto"], array(rs::cuProductosImagen1));
            if (isset($tmp["imagen_1"])) {
                $result["records"][$i]["imagen_1"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }



        foreach ($result["records"] as $kopiniones) {
            $kopiniones["opinion"] = wordwrap($kopiniones["opinion"], 60, "</br>", true);
            $records[] = $kopiniones;
        }
        $result["records"] = $records;
        return $result;
    }

    public static function loadOneProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select nombres_cliente,email_cliente,telefono_celular_cliente,direccion_destinatario,pin,codigo_pedido,fechahora_pedido
				from cu_pedidos_enc where 1=1
					and pin =:pin::text ";
        $ca->prepare($sql);
        $ca->bindValue(":pin", $p["ref_venta"]);
        $ca->exec();
        $result = $ca->fetch();


        $sql = "select 	po.id_producto,po.ref_venta, 
				po.nombre ||' '|| po.apellidos as nombres,
				po.id_opinion,
		        po.email,
		        opinion,
		        cu.nombre as nombre_producto,
		        cu.referencia,
		        po.tipo,
		        po.valoracion,
				func_url(cu.nombre) AS nombre_url,
				po.publicado as opinion_publicada,   
		       coalesce ((select a.valoracion from cu_pedidos_productos_opiniones a where a.id_producto=po.id_producto and a.ref_venta=po.ref_venta and a.tipo='mediacion' )::text ,'') as valoracion_mediada,
		        (select a.respuesta_asesor from cu_pedidos_productos_opiniones a where a.id_producto=po.id_producto and a.ref_venta=po.ref_venta and a.tipo='mediacion' ) as respuesta_asesor,
				(select a.opinion from cu_pedidos_productos_opiniones a where a.id_producto=po.id_producto and a.ref_venta=po.ref_venta and a.tipo='mediacion' ) as opinion_mediacion_ciente  
				from  cu_pedidos_productos_opiniones po
				--join cu_productos_calificaciones pc on (pc.codigo_proveedor_pp=:codigo_proveedor_pp and pc.codigo_producto=po.id_producto)
				join cu_productos cu on (cu.codigo_producto=po.id_producto::integer)
				where 1=1
				and po.id_producto=:id_producto::text  
				and po.tipo= 'opinion' 
				and po.ref_venta=:ref_venta";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->bindValue(":id_producto", $p["id_producto"], false);
        $ca->bindValue(":ref_venta", $p["ref_venta"], true);
        //print_r($ca->preparedQuery());
        //throw new JPublicException(print_r($ca->preparedQuery(),1));
        $ca->exec();
        $result["producto"] = $ca->fetch();

        $result["producto"]["url"] = "http://{$si["url_dominio"]}/item-{$result["producto"]["nombre_url"]}-{$p["id_producto"]}";

        $result["producto"]["rs"] = modRs::load(rs::cuProductos, $p["id_producto"], array(
                    rs::cuProductosImagen1
        ));

        foreach ($result["producto"]["rs"] as $k => $v) {
            $result["producto"]["rs"][$k]["preview"] = JDbFile::url($v["id"], array("w" => 200, "h" => 100, "frm" => 1));
        }



        $result["rs"] = modRs::load(rs::cuProveedores, $si["codigo_proveedor"], array(rs::cuProveedoresLogo));

        foreach ($result["rs"] as $k => $v) {
            $result["rs"][$k]["preview"] = JDbFile::url($v["id"], array("w" => 260, "h" => 120, "frm" => 1));
        }

        $ca->prepareSelect("cu_mensajes", "cuerpo", "codigo_referencia=:codigo_referencia and asunto='nota asesor mediacion (opiniones)'");
        $ca->bindValue(":codigo_referencia", $result["producto"]["id_opinion"], true);
        $ca->exec();

        $result["producto"]["nota_asesor"] = "";

        if ($ca->size() > 0) {
            $data = $ca->fetch();
            $result["producto"]["nota_asesor"] = $data["cuerpo"];
        }




        return $result;
    }

    public static function enviarMedicion($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        if (empty($p["mensaje_medicion_asesor"])) {

            throw new JPublicException("Por favor ingrese mensaje mediacion");
        }



        $campos = "accion,ref_venta,respuesta_asesor,apellidos,nombre,email,id_producto,tipo";
        $ca->prepareInsert("cu_pedidos_productos_opiniones", $campos);

        $ca->bindValue(":accion", 'NEW', true);
        $ca->bindValue(":ref_venta", $p["datos_compra"]["ref_venta"], true);
        $ca->bindValue(":respuesta_asesor", $p["mensaje_medicion_asesor"], true);
        $ca->bindValue(":apellidos", $p["datos_compra"]["apellidos"], true);
        $ca->bindValue(":nombre", $p["datos_compra"]["nombres"], true);
        $ca->bindValue(":email", $p["datos_compra"]["email"], true);
        $ca->bindValue(":id_producto", $p["datos_compra"]["id_producto"], false);
        $ca->bindValue(":tipo", 'mediacion', true);

        $db->transaction();
        $ca->exec();

        modMails::clienteNotificacionMediacion(array("pin" => $p["datos_compra"]["ref_venta"], "codigo_producto" => $p ["datos_compra"] ["id_producto"]));
        $db->commit();

        return;
    }

    public static function enviarMedicionCompra($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["mensaje_medicion_asesor"])) {

            throw new JPublicException("Por favor ingrese mensaje mediacion");
        }

        $campos = "pin,estado,codigo_pedido,codigo_proveedor_pp,tipo,respuesta_asesor,token";
        $ca->prepareInsert("cu_pedidos_opiniones", $campos);

        $ca->bindValue(":pin", $p["datos_compra"]["pin"], true);
        $ca->bindValue(":respuesta_asesor", $p["mensaje_medicion_asesor"], true);
        $ca->bindValue(":codigo_pedido", $p["datos_compra"]["codigo_pedido"], true);
        $ca->bindValue(":token", $p["datos_compra"]["token"], true);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], true);
        $ca->bindValue(":estado", "ok", true);
        $ca->bindValue(":tipo", 'mediacion', true);
        $db->transaction();
        $ca->exec();

        modMails::clienteNotificacionMediacion(array("pin" => $p["datos_compra"]["pin"], "codigo_producto" => ""));
        $db->commit();

        return;
    }

    public static function publicar($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $db->transaction();
        ///publicar 
        $sql = "update  cu_pedidos_productos_opiniones  set publicado = true where ref_venta=:ref_venta and id_producto=:id_producto ";

        $ca->prepare($sql);
        $ca->bindValue(":ref_venta", $p["datos_compra"]["ref_venta"], true);
        $ca->bindValue(":id_producto", $p["datos_compra"]["id_producto"], true);
        $ca->exec();

        // promediar calificacion
        self::calcularCalificacionOpinionesProductos($p["datos_compra"]["id_producto"]);

        $db->commit();

        return;
    }

    public static function publicarcompra($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $db->transaction();

        ///publicar
        $sql = "update  cu_pedidos_opiniones  set publicado = true where pin=:pin ";

        $ca->prepare($sql);
        $ca->bindValue(":pin", $p["datos_compra"]["pin"], true);
        $ca->exec();

        // promediar calificacion
        self::calcularCalificacionOpinionesCompra();
        $db->commit();
        return;
    }

    public static function calcularCalificacionOpinionesProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $k = array();
        $k["codigo_producto"] = $p;


        $sql = "select avg(valoracion)
					from VIEW_CU_PEDIDOS_PRODUCTOS_OPINIONES
					where id_producto=:id_producto and publicado = true ";
        $ca->prepare($sql);
        $ca->bindValue(":id_producto", $k["codigo_producto"], true);
        $ca->exec();
        $res = $ca->fetch();

        $sql = "select codigo_producto
					from cu_productos_calificaciones
					where codigo_producto=:codigo_producto";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_producto", $k["codigo_producto"], true);
        $ca->exec();
        if ($ca->size() > 0) {
            $campos = "calificacion";
            $ca->prepareUpdate("cu_productos_calificaciones", $campos, "codigo_producto=:codigo_producto and codigo_proveedor_pp=:codigo_proveedor_pp");
            $ca->bindValue(":calificacion", round($res["avg"], 2), false);
            $ca->bindValue(":codigo_producto", $k["codigo_producto"], true);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();
        } else {
            $campos = "codigo_producto,codigo_proveedor_pp,calificacion";
            $ca->prepareInsert("cu_productos_calificaciones", $campos);
            $ca->bindValue(":codigo_producto", $k["codigo_producto"], true);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->bindValue(":calificacion", round($res["avg"], 2), false);
            $ca->exec();
        }
        return;
    }

    public static function calcularCalificacionOpinionesCompra() {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);



        $sql = "select avg(calificacion_servicio)
					from VIEW_CU_PEDIDOS_OPINIONES
					where codigo_proveedor_pp=:codigo_proveedor_pp and publicado = true ";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], true);
        $ca->exec();
        $res = $ca->fetch();

        $sql = "select calificacion
					from cu_proveedores_pp
					where codigo_proveedor=:codigo_proveedor";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], true);
        $ca->exec();
        if ($ca->size() > 0) {
            $campos = "calificacion";
            $ca->prepareUpdate("cu_proveedores_pp", $campos, "codigo_proveedor=:codigo_proveedor_pp");
            $ca->bindValue(":calificacion", round($res["avg"], 2), false);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();
        } else {
            $campos = "codigo_proveedor,calificacion";
            $ca->prepareInsert("cu_proveedores_pp", $campos);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":calificacion", round($res["avg"], 2), false);
            $ca->exec();
        }
        return;
    }

    public static function GuardarMensajeAsesor($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $db->transaction();


        $ca->prepareSelect("cu_mensajes", "*", "codigo_referencia=:codigo_referencia");
        $ca->bindValue(":codigo_referencia", $p["datos_compra"]["id_opinion"], true);
        $ca->exec();

        $campos = "asunto,cuerpo,codigo_mensaje,codigo_referencia";
        if ($ca->size() > 0) {
            $data = $ca->fetch();
            $ca->prepareUpdate("cu_mensajes", $campos, "codigo_referencia=:codigo_referencia and codigo_mensaje=:codigo_mensaje");
            $ca->bindValue(":codigo_mensaje", $data["codigo_mensaje"], false);
        } else {
            $ca->prepareInsert("cu_mensajes", $campos);
            $codigoMensaje = $db->nextVal("cu_mensajes_codigo_mensaje");
            $ca->bindValue(":codigo_mensaje", $codigoMensaje, false);
        }
        $ca->bindValue(":asunto", "nota asesor mediacion (opiniones)", true);
        $ca->bindValue(":cuerpo", $p["nota_asesor"], true);
        $ca->bindValue(":codigo_referencia", $p["datos_compra"]["id_opinion"], true);
        $ca->exec();

        $db->commit();

        return;
    }

}