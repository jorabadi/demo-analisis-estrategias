<?php
class Banners {

    public static function loadUbicacionesPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "u.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        
        foreach ($p["column_filters"] as $colum  => $valorCulumn){
        	if($valorCulumn != ""){
        		$where .= " and ". $ca->sqlFieldsFilters($colum,$valorCulumn);
        	}
        }
        
        $sql = "select u.* from  (select  u.codigo_ubicacion,u.nombre,u.alto,u.ancho,u.cantidad_banners,array_to_string(array_agg(distinct(upt.nombre)),', ') as tipo_ubicacion
        from mod_banners_ubicaciones u
        left join mod_banners_ubicaciones_pp up on u.codigo_ubicacion = up.codigo_ubicacion
        left join mod_banners_ubicaciones_pp_tipo upt on up.codigo_tipo = upt.codigo_tipo
        where 1=1  and u.codigo_proveedor_pp=:codigo_proveedor_pp
        GROUP BY u.codigo_ubicacion,u.nombre,u.alto,u.ancho,u.cantidad_banners  ) u
        where 1=1
        and ({$where}) ";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
        
    }
    
    public static function loadHorarios($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("view_mod_banners_horarios_pp a
                    left join mod_banners_ubicaciones b on (b.codigo_ubicacion = any (a.codigos_ubicacion))",
            "a.codigo_banner,a.nombre,a.codigo_horario,a.codigos_ubicacion,a.tipo_impresion,a.orden,a.duracion_segundos,a.impresiones_ciclo,
                    horas_0,horas_1,horas_2,horas_3,horas_4,horas_5,horas_6,
                    array_accum(b.nombre) as nombres_ubicacion",
            "a.codigo_banner=:codigo_banner
                    group by a.codigo_banner,a.nombre,a.codigo_horario,a.codigos_ubicacion,a.tipo_impresion,a.orden,a.duracion_segundos,a.impresiones_ciclo,
                    a.horas_0,a.horas_1,a.horas_2,a.horas_3,a.horas_4,a.horas_5,a.horas_6",
            "a.codigo_banner");

        $ca->bindValue(":codigo_banner", $p["codigo_banner"], false);

        $ca->exec();
        $result = array();
        if ($ca->size() != 0) {
            $result = $ca->fetchAll();
        }


        return $result;
    }

    public static function saveHorarios($p) {
        $si = session::info();

        if (!isset($p["codigo_banner"]) || $p["codigo_banner"] == "") {
            throw new JPublicException("Error, falta código banner");
        }

        $codigo_banner = $p["codigo_banner"];

        if (!isset($p["horarios"]) || count($p["horarios"]) == 0) {
            throw new JPublicException("Error, falta ubicación");
        }

        $db = JDatabase::database();
        $ca = $db->query();

        $db->transaction();

        foreach ($p["horarios"] as $h) {

            if (!isset($h["codigo_ubicacion"]) || $h["codigo_ubicacion"] == "") {
                throw new JPublicException("Error, falta código ubicación " . print_r($h, 1));
            }

            $campos = "codigo_banner,codigo_horario,impresiones,orden,impresiones_ciclo,duracion_segundos,tipo_impresion";

            if ($h["codigo_horario"] === "") {
                $codigo_horario = $db->nextVal("mod_banners_horarios_codigo_horario");
                $ca->prepareInsert("mod_banners_horarios", $campos);
            }
            else {
                $codigo_horario = $h["codigo_horario"];
                $ca->prepareUpdate("mod_banners_horarios", $campos, "codigo_horario=:codigo_horario and codigo_banner=:codigo_banner");
            }

            $ca->bindValue(":codigo_horario", $codigo_horario, false);
            $ca->bindValue(":codigo_banner", $codigo_banner, false);
            $ca->bindValue(":impresiones", -1, false);
            $ca->bindValue(":orden", ($h["orden"] === "" ? "0" : $h["orden"]), false);
            $ca->bindValue(":impresiones_ciclo", ($h["impresiones_ciclo"] === "" ? "0" : $h["impresiones_ciclo"]), false);
            $ca->bindValue(":duracion_segundos", ($h["duracion"] === "" ? "0" : $h["duracion"]), false);
            $ca->bindValue(":tipo_impresion", $h["tipo_impresion"], true);

            $ca->exec();

            $rCodigosHorario[] = $codigo_horario;

            if ($h["codigo_horario"] !== "") {
                $ca->prepareDelete("mod_banners_horarios_ubicaciones", "codigo_horario=:codigo_horario");
                $ca->bindValue(":codigo_horario", $codigo_horario, false);
                $ca->exec();

                $ca->prepareDelete("mod_banners_horario_horas", "codigo_horario=:codigo_horario");
                $ca->bindValue(":codigo_horario", $codigo_horario, false);

                $ca->exec();
            }


            $ca->prepareInsert("mod_banners_horarios_ubicaciones", "codigo_horario,codigo_ubicacion");
            $ca->bindValue(":codigo_horario", $codigo_horario, false);
            $ca->bindValue(":codigo_ubicacion", $h["codigo_ubicacion"], false);
            $ca->exec();


            $horas = json_decode($p["horarios"][0]["horas"]);

            $campos = "codigo_horario,dia,hora";

            for ($i = 0; $i < 7; $i++) {
                for ($j = 0; $j < 24; $j++) {
                    if ($horas[$i][$j] == 1) {
                        $ca->prepareInsert("mod_banners_horario_horas", $campos);
                        $ca->bindValue(":codigo_horario", $codigo_horario);
                        $ca->bindValue(":dia", $i, false);
                        $ca->bindValue(":hora", $j, false);
                        $ca->exec();
                    }
                }
            }

        }

        $ca->prepareSelect("mod_banners_horarios", "codigo_horario", "codigo_banner=:codigo_banner and codigo_horario not in (:codigos_horario)");
        $ca->bindValue(":codigo_banner", $codigo_banner, false);
        $ca->bindValue(":codigos_horario", implode(",", $rCodigosHorario), false);
        $ca->exec();
        $rHorariosBorrar = $ca->fetchAll();
        foreach ($rHorariosBorrar as $r) {
            $ca->prepareDelete("mod_banners_horarios_ubicaciones", "codigo_horario=:codigo_horario");
            $ca->bindValue(":codigo_horario", $r["codigo_horario"], false);
            $ca->exec();

            $ca->prepareDelete("mod_banners_horario_horas", "codigo_horario=:codigo_horario");
            $ca->bindValue(":codigo_horario", $r["codigo_horario"], false);
            $ca->exec();

            $ca->prepareDelete("mod_banners_horarios", "codigo_horario=:codigo_horario and codigo_banner=:codigo_banner");
            $ca->bindValue(":codigo_horario", $r["codigo_horario"]);
            $ca->bindValue(":codigo_banner", $codigo_banner, false);
            $ca->exec();
        }

        $db->commit();
    }
    
    public static function loadUbicaciones() {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select a.codigo_ubicacion as data,a.nombre as label
                 from mod_banners_ubicaciones a
                 where codigo_proveedor_pp =:codigo_proveedor_pp
                 order by a.nombre";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function loadUbicacionOne($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("mod_banners_ubicaciones u left join mod_banners_ubicaciones_pp up on u.codigo_ubicacion = up.codigo_ubicacion", "u.codigo_ubicacion,u.nombre,u.alto,u.ancho,u.cantidad_banners,u.observaciones,up.codigo_tipo as tipo_ubicacion,up.codigo_asociado", "u.codigo_ubicacion=:codigo_ubicacion and codigo_proveedor_pp=:codigo_proveedor_pp");
        $ca->bindValue(":codigo_ubicacion", $p["codigo_ubicacion"]);
        $ca->bindValue(":codigo_proveedor_pp",$si["codigo_proveedor"]);
        $ca->exec();
        $result = $ca->fetchAll();
        
       //throw new JPublicException("Error almacenando datos" . print_r($result, 1));
       
        foreach ($result as $ubicanes) {
        	
        //	throw new JPublicException("Error almacenando datos" . print_r($ubicanes["tipo_ubicacion"], 1));
	        $rEntidad = array();
	        //busco la entidad asociada para mostrarlo en el formulario
	        if ($ubicanes["tipo_ubicacion"] == "2") {
	            $sql = "select distinct
	                    case when b.flags->'flag_categorizacion'='0' then c.nombre
	                         when b.flags->'flag_categorizacion'='1' then d.nombre
	                    end as label,
	                    case when b.flags->'flag_categorizacion'='0' then c.codigo_categoria
	                         when b.flags->'flag_categorizacion'='1' then d.codigo_categoria
	                    end as data
	                    from cu_proveedores b 
	                    left join cu_categoriasn c on (c.codigo_categoria in 
	                        (select unnest(negocios) from view_cu_productos_tienda where codigo_proveedor_pp=:codigo_proveedor_pp)
	                    )
	                    left join cu_categorias_pp d on (d.codigo_categoria in 
	                        (select codigo_categoria1_pp from view_cu_productos_tienda where codigo_proveedor_pp=:codigo_proveedor_pp)
	                    )
	                    where b.codigo_proveedor=:codigo_proveedor_pp and 
	                    (case when b.flags->'flag_categorizacion'='0' then c.codigo_categoria =:codigo_categoria
	                    when b.flags->'flag_categorizacion'='1' then d.codigo_categoria=:codigo_categoria end)";
	            $ca->prepare($sql);
	            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
	            $ca->bindValue(":codigo_categoria",$ubicanes["codigo_asociado"], false);
	            $ca->exec();
	        } elseif ($ubicanes["tipo_ubicacion"] == "3") {
	            $sql = "select distinct
	                    case when b.flags->'flag_categorizacion'='0' then c.nombre
	                         when b.flags->'flag_categorizacion'='1' then d.nombre
	                    end as label,
	                    case when b.flags->'flag_categorizacion'='0' then c.codigo_categoria
	                         when b.flags->'flag_categorizacion'='1' then d.codigo_categoria
	                    end as data
	                    from cu_proveedores b 
	                    left join cu_categoriasn c on (c.codigo_categoria in 
	                        (select codigo_categoria2 from view_cu_productos_tienda where codigo_proveedor_pp=:codigo_proveedor_pp)
	                    )
	                    left join cu_categorias_pp d on (d.codigo_categoria in 
	                        (select codigo_categoria2_pp from view_cu_productos_tienda where codigo_proveedor_pp=:codigo_proveedor_pp)
	                    )
	                    where b.codigo_proveedor=:codigo_proveedor_pp and 
	                    (case when b.flags->'flag_categorizacion'='0' then c.codigo_categoria =:codigo_categoria
	                    when b.flags->'flag_categorizacion'='1' then d.codigo_categoria=:codigo_categoria end)";
	            $ca->prepare($sql);
	            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
	            $ca->bindValue(":codigo_categoria",$ubicanes["codigo_asociado"], false);
	            $ca->exec();
	        } elseif ($ubicanes["tipo_ubicacion"] == "4") {
	            $sql = "select distinct
	                    case when b.flags->'flag_categorizacion'='0' then c.vpath
	                         when b.flags->'flag_categorizacion'='1' then d.vpath
	                    end as label,
	                    case when b.flags->'flag_categorizacion'='0' then c.codigo_categoria
	                         when b.flags->'flag_categorizacion'='1' then d.codigo_categoria
	                    end as data
	                    from cu_proveedores b 
	                    left join cu_categoriasn c on (c.codigo_categoria in 
	                        (select codigo_categoria3 from view_cu_productos_tienda p where codigo_proveedor_pp=:codigo_proveedor_pp )
	                    )
	                    left join cu_categorias_pp d on (d.codigo_categoria in 
	                        (select codigo_categoria3_pp from view_cu_productos_tienda p where codigo_proveedor_pp=:codigo_proveedor_pp)
	                    )
	                    where b.codigo_proveedor=:codigo_proveedor_pp and 
	                    (case when b.flags->'flag_categorizacion'='0' then c.codigo_categoria =:codigo_categoria
	                    when b.flags->'flag_categorizacion'='1' then d.codigo_categoria=:codigo_categoria end)";
	            $ca->prepare($sql);
	            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
	            $ca->bindValue(":codigo_categoria", $ubicanes["codigo_asociado"], false);
	            $ca->exec();
	        } elseif ($ubicanes["tipo_ubicacion"] == "5") {
	            $sql = "select distinct m.nombre as label,m.codigo_marca as data
	                from view_cu_marcas m
	                where 1=1 and codigo_marca=:codigo_marca
	                and m.codigo_marca in 
	                (select distinct codigo_marca from view_cu_productos_tienda p where p.codigo_proveedor_pp =:codigo_proveedor_pp)";
	            $ca->prepare($sql);
	            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
	            $ca->bindValue(":codigo_marca", $ubicanes["codigo_asociado"], false);
	            $ca->exec();
	        } elseif ($ubicanes["tipo_ubicacion"] == "8") {
	            $sql = "select distinct c.nombre as label,c.codigo_categoria as data
	                from cu_categoriasv c
	                where 1=1 and c.codigo_categoria=:codigo_categoria
	                and c.codigo_proveedor=:codigo_proveedor_pp";
	            $ca->prepare($sql);
	            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
	            $ca->bindValue(":codigo_categoria", $ubicanes["codigo_asociado"], false);
	            $ca->exec();
	        }
	        elseif ($ubicanes["tipo_ubicacion"] == "12") {
	        	$sql = "select distinct c.nombre as label,c.codigo_categoria as data
	                from cu_categorias_pp c
	                where 1=1 and c.codigo_categoria=:codigo_categoria
	                and c.codigo_proveedor=:codigo_proveedor_pp
	        		and c.tipo =1 ";
	        	$ca->prepare($sql);
	        	$ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
	        	$ca->bindValue(":codigo_categoria", $ubicanes["codigo_asociado"], false);
	        	$ca->exec();
	        }elseif ($ubicanes["tipo_ubicacion"] == "13") {
	        	$sql = "select distinct c.nombre as label,c.codigo_categoria as data
	                from cu_categorias_pp c
	                where 1=1 and c.codigo_categoria=:codigo_categoria
	                and c.codigo_proveedor=:codigo_proveedor_pp
	        		and c.tipo =2 ";
	        	$ca->prepare($sql);
	        	$ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
	        	$ca->bindValue(":codigo_categoria", $ubicanes["codigo_asociado"], false);
	        	$ca->exec();
	        }
	        
	        if($ca->size() > 0) {
	         	$rEntidad = $ca->fetch();
	        }
	        //verifico que no venga vacio
	        if(!empty($rEntidad["data"])) {
	            $r["codigo_entidad"] = array("data" => $rEntidad["data"], "label" => $rEntidad["label"]);
	        } else {
	            $r["codigo_entidad"] = array( array("data" => 0, "label" => ""));
	      }
	      
	      //Tipos productos
	      $sql = "select codigo_tipo as data,nombre as label from mod_banners_ubicaciones_pp_tipo where codigo_tipo=:codigo_tipo";
	      $ca->prepare($sql);
	      $ca->bindValue(":codigo_tipo",$ubicanes["tipo_ubicacion"], true);
	      $ca->exec();
	      
	      $r["tipo_ubicacion"] = $ca->fetchAll();
	      
	      $rData["ubicaciones"][]=$r; 
	      
        
       }  
       $result["ubicaciones"]=$rData["ubicaciones"];

        return $result;
    }

    public static function saveUbicacion($p) {
        $si = session::info();
        //throw new JPublicException("Error almacenando datos" . print_r($p, 1));

        $db = JDatabase::database();
        $ca = $db->query();

        if (empty($p["nombre"])) {
            throw new JPublicException("Nombre inválido");
        }

        $iv = array();
        $iv[] = array("name" => "cantidad_banners", 'label' => 'Cantida de Banners', "filter" => FILTER_VALIDATE_INT);
        $iv[] = array("name" => "alto", 'label' => 'Alto', "filter" => FILTER_VALIDATE_INT);
        $iv[] = array("name" => "ancho", 'label' => 'Ancho', "filter" => FILTER_VALIDATE_INT);

        $validationErrors = JInputValidator::validateVars($p, $iv, array('output' => 'string'));
        if ($validationErrors) {
            throw new JPublicException($validationErrors);
        }
        
        if(!empty($p["tipo_ubicacion"]) && in_array($p["tipo_ubicacion"],array(2,3,4,5)) && empty($p["codigo_entidad"])) {
            throw new JPublicException("Debe seleccionar una entidad"); 
        }

        $campos = "codigo_ubicacion,nombre,observaciones,alto,ancho,cantidad_banners,codigo_proveedor_pp";

        $db->transaction();

        if ($p["codigo_ubicacion"] === "") {
            $ca->prepareInsert("mod_banners_ubicaciones", $campos);
            $codigoUbicacion = $db->nextVal("mod_banners_ubicaciones_codigo_ubicacion");
        } else {
          $campos = "nombre,observaciones,alto,ancho,cantidad_banners,codigo_proveedor_pp";
            $ca->prepareUpdate("mod_banners_ubicaciones", $campos, "codigo_ubicacion=:codigo_ubicacion and codigo_proveedor_pp =:codigo_proveedor_pp");
            $codigoUbicacion = $p["codigo_ubicacion"];
        }
        
        
        
        $ca->bindValue(":codigo_ubicacion", $codigoUbicacion);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":observaciones", $p["observaciones"], true);
        $ca->bindValue(":alto", $p["alto"]);
        $ca->bindValue(":ancho", $p["ancho"]);
        $ca->bindValue(":cantidad_banners", $p["cantidad_banners"]);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();

        $ca->prepareDelete("mod_banners_ubicaciones_pp", "codigo_ubicacion=:codigo_ubicacion");
        $ca->bindValue(":codigo_ubicacion", $codigoUbicacion, false);
        $ca->exec();
        
        foreach ($p["ubicaciones"] as $ubicaciones ) {
        	
        	$ca->prepareInsert("mod_banners_ubicaciones_pp", "codigo_ubicacion,codigo_tipo,codigo_asociado");
	        $ca->bindValue(":codigo_ubicacion", $codigoUbicacion, false);
	        $ca->bindValue(":codigo_tipo",$ubicaciones["tipo_ubicacion"][0]["data"], false);
	        $ca->bindValue(":codigo_asociado", !empty($ubicaciones["codigo_referencia"][0]["data"]) ? $ubicaciones["codigo_referencia"][0]["data"] : 0, false);
	        $ca->exec();
        }
        
        $db->commit();
        return;
    }

    public static function autocompleteEntidades($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
      // throw new JPublicException(print_r($p,1));

        $result = array();

        if ($p["tipo_ubicacion"] == "2") {
            $campos = "c.nombre,d.nombre";
            $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
            $sql = "select distinct
                    case when b.flags->'flag_categorizacion'='0' then c.nombre
                         when b.flags->'flag_categorizacion'='1' then d.nombre
                    end as label,
                    case when b.flags->'flag_categorizacion'='0' then c.codigo_categoria
                         when b.flags->'flag_categorizacion'='1' then d.codigo_categoria
                    end as data
                    from cu_proveedores b 
                    left join cu_categoriasn c on (c.codigo_categoria in 
                        (select unnest(negocios) from view_cu_productos_tienda where codigo_proveedor_pp=:codigo_proveedor_pp)
                    )
                    left join cu_categorias_pp d on (d.codigo_categoria in 
                        (select codigo_categoria1_pp from view_cu_productos_tienda where codigo_proveedor_pp=:codigo_proveedor_pp)
                    )
                    where b.codigo_proveedor=:codigo_proveedor_pp and {$where}";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } elseif ($p["tipo_ubicacion"] == "3") {
            $campos = "c.nombre,d.nombre";
            $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
            $sql = "select distinct
                    case when b.flags->'flag_categorizacion'='0' then c.nombre
                         when b.flags->'flag_categorizacion'='1' then d.nombre
                    end as label,
                    case when b.flags->'flag_categorizacion'='0' then c.codigo_categoria
                         when b.flags->'flag_categorizacion'='1' then d.codigo_categoria
                    end as data
                    from cu_proveedores b 
                    left join cu_categoriasn c on (c.codigo_categoria in 
                        (select codigo_categoria2 from view_cu_productos_tienda where codigo_proveedor_pp=:codigo_proveedor_pp)
                    )
                    left join cu_categorias_pp d on (d.codigo_categoria in 
                        (select codigo_categoria2_pp from view_cu_productos_tienda where codigo_proveedor_pp=:codigo_proveedor_pp)
                    )
                    where b.codigo_proveedor=:codigo_proveedor_pp and {$where}";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } elseif ($p["tipo_ubicacion"] == "4") {
            $campos = "c.nombre,d.nombre";
            $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
            $sql = "select distinct
                    case when b.flags->'flag_categorizacion'='0' then c.vpath
                         when b.flags->'flag_categorizacion'='1' then d.vpath
                    end as label,
                    case when b.flags->'flag_categorizacion'='0' then c.codigo_categoria
                         when b.flags->'flag_categorizacion'='1' then d.codigo_categoria
                    end as data
                    from cu_proveedores b 
                    left join cu_categoriasn c on (c.codigo_categoria in 
                        (select codigo_categoria3 from view_cu_productos_tienda p where codigo_proveedor_pp=:codigo_proveedor_pp )
                    )
                    left join cu_categorias_pp d on (d.codigo_categoria in 
                        (select codigo_categoria3_pp from view_cu_productos_tienda p where codigo_proveedor_pp=:codigo_proveedor_pp)
                    )
                    where b.codigo_proveedor=:codigo_proveedor_pp and {$where}";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } elseif ($p["tipo_ubicacion"] == "5") {
            $campos = "m.nombre";
            $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
            $sql = "select distinct m.nombre as label,m.codigo_marca as data
                from view_cu_marcas m
                where 1=1 and {$where} 
                and m.codigo_marca in 
                (select distinct codigo_marca from view_cu_productos_tienda p where p.codigo_proveedor_pp =:codigo_proveedor_pp)";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } elseif ($p["tipo_ubicacion"] == "8") {
            $campos = "c.nombre";
            $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
            $sql = "select distinct c.nombre as label,c.codigo_categoria as data
                from cu_categoriasv c
                where 1=1 and {$where} 
                and c.codigo_proveedor =:codigo_proveedor_pp";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } elseif ($p["tipo_ubicacion"] == "12") {
        	$campos = "c.nombre";
        	$where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        	$sql = "select distinct c.nombre as label,c.codigo_categoria as data
        	from cu_categorias_pp c
        	where 1=1 and {$where}
        	and c.codigo_proveedor =:codigo_proveedor_pp
        	and c.tipo = 1";
        	$ca->prepare($sql);
        	$ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        	$ca->exec();
            $result = $ca->fetchAll();
        }elseif ($p["tipo_ubicacion"] == "13") {
        	$campos = "c.nombre";
        	$where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        	$sql = "select distinct c.nombre as label,c.codigo_categoria as data
        	from cu_categorias_pp c
        	where 1=1 and {$where}
        	and c.codigo_proveedor =:codigo_proveedor_pp
        	and c.tipo in (1,2) ";
        	$ca->prepare($sql);
        	$ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        	$ca->exec();
            $result = $ca->fetchAll();
        }
        return $result;

    }

    public static function loadEditRs() {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();
        
        $where ="1=1";
        if($si['restricciones_paquetes'][get_class()]['UbicacionesCompletas']== "activo"){
        	
        	$where .="";
        }else{
        	if($si['restricciones_paquetes'][get_class()]['UbicacionHome']== "activo"){
        		$where .=" and codigo_tipo=1";
        	}
        	
        }

        //Tipos productos
        $sql = "select codigo_tipo as data,nombre as label from mod_banners_ubicaciones_pp_tipo where {$where} ";
        $ca->prepare($sql);
        $ca->exec();
        
        $result["tipos_ubicaciones"] = $ca->fetchAll();
        
      
       

        //Defaults
        $result["defaults"] = array("estado" => "activo");

        return $result;
    }
    
    public static function loadCampanasPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if (isset($p["filters"]["fecha_inicial"]) && $p["filters"]["fecha_inicial"] != "" && isset($p["filters"]["fecha_final"]) && $p["filters"]["fecha_final"] != "") {
            $where .= " and (cast(a.fecha_inicial as date),cast(a.fecha_final as date)) overlaps (cast('{$p["filters"]["fecha_inicial"]}' as date),cast('{$p["filters"]["fecha_final"]}' as date)) ";
        }
        foreach ($p["column_filters"] as $colum  => $valorCulumn){
        	if($valorCulumn != ""){
        		$where .= " and ". $ca->sqlFieldsFilters($colum,$valorCulumn);
        	}
        }

        $sql = " select *  from  ( select a.codigo_campana,a.nombre,a.fecha_inicial,a.fecha_final,b.nombre as sitio
                from gen_campanas a left join gen_sitios b on (a.codigo_sitio=b.codigo_sitio)
                where 1=1 and a.codigo_proveedor_pp=:codigo_proveedor_pp ) a where 1=1 and ({$where}) ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function loadCampanaOne($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("gen_campanas", "codigo_campana,nombre,fecha_inicial,fecha_final,codigo_sitio", "codigo_campana=:codigo_campana");
        $ca->bindValue(":codigo_campana", $p["codigo_campana"]);
        $ca->exec();
        $result = $ca->fetch();

        $ca->prepareSelect("gen_sitios", "codigo_sitio as data,nombre as label", "codigo_sitio=:codigo_sitio");

        $ca->bindValue(":codigo_sitio", $result["codigo_sitio"]);
        $ca->exec();
        $result["sitios"] = $ca->fetchAll();
        return $result;
    }
    
    public static function saveCampana($p) {
        $si = session::info();

        if (empty($p["nombre"])) {
            throw new JPublicException("Nombre inválido");
        }

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "codigo_campana,nombre,fecha_inicial,fecha_final,codigo_sitio,codigo_proveedor_pp";

        $db->transaction();

        if ($p["codigo_campana"] === "") {
            $ca->prepareInsert("gen_campanas", $campos);
            $codigo_campana = $db->nextVal("gen_campanas_codigo_campana");
        } 
        else {
            $ca->prepareUpdate("gen_campanas", $campos, "codigo_campana=:codigo_campana");
            $codigo_campana = $p["codigo_campana"];
        }

        $ca->bindValue(":codigo_campana", $codigo_campana);
        $ca->bindValue(":nombre", $p["nombre"]);
        $ca->bindValue(":fecha_inicial", $p["fecha_inicial"]);
        $ca->bindValue(":fecha_final", $p["fecha_final"]);
        $ca->bindValue(":codigo_sitio", "1", false);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        $db->commit();

        return $codigo_campana;
    }
    
    public static function loadPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "nombre,campana";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if ($p["filters"]["campana"] != "todos") {
            $where .= " and current_date " . ($p["filters"]["campana"] == "activa" ? "" : "not") . " between  fecha_inicial and  fecha_final ";
        }

        if ($p["filters"]["estado"] != "todos") {
            $where .= " and estado='{$p["filters"]["estado"]}' ";
        }
        
        foreach ($p["column_filters"] as $colum  => $valorCulumn){
        	if($valorCulumn != ""){
        		$where .= " and ". $ca->sqlFieldsFilters($colum,$valorCulumn);
        	}
        }

        $sql = "
        	  select * from (	
        	  select b.codigo_banner,b.nombre,
              array_to_string(array_accum(c.nombre),',') as ubicaciones,
              d.codigo_campana,d.nombre as campana,
              case when current_date between d.fecha_inicial and d.fecha_final then 'activa' else 'inactiva' end as estado_campana,
              b.url,b.archivo,b.estado,d.fecha_inicial,d.fecha_final
              from mod_banners b
              left join mod_banners_horarios h on (b.codigo_banner=h.codigo_banner)
              left join mod_banners_horarios_ubicaciones hu on ( hu.codigo_horario=h.codigo_horario)
              left join mod_banners_ubicaciones c on (hu.codigo_ubicacion=c.codigo_ubicacion)
              left join gen_campanas d on (b.codigo_campana=d.codigo_campana)
              where 1=1  and b.codigo_proveedor_pp =:codigo_proveedor_pp
              group by b.codigo_banner,b.nombre,d.codigo_campana,d.nombre,b.url,b.archivo,b.estado,d.fecha_inicial,d.fecha_final) tl  where 1=1 and ({$where}) 
        ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }
    
    public static function loadOne($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $ca->prepareSelect("mod_banners",
            "codigo_banner,codigo_campana,codigo_categoria,nombre,url,archivo,observaciones,estado,alto,ancho,target",
            "codigo_banner=:codigo_banner");
        $ca->bindValue(":codigo_banner", $p["codigo_banner"]);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Banner no localizado");

        }

        $result = $ca->fetch();

        $result["rs"] = modRs::load(rs::modBanners, $p["codigo_banner"], array(
            rs::modBannersArchivo,
            rs::modBannersThumb
        ));

        $sizeImagen = array(
            rs::modBannersArchivo => "350",
            rs::modBannersThumb => "200"
        );

        foreach ($result["rs"] as $k => $v) {
            $result["rs"][$k]["preview"] = JDbFile::url($v["id"], array("w" => $sizeImagen[$k], "h" => 120, "frm" => 1));
        }

        $ca->prepareSelect("gen_campanas",
            "codigo_campana as data,nombre as label",
            "codigo_campana=:codigo_campana");

        $ca->bindValue(":codigo_campana", $result["codigo_campana"]);
        $ca->exec();
        $result["campanas"] = $ca->fetchAll();

        return $result;
    }

    public static function autocompleteCampanas($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (isset($p["ids"])) {
            $ids = implode(",", $p["ids"]);
            $where = "a.codigo_campana in ({$ids})";
        } else {
            $campos = "a.nombre";
            $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        }

        $sql = "select a.codigo_campana as data,a.nombre as label
             from gen_campanas a
             where 1=1 and ({$where}) and a.codigo_proveedor_pp=:codigo_proveedor_pp
             order by a.nombre";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }
    
    public static function save($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["nombre"])) {
            throw new JPublicException("Nombre inválido");
        }

        $iv = array();
        $iv[] = array("name" => "alto", 'label' => 'Alto', "filter" => FILTER_VALIDATE_INT);
        $iv[] = array("name" => "ancho", 'label' => 'Ancho', "filter" => FILTER_VALIDATE_INT);

        $validationErrors = JInputValidator::validateVars($p, $iv, array('output' => 'string'));
        if ($validationErrors) {
            throw new JPublicException($validationErrors);
        }

        $db->transaction();

        $campos = "codigo_banner,codigo_campana,nombre,url,observaciones,estado,alto,ancho,target,codigo_proveedor_pp";
        if ($p["codigo_banner"] === "") {
            $ca->prepareInsert("mod_banners", $campos);
            $codigoBanner = $db->nextVal("mod_banners_codigo_banner");
        }
        else {
            $ca->prepareUpdate("mod_banners", $campos, "codigo_banner=:codigo_banner");
            $codigoBanner = $p["codigo_banner"];
        }

        $codigoCategoria = "-1";
        if (isset($p["categoria"]) && is_array($p["categoria"]) && count($p["categoria"]) > 0) {
            $codigoCategoria = $p["categoria"][0] == "" ? "-1" : $p["categoria"][0];
        }

        $codigoCampana = "-1";
        if (isset($p["campana"]) && is_array($p["campana"]) && count($p["campana"]) > 0) {
            $codigoCampana = $p["campana"][0] == "" ? "-1" : $p["campana"][0];
        }

        $ca->bindValue(":codigo_banner", $codigoBanner, false);
        $ca->bindValue(":codigo_campana", $codigoCampana, false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":url", $p["url"], true);
        $ca->bindValue(":observaciones", $p["observaciones"], true);
        $ca->bindValue(":estado", $p["estado"], true);
        $ca->bindValue(":alto", ($p["alto"] === "" ? "0" : $p["alto"]), false);
        $ca->bindValue(":ancho", ($p["ancho"] === "" ? "0" : $p["ancho"]), false);
        $ca->bindValue(":target", $p["target"], true);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        
        if(!empty($p["thumb"])) {
          modRs::save(rs::modBanners, $codigoBanner, rs::modBannersThumb, $p["thumb"]);
        }
        
        if(!empty($p["archivo"])) {
          modRs::save(rs::modBanners, $codigoBanner, rs::modBannersArchivo, $p["archivo"]);
        }

        // guardar horarios
        if($p["codigo_banner"] == ''){
            $p["codigo_banner"] = $codigoBanner;
        }
            
        if (!isset($p["codigo_banner"]) || $p["codigo_banner"] == "") {
            throw new JPublicException("Error, falta código banner");
        }

        $codigo_banner = $p["codigo_banner"];

        if (!isset($p["horarios"]) || count($p["horarios"]) == 0) {
            throw new JPublicException("Error, falta ubicación");
        }

        foreach ($p["horarios"] as $h) {
            if (!isset($h["codigo_ubicacion"]) || $h["codigo_ubicacion"] == "") {
                throw new JPublicException("Error, falta código ubicación " . print_r($h, 1));
            }

            $campos = "codigo_banner,codigo_horario,impresiones,orden,impresiones_ciclo,duracion_segundos,tipo_impresion";

            if ($h["codigo_horario"] === "") {
                $codigo_horario = $db->nextVal("mod_banners_horarios_codigo_horario");
                $ca->prepareInsert("mod_banners_horarios", $campos);
            }
            else {
                $codigo_horario = $h["codigo_horario"];
                $ca->prepareUpdate("mod_banners_horarios", $campos, "codigo_horario=:codigo_horario and codigo_banner=:codigo_banner");
            }

            $ca->bindValue(":codigo_horario", $codigo_horario, false);
            $ca->bindValue(":codigo_banner", $codigo_banner, false);
            $ca->bindValue(":impresiones", -1, false);
            $ca->bindValue(":orden", ($h["orden"] === "" ? "0" : $h["orden"]), false);
            $ca->bindValue(":impresiones_ciclo", ($h["impresiones_ciclo"] === "" ? "0" : $h["impresiones_ciclo"]), false);
            $ca->bindValue(":duracion_segundos", ($h["duracion"] === "" ? "0" : $h["duracion"]), false);
            $ca->bindValue(":tipo_impresion", $h["tipo_impresion"], true);
            $ca->exec();

            $rCodigosHorario[] = $codigo_horario;
            if ($h["codigo_horario"] !== "") {
                $ca->prepareDelete("mod_banners_horarios_ubicaciones", "codigo_horario=:codigo_horario");
                $ca->bindValue(":codigo_horario", $codigo_horario, false);
                $ca->exec();

                $ca->prepareDelete("mod_banners_horario_horas", "codigo_horario=:codigo_horario");
                $ca->bindValue(":codigo_horario", $codigo_horario, false);
                $ca->exec();
            }

            $ca->prepareInsert("mod_banners_horarios_ubicaciones", "codigo_horario,codigo_ubicacion");
            $ca->bindValue(":codigo_horario", $codigo_horario, false);
            $ca->bindValue(":codigo_ubicacion", $h["codigo_ubicacion"], false);
            $ca->exec();

            $horas = json_decode($p["horarios"][0]["horas"]);

            $campos = "codigo_horario,dia,hora";
            for ($i = 0; $i < 7; $i++) {
                for ($j = 0; $j < 24; $j++) {
                    if ($horas[$i][$j] == 1) {
                        $ca->prepareInsert("mod_banners_horario_horas", $campos);
                        $ca->bindValue(":codigo_horario", $codigo_horario);
                        $ca->bindValue(":dia", $i, false);
                        $ca->bindValue(":hora", $j, false);
                        $ca->exec();
                    }
                }
            }
        }

        $ca->prepareSelect("mod_banners_horarios", "codigo_horario", "codigo_banner=:codigo_banner and codigo_horario not in (:codigos_horario)");
        $ca->bindValue(":codigo_banner", $codigo_banner, false);
        $ca->bindValue(":codigos_horario", implode(",", $rCodigosHorario), false);
        $ca->exec();
        
        $rHorariosBorrar = $ca->fetchAll();
        foreach ($rHorariosBorrar as $r) {
            $ca->prepareDelete("mod_banners_horarios_ubicaciones", "codigo_horario=:codigo_horario");
            $ca->bindValue(":codigo_horario", $r["codigo_horario"], false);
            $ca->exec();

            $ca->prepareDelete("mod_banners_horario_horas", "codigo_horario=:codigo_horario");
            $ca->bindValue(":codigo_horario", $r["codigo_horario"], false);
            $ca->exec();

            $ca->prepareDelete("mod_banners_horarios", "codigo_horario=:codigo_horario and codigo_banner=:codigo_banner");
            $ca->bindValue(":codigo_horario", $r["codigo_horario"]);
            $ca->bindValue(":codigo_banner", $codigo_banner, false);
            $ca->exec();
        }

        $db->commit();
        return;
    }
}