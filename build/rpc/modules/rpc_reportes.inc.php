<?php
require_once $cfg["rutaModeloLogico"]."mod_reportes.inc.php";

class Reportes {
    public static function loadInfoVentas($p) {
      $si = Session::info();
      $db = JDatabase::database();
      $ca = new JDbQuery($db);

      $sql = "select func_number_format(sum(ventas_dia),0) as ventas_dia, func_number_format(sum(ventas_ano),0) as ventas_ano
        from (
        select coalesce(sum(p.total_coniva),0) as ventas_dia,0 as ventas_ano
                          from view_cu_pedidos_enc p
                          where p.codigo_proveedor_pp = :codigo_proveedor
                          and p.estado = 'pagado' and p.estado_autorizacion = 'aprobado'
                          and p.fechahora_pedido::date =CURRENT_DATE
        union all
        select 0 as ventas_dia,coalesce(sum(p.total_coniva),0) as ventas_ano
                          from view_cu_pedidos_enc p
                          where p.codigo_proveedor_pp = :codigo_proveedor
                          and p.estado = 'pagado' and p.estado_autorizacion = 'aprobado'
                          and substr(p.fechahora_pedido::text,1,4) = substr(CURRENT_DATE::text,1,4)
        ) t ";

      $ca->prepare($sql);
      $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
      $ca->exec();
      return $ca->fetch();
    }

    public static function loadVentasProductos($p) {
      $db = JDatabase::database();
      $ca = new JDbQuery($db);
      $si = Session::info();

      $sql = "select t.nombre_producto,t.cantidad,func_number_format(t.monto,0) as monto
      from
      (select p.referencia||' - '||p.nombre as nombre_producto,sum(p.unidades) as cantidad,
      sum(p.precio_venta_coniva * p.unidades) as monto
      from view_cu_pedidos_det p
      where p.codigo_proveedor_pp = :codigo_proveedor
      and p.fechahora_pedido::Date between current_date-30 and current_Date
      and p.estado_pin = 'pagado' and p.estado_autorizacion = 'aprobado'
      group by p.referencia||' - '||p.nombre) t
      order by monto::numeric desc,cantidad desc
      limit 5";

      $ca->prepare($sql);
      $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
      $ca->exec();
      return $ca->fetchAll();
    }

    public static function loadVentasMeses($p) {
      $db = JDatabase::database();
      $ca = new JDbQuery($db);
      $si = Session::info();

      $sql = "select round(sum(total_coniva),2) as total,substr(fechahora_pedido::text,1,7) as mes
        from cu_pedidos_enc ped
        where estado='pagado' and estado_autorizacion='aprobado'
        and ped.codigo_proveedor_pp = :codigo_proveedor
        and substr(ped.fechahora_pedido::text,1,7) between substr((current_date - 360)::text,1,7) and substr((current_date)::text,1,7)
        group by substr(fechahora_pedido::text,1,7)
        order by substr(fechahora_pedido::text,1,7) asc
        ";

      $ca->prepare($sql);
      $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
      $ca->exec();
      return $ca->fetchAll();
    }
    
	public static function loadVentasFormasPago($p){
   	 $db = JDatabase::database();
     $ca = new JDbQuery($db);
     $si = Session::info();
    	
     $sql ="select count(pe.pin) as pines,sum(pe.total_coniva) as total_venta,
		case when pe.codigo_forma_pago=1 and pe.forma_pago_elegida=10 then 'PCE'
		else pe.nombre_forma_pago end as forma_pago
		from view_cu_pedidos_enc pe
		where  pe.estado='pagado'
		and pe.estado_autorizacion='aprobado'
		and pe.fechahora_pedido::date between '2017-01-01' and '2017-01-31'
		and pe.codigo_proveedor_pp=:codigo_proveedor
		group by case when pe.codigo_forma_pago=1 and pe.forma_pago_elegida=10 then 'PCE'
		else pe.nombre_forma_pago end";
     
      $ca->prepare($sql);
      $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
      $ca->exec();
      return $ca->fetchAll();
    }
    
	public static function loadVentasCiudadCompra($p){
   	 $db = JDatabase::database();
     $ca = new JDbQuery($db);
     $si = Session::info();

     	$sql ="select count(pe.pin)as pines,pe.nombre_ciudad_cliente,cc.latitud,cc.longitud
		from view_cu_pedidos_enc pe
		join cu_ciudades_coordenadas cc on cc.id_ciudad=pe.idciudad_cliente
		where 
		pe.fechahora_pedido::date BETWEEN '2015-01-01' and CURRENT_DATE
		and pe.estado='pagado'
		and pe.estado_autorizacion='aprobado'
		and pe.codigo_proveedor_pp=:codigo_proveedor
		group by pe.nombre_ciudad_cliente,cc.latitud,cc.longitud";
	     
      $ca->prepare($sql);
      $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
      $ca->exec();
      return $ca->fetchAll();
    }
    
    public static function loadReportesVentas(){
        $si = Session::info();
       
        $result = array();
        $p = array("codigo_proveedor" => $si["codigo_proveedor"]);
        $p["url_dominio"] = $si["url_dominio"];
        $result["ventas_ultimos_meses"] = modReportes::ventasUltimosMeses($p);
        $result["productos_mas_vendidos"] = modReportes::ventasProductosMasVendidos($p);
        $result["kpis"] = modReportes::kpis($p);
        return $result;
    }
}