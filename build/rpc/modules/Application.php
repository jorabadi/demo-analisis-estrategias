<?php
class Application {
    public static $appId = 1;
    public static $appVersion = 2.24;

    public static function uploadBytes($p) {
        global $cfg;
        $tmp_name = uniqid("upload");
        $data = file_get_contents($p["bytes"]); //read stream data://text/plain;base64,SSBsb3ZlIFBIUAo=
        file_put_contents("{$cfg["appPath"]}/private_tmp/{$tmp_name}", $data);
        return array("tmp_name" => $tmp_name);
    }

}