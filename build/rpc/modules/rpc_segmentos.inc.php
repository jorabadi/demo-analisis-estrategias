<?php
require_once $cfg["rutaModeloLogico"]."mod_segmentos_mk.inc.php";

class Segmentos {
    public static function loadPage($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modSegmentosMarketing::listarSegmentosPaginado($p);
    }
    
    public static function loadEditRs() {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modSegmentosMarketing::loadEditRs($p);
    }
    
    /***
	 * 
	 * @param $p=> array(
	 * nombre
	 * reglas => array(
	 * codigo_segmento
	 * entidad
	 * campo
	 * operador
	 * conjuncion
	 * tipo_campo
	 * rango_fijo_fechas
	 * fecha_inicial
	 * fecha_final
	 * tipo_periodo
	 * periodo
	 * )
	 * )
	 */
    public static function guardarSegmento($p) {
        $db = JDatabase::database();
        $si = session::info();
        $db->transaction();
        try {
            $p["codigo_proveedor"] = $si["codigo_proveedor"];
            $result = modSegmentosMarketing::guardarSegmento($p);
            $db->commit();
            return $result;
        } catch(Exception $ex) {
            $db->rollback();
            throw new JPublicException($ex->getMessage());
        }
    }
    
    public static function cargarDetalleSegmento($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modSegmentosMarketing::cargarDetalle($p);
    }
    
    public static function cargarEntidad($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modSegmentosMarketing::cargarEntidad($p);
    }
    
    public static function autocompletar($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        $p["flags"] = $si["flags"];
        return modSegmentosMarketing::autocompletar($p);
    }
    
    public static function loadOperador($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modSegmentosMarketing::loadOperador($p);
    }
}