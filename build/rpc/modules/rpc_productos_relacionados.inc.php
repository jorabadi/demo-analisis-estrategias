<?php
require_once $cfg["rutaModeloLogico"] . "mod_productos_relacionados.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_reglas_productos_relacionados_producto.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_tipos_filtro_reglas.inc.php";

class ProductosRelacionados {

    public static function obtenerTiposFiltro() {
        $parametros = null;
        return modTiposFiltroReglas::ObtenerTiposFiltroReglas($parametros);
    }

    /*
     * array(
     *   codigo_proveedor,
     *   reglas,
     *   codigo_producto
     *  )
     */
    public static function GuardarReglas($parametrosGuardarReglas) {
        $si = Session::info();
        $parametrosGuardarReglas["codigo_proveedor"] = $si["codigo_proveedor"];

        return modReglasProductosRelacionadosProducto::GuardarReglasProductosRelacionadosProducto($parametrosGuardarReglas);
    }

    public static function ObtenerReglasProductosRelacionadosProducto($codigoProducto) {
        return modReglasProductosRelacionadosProducto::ObtenerReglasProductosRelacionadosProducto($codigoProducto);
    }

}