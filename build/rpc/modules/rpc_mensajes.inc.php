<?php
class Mensajes {

    public static function loadPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $fields = "asunto";
        $where = $ca->sqlFieldsFilters($fields, $p["filters"]["filtro"]);

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        $sql = "select codigo_mensaje,asunto,cuerpo,substr(fechahora::text,1,19) as fechahora 
		  from cu_mail_inbox where 1=1 and ({$where})";
        $ca->prepare($sql);
        return $ca->execPage($p);
    }

    public static function loadOne($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();

        $sql = "select * from cu_mail_inbox where codigo_mensaje=:codigo_mensaje";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_mensaje", $p["codigo_mensaje"], false);
        $ca->exec();
        $result = $ca->fetch();

        return $result;
    }

}