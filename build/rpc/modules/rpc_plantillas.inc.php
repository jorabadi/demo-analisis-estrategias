<?php
require_once $cfg["rutaModeloLogico"]."mod_plantillas.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_plantillas_variables.inc.php";

class Plantillas {
    
    public static function loadCss() {
        global $cfg;
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $templatePath = $cfg["template_path"];
        $exludedFiles = array("_base");
        $files = glob("{$templatePath}*.scss");
        
        $estados = array();
        $result = array();
        $cssPlantillas = array();
        $cssExcluidos = array();
        
        $sql = "select nombre,contenido,estado
           from cu_plantillas_catalogo
           where tipo = 'css' and codigo_proveedor_pp=:codigo_proveedor_pp";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        $plantillas = $ca->fetchAll();
        
        $plantillasBD = array_reduce($plantillas, function($acum, $item) {
            $acum[] = $item["nombre"];
            return $acum;
        });
        
        foreach ($files as $file) {
            $nombreArchivo = basename($file);
            if(in_array(str_replace(".scss","",$nombreArchivo), $exludedFiles)) {
                continue;
            }
            
            if(!empty($plantillasBD)){ 
                if(in_array(str_replace(".scss","",$nombreArchivo), $plantillasBD)) {
                    continue;
                }
            }
            
            $contenido = file_get_contents("{$templatePath}/{$nombreArchivo}");
            $plantilla = array(
                "nombre" => str_replace(".scss","",$nombreArchivo),
                "contenido" => $contenido,
                "estado" => "sin_asignar"
            );
            $cssPlantillas[] = $plantilla;
        }
        
        $cssPlantillas = array_merge($cssPlantillas,$plantillas);
        
        foreach ($exludedFiles as $file) {
            $contenido = file_get_contents("{$templatePath}{$file}.scss");
            $plantilla = array(
                "nombre" => $file,
                "contenido" => $contenido,
                "estado" => "sin_asignar"
            );
            $cssExcluidos[] = $plantilla;
        }
        
        $templatesPath = $cfg["pp_template_path"];
        $previewPath = "{$templatesPath}{$si["url_pp"]}/css/preview";
        $publishPath = "{$templatesPath}{$si["url_pp"]}/css";
        
        if(file_exists("{$previewPath}/_base.scss")) {
            $estados["preview"] = true;
        }
        
        if(file_exists("{$publishPath}/_base.scss")) {
            $estados["publicado"] = true;
        }
        
        $result["plantillas"] = $cssPlantillas;
        $result["excluidos"] = $cssExcluidos;
        $result["estados"] = $estados;
        
        return $result;
    }
    
    public static function save($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $ca->prepareSelect("cu_plantillas_catalogo", "codigo_plantilla","nombre=:nombre and tipo='css' and codigo_proveedor_pp=:codigo_proveedor_pp");
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"],false);
        $ca->bindValue(":nombre", $p["nombre"],true);
        $ca->exec();
        
        $campos = "codigo_plantilla,nombre,contenido,codigo_proveedor_pp,tipo,estado";
        
        if ($ca->size() == 0) {
            $codigoPlantilla = $db->nextVal("cu_plantillas_catalogo_codigo_plantilla");
            $ca->prepareInsert("cu_plantillas_catalogo", $campos);
        } else {
            $rPlantilla = $ca->fetch();
            $codigoPlantilla = $rPlantilla["codigo_plantilla"];
            $ca->prepareUpdate("cu_plantillas_catalogo", $campos, "codigo_plantilla=:codigo_plantilla and tipo='css' and codigo_proveedor_pp=:codigo_proveedor_pp");
        }
        
        $ca->bindValue(":codigo_plantilla", $codigoPlantilla, false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":contenido", $p["contenido"], true);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"],false);
        $ca->bindValue(":tipo", "css",true);
        $ca->bindValue(":estado", "pendiente", true);
        $ca->exec();
        
        //reseteo el cache de la info del proveedor
        global $manejadorCache;
        $manejadorCache->set("PP_informacionProveedor_{$si["codigo_proveedor"]}",false,900);
        
        $ca->prepare("select func_next_version_personalizacion(:codigo_proveedor_pp)");
        $ca->bindValue(":codigo_proveedor_pp",$si["codigo_proveedor"], false);
        $ca->exec();
    }
    
    public static function preview() {
        global $cfg;
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $token = self::getToken();
        
        $templatesPath = $cfg["pp_template_path"];
        $previewPath = "{$templatesPath}{$si["url_pp"]}/css/preview";
        
        self::uploadCssPath($previewPath);
        self::updateEstadoPlantillas("preview");
        
        
        $nameToken = "token_preview_{$token}";
        
        self::aumentarVersionCss();
        
        return array("url"=>"http://".$si["url_dominio"]."?token=".$nameToken);
    }
    
    public static function publish() {
        global $cfg;
        $si = session::info();
        $token = self::getToken();
        
        $templatesPath = $cfg["pp_template_path"];
        $publishPath = "{$templatesPath}{$si["url_pp"]}/css";
        self::uploadCssPath($publishPath);
        self::updateEstadoPlantillas("publicado");
        self::aumentarVersionCss();
        
        $nameToken = "token_preview_{$token}";
        return array("url"=>"http://".$si["url_dominio"]."?untoken=".$nameToken);
    }
    
    public static function getToken() {
        $si = session::info();
        $parToken = array(
            "codigo_proveedor_pp" => $si["codigo_proveedor"],
            "fecha_hora" => date("Y-m-d")
        );
        $token = str_replace("=","",base64_encode(json_encode($parToken)));
        return $token;
    }
    
    public static function uploadCssPath($path) {
        global $cfg;
        
        $isCreated = is_dir($path) || mkdir($path,0777,true);
        $templatePath = $cfg["template_path"];
        if($isCreated) {
            $css = self::loadCss();
           // $plantillas = array_merge($css["plantillas"],$css["excluidos"]);
           
            /*foreach ($css["excluidos"] as $plantilla) {
                file_put_contents("{$path}/{$plantilla["nombre"]}.scss", $plantilla["contenido"]);
            }*/
            //se agregan los archivos customizados
            foreach ($css["plantillas"] as $plantilla) {
                $contenido= $plantilla["contenido"];
                $patronImport = '/(@import)\s*\(?"([^;]+?)"\)?;/';
                $patronRemplazoImport = '${1} "custom_${2}";';
                $cssReemplazado = preg_replace($patronImport, $patronRemplazoImport, $contenido);
                $contenido = "@import \"{$plantilla["nombre"]}\";\n".$cssReemplazado;
                $contenido = str_replace("@import \"custom__base\";","@import \"_base\";",$contenido);
                file_put_contents("{$path}/custom_{$plantilla["nombre"]}.scss", $contenido);
            }
        }
    }
    
    public static function updateEstadoPlantillas($estado) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $ca->prepareUpdate("cu_plantillas_catalogo", "estado", "codigo_proveedor_pp=:codigo_proveedor_pp and tipo='css'");
        $ca->bindValue(":estado", $estado, true);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"],false);
        $ca->exec();
    }
    
    public static function revertirADefault($p) {
        global $cfg;
        $templatePath = $cfg["template_path"];
        $contenido = file_get_contents("{$templatePath}{$p["nombre"]}.scss");
        
        $result = array();
        $result["contenido"] = $contenido;
        return $result;
    }
    
    public static function revertirAPreview($p) {
        global $cfg;
        $si = session::info();
        $templatesPath = $cfg["pp_template_path"];
        $previewPath = "{$templatesPath}{$si["url_pp"]}/css/preview";
        $contenido = file_get_contents("{$previewPath}/{$p["nombre"]}.scss");
        
        $result = array();
        $result["contenido"] = $contenido;
        return $result;
    }
    
    public static function revertirAPublicado($p) {
        global $cfg;
        $si = session::info();
        $templatesPath = $cfg["pp_template_path"];
        $publishPath = "{$templatesPath}{$si["url_pp"]}/css";
        $contenido = file_get_contents("{$publishPath}/{$p["nombre"]}.scss");
        
        $result = array();
        $result["contenido"] = $contenido;
        return $result;
    }
    
    public static function aumentarVersionCss() {
        global $cfg;
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $sql = "update cu_proveedores_pp set version_css = version_css + 1 
            where codigo_proveedor=:codigo_proveedor_pp";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"],false);
        $ca->exec();
    }

    public static function obtenerPlantillasContenido(){
        $p = "contenido";
        return modPlantillas::obtenerPlantillasHeaderTipo($p);
    }

    public static function obtenerVariablesPlantilla($codigoPlantilla){
        return modPlantillasVariables::obtenerVariablesPlantilla($codigoPlantilla);
    }

    public static function renderPlantilla($p){  
        return modPlantillas::renderPlantilla($p['plantilla'], $p['variables_plantillas']);
    }

    public static function obtenerConfiguracionTemplateProveedor($plantilla){
        $si = Session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        $p["plantilla"] = $plantilla;
        $res = modPlantillas::obtenerConfiguracionTemplateProveedor($p);
        
        return $res;
    }

    public static function guardarConfiguracionTemplateProveedor($plantilla){
        $si = Session::info();
        $res = modPlantillas::guardarConfiguracionTemplateProveedor($si["codigo_proveedor"], $plantilla);
        
        return $res;
    }

    public static function existePlantillaPersonalizada($codigoContenido){
        return modPlantillas::existePlantillaPersonalizada($codigoContenido);
    }
}