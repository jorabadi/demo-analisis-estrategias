<?php
require_once $cfg["rutaModeloLogico"] . "mod_resources.inc.php";

class ProveedoresAcl
{

    const FInformacionProveedorNe = "Información del proveedor";

    const FInformacionProveedorNe_Save = "Información del proveedor: Guardar";

    const FPreferenciasNe = "Sitio personalizado";

    const FPreferenciasNe_Save = "Sitio personalizado: Guardar";

    const LUsuarios = "Consulta de usuarios";

    const LVendedores = "Consulta de vendedores";

    // const FUsuariosNe = "Edición de usuarios";
    const FUsuariosNe_Save = "Edición de usuarios: Guardar";

    const LPuntosDespacho = "Consulta de puntos de despacho";

    // const FPuntosDespachoNe = "Edición de Puntos de despacho";
    const FPuntosDespachoNe_Save = "Edición de Puntos de despacho: Guardar";

    const LGruposCiudades = "Consulta de grupos de ciudades";

    // const FGruposCiudadesNe = "Edición de grupos de ciudades";
    const FGruposCiudadesNe_Save = "Edición de grupos de ciudades: Guardar";

    const LProductos = "Consulta de productos";

    // const FProductosNe = "Edición de productos";
    const FProductosNe_Save = "Edición de productos: Guardar";

    const FCategoriasnppNe = "Consulta de categorias";

    const FCategoriasnppNe_Save = "Edición de categorias: Guardar";

    const LProductosInventario = "Inventario de Productos";

    const LProductosInventario_Save = "Inventario de Productos: Ajustar inventario";

    const LProductosInventarioPorBodega = "Inventario de Productos B";

    const LPromociones = "Consulta de promociones";

    // const FPromocionesNe = "Edición de promociones";
    const FPromocionesNe_Save = "Edición de promociones: Guardar";

    const LGarantiasEspecificas = "Consulta de garantias específicas";

    const FGarantiasEspecificasNe_Save = "Edición de garantías específicas: Guardar";

    const LPedidosPorDespachar = "Consulta de pedidos por despachar";

    const LPedidosPorDespacharMasivo = "Consulta de pedidos por despachar masivamente";

    // const FPedidosPorDespacharNe = "Edición de pedidos por despachar";
    const FPedidosPorDespacharNe_ImprimirDocumentos = "Edición de pedidos por despachar: Imprimir documentos";

    const FPedidosPorDespacharNe_SolicitarRecogida = "Edición de pedidos por despachar: Solicitar recigida";

    const LHistoricoPedidos = "Consulta de historico de pedidos";

    const LTransportePedidos = "Consulta de estado de transporte para despachos manuales";

    const FTransportePedidos_Save = "Edición de estado de transporte: Guardar";

    const LAnexosFactura = "Consulta de anexos factura";

    const LAnexosFacturaTienda = "Consulta de anexos factura - Portal personalizado";

    const LTransacciones = "Consulta de transacciones - Portal personalizado";

    const LEstadisticas = "Consulta de estadisticas de visitas y ventas";

    const LEnviarMensajes = "Consulta de mesajes a proveedores";

    const FEnviarMensajes = "Envio de mesajes a proveedores";

    // productos incluidos
    const FProductosIncluidos = "Administración de productos incluidos";

    const LProductosIncluidos = "Listado de productos incluidos";

    const LMargenesProductosAliados = "Listado de productos incluidos";

    const LAnexosFacturaAliados = "Consulta de anexos factura - Portal personalizado A";

    const FImportarInventarios = "Importar ajuste inventario de provedor";

    const FImportarProductos = "Importar productos proveedor";

    const LCategoriasVirtuales = "Consulta de Categorias Virtuales";

    const LUbicaciones = "Consulta de Ubicaciones";

    const LCampanas = "Consulta de Campañas";

    const LBanners = "Consulta de Banners";

    // const promocionProductosIncluidos ="promociones productos incluidos aliadios";
    // reclamaciones
    const LConsultaPQR = "Consulta de PQR";

    const LConsultaPlantillaPQR = "Consulta de Plantillas Correos PQR";

    const LConsultaNotasCredito = "Consulta de Notas Credito";

    const LBonos = "Consulta de Bonos";

    const LAcuerdosCreacion = "Consulta de Acuerdos Creacion";

    const LAcuerdosRedencion = "Consulta de Acuerdos Redencion";

    const LCanastas = "Consulta de Canastas";

    const LListasDeseos = "Consulta de Listas de deseos";

    // Permisos de opciones de Exportar informacion
    const LExportacion = "Poder utilizar todas las opciones de exportar que tiene el administrador";

    const LExportarInventario = "Exportar Inventarios";

    const LExportarPrecios = "Exportar Precios";

    const ExportarUsuarios = "Exportar usuarios registrados en Portal personalizado";

    const ExportarClientes = "Exportar clientes del Portal personalizado";

    // configuraciones
    const LPopups = "Consulta de popups";

    const FPlantillasCss = "Edicion de plantillas Css";

    const FContenido = "Edicion de contenidos";

    const LResoluciones = "Consulta de resoluciones de facturacion";

    const LOpinionesProductos = "Consultar Opiniones Productos";

    const LOpinionesCompra = "Consultar Opiniones Compra";

    /* Marketing */
    const LMarketing = "Consulta Listado de Campañas";

    const LMarcas = "Administracion de las marcas";

    const FImportarProductostienda = "Importar productos en la tienda";

    const FImportarPedidosPCE = "Importar pedidos de pago contra entrega";

    const FImportarPedidos = "Importar pedidos desde un archivo";

    const FImportarPromociones = "Importar promocione de productos";

    const FImportarInventarioAliado = "Importar Inventarios para los aliados";

    const FImportarPedidosPorDespachar = "Importar pedidos pendientes por despachar desde un archivo";

    const FImportarPedidosYDespachos = "Importar pedidos y dejarlos listos para solicitar la recogida";

    const LPromocionesAliados = "Cargar promociones para los aliados";

    const Layudas = "Ayuda";

    const LSegmentos = "Cargar segmentos";

    const FImportarContenidos = "Importar contenidos";

    const FDashboard = "Dashboard";
}

class Session
{

    public static function echoTest($p)
    {
        return "Hello World {$p["message"]}";
    }

    public static function checkAcl($action)
    {
        if ($_SESSION["info"]["tipo"] == "admin") {
            return true;
        }
        return in_array($action, $_SESSION["info"]["permisos"]);
    }

    public static function signin($p)
    {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        global $cfg;
        $errorMessage = "Usuario o clave invalidos";
        
        $rUsuario = explode(".", $p["username"]);
        $ca->prepareSelect("view_cu_proveedores_usuarios  a join cu_proveedores b on (b.codigo_proveedor=a.codigo_proveedor)", "a.*,b.estado as estado_coordiutil", "usuario=:username");
        $ca->bindValue(":username", $p["username"], true);
        $ca->exec();
        
        $r = array();
        
        if (count($rUsuario) > 1 && $ca->size() == 0) {
            $ca->prepareSelect("view_cu_proveedores_usuarios  a join cu_proveedores b on (b.codigo_proveedor=a.codigo_proveedor)", "a.*,b.estado as estado_coordiutil", "usuario=:username");
            $ca->bindValue(":username", $rUsuario[0], true);
            $ca->exec();
            
            if ($ca->size() == 0) {
                throw new JPublicException("Usuario del proveedor no encontrado");
            }
            
            $r = $ca->fetch();
            
            unset($rUsuario[0]);
            $ca->prepareSelect("cu_usuarios", "*", "usuario=:username");
            $ca->bindValue(":username", implode(".", $rUsuario), true);
            $ca->exec();
            
            if ($ca->size() == 0) {
                throw new JPublicException($errorMessage);
            }
            
            $rTmp = $ca->fetch();
            
            if ($rTmp["clave"] != $p["password"]) {
                throw new JPublicException($errorMessage);
            }
            
            if ($rTmp["estado"] != 'activo') {
                throw new JPublicException("El usuario no esta activo");
            }
        } else {
            if ($ca->size() == 0) {
                throw new JPublicException($errorMessage);
            }
            $r = $ca->fetch();
            if ($r["clave"] != $p["password"]) {
                throw new JPublicException($errorMessage);
            }
            
            if ($r["estado"] != 'activo') {
                throw new JPublicException("El usuario no esta activo");
            }
        }
        
        $r["permisos"] = explode(",", $r["permisos"]);
        
        // if( !in_array(Acl::LProductos, $r["permisos"]) ){
        // throw new JPublicException("Usted no posee permisos para realizar esta accion");
        // }
        
        $ca->prepareSelect("cu_restricciones_paquetes_proveedores_pp", "*", "codigo_proveedor_pp=:codigo_proveedor_pp and codigo_paquete in (select  codigo_paquete from  cu_proveedores where codigo_proveedor=:codigo_proveedor_pp)");
        $ca->bindValue(":codigo_proveedor_pp", $r["codigo_proveedor"], false);
        // throw new JPublicException(print_r($ca->preparedQuery(),1));
        $ca->exec();
        $rRestricciones = $ca->fetchall();
        
        $restriccionesModulos = array();
        foreach ($rRestricciones as $restricciones) {
            $restriccionesModulos[$restricciones['modulo']] = JUtils::pgsqlHStoreToArray($restricciones['flags']);
        }
        $r['restricciones_paquetes'] = $restriccionesModulos;
        
        $sql = "SELECT CASE WHEN flags_notificaciones -> 'documentos_pendientes' = '1'                                 
                        THEN 'pendientes'
                    ELSE 'aprobados'
                END AS estado_documentos
                FROM cu_documentos_contratacion_tienda               
                WHERE codigo_proveedor = :codigo_proveedor";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $r["codigo_proveedor"], false);
        $ca->exec();
        if ($ca->size() > 0) {
            $arrayEstadoDocumentos = $ca->fetch();
            $r["estado_documentos"] = $arrayEstadoDocumentos['estado_documentos'];
        } else {
            /*
             * este else es por si la tienda (codigo_proveedor) no tiene registro en cu_documentos_contratacion_tienda , o no cuenta con la sistematicacion de los
             * Documentos de contratacion que se comenzo a implementar en septiembre del 2017, y por lo tanto ya deberia de tener un contrato firmado
             * y no necesita la valiacion de los Documentos
             */
            
            $r["estado_documentos"] = "aprobados";
        }
        unset($r["clave"]);
        $_SESSION["info"] = $r;
        
        $flags = JUtils::pgsqlHStoreToArray($r["flags"]);
        $flagsPp = JUtils::pgsqlHStoreToArray($r["flags_pp"]);
        
        /*
         * $sql = "select count(*) as mensajes from cu_mail_inbox";
         * $ca->prepare($sql);
         * $ca->exec();
         * $rMensajes = $ca->fetch();
         */
        
        $ca->prepareUpdate("cu_proveedores", "fechahora_ultimo_acceso,notif", "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $r["codigo_proveedor"], false);
        $ca->bindValue(":fechahora_ultimo_acceso", "current_timestamp", false);
        $ca->bindValue(":notif", "notif||hstore('ausencia_1', '0')||hstore('ausencia_2', '0')||hstore('ausencia_3', '0')", false);
        $db->transaction();
        $ca->exec();
        $db->commit();
        
        $sql = "select pu.resources,pu.nombre_tienda from view_cu_proveedores pu
        	  where pu.codigo_proveedor=:codigo_proveedor";
        
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $r["codigo_proveedor"], false);
        $ca->exec();
        $rProveedor = $ca->fetch();
        
        $result["rs"] = modRs::load(rs::cuProveedores, $r["codigo_proveedor"], array(
            rs::cuProveedoresLogo
        ));
        
        foreach ($result["rs"] as $k => $v) {
            $result["rs"][$k]["preview"] = JDbFile::url($v["id"], array(
                "w" => 25,
                "h" => 25,
                "frm" => 1
            ));
        }
        
        $m = array();
        
        $nombreTienda = ! empty($rProveedor["nombre_tienda"]) ? $rProveedor["nombre_tienda"] : "Proveedores";
        $iconoTienda = ! empty($result["rs"]["logo"]["preview"]) ? $result["rs"]["logo"]["preview"] : "com/coordiutil/proveedores/vendesfacil/logoMenu.png";
        if ($r["estado_documentos"] == "pendientes" || (isset($flagsPp["flag_activacion_menu_admin"]) && $flagsPp["flag_activacion_menu_admin"] == "0")) {
            $m["proveedor"] = array(
                "label" => $nombreTienda,
                "icon" => $iconoTienda,
                "items" => array()
            );
            $m["proveedor"]["items"]["lcambiarclave"] = array(
                "label" => "Cambiar Clave",
                "action" => "admin_tiendas.FMain.actionCambiarClave"
            );
            
            if (Session::checkAcl("FInformacionProveedorNe")) {
                $m["proveedor"]["items"]["informacionproveedor"] = array(
                    "label" => "Información de la Tienda",
                    "action" => "admin_tiendas.FMain.actionInformacionProveedor"
                );
            }
            
            $m["proveedor"]["items"]["exit"] = array(
                "label" => "Salir",
                "action" => "admin_tiendas.FMain.actionFileExit"
            );
        } else {
            // Proveedor
            $m["proveedor"] = array(
                "label" => $nombreTienda,
                "icon" => $iconoTienda,
                "items" => array()
            );
            $m["proveedor"]["items"]["lcambiarclave"] = array(
                "label" => "Cambiar Clave",
                "action" => "admin_tiendas.FMain.actionCambiarClave"
            );
            if (Session::checkAcl("FInformacionProveedorNe")) {
                $m["proveedor"]["items"]["informacionproveedor"] = array(
                    "label" => "Información del Proveedor",
                    "action" => "admin_tiendas.FMain.actionInformacionProveedor"
                );
            }
            if (isset($flagsPp["flag_facturacion_tienda"]) && $flagsPp["flag_facturacion_tienda"] && Session::checkAcl("LResoluciones")) {
                $m["proveedor"]["items"]["lresoluciones"] = array(
                    "label" => "Resoluciones Facturación",
                    "action" => "admin_tiendas.FMain.actionResoluciones"
                );
            }
            if (Session::checkAcl("LUsuarios")) {
                $m["proveedor"]["items"]["lusuarios"] = array(
                    "label" => "Usuarios",
                    "action" => "admin_tiendas.FMain.actionUsuarios"
                );
            }
            
            if (Session::checkAcl("LPuntosDespacho")) {
                $m["proveedor"]["items"]["lpuntosdedespacho"] = array(
                    "label" => "Puntos de Despacho",
                    "action" => "admin_tiendas.FMain.actionPuntosDeDespacho"
                );
            }
            if (Session::checkAcl("LGruposCiudades") && ($r["flag_sintransporte"] == "1" || $r["flag_grupos_ciudades"] == "1")) {
                $m["proveedor"]["items"]["lgruposciudades"] = array(
                    "label" => "Grupos de Ciudades",
                    "action" => "admin_tiendas.FMain.actionGruposCiudades"
                );
            }
            if ($r["flag_tienda"] == "1") {
                $m["proveedor"]["items"]["irtienda"] = array(
                    "label" => "Ir a tienda",
                    "action" => "admin_tiendas.FMain.actionIrTienda"
                );
            }
            
            if ((Session::checkAcl("LEstadisticas")) && ($restriccionesModulos['Proveedores']['EstadoEstadisticas'] == "activo")) {
                $m["proveedor"]["items"]["lestadisticas"] = array(
                    "label" => "Visitas y ventas de productos",
                    "action" => "admin_tiendas.FMain.actionLEstadisticas"
                );
            }
            
            $m["proveedor"]["items"]["exit"] = array(
                "label" => "Salir",
                "action" => "admin_tiendas.FMain.actionFileExit"
            );
            
            // REPORTES
            $m["reportes"] = array(
                "label" => "Reportes",
                "items" => array()
            );
            if (Session::checkAcl("FDashboard")) {
                $m["reportes"]["items"]["fdashboard"] = array(
                    "label" => "Dashboard",
                    "action" => "admin_tiendas.FMain.actionDashboard"
                );
            }
            
            // CATALOGO
            $m["catalogo"] = array(
                "label" => "Catálogo",
                "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoCatalogo.png",
                "items" => array()
            );
            if (Session::checkAcl("LProductosInventario")) {
                if ($flagsPp["gestion_inventario_por_bodega"] == "1") {
                    $m["catalogo"]["items"]["lproductosinventarioporbodega"] = array(
                        "label" => "Inventario de productos por Bodega",
                        "action" => "admin_tiendas.FMain.actionLProductosInventarioPorBodega"
                    );
                } else {
                    $m["catalogo"]["items"]["lproductosinventario"] = array(
                        "label" => "Inventario de productos",
                        "action" => "admin_tiendas.FMain.actionLProductosInventario"
                    );
                }
            }
            if (Session::checkAcl("LPromociones")) {
                $m["catalogo"]["items"]["lpromociones"] = array(
                    "label" => "Promociones",
                    "action" => "admin_tiendas.FMain.actionLPromociones"
                );
            }
            if (Session::checkAcl("LProductos")) {
                $m["catalogo"]["items"]["lproductos"] = array(
                    "label" => "Productos",
                    "action" => "admin_tiendas.FMain.actionLProductos"
                );
            }
            if (Session::checkAcl("FCategoriasnppNe") && $r["flag_tienda"] == "1") {
                $m["catalogo"]["items"]["lcategoriasnpp"] = array(
                    "label" => "Categorías",
                    "action" => "admin_tiendas.FMain.actionFCategoriasnpp"
                );
            }
            if (Session::checkAcl("LMarcas") && $r["flag_tienda"] == "1") {
                $m["catalogo"]["items"]["LMarcas"] = array(
                    "label" => "Marcas",
                    "action" => "admin_tiendas.FMain.actionMarcas"
                );
            }
            if (Session::checkAcl("LCategoriasVirtuales") && $r["flag_tienda"] == "1") {
                $m["catalogo"]["items"]["lcategoriasvirtuales"] = array(
                    "label" => "Categorías virtuales",
                    "action" => "admin_tiendas.FMain.actionCategoriasVirtuales"
                );
            }
            /*
             * if (Session::checkAcl("LGarantiasEspecificas")) {
             * $m["catalogo"]["items"]["lgarantiasespecificas"] = array("label" => "Garantías específicas", "action" => "admin_tiendas.FMain.actionLGarantiasEspecificas");
             * }
             */
            if (Session::checkAcl("LCanastas")) {
                $m["catalogo"]["items"]["lcanastas"] = array(
                    "label" => "Canastas",
                    "action" => "admin_tiendas.FMain.actionCatalogoCanastas"
                );
            }
            if (Session::checkAcl("LListasDeseos")) {
                $m["catalogo"]["items"]["llistasdeseos"] = array(
                    "label" => "Listas deseos",
                    "action" => "admin_tiendas.FMain.actionListasDeseos"
                );
            }
            
            // Banners
            if ($r["flag_tienda"] == "1") {
                $m["banners"] = array(
                    "label" => "Banners",
                    "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoBanner.png",
                    "items" => array()
                );
                if (Session::checkAcl("LUbicaciones")) {
                    $m["banners"]["items"]["lubicaciones"] = array(
                        "label" => "Ubicaciones",
                        "action" => "admin_tiendas.FMain.actionLUbicaciones"
                    );
                }
                if (Session::checkAcl("LCampanas")) {
                    $m["banners"]["items"]["lcampanas"] = array(
                        "label" => "Campañas",
                        "action" => "admin_tiendas.FMain.actionLCampanas"
                    );
                }
                if (Session::checkAcl("LBanners")) {
                    $m["banners"]["items"]["lbanners"] = array(
                        "label" => "Banners",
                        "action" => "admin_tiendas.FMain.actionLBanners"
                    );
                }
            }
            
            // DEspachos
            $m["despachos"] = array(
                "label" => "Despachos",
                "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoDespachos.png",
                "items" => array()
            );
            if (Session::checkAcl("LPedidosPorDespachar")) {
                $m["despachos"]["items"]["lpedidospordespachar"] = array(
                    "label" => "Pedidos por despachar",
                    "action" => "admin_tiendas.FMain.actionPedidosPorDespachar"
                );
            }
            if (isset($flags["flag_proveedor_internacional"]) && $flags["flag_proveedor_internacional"] == "1" && isset($flags["flag_tienda_externa"]) && $flags["flag_tienda_externa"] == "1") {
                $m["despachos"]["items"]["lcomprasimportados"] = array(
                    "label" => "Compras importados",
                    "action" => "admin_tiendas.FMain.actionComprasImportados"
                );
            }
            if (Session::checkAcl("LHistoricoPedidos")) {
                $m["despachos"]["items"]["lhistoricopedidos"] = array(
                    "label" => "Histórico de despachos",
                    "action" => "admin_tiendas.FMain.actionHistoricoPedidos"
                );
            }
            if (Session::checkAcl("LTransportePedidos") && ($r["flag_sintransporte"] == "1" || $flags["flag_despacho_manual"] == "1")) {
                $m["despachos"]["items"]["ltransportepedidos"] = array(
                    "label" => "Despachos manuales",
                    "action" => "admin_tiendas.FMain.actionTransportePedidos"
                );
            }
            if (Session::checkAcl("LPedidosPorDespacharMasivo")) {
                $m["despachos"]["items"]["lpedidospordespacharmasivo"] = array(
                    "label" => "Pedidos por despachar desde archivo",
                    "action" => "admin_tiendas.FMain.actionPedidosPorDespacharMasivo"
                );
            }
            
            // Importaciones
            if ($restriccionesModulos['Importaciones']['EstadoModulo'] == "activo") {
                $m["importaciones"] = array(
                    "label" => "Importaciones",
                    "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoImportaciones.png",
                    "items" => array()
                );
                if (Session::checkAcl("FImportarInventarios")) {
                    $m["importaciones"]["items"]["fimportarinventarios"] = array(
                        "label" => "Importar inventarios",
                        "action" => "admin_tiendas.FMain.actionImportarInventarios"
                    );
                }
                if ($flags["flag_aliado"] == "1" || $r['flag_tienda'] == 0) {
                    if (Session::checkAcl("FImportarProductos")) {
                        $m["importaciones"]["items"]["fimportarproductos"] = array(
                            "label" => "Importar productos",
                            "action" => "admin_tiendas.FMain.actionImportarProductos"
                        );
                    }
                }
                if ($r["flag_tienda"] == "1" && $restriccionesModulos['Proveedores']['EstadoEstadisticas'] == "activo") {
                    if (Session::checkAcl("FImportarProductostienda")) {
                        $m["importaciones"]["items"]["fimportarproductostienda"] = array(
                            "label" => "Importar productos tienda",
                            "action" => "admin_tiendas.FMain.actionImportarProductostienda"
                        );
                    }
                    if (Session::checkAcl("FImportarPedidosPCE")) {
                        $m["importaciones"]["items"]["fimportarpedidopce"] = array(
                            "label" => "Importar pedidos PCE",
                            "action" => "admin_tiendas.FMain.actionImportarPedidoPce"
                        );
                    }
                }
                if (Session::checkAcl("FImportarPromociones")) {
                    $m["importaciones"]["items"]["fimportarpromociones"] = array(
                        "label" => "Importar precios ",
                        "action" => "admin_tiendas.FMain.actionImportarPromociones"
                    );
                }
                if ($r["flag_tienda"] == "1") {
                    if ($flags["flag_aliado"] == "1") {
                        if (Session::checkAcl("FImportarPedidos")) {
                            $m["importaciones"]["items"]["fimportarPedidos"] = array(
                                "label" => "Importar pedidos",
                                "action" => "admin_tiendas.FMain.actionImportarPedidos"
                            );
                        }
                        
                        if (Session::checkAcl("FImportarInventarioAliado")) {
                            $m["importaciones"]["items"]["fimportarIventarioAliado"] = array(
                                "label" => "Importar inventario Aliado",
                                "action" => "admin_tiendas.FMain.actionImportarInventarioAliado"
                            );
                        }
                    }
                    
                    if (Session::checkAcl("FImportarPedidosPorDespachar")) {
                        $m["importaciones"]["items"]["fimportarpedidospordespachar"] = array(
                            "label" => "Importar pedidos por despachar ",
                            "action" => "admin_tiendas.FMain.actionImportarPedidosPorDespachar"
                        );
                    }
                    
                    if (Session::checkAcl("FImportarContenidos")) {
                        $m["importaciones"]["items"]["fimportarcontenidos"] = array(
                            "label" => "Importar contenidos ",
                            "action" => "admin_tiendas.FMain.actionImportarContenidos"
                        );
                    }
                    
                    if (Session::checkAcl("FImportarPedidosYDespachos")) {
                        $m["importaciones"]["items"]["fimportarpedidosydespachos"] = array(
                            "label" => "Importar pedidos y despachos",
                            "action" => "admin_tiendas.FMain.actionImportarPedidosYDespachos"
                        );
                    }
                }
            }
            
            // Informes
            $m["informes"] = array(
                "label" => "Informes",
                "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoInformes.png",
                "items" => array()
            );
            if ((Session::checkAcl("LAnexosFactura")) && ($r["estado_coordiutil"] == "activo")) {
                $m["informes"]["items"]["lanexosfacturas"] = array(
                    "label" => "Anexos factura",
                    "action" => "admin_tiendas.FMain.actionAnexosFacturas"
                );
            }
            if (Session::checkAcl("LAnexosFacturaTienda") && $r["flag_tienda"] == "1") {
                $m["informes"]["items"]["lanexosfacturastienda"] = array(
                    "label" => "Anexos factura Tienda Personalizada",
                    "action" => "admin_tiendas.FMain.actionAnexosFacturasTienda"
                );
            }
            if (Session::checkAcl("LTransacciones") && $r["flag_tienda"] == "1") {
                $m["informes"]["items"]["ltransacciones"] = array(
                    "label" => "Transacciones",
                    "action" => "admin_tiendas.FMain.actionTransacciones"
                );
            }
            /*
             * $m["mensajes"] = array("label" => "Mensajes", "items" => array());
             * if (Session::checkAcl("LEnviarMensajes")) {
             * $m["mensajes"]["items"]["lenviarmensajes"] = array("label" => "Bandeja de entrada", "action" => "admin_tiendas.FMain.actionEnviarMensajes");
             * }
             */
            if ($r["flag_tienda"] == "1") {
                // $m["portalpersonalizado"] = array("label" => "Portal personalizado", "items" => array());
                if (Session::checkAcl("ExportarClientes") || Session::checkAcl("LExportacion")) {
                    $m["informes"]["items"]["exportarclientes"] = array(
                        "label" => "Exportar clientes",
                        "action" => "admin_tiendas.FMain.actionExportarClientes"
                    );
                }
                if (Session::checkAcl("ExportarUsuarios") || Session::checkAcl("LExportacion")) {
                    $m["informes"]["items"]["exportarusuarios"] = array(
                        "label" => "Exportar usuarios registrados",
                        "action" => "admin_tiendas.FMain.actionExportarUsuariosRegistrados"
                    );
                }
                if (Session::checkAcl("LListasDeseos")) {
                    $m["informes"]["items"]["llistasdeseos"] = array(
                        "label" => "Listas deseos",
                        "action" => "admin_tiendas.FMain.actionListasDeseos"
                    );
                }
            }
            
            // Aliados
            if ($flags["flag_aliado"] == "1") {
                $m["aliados"] = array(
                    "label" => "Aliados",
                    "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoAliados.png",
                    "items" => array()
                );
                /*
                 * if (Session::checkAcl("FProductosIncluidos")) {
                 * $m["aliados"]["items"]["fproductosincluidos"] = array("label" => "Productos Incluidos", "action" => "admin_tiendas.FMain.actionAdministrarProductosIncluidos");
                 * }
                 */
                if (Session::checkAcl("LProductosIncluidos")) {
                    $m["aliados"]["items"]["lproductosincluidos"] = array(
                        "label" => "Productos Incluidos",
                        "action" => "admin_tiendas.FMain.actionProductosIncluidos"
                    );
                }
                if (Session::checkAcl("LPromocionesAliados")) {
                    $m["aliados"]["items"]["pPInclidos"] = array(
                        "label" => "Promocion productos incluidos",
                        "action" => "admin_tiendas.FMain.actionLPromocionesAliados"
                    );
                }
                if (Session::checkAcl("LAnexosFacturaAliados")) {
                    $m["aliados"]["items"]["lanexosfacturasaliados"] = array(
                        "label" => "Anexos factura",
                        "action" => "admin_tiendas.FMain.actionAnexosFacturasAliados"
                    );
                }
                if (Session::checkAcl("LAnexosFacturaAliados")) {
                    $m["aliados"]["items"]["lpinesanexosfacturasaliados"] = array(
                        "label" => "Pines anexos factura",
                        "action" => "admin_tiendas.FMain.actionPinesAnexosFacturasAliados"
                    );
                }
                if (Session::checkAcl("LMargenesProductosAliados")) {
                    $m["aliados"]["items"]["lmargenesproductosaliados"] = array(
                        "label" => "Margenes productos incluidos",
                        "action" => "admin_tiendas.FMain.actionMargenesProductosIncluidos"
                    );
                }
                if (Session::checkAcl("LExportarInventario")) {
                    $m["aliados"]["items"]["lexportarinventario"] = array(
                        "label" => "Exportar inventarios",
                        "action" => "admin_tiendas.FMain.actionExportarInventarios"
                    );
                }
                if (Session::checkAcl("LExportarPrecios")) {
                    $m["aliados"]["items"]["lexportarprecios"] = array(
                        "label" => "Exportar precios",
                        "action" => "admin_tiendas.FMain.actionExportarPrecios"
                    );
                }
            }
            
            // PQR
            if ($r["flag_tienda"] == "1" && $restriccionesModulos['Reclamaciones']['EstadoModulo'] == "activo") {
                $m["pqr"] = array(
                    "label" => "Servicio al cliente",
                    "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoServicioCliente.png",
                    "items" => array()
                );
                if (Session::checkAcl("LConsultaPQR")) {
                    $m["pqr"]["items"]["lreclamaciones"] = array(
                        "label" => "Reclamaciones",
                        "action" => "admin_tiendas.FMain.actionReclamaciones"
                    );
                }
                
                // Devoluciones
                if (Session::checkAcl("LConsultaNotasCredito")) {
                    $m["pqr"]["items"]["lconsultanotascredito"] = array(
                        "label" => "Notas crédito",
                        "action" => "admin_tiendas.FMain.actionNotasCredito"
                    );
                }
            }
            
            // Comentarios
            if ($r["flag_tienda"] == "1" && $restriccionesModulos['Opiniones']['EstadoModulo'] == "activo") {
                $flagsPp["flag_comentarios"] = isset($flagsPp["flag_comentarios"]) ? $flagsPp["flag_comentarios"] : "0";
                if ($flagsPp["flag_comentarios"] == "1") {
                    $m["comentarios"] = array(
                        "label" => "Comentarios",
                        "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoComentarios.png",
                        "items" => array()
                    );
                    
                    if (Session::checkAcl("LOpinionesCompra")) {
                        $m["comentarios"]["items"]["lComentarios"] = array(
                            "label" => "Comentarios compra",
                            "action" => "admin_tiendas.FMain.actionComentariosCompras"
                        );
                    }
                    if (Session::checkAcl("LOpinionesProductos")) {
                        $m["comentarios"]["items"]["lOpinionesProductos"] = array(
                            "label" => "Comentarios productos",
                            "action" => "admin_tiendas.FMain.actionComentariosProductos"
                        );
                    }
                }
                if (Session::checkAcl("LConsultaPlantillaPQR")) {
                    $m["pqr"]["items"]["lplantillascorreospqr"] = array(
                        "label" => "Plantillas de correos",
                        "action" => "admin_tiendas.FMain.actionPlantillasCorreos"
                    );
                }
            }
            
            // Bonos
            if ($r["flag_tienda"] == "1") {
                if ($flagsPp["flag_recibe_bonos"] == '1') {
                    $m["bonos"] = array(
                        "label" => "Bonos",
                        "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoBono.png",
                        "items" => array()
                    );
                    if (Session::checkAcl("LBonos")) {
                        $m["bonos"]["items"]["lconsultabonos"] = array(
                            "label" => "Bonos",
                            "action" => "admin_tiendas.FMain.actionBonos"
                        );
                    }
                    if (Session::checkAcl("LAcuerdosCreacion")) {
                        $m["bonos"]["items"]["lconsultaacuerdoscreacion"] = array(
                            "label" => "Acuerdos Creación",
                            "action" => "admin_tiendas.FMain.actionAcuerdosCreacion"
                        );
                    }
                    if (Session::checkAcl("LAcuerdosRedencion")) {
                        $m["bonos"]["items"]["lconsultaacuerdosredencion"] = array(
                            "label" => "Acuerdos Redención",
                            "action" => "admin_tiendas.FMain.actionAcuerdosRedencion"
                        );
                    }
                }
            }
            
            /* Marketing */
            if ($flagsPp['email_marketing'] == 1 || $flagsPp['sms_marketing'] == 1 || $flagsPp['carritos_abandonados_marketing'] == 1) {
                $m["marketing"] = array(
                    "label" => "Marketing",
                    "icon" => "resource/com/coordiutil/proveedores/vendesfacil/IconoMerkanting.png",
                    "items" => array()
                );
                if (Session::checkAcl("LMarketing")) {
                    $m["marketing"]["items"]["LMarketing"] = array(
                        "label" => "Listado Campañas",
                        "action" => "admin_tiendas.FMain.actionCampañas"
                    );
                }
                if (Session::checkAcl("LSegmentos")) {
                    $m["marketing"]["items"]["LSegmentos"] = array(
                        "label" => "Segmentos",
                        "action" => "admin_tiendas.FMain.actionListarSegmentos"
                    );
                }
            }
            
            // CONFIGURACIONES
            $m["configuraciones"] = array(
                "label" => "Configuración general",
                "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoConfiguracion.png",
                "items" => array()
            );
            if (Session::checkAcl("FPreferenciasNe") && $r["flag_tienda"] == "1") {
                $m["configuraciones"]["items"]["preferences"] = array(
                    "label" => "Preferencias portal personalizado",
                    "action" => "admin_tiendas.FMain.actionPreferencias"
                );
            }
            if (Session::checkAcl("LPopups") && $r["flag_tienda"] == "1" && $restriccionesModulos['Popups']['EstadoModulo'] == "activo") {
                $m["configuraciones"]["items"]["popup"] = array(
                    "label" => "Popups",
                    "action" => "admin_tiendas.FMain.actionPopups"
                );
            }
            if (Session::checkAcl("FPlantillasCss") && $r["flag_tienda"] == "1") {
                $m["configuraciones"]["items"]["plantillas_css"] = array(
                    "label" => "Plantillas CSS",
                    "action" => "admin_tiendas.FMain.actionPlantillasCss"
                );
            }
            if (Session::checkAcl("LConsultaPlantillaPQR")) {
                $m["configuraciones"]["items"]["lplantillascorreospqr"] = array(
                    "label" => "Plantillas de correos",
                    "action" => "admin_tiendas.FMain.actionPlantillasCorreos"
                );
            }
            if (Session::checkAcl("FContenido") && $restriccionesModulos['Contenidos']['EstadoModulo'] == "activo") {
                $m["configuraciones"]["items"]["FContenido"] = array(
                    "label" => "Contenidos",
                    "action" => "admin_tiendas.FMain.actionContenidos"
                );
            }
            
            if (Session::checkAcl("LVendedores") && $r["flag_tienda"] == "1" && isset($flagsPp["usa_vendedores"]) && $flagsPp["usa_vendedores"] == '1') {
                $m["configuraciones"]["items"]["lvendedores"] = array(
                    "label" => "Vendedores",
                    "action" => "admin_tiendas.FMain.actionVendedores"
                );
            }
            
            // Ayudas
            if ($r["flag_tienda"] == "1") {
                $m["ayudas"] = array(
                    "label" => "Ayudas",
                    "icon" => "resource/com/coordiutil/proveedores/vendesfacil/iconoAyuda.png",
                    "items" => array()
                );
                if (Session::checkAcl("Layudas")) {
                    $m["ayudas"]["items"]["Tutoriales"] = array(
                        "label" => "Vendesfacil",
                        "action" => "admin_tiendas.FMain.actionTutoriales"
                    );
                    $m["ayudas"]["items"]["Novedades"] = array(
                        "label" => "Novedades",
                        "action" => "admin_tiendas.FMain.actionNovedades"
                    );
                    // $m["ayudas"]["items"]["Chat"] = array("label" => "Iniciar el Chat", "action" => "admin_tiendas.FMain.actionChat");
                }
            }
        }
        
        $_SESSION["info"]["menu"] = $m;
            
        return array(
            "menu" => $m,
            "nueva_clave" => $r["nueva_clave"],
            "logo" => $rProveedor
        );
        
    }

    public static function info()
    {
        global $cfg;
        
        if (isset($_SESSION["info"])) {
            $_SESSION["info"]["env"] = $cfg["env"];
            return $_SESSION["info"];
        } else {
            throw new JPublicException("Sesión invalida");
        }
    }

    public static function LRs()
    {
        global $cfg;
        
        $result = array();
        $result["env"] = $cfg["env"];
        return $result;
    }

    public static function signinToken($token)
    {
        global $cfg;
        $arToken = unserialize(base64_decode($token));
        if (empty($arToken["usuario"]) || empty($arToken["clave"]) || empty($arToken["codigo_proveedor"])) {
            throw new Exception("El token no esta bien formado");
        }
        
        if (! isset($_SESSION["info"])) {
            Session::signin(array(
                "username" => $arToken["usuario"],
                "password" => $arToken["clave"]
            ));
        }
        
        return Session::info();
    }

    public static function obtenerTokenSesion($token)
    {
        global $cfg;
        $si = Session::info();
        
        $arToken = array(
            "codigo_proveedor" => $si["codigo_proveedor"],
            "username" => $si["usuario"],
            "clave" => $si["clave"],
            "fecha" => date("Y-m-d")
        );
        
        return base64_encode($arToken);
    }
    
    public static function cerrarSesion($token)
    {
        unset($_SESSION["info"]);
    }
}