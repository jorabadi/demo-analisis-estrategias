<?php
require_once $cfg["rutaModeloLogico"] . "mod_productos.inc.php";
JLib::requireOnceModule("utils/jutils.inc.php");

class Productos{

    public static function loadPage($p){
        $si = Session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        $result = modProductos::loadPage($p);

        for ($i = 0; $i < count($result["records"]); $i++) {
            $tmp = modRs::load(rs::cuProductos, $result["records"][$i]["codigo_producto"], array(rs::cuProductosImagen1));
            if (isset($tmp["imagen_1"])) {
                $result["records"][$i]["imagen_1"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }

        return $result;
    }

    public static function loadEditRs(){
        $si = Session::info();
        $result = array();

        $result["sesion"] = $si;
        $result["sesion"]["flags_pp"] = JUtils::pgsqlHStoreToArray($si['flags_pp']);

        $result["estados"] = array(
            array("label" => "Activo", "data" => "activo"),
            array("label" => "Inactivo", "data" => "inactivo")
        );

        $result["estados_revision"] = array(
            array("label" => "Pendiente", "data" => "pendiente"),
            array("label" => "Aceptado", "data" => "aceptado"),
            array("label" => "Rechazado", "data" => "rechazado")
        );

        //Tipos
        $result["flag_compuesto"] = array(
            array("label" => "No", "data" => "0"),
            array("label" => "Si", "data" => "1")
        );

        //Etiquetas
        $result["tags"] = array(
            array("label" => "Sin etiqueta", "data" => ""),
            array("label" => "Autoasignada", "data" => "auto"),
            array("label" => "Oferta", "data" => "oferta"),
            array("label" => "Nuevo", "data" => "nuevo")
        );

        //Venta
        $result["flag_venta"] = array(
            array("label" => "No", "data" => "0"),
            array("label" => "Si", "data" => "1")
        );

        $result["monedas"] = array(
            array("label" => "COP", "data" => "cop"),
            array("label" => "USD", "data" => "usd")
        );

        $result["por_iva"] = array(
            array("label" => "Seleccione iva", "data" => ""),
            array("label" => "0", "data" => "0.00"),
            array("label" => "5", "data" => "5.00"),
            array("label" => "19", "data" => "19.00")
        );

        $result["unidades_volumen"] = array(
            array("label" => "Centimetros", "data" => "cm") //,
                //array("label"=>"Metros","data"=>"m")
        );

        $result["unidades_peso"] = array(
            array("label" => "Gramos", "data" => "g"),
            array("label" => "Kilogramos", "data" => "kg")
        );

        $result["visibilidad"] = array(
            array("label" => "Seleccione visibilidad", "data" => ""),
            array("label" => "Coordiutil", "data" => "vis_cu"),
            array("label" => "Portal personalizado", "data" => "vis_ti"),
            array("label" => "Coordiutil y Portal personalizado", "data" => "vis_cu,vis_ti")
        );

        if ($si["flag_tienda"] == "0") {
            $result["visibilidad"] = array(
                array("label" => "Coordiutil", "data" => "vis_cu")
            );
        }

        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);
        if (!empty($flags["mixto"]) && $flags["mixto"] == "1") {
            $result["visibilidad"][] = array("label" => "Coordiutil enlace Portal personalizado", "data" => "vis_link");
        }

        if (isset($flags["flag_proveedor_internacional"]) && $flags["flag_proveedor_internacional"] == "1" && isset($flags["flag_tienda_externa"]) && $flags["flag_tienda_externa"] == "0") {
            $result["proveedor_internacional"] = "1";
        } else {
            $result["proveedor_internacional"] = "0";
        }

        $tmp = array();
        for ($i = 1; $i <= 10; $i++) {
            $tmp[] = array("label" => "{$i}", "data" => "{$i}");
        }
        $result["unidades_empaque"] = $tmp;

        //Dias alistamiento maximo
        $tmp = array();
        for ($i = 1; $i <= $si["alistamiento_maximo"]; $i++) {
            $tmp[] = array("label" => "{$i}", "data" => "{$i}");
        }
        $result["dias_alistamiento"] = $tmp;

        $result["importacion"] = array(
            array("label" => "Si", "data" => "1"),
            array("label" => "No", "data" => "0")
        );

        $result["mayoria_edad"] = array(
            array("label" => "Si", "data" => "1"),
            array("label" => "No", "data" => "0")
        );

        //Defaults
        $result["defaults"] = array(
            "estado" => "activo",
            "estado_revision" => "pendiente",
            "marca" => "",
            "flag_compuesto" => "0",
            "flag_venta" => "1",
            "moneda" => "cop",
            "dia_alistamiento" => "1",
            "unidad_volumen" => "cm",
            "unidad_peso" => "",
            "unidad_empaque" => "1",
            "importacion" => "0",
            "mayoria_edad" => "0",
            "codigo_grupo_destinos" => "",
            "visibilidad" => "",
            "tag" => "auto",
            "por_iva" => "",
            "tipos_productos" => "1"
        );

        $r = modProductos::loadEditRs($si["codigo_proveedor"]);
        $result["codigo_grupos_destino"] = $r["codigo_grupos_destino"];
        $result["tipos_productos"] = $r["tipos_productos"];
        $result["estado_proveedor_coordiutil"] = $r["estado_proveedor_coordiutil"];

        return $result;
    }

    public static function retornaSesion(){
        $result = array("sesion" => Session::info());
        return $result;
    }

    public static function loadOne($p){
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        $p["flag_tienda"] = $si["flag_tienda"];
        $p["url_dominio"] = $si["url_dominio"];
        $result = modProductos::loadOne($p);
        return $result;
    }

    public static function loadPromocionesProductos($p){
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        $sql = "select
				cast(a.fechahora_inicial as date)||' '||cast(a.fechahora_inicial as time) as fechahora_inicial,
			 	cast(a.fechahora_final as date)||' '|| cast(a.fechahora_final as time)  as fechahora_final,
				d.precio_compra as precio_compra,
			 	round(d.precio_compra::numeric(12,2) * ((100 + c.por_iva::numeric(12,2))/100),2) as precio_compra_con_iva,
				d.precio_coniva as precio_coniva,
				d.descuento_proveedor as descuento_proveedor,
				d.precio_coniva as precio_coniva_sinpromocion,
				round(d.precio_coniva::numeric(12,2) / ((100 + c.por_iva::numeric(12,2)) / 100),2) as precio_siniva
         from cu_productos_precios a
			 join cu_estados_revision b on (a.codigo_estado_revision=b.codigo_estado)
			 join view_cu_productos_base c on (a.codigo_producto=c.codigo_producto)
			 join cu_productos_precios d on (d.codigo_producto=a.codigo_producto and d.codigo_tipo=(case when a.codigo_tipo=2 then 1 when a.codigo_tipo=4 then 3 end))
             where a.codigo_producto =:codigo_producto
			 and a.codigo_tipo=:codigo_tipo
			 and (a.fechahora_inicial::date,a.fechahora_final) overlaps( date(current_date),date(current_date))";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_producto", $p["codigo_producto"], false);
        $ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
        $ca->exec();

        if ($ca->size() > 0) {
            return $result = $ca->fetch();
        }

        return null;
    }

    public static function save($p){
        $si = Session::info();
        $db = JDatabase::database();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        $p["codigo_marca"] = isset($p["codigo_marca"][0]) ? $p["codigo_marca"][0] : '';

        if (!in_array("FProductosNe_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("No tienes permisos para realizar esta acción");
        }

        if (empty($p["detalle"])) {
            $p["detalle"] = array(array("codigo_ean" => "", "codigo_inventario" => "", 'color' => "-", 'inventario' => 0, 'refinventario' => "", 'talla' => "-"));
        }

//        $p["codigo_categorian"] = isset($p["codigo_categorian"][0]) ? $p["codigo_categorian"][0] : null;
        $p["codigo_categorian"] = isset($p["categorias_habilitadas"][0]) ? $p["categorias_habilitadas"][0] : null;
        $p["codigo_categoria_pp"] = isset($p["codigo_categoria_pp"][0]) ? $p["codigo_categoria_pp"][0] : null;
        $p["codigo_garantia"] = isset($p["codigo_garantia"][0]) ? $p["codigo_garantia"][0] : 0;
        $p["codigo_plantilla"] = isset($p["codigo_plantilla"][0]) ? $p["codigo_plantilla"][0] : 0;
        $p["codigo_estado_revision"] = isset($p["codigo_estado_revision"][0]) ? $p["codigo_estado_revision"][0] : 0;
        $p["codigo_grupo_destinos"] = isset($p["codigo_grupo_destinos"]) ? $p["codigo_grupo_destinos"] : 0;
        $p["origen"] = "proveedor";
        $p["id_gravamen"] = isset($p["id_gravamen"][0]) ? $p["id_gravamen"][0] : -1;
        $p["estado_revision"] = "aceptado";

        if ($p["precio_por_variaciones"] == "0") {
            if (isset($p["precio_sin_iva_coordiutil"])) {
                $p["precio_compra"] = $p["precio_sin_iva_coordiutil"];
                $p["precio_coniva"] = $p["precio_venta_publico"];
            }
            if (isset($p["precio_venta_tienda"])) {
                $p["precio_compra_tienda"] = $p["precio_venta_tienda"];
                $p["precio_coniva_tienda"] = $p["precio_venta_tienda"];
                $p["descuento_proveedor_tienda"] = 0;
                $p["valor_transporte_tienda"] = $p["valor_transporte"];
                $p["estado_revision"] = "aceptado";
            }
        } else {
            foreach ($p["detalle"] as $k => $v) {
                $p["detalle"][$k]["precio_compra_tienda"] = $p["detalle"][$k]["precio_coniva_tienda"];
                $p["detalle"][$k]["descuento_proveedor_tienda"] = 0;
                $p["detalle"][$k]["valor_transporte_tienda"] = $p["detalle"][$k]["valor_transporte"];
            }
        }
        $p["last_user"] = $si["usuario"];

        $db->transaction();
        try {
            $result = modProductos::save($p);
            

            if ($p["reglas"]) {
                $parametrosGuardarReglas["codigo_producto"] = $result["codigo_producto"];
                $parametrosGuardarReglas["codigo_proveedor"] = $si["codigo_proveedor"];
                $parametrosGuardarReglas["reglas"] = $p["reglas"];

                ProductosRelacionados::GuardarReglas($parametrosGuardarReglas);
            }
        } catch (Exception $e) {
            $db->rollback();
            throw new JPublicException($e->getMessage());
        }
        $db->commit();
        return;
    }

  /*  public static function saveProductosRelacionados($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        $aProductosRelacionados = array();
        foreach ($p["productos_relacionados"] as $r) {
            $aProductosRelacionados[] = $r["data"];
        }
        $productos_relacionados = implode(",", $aProductosRelacionados);

        $db->transaction();

        $ca->prepareUpdate("cu_productos", "productos_relacionados", "codigo_producto=:codigo_producto");
        $ca->bindValue(":codigo_producto", $p["codigo_producto"], false);
        $ca->bindValue(":productos_relacionados", "{" . $productos_relacionados . "}", true);

        if (!$ca->exec()) {
            $db->rollback();
            throw new JPublicException("Error, fallo registrando productos relacionados");
        }

        $db->commit();

        return;
    }

    public static function savePalabrasClaveProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $aPalabrasClaves = array();
        foreach ($p["palabras_clave"] as $r) {
            $aPalabrasClaves[] = $r["palabra_clave"];
        }

        $palabrasClaves = !empty($aPalabrasClaves) ? implode(",", $aPalabrasClaves) : "";

        $db->transaction();

        $ca->prepareUpdate("cu_productos", "palabras_clave", "codigo_producto=:codigo_producto");
        $ca->bindValue(":codigo_producto", $p["codigo_producto"], false);
        $ca->bindValue(":palabras_clave", "{" . $palabrasClaves . "}", true);

        if (!$ca->exec()) {
            $db->rollback();
            throw new JPublicException("Error, fallo registrando productos relacionados");
        }

        $db->commit();
    }

    public static function saveAtributosProducto($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareDelete("cu_productos_tipos_attrs_valores", "codigo_producto=:codigo_producto");
        $ca->bindValue(":codigo_producto", $p["codigo_producto"], false);
        $ca->exec();

        if (isset($p["atributos"]) && !empty($p["atributos"])) {
            foreach ($p["atributos"] as $atributo) {

                if (empty($atributo["valor"])) {
                    throw new JPublicException("Debe ingresar el valor");
                }

                $ca->prepareInsert("cu_productos_tipos_attrs_valores", "codigo_producto,codigo_tipo_atributo,valor");
                $ca->bindValue(":codigo_producto", $p["codigo_producto"], false);
                $ca->bindValue(":codigo_tipo_atributo", $atributo["codigo_tipo_atributo"], false);
                $ca->bindValue(":valor", $atributo["valor"], true);
                $ca->exec();
            }
        }

        $ca->prepare("select func_producto_actualizar_atributos({$p["codigo_producto"]})");
        $ca->exec();
    }*/

    public static function loadPageInventarios($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "nombre_marca,referencia,nombre,talla,color";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if ($p["filters"]["estado"] != "todos") {
            $where .= " and estado_inventario='{$p["filters"]["estado"]}' ";
        }
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $sql = "select codigo_inventario,
                    referencia,
                    nombre,
                    nombre_marca,
                    talla,
                    color,
                    inventario as inventario_disponible,
                    inventario_reservado,
                    0 as ajuste,
                    codigo_proveedor
                from view_cu_productos_inventario_base 
                where codigo_proveedor = :codigo_proveedor
                    --and estado_base = 'activo'
                    and ({$where})
                order by nombre
        ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function ajustarInventario($p){
        $si = session::info();

        if ($si["codigo_proveedor"] == 248) {
            throw new JPublicException("El inventario de Coordiutil se ajusta por el administrador");
        }

        if (!in_array("LProductosInventario_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }

        $db = JDatabase::database();
        $db->transaction();
        foreach ($p as $k => $v) {
            if ($v == 0) {
                continue;
            }
            $param = array(
                "codigo_proveedor" => $si["codigo_proveedor"],
                "codigo_inventario" => $k,
                "ajuste" => $v
            );
            try {
                modProductos::ajustarInventario($param);
            } 
            catch (Exception $ex) {
                $db->rollback();
                throw new JPublicException("Error ajustando inventario: " . $ex->getMessage());
            }
        }

        $db->commit();
        return;
    }

    public static function autocompleteProductos($p)
    {
        $si = session::info();

        $p["codigo_proveedor"] = $si["codigo_proveedor"];

        return modProductos::autocompleteProductos($p);
    }

    public static function autocompleteMarcas($p)
    {
        $si = session::info();

        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        $p["flags"] = $si["flags"];

        return modProductos::autocompleteMarcas($p);
    }

    /*     * *
     * Permite buscar las categorias dependiendo del nivel, filtrando por el tipo que se envia como parametro
     * y el nombre, cada filtro cambia dependiendo de si es categoria propia o heredada de coordiutil
     */

    public static function autocompleteCategoriasxNivel($p)
    {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        $p["flags"] = $si["flags"];
        return modProductos::autocompleteCategoriasxNivel($p);
    }

    public static function agregarRelacionados($p)
    {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["marcas_relacionados"][0]) && empty($p["categorias_nivel1_relacionados"][0]) && empty($p["categorias_nivel2_relacionados"][0]) && empty($p["categorias_nivel2_relacionados"][0]) && empty($p["categorias_nivel3_relacionados"][0]) && empty($p["productos_relacionados"][0])) {
            throw new JPublicException("Por favor ingrese un Filtro");
        }

        $codigoMarca = empty($p["marcas_relacionados"][0]) ? "1=1" : "codigo_marca=" . $p["marcas_relacionados"][0];
        $codigoCategoria1 = empty($p["categorias_nivel1_relacionados"][0]) ? "" : $p["categorias_nivel1_relacionados"][0];
        $codigoCategoria2 = empty($p["categorias_nivel2_relacionados"][0]) ? "" : $p["categorias_nivel2_relacionados"][0];
        $codigoCategoria3 = empty($p["categorias_nivel3_relacionados"][0]) ? "" : $p["categorias_nivel3_relacionados"][0];
        $codigoProducto = empty($p["productos_relacionados"][0]) ? "1=1" : "codigo_producto=" . $p["productos_relacionados"][0];

        $flagsProv = JUtils::pgsqlHStoreToArray($si["flags"]);

        if ($flagsProv["flag_categorizacion"] == 0) {

            if ($codigoCategoria1 != "") {
                $codigoCategoria1 = $codigoCategoria1 . "=any(p.negocios)";
            } else {
                $codigoCategoria1 = "1=1";
            }

            if ($codigoCategoria2 != "") {
                $codigoCategoria2 = "codigo_categoria2=" . $codigoCategoria2;
            } else {
                $codigoCategoria2 = "1=1";
            }
            if ($codigoCategoria3 != "") {
                $codigoCategoria3 = "codigo_categoria3=" . $codigoCategoria3;
            } else {
                $codigoCategoria3 = "1=1";
            }
            $sql = "select codigo_producto as data,nombre
		          from view_cu_productos p
		           where 1=1
		           and estado='activo'
		           and inventario>=1
		           and estado_marca='activo'
		           and codigo_proveedor=:codigo_proveedor
		           and $codigoMarca
		           and $codigoCategoria1
		           and $codigoCategoria2
		           and $codigoCategoria3
		           and $codigoProducto";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        }
        if ($flagsProv["flag_categorizacion"] == 1) {

            if ($codigoCategoria1 != "") {
                $codigoCategoria1 = "codigo_categoria1_pp=" . $codigoCategoria1;
            } else {
                $codigoCategoria1 = "1=1";
            }

            if ($codigoCategoria2 !== "") {
                $codigoCategoria2 = "codigo_categoria2_pp=" . $codigoCategoria2;
            } else {
                $codigoCategoria2 = "1=1";
            }
            if ($codigoCategoria3 != "") {
                $codigoCategoria3 = "codigo_categoria3_pp=" . $codigoCategoria3;
            } else {
                $codigoCategoria3 = "1=1";
            }
            $sql = "select codigo_producto as data,nombre
              from view_cu_productos_tienda 
                   where 1=1 
                   and estado='activo'
                   and inventario>=1
                   and estado_marca='activo'
                   and codigo_proveedor_pp=:codigo_proveedor_pp
                   and $codigoMarca
                   and $codigoCategoria1
                   and $codigoCategoria2
                   and $codigoCategoria3
                   and $codigoProducto";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        }

        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteGarantias($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.codigo_garantia as data,a.nombre as label
         from cu_garantias_especificas a
		 where 1=1 and {$where} and codigo_proveedor=:codigo_proveedor
		 order by a.nombre";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function exportarProductos($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        $campos = "referencia,nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if ($p["filters"]["estado"] != "todos") {
            $where .= " and a.estado='{$p["filters"]["estado"]}' ";
        }


        $sql = "select a.codigo_producto,
			       a.referencia,
			       a.nombre,
			       a.nombre_marca,
			      -- b.precio as precio_menor_venta_publico,
			      -- a.vpath as categoria_cu,
                   a.nombre_categoria1_pp ||' / '|| a.nombre_categoria2_pp ||' / '|| a.nombre_categoria3_pp as categorias_pp,
			       c.precio as precio_menor_venta_publico_pp,
			       a.inventario,
			       a.estado,
			       a.estado_revision,
			       trim(regexp_replace(strip_tags(regexp_replace(func_acute_to_tilde(
			         a.descripcion), E'[\\n\\r\\u2028\\u2029]+', ' ', 'g'), ''), E'', ' ',
			         'g')) as descripcion
				from view_cu_productos_tienda a
				     left join cu_productos_precios_menores b on (a.codigo_producto = b.codigo_producto and b.codigo_tipo = 1 and a.flags->'vis_cu' = '1')
				     left join cu_productos_precios_menores c on (a.codigo_producto = c.codigo_producto and c.codigo_tipo=3 and a.flags->'vis_ti' = '1')
				where 1 = 1 and
				      codigo_proveedor =:codigo_proveedor
        			   and {$where} 
				group by a.codigo_producto,
			         a.referencia,
			         a.nombre,
			         a.nombre_marca,
			         b.precio,
			         a.vpath,
			         c.precio,
			         a.inventario,
			         a.estado,
			         a.estado_revision,
			         a.descripcion,
                     a.nombre_categoria1_pp,
                     a.nombre_categoria2_pp,
                     a.nombre_categoria3_pp";

        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);

        if (!empty($flags["flag_tienda_externa"]) && $flags["flag_tienda_externa"] == "1") {
            $sql = "select a.codigo_producto,
					a.referencia,
                    a.nombre,
                    a.nombre_marca,
                    b.precio_coniva as precio_venta_publico,
                    b.precio_compra as precio_coordiutil,
                    b.descuento_proveedor,
					a.vpath as categoria_cu,
                    c.precio_coniva as precio_venta_publico_pp,
                    c.precio_compra as precio_coordiutil_pp,
                    c.descuento_proveedor as descuento_proveedor_pp,
                    a.inventario,
                    a.estado,
                    a.estado_revision,
                    a.val_manejo_inter,
                    d.url_producto,
                    a.alto as alto_cm,a.anchoas ancho_cm,a.largo as largo_cm,
					trim(regexp_replace(strip_tags(regexp_replace(func_acute_to_tilde(a.descripcion), E'[\\n\\r\\u2028\\u2029]+', ' ', 'g' ),''), E'', ' ', 'g')) as descripcion
                from view_cu_productos_base a 
                    join cu_productos d on (a.codigo_producto=d.codigo_producto)
                    left join cu_productos_precios b on (a.codigo_producto=b.codigo_producto and b.codigo_tipo=1 and a.flags->'vis_cu'='1')
                    left join cu_productos_precios c on (a.codigo_producto=c.codigo_producto and c.codigo_tipo=3 and a.flags->'vis_ti'='1')
                where 1=1 and a.codigo_proveedor=:codigo_proveedor";
        }


        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/productos_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $format = &$wb->addFormat(array('Align' => 'center', 'Bold' => 1));
        $ws = &$wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        if ($si["flag_tienda"] == "0") {
                            if ($k == "precio_venta_publico_pp" || $k == "precio_coordiutil_pp" || $k == "descuento_proveedor_pp") {
                                continue;
                            }
                        }
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k, $format);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {

                    if (in_array($k, array("precio_venta_publico", "precio_coordiutil", "inventario", "descuento_proveedor", "alto_cm", "ancho_cm", "largo_cm", "precio_menor_venta_publico", "precio_menor_venta_publico_pp"))) {
                        $ws->writeNumber($row, $col, $v);
                        $col++;
                        continue;
                    }

                    if ($si["flag_tienda"] == "1") {
                        if ($k == "precio_venta_publico_pp" || $k == "precio_coordiutil_pp" || $k == "descuento_proveedor_pp") {
                            $ws->writeNumber($row, $col, $v);
                            $col++;
                            continue;
                        }
                    } else {
                        if ($k == "precio_venta_publico_pp" || $k == "precio_coordiutil_pp" || $k == "descuento_proveedor_pp") {
                            continue;
                        }
                    }

                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }

    public static function exportarInventarios($p){
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre_marca,a.referencia,a.nombre,a.talla,a.color";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if ($p["filters"]["estado"] != "todos") {
            $where .= " and a.estado_base='{$p["filters"]["estado"]}' ";
        }
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $sql = "select a.codigo_inventario,
                    a.codigo_producto,a.referencia,a.nombre,a.nombre_marca,a.talla,a.color,
                    a.inventario as inventario_disponible,a.inventario_reservado,0 as ajuste,
                    a.alto,a.ancho,a.largo,a.unidad_volumen,a.peso,a.unidad_peso,c.precio_coniva,a.codigo_ean
                from view_cu_productos_inventario_base a
                    left join cu_productos_precios c on (a.codigo_producto = c.codigo_producto and a.codigo_inventario=c.codigo_inventario and c.codigo_tipo = 3 )
                where a.codigo_proveedor=:codigo_proveedor
                    and ({$where})
                order by a.nombre
        ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/productos_inventario_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = &$wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                $rCampos = array("precio_coniva", "unidad_peso", "inventario_disponible", "inventario_reservado", "peso", "alto", "ancho", "largo", "unidad_volumen");
                foreach ($r as $k => $v) {

                    if (in_array($k, $rCampos)) {
                        $ws->writeNumber($row, $col, $v);
                        $col++;
                        continue;
                    }

                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }

    public static function autocompletePartidasArancelarias($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "codigo_gravamen,nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select id_gravamen as data,codigo_gravamen||' '||nombre as label
		from cu_partidas_arancelarias
		where 1=1 and {$where}
		order by nombre";

        $ca->prepare($sql);
        $ca->exec();
        return $ca->assocAll();
    }

    public static function autocompletePlantillaProductos($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.codigo_plantilla as data,a.nombre as label
		from cu_plantillas_productos a
		where 1=1 and {$where} and codigo_proveedor=:codigo_proveedor
		order by a.nombre";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function loadAtributosTipoProducto($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select pta.codigo_tipo_atributo,pta.etiqueta,pta.tipo,pta.datasource,
	        coalesce(ptav.valor,'') as valor
	    from cu_productos_tipos_attrs pta
	    left join cu_productos_tipos_attrs_valores ptav on pta.codigo_tipo_atributo = ptav.codigo_tipo_atributo and ptav.codigo_producto = :codigo_producto
	    where pta.codigo_tipo=:codigo_tipo
	    order by pta.etiqueta";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
        $ca->bindValue(":codigo_producto", !empty($p["codigo_producto"]) ? $p["codigo_producto"] : "-1", false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteNegociosHabilitados($p){
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "SELECT negocios_habilitados
                FROM cu_proveedores
                WHERE codigo_proveedor = :codigo_proveedor";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        if ($ca->size() > 0) {
            $rProveedor = $ca->fetch();

            $sql = "SELECT nombre1||'/'||nombre2||'/'||nombre3 as label,
                        codigo_categoria3 as data
                    FROM view_cu_categoriasn
                    WHERE estado1='activo' and estado2='activo' and estado3='activo'
                        and codigo_categoria1 = any(:negocios_habilitados) 
                    ORDER BY nombre1, nombre2, nombre3";

            $ca->prepare($sql);
            $ca->bindValue(":negocios_habilitados", $rProveedor['negocios_habilitados'], true);
            $ca->exec();

            return $ca->fetchAll();

        } else {
            return array();
        }

    }

    public static function listasDeseos($p){
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modProductos::listasDeseos($p);
    }
    
    public static function retornaBodegas($p){
        $si = session::info();
        return modProductos::retornaBodegas($si["codigo_proveedor"]);
    }
    
    public static function loadPageInventariosPorBodega($p){
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modProductos::retornaInventariosPorBodegaPaginado($p);
    }
    
    public static function ajustarInventarioPorBodegas($p){
        $si = session::info();

        if (!in_array("LProductosInventario_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }

        $db = JDatabase::database();
        $db->transaction();
        foreach ($p as $k => $v) {
            try {
                if ($v == 0) {
                    continue;
                }
                $param = array(
                    "codigo_proveedor" => $si["codigo_proveedor"],
                    "codigo_inventario" => $k,
                    "ajuste" => $v
                );
                modProductos::ajustarInventarioPorBodegas($param);
            } 
            catch (Exception $ex) {
                $db->rollback();
                throw new JPublicException("Error ajustando inventario: " . $ex->getMessage());
            }
        }

        $db->commit();
        return;
    }
    
    public static function exportarInventariosPorBodega($p){
        $si = Session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        $datos = modProductos::retornaInventariosPorBodegaExportar($p);
        $path = JApp::privateTempPath() . "/productos_inventario_por_bodegas_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = &$wb->addWorksheet('Hoja1');

        if ($datos == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($datos as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                $rCampos = array();
                foreach ($r as $k => $v) {

                    if (in_array($k, $rCampos)) {
                        $ws->writeNumber($row, $col, $v);
                        $col++;
                        continue;
                    }

                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }
}