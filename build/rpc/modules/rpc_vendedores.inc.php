<?php
class Vendedores {
    
    public static function loadAll($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        foreach ($p["column_filters"] as $colum  => $valorCulumn){
        	if($valorCulumn != ""){
        		$where .= " and ". $ca->sqlFieldsFilters($colum, $valorCulumn);
        	}
        }

        $sql = "select * 
                from cu_vendedores_pp 
		where codigo_proveedor_pp = :codigo_proveedor_pp and {$where}";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function save($p) {
        if ($p["nombre"] == "") {
            throw new JPublicException("Debes ingresar el nombre del vendedor");
        }

        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if ($p["codigo_vendedor"] === "") {
	    $codigoVendedor = $db->nextVal("cu_vendedores_pp_codigo_vendedor");
            $ca -> prepareInsert("cu_vendedores_pp", "codigo_vendedor, codigo_proveedor_pp, nombre");
        } 
	else {
	    $codigoVendedor = $p["codigo_vendedor"];
            $ca -> prepareUpdate("cu_vendedores_pp", "nombre", "codigo_proveedor_pp = :codigo_proveedor_pp and codigo_vendedor = :codigo_vendedor");
        }

        $ca -> bindValue(":codigo_vendedor", $codigoVendedor);
        $ca -> bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], true);
        $ca -> bindValue(":nombre", $p["nombre"], true);
        $ca -> exec();
    }
    
    public static function load($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca -> prepareSelect("cu_vendedores_pp", "*", "codigo_vendedor = :codigo_vendedor and codigo_proveedor_pp = :codigo_proveedor_pp");
        $ca -> bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"]);
        $ca -> bindValue(":codigo_vendedor", $p["codigo_vendedor"], true);
        $ca -> exec();
        if ($ca -> size() == 0) {
            throw new JPublicException("Vendedor no localizado");
        }

        return $ca->fetch();
    }
   
}