<?php
require_once $cfg["rutaModeloLogico"]."mod_marcas.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_productos_importados.inc.php";

class Marcas {
    
    public static function loadPage($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        
        $result = modMarcas::loadPage($p);
        for ($i = 0; $i < count($result["records"]); $i++) {
            
            $tmp = modRs::load(rs::cuProveedoresMarcas, $result["records"][$i]["codigo_proveedores_marcas"], array(rs::cuMarcasLogo));
            if (!isset($tmp["logo"])) {
                $tmp = modRs::load(rs::cuMarcas, $result["records"][$i]["codigo_marca"], array(rs::cuMarcasLogo));
                if (isset($tmp["logo"])) {
                    $result["records"][$i]["logo"] = JDbFile::url($tmp["logo"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
                }
            }
            else {
                $result["records"][$i]["logo"] = JDbFile::url($tmp["logo"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }
        
        return $result;
    }
    
    public static function loadOne($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        
        $result = modMarcas::loadOne($p);
        
        $result["rs"] = modRs::load(rs::cuProveedoresMarcas, $result["codigo_proveedores_marcas"], array(
            rs::cuMarcasLogo,
            rs::cuMarcasCabezote,
            rs::cuMarcasBanner1,
            rs::cuMarcasBanner2
        ));
        
        if(empty($result["rs"])) {
            $result["rs"] = modRs::load(rs::cuMarcas, $p["codigo_marca"], array(
                rs::cuMarcasLogo,
                rs::cuMarcasCabezote,
                rs::cuMarcasBanner1,
                rs::cuMarcasBanner2
            ));
        }
        
        foreach ($result["rs"] as $k => $v) {
            $result["rs"][$k]["preview"] = JDbFile::url($v["id"], array("w" => 130, "h" => 60, "frm" => 1));
        }
        
        return $result;
    }
    
    public static function save($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        //throw new JPublicException(print_r($p,1));
        $codigo_proveedor = $si["codigo_proveedor"];
        
        if(empty($p["nombre"])) {
            throw new JPublicException("Nombre invalido");
        }
        
        /*if(empty($p["orden"])) {
         throw new JPublicException("El orden es invalido");
         }*/
        
        $p["orden"] = !empty($p["orden"])?$p["orden"]:"10000";
        
        $rOld = array();
        $db->transaction();
        
        $campos = "codigo_marca,nombre,descripcion_html"; //,file_logo,file_cabezote,file_banner1,file_banner2";
        $p["fechahora_activacion"] = "fechahora_activacion";
        
        
        // Inserto la informacion de las marcas
        if(empty($p["codigo_marca"])) {
            
            //solo se inserta la marca si no fue creada anteriormente
            $ca->prepareSelect("cu_marcas", "codigo_marca", "nombre=:nombre");
            $ca->bindValue(":nombre", trim($p["nombre"]), true);
            $ca->exec();
            $nMarca = $ca->size();
            
            if($nMarca == 0) {
                $codigoMarca=$db->nextval("cu_marcas_codigo_marca");
                $campos .=",estado";
                $ca->prepareInsert("cu_marcas",$campos);
                $ca->bindValue(":codigo_marca",$codigoMarca,false);
                $ca->bindValue(":nombre",trim($p["nombre"]),true);
                $ca->bindValue(":estado","activo",true);
                $ca->bindValue(":fechahora_activacion", $p["fechahora_activacion"], false);
                $ca->bindValue(":descripcion_html", $p["descripcion_html"], true);
                $ca->exec();
            } else {
                $rMarca = $ca->fetch();
                $codigoMarca = $rMarca["codigo_marca"];
            }
        } else {
            $codigoMarca = $p["codigo_marca"];
            
            //se valida que no se pueda cambiar el nombre a la marca que sea compartida
            $ca->prepareSelect("cu_marcas", "codigo_marca", "codigo_marca=:codigo_marca and nombre <>:nombre and flag_compartida = '1' ");
            $ca->bindValue(":nombre", trim($p["nombre"]), true);
            $ca->bindValue(":codigo_marca", $codigoMarca, false);
            $ca->exec();
            $nMarcaNombreDiferente = $ca->size();
            
            if($nMarcaNombreDiferente > 0) {
                throw new JPublicException("No puede cambiar el nombre de la marca");
            }
            
            
            $campos .= ",fechahora_activacion";
            $ca->prepareUpdate("cu_marcas", $campos, "codigo_marca=:codigo_marca");
            
            $ca->bindValue(":codigo_marca", $codigoMarca);
            $ca->bindValue(":nombre", trim($p["nombre"]), true);
            $ca->bindValue(":fechahora_activacion", $p["fechahora_activacion"], false);
            $ca->bindValue(":descripcion_html", $p["descripcion_html"], true);
            $ca->exec();
        }
        
        //inserto el registro especifico para la tienda
        $ca->prepareSelect("cu_proveedores_marcas", "codigo_marca,codigo_proveedores_marcas", "codigo_proveedor=:codigo_proveedor and codigo_marca=:codigo_marca");
        $ca->bindValue(":codigo_marca", $codigoMarca, false);
        $ca->bindValue(":codigo_proveedor", $codigo_proveedor, false);
        $ca->exec();
        $nProveedoresMarcas = $ca->size();
        
        
        if($nProveedoresMarcas== 0) {
            $ca->prepareInsert("cu_proveedores_marcas", "codigo_marca,codigo_proveedor,estado,orden","returning codigo_proveedores_marcas");
            $ca->bindValue(":codigo_marca", $codigoMarca, false);
            $ca->bindValue(":codigo_proveedor", $codigo_proveedor, false);
            $ca->bindValue(":estado", "activo", true);
            $ca->bindValue(":orden", $p["orden"], false);
            $ca->exec();
            
            $rProveedorMarca = $ca->fetch();
            $codigoProveedorMarca = $rProveedorMarca["codigo_proveedores_marcas"];
        } else {
            $ca->prepareUpdate("cu_proveedores_marcas", "estado, orden", "codigo_marca=:codigo_marca and codigo_proveedor=:codigo_proveedor","returning codigo_proveedores_marcas");
            $ca->bindValue(":codigo_proveedor", $codigo_proveedor, false);
            $ca->bindValue(":codigo_marca", $codigoMarca, false);
            $ca->bindValue(":estado", "activo", true);
            $ca->bindValue(":orden", $p["orden"], false);
            $ca->exec();
            $rProveedorMarca = $ca->fetch();
            $codigoProveedorMarca = $rProveedorMarca["codigo_proveedores_marcas"];
        }
        
        $ca->prepareSelect("cu_marcas", "codigo_marca", "codigo_marca=:codigo_marca and resources is not null");
        $ca->bindValue(":codigo_marca", $codigoMarca, false);
        $ca->exec();
        $nResourceMarcaExistente = $ca->size();
        
        /*cuando no exista el recurso para la marca*/
        if($nResourceMarcaExistente== 0) {
            if(!empty($p["logo"])) {
                modRs::save(rs::cuMarcas, $codigoMarca, rs::cuMarcasLogo, $p["logo"]);
            }
            
            if(!empty($p["cabezote"])) {
                modRs::save(rs::cuMarcas, $codigoMarca, rs::cuMarcasCabezote, $p["cabezote"]);
            }
            
            if(!empty($p["banner1"])) {
                modRs::save(rs::cuMarcas, $codigoMarca, rs::cuMarcasBanner1, $p["banner1"]);
            }
            
            if(!empty($p["banner2"])) {
                modRs::save(rs::cuMarcas, $codigoMarca, rs::cuMarcasBanner2, $p["banner2"]);
            }
        }
        
        
        //cuando se crea una marca en una tienda siempre se asocia al registro de proveedor marca
        if(!empty($p["logo"])) {
            modRs::save(rs::cuProveedoresMarcas, $codigoProveedorMarca, rs::cuMarcasLogo, $p["logo"]);
        }
        
        if(!empty($p["cabezote"])) {
            modRs::save(rs::cuProveedoresMarcas, $codigoProveedorMarca, rs::cuMarcasCabezote, $p["cabezote"]);
        }
        
        if(!empty($p["banner1"])) {
            modRs::save(rs::cuProveedoresMarcas, $codigoProveedorMarca, rs::cuMarcasBanner1, $p["banner1"]);
        }
        
        if(!empty($p["banner2"])) {
            modRs::save(rs::cuProveedoresMarcas, $codigoProveedorMarca, rs::cuMarcasBanner2, $p["banner2"]);
        }
        
        $db->commit();
        return;
    }
    
    public static function autocompleteMarcas($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        
        $sql = "select a.codigo_marca as data,a.nombre as label
        from view_cu_marcas_proveedor a
        where 1=1 and a.codigo_proveedor=:codigo_proveedor and {$where}
        order by a.nombre";
        
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }
    
    /*
     La funcion inactiva los productosde una marca especifica que correspondan a un provedor en especifico
     Parametro $p se envia un arreglo con la marca seleccionada y el provedor que se encuentra en ese momento
     
     public static function inactivarProductoPorProvedor($p) {
     $si = session::info();
     //conexion a la base de satos
     $db = JDatabase::database();
     $ca = new JDbQuery($db);
     
     //consulta update que se ejecuta para poner en estado inactivo los productosde una marca y un proveedor en especifico
     $ca->prepareUpdate("cu_productos","estado='inactivo'","codigo_marca=:codigo_marca and codigo_proveedor=:codigo_proveedor and estado='activo'");
     
     //se obtiene las variables enviadas
     $ca->bindValue(":codigo_marca",$p["codigo_marca"],false);  //codigo marca
     $ca->bindValue(":codigo_proveedor",$p["codigo_proveedor"],false);  //codigo proveedor
     
     $db->transaction();
     //se ejcuta el sql
     
     $ca->exec();
     
     $db->commit();
     return;
     }
     */
    
}