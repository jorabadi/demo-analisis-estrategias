<?php
require_once $cfg["rutaModeloLogico"] . "mod_resources.inc.php";

class Canastas {

    public static function loadPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "c.nombre";

        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        $sql = "select c.* from (select c.codigo_canasta,c.nombre,c.posicion,c.estado,c.cantidad_items,
		array_to_string(array_agg(p.titulo),',') as ubicaciones
		from cu_canastas c
        join cu_ubicaciones_pp u on u.tipo_objeto = 10 and u.codigo_referencia_tipo_ubicacion = c.codigo_canasta::text
        and c.codigo_proveedor_pp = u.codigo_proveedor_pp
        join cu_paginas p on u.codigo_pagina = p.codigo_pagina
		where 1=1  and c.codigo_proveedor_pp=:codigo_proveedor
		group by c.codigo_canasta,c.nombre,c.posicion,c.estado,c.cantidad_items ) c  where {$where} ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function loadEditRs($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        //Grupos destino
        $sql = "select p.titulo as label,p.url_request as data
            from cu_paginas p
            where p.titulo is not null";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $result["paginas"] = $ca->fetchAll();

        $sql = "select ut.codigo_tipo as data, ut.nombre as label
            from cu_tipos_objetos ut
	         order by ut.codigo_tipo";

        $ca->prepare($sql);
        $ca->exec();
        $result["tipos_objetos"] = $ca->fetchAll();

        $sql = "select codigo_pagina as data,titulo as label from cu_paginas where titulo <>''";
        $ca->prepare($sql);
        $ca->exec();
        $result["tipos_ubicaciones"] = $ca->fetchAll();

        $result["estados"] = array(
            array("label" => "Activo", "data" => "activo"),
            array("label" => "Inactivo", "data" => "inactivo")
        );

        $cantidadItems = array();

        for ($i = 4; $i <= 64; $i = $i + 4) {
            $cantidadItems[] = array("label" => strval($i), "data" => strval($i));
        }

        $result["cantidad_items"] = $cantidadItems;


        $result["defaults"] = array(
            "paginas" => "/",
            "tipos_objetos" => "",
            "estados" => "activo",
            "cantidad_items" => "4"
        );

        return $result;
    }

    public static function autocompleteRefereciaTipo($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $flagsProv = JUtils::pgsqlHStoreToArray($si["flags"]);

        $result = array();

        if ($p["tipo_objeto"] == "1" && $flagsProv["flag_categorizacion"] == 0) {
            $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categoriasn c 
                where c.tipo=1 
                and c.codigo_categoria in (
                    select unnest(p.negocios)
                    from matv_cu_productos_tienda p 
                    where p.codigo_proveedor_pp=:codigo_proveedor and p.estado_marca='activo' and p.estado='activo' 
                        and p.estado_revision='aceptado' and p.inventario > 0 
                        and ((p.estado_tienda='activo' and p.codigo_proveedor=p.codigo_proveedor_pp) or (p.codigo_proveedor<>p.codigo_proveedor_pp))
                )";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } else if ($p["tipo_objeto"] == "1" && $flagsProv["flag_categorizacion"] == 1) {
            $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categorias_pp c
                 where c.tipo=1 
                 and c.codigo_categoria in (
                    select p.codigo_categoria1_pp
                    from matv_cu_productos_tienda p 
                    where 
                        p.codigo_proveedor_pp=:codigo_proveedor and p.estado_marca='activo' and p.estado='activo' and p.estado_revision='aceptado' and p.inventario > 0 
                        and ((p.estado_tienda='activo' and p.codigo_proveedor=p.codigo_proveedor_pp) or (p.codigo_proveedor<>p.codigo_proveedor_pp))
                 )
                 and c.codigo_proveedor=:codigo_proveedor ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } else if ($p["tipo_objeto"] == "2" && $flagsProv["flag_categorizacion"] == 0) {
            $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categoriasn c 
                where c.tipo=2 
                and c.codigo_categoria in (
                    select p.codigo_categoria2
                    from matv_cu_productos_tienda p 
                    where p.codigo_proveedor_pp=:codigo_proveedor and p.estado_marca='activo' and p.estado='activo' 
                        and p.estado_revision='aceptado' and p.inventario > 0 
                        and ((p.estado_tienda='activo' and p.codigo_proveedor=p.codigo_proveedor_pp) or (p.codigo_proveedor<>p.codigo_proveedor_pp))
                )";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } else if ($p["tipo_objeto"] == "2" && $flagsProv["flag_categorizacion"] == 1) {
            $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categorias_pp c
                 where c.tipo=2 
                 and c.codigo_categoria in (
                    select p.codigo_categoria2_pp
                    from matv_cu_productos_tienda p 
                    where 
                        p.codigo_proveedor_pp=:codigo_proveedor and p.estado_marca='activo' and p.estado='activo' and p.estado_revision='aceptado' and p.inventario > 0 
                        and ((p.estado_tienda='activo' and p.codigo_proveedor=p.codigo_proveedor_pp) or (p.codigo_proveedor<>p.codigo_proveedor_pp))
                 )
                 and c.codigo_proveedor=:codigo_proveedor ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } else if ($p["tipo_objeto"] == "3" && $flagsProv["flag_categorizacion"] == 0) {
            $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categoriasn c 
                where c.tipo=3 
                and c.codigo_categoria in (
                    select p.codigo_categoria3
                    from matv_cu_productos_tienda p 
                    where p.codigo_proveedor_pp=:codigo_proveedor and p.estado_marca='activo' and p.estado='activo' 
                        and p.estado_revision='aceptado' and p.inventario > 0 
                        and ((p.estado_tienda='activo' and p.codigo_proveedor=p.codigo_proveedor_pp) or (p.codigo_proveedor<>p.codigo_proveedor_pp))
                )";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } else if ($p["tipo_objeto"] == "3" && $flagsProv["flag_categorizacion"] == 1) {
            $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categorias_pp c
                 where c.tipo=3
                 and c.codigo_categoria in (
                    select p.codigo_categoria3_pp
                    from matv_cu_productos_tienda p 
                    where 
                        p.codigo_proveedor_pp=:codigo_proveedor and p.estado_marca='activo' and p.estado='activo' and p.estado_revision='aceptado' and p.inventario > 0 
                        and ((p.estado_tienda='activo' and p.codigo_proveedor=p.codigo_proveedor_pp) or (p.codigo_proveedor<>p.codigo_proveedor_pp))
                 )
                 and c.codigo_proveedor=:codigo_proveedor ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } else if ($p["tipo_objeto"] == "4") {
            $sql = "select distinct a.codigo_marca as data, a.nombre_marca as label
                from matv_cu_productos_tienda a
                 where a.codigo_proveedor_pp=:codigo_proveedor and a.estado_marca='activo' and a.estado='activo'
				 and a.estado_revision='aceptado' and a.inventario > 0 and
				 ((a.estado_tienda='activo' and a.codigo_proveedor=a.codigo_proveedor_pp) or (a.codigo_proveedor<>a.codigo_proveedor_pp)) ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } else if ($p["tipo_objeto"] == "5") {
            $sql = "SELECT distinct c.codigo_categoria as data,c.nombre as label
            from cu_categoriasv c
            where 1=1 and c.estado='activo' and c.codigo_proveedor=:codigo_proveedor";

            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } else if ($p["tipo_objeto"] == "6") {
            $sql = "SELECT distinct c.codigo_contenido as data,c.nombre as label
            from cu_contenidos c
            where 1=1 and c.codigo_proveedor_pp=:codigo_proveedor";

            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        } else if ($p["tipo_objeto"] == "11") {

            $campos = "
			p.nombre,
			p.referencia,
			p.nombre_marca
		";
            $where = $ca->sqlFieldsFilters($campos, $p["filter"]);


            $sql = "select distinct p.codigo_producto as data ,p.referencia||'--'||p.codigo_producto||'--'||p.nombre as label
                from matv_cu_productos_tienda p
                 where p.codigo_proveedor_pp=:codigo_proveedor and p.estado_marca='activo' and p.estado='activo'
				 and p.estado_revision='aceptado' and p.inventario > 0 and
             	  {$where}	and
				 ((p.estado_tienda='activo' and p.codigo_proveedor=p.codigo_proveedor_pp) or (p.codigo_proveedor<>p.codigo_proveedor_pp)) ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        }

        return $result;
    }

    public static function guardar($p) {


        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["nombre"])) {
            throw new JPublicException("Debe ingresar el nombre");
        }

        if (empty($p["titulo"])) {
            throw new JPublicException("Debe ingresar el titulo");
        }

        if (empty($p["cantidad_items"])) {
            throw new JPublicException("Debe ingresar la cantidad de items de la canasta");
        }

        $campos = "codigo_canasta,codigo_proveedor_pp,nombre,posicion,etiqueta,cantidad_items,titulo,url_encabezado,estado";
        $db->transaction();
        if (empty($p["codigo_canasta"])) {
            $ca->prepareInsert("cu_canastas", $campos);
            $codigoCanasta = $db->nextVal("cu_canastas_codigo_canasta");
            $p["codigo_canasta"] = $codigoCanasta;
        } else {
            $ca->prepareUpdate("cu_canastas", $campos, "codigo_canasta=:codigo_canasta");
            $codigoCanasta = $p["codigo_canasta"];
        }

        $ca->bindValue(":codigo_canasta", $codigoCanasta, false);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":titulo", $p["titulo"], true);
        $ca->bindValue(":url_encabezado", $p["url_encabezado"], true);
        $ca->bindValue(":posicion", "1", false);
        $ca->bindValue(":etiqueta", "", true);
        $ca->bindValue(":cantidad_items", $p["cantidad_items"], false);
        $ca->bindValue(":estado", $p["estado"], true);
        $ca->exec();

        if (!empty($p["imagen_encabezado"])) {
            modRs::save(rs::cuCanastas, $codigoCanasta, rs::cuCanastasImagenEncabezado, $p["imagen_encabezado"]);
        }

        self::guardarUbicaciones($p);
        self::guardarDetalleCanasta($p);

        $ca->prepare("select func_canastas_actualizar_pp()");
        $ca->exec();

        $db->commit();
    }

    public static function guardarUbicaciones($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["ubicaciones"])) {
            throw new JPublicException("Debe ingresar al menos una ubicacion");
        }

        //elimino las ubicaciones anteriores
        $ca->prepareDelete("cu_ubicaciones_pp", "tipo_objeto = 10 and codigo_referencia_tipo_ubicacion=:codigo_canasta
    	        and codigo_proveedor_pp=:codigo_proveedor_pp");
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_canasta", $p["codigo_canasta"], true);
        $ca->exec();

        $i = 1;
        global $manejadorCache;
        foreach ($p["ubicaciones"] as $rUbicacion) {

            if (empty($rUbicacion["codigo_pagina"])) {
                throw new JPublicException("Debe ingresar la pagina de la fila {$i} de las ubicaciones ");
            }

            if (empty($rUbicacion["orden"])) {
                throw new JPublicException("Debe ingresar el orden de la fila {$i} de las ubicaciones ");
            }

            $campos = "codigo_ubicacion,codigo_pagina,codigo_referencia,orden,tipo_objeto,codigo_proveedor_pp,
    	    codigo_referencia_tipo_ubicacion";

            $codigoUbicacion = $db->nextVal("cu_ubicaciones_pp_codigo_ubicacion");
            $tipoObjetoUbicacion = "10";

            $referencia = !empty($rUbicacion["codigo_referencia"]) ? $rUbicacion["codigo_referencia"][0]["data"] : -1;

            $ca->prepareInsert("cu_ubicaciones_pp", $campos);
            $ca->bindValue(":codigo_ubicacion", $codigoUbicacion, false);
            $ca->bindValue(":codigo_pagina", $rUbicacion["codigo_pagina"], false);
            $ca->bindValue(":codigo_referencia", $referencia, false);
            $ca->bindValue(":orden", $rUbicacion["orden"], false);
            $ca->bindValue(":tipo_objeto", $tipoObjetoUbicacion, false);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->bindValue(":codigo_referencia_tipo_ubicacion", $p["codigo_canasta"], true);
            $ca->exec();

            $ca->prepare("select * from cu_paginas where codigo_pagina =:codigo_pagina");
            $ca->bindValue(":codigo_pagina", $rUbicacion["codigo_pagina"], false);
            $ca->exec();
            $rPagina = $ca->fetch();

            //elimino la canasta
            $keyCache = "CanastasPP_productosCanasta_{$si["codigo_proveedor"]}_{$rPagina["url_request"]}";
            $manejadorCache->set($keyCache, false, 900);
            $i++;
        }
    }

    public static function guardarDetalleCanasta($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["items"])) {
            throw new JPublicException("Debe ingresar al menos un item");
        }

        //elimino las ubicaciones anteriores
        $ca->prepareDelete("cu_canastas_det", "codigo_canasta =:codigo_canasta");
        $ca->bindValue(":codigo_canasta", $p["codigo_canasta"], false);
        $ca->exec();

        $i = 1;

        foreach ($p["items"] as $rItem) {

            if (empty($rItem["numero_item"])) {
                throw new JPublicException("Debe ingresar el numero del item de la fila {$i} de los items ");
            }

            if (empty($rItem["tipo_objeto"])) {
                throw new JPublicException("Debe ingresar el tipo de objeto de la fila {$i} de los items ");
            }


            $referencia = !empty($rItem["codigo_referencia"]) ? $rItem["codigo_referencia"][0]["data"] : "";


            if (!empty($rItem["tipo_objeto"]) && in_array($rItem["tipo_objeto"], array("1", "2", "3", "4", "5", "6", "11")) && empty($referencia)) {
                throw new JPublicException("Debe ingresar la referencia del objeto de la fila {$i} de los items ");
            }

            $campos = "codigo_canasta,tipo_objeto,referencia_objeto,numero_item";
            $ca->prepareInsert("cu_canastas_det", $campos);
            $ca->bindValue(":codigo_canasta", $p["codigo_canasta"], true);
            $ca->bindValue(":numero_item", $rItem["numero_item"], false);
            $ca->bindValue(":tipo_objeto", $rItem["tipo_objeto"], false);
            $ca->bindValue(":referencia_objeto", $referencia, true);
            $ca->exec();

            $i++;
        }
    }

    public static function loadOne($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select
					nombre,
					titulo,
					estado,
	                cantidad_items,
	                url_encabezado
				from cu_canastas where 1=1
					and codigo_canasta =:codigo_canasta
					and codigo_proveedor_pp=:codigo_proveedor_pp ";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"]);
        $ca->bindValue(":codigo_canasta", $p["codigo_canasta"]);
        $ca->exec();
        $result = $ca->fetch();

        $result["items"] = self::loadItemsCanasta($p);
        $result["ubicaciones"] = self::loadUbicacionesCanasta($p);

        return $result;
    }

    public static function loadItemsCanasta($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $flagsProv = JUtils::pgsqlHStoreToArray($si["flags"]);

        $sql = "select
					codigo_canasta,
	                tipo_objeto,
	                referencia_objeto as codigo_referencia,
	                numero_item
				from cu_canastas_det where 1=1
					and codigo_canasta=:codigo_canasta";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_canasta", $p["codigo_canasta"]);
        $ca->exec();
        $items = $ca->fetchAll();

        foreach ($items as $kItem => $item) {
            $referencia = array();
            if ($item["tipo_objeto"] == "1" && $flagsProv["flag_categorizacion"] == 0) {
                $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categoriasn c
                where c.tipo=1
                and c.codigo_categoria =:codigo_referencia";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_referencia", $item["codigo_referencia"], false);
                $ca->exec();
            } else if ($item["tipo_objeto"] == "1" && $flagsProv["flag_categorizacion"] == 1) {
                $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categorias_pp c
                 where c.tipo=1
                 and c.codigo_categoria =:codigo_referencia
                 and c.codigo_proveedor=:codigo_proveedor ";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_referencia", $item["codigo_referencia"], false);
                $ca->exec();
            } else if ($item["tipo_objeto"] == "2" && $flagsProv["flag_categorizacion"] == 0) {
                $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categoriasn c
                where c.tipo=2
                and c.codigo_categoria =:codigo_referencia";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_referencia", $item["codigo_referencia"], false);
                $ca->exec();
            } else if ($item["tipo_objeto"] == "2" && $flagsProv["flag_categorizacion"] == 1) {
                $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categorias_pp c
                 where c.tipo=2
                 and c.codigo_categoria =:codigo_referencia
                 and c.codigo_proveedor=:codigo_proveedor ";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_referencia", $item["codigo_referencia"], false);
                $ca->exec();
            } else if ($item["tipo_objeto"] == "3" && $flagsProv["flag_categorizacion"] == 0) {
                $sql = "select distinct c.codigo_categoria as data,c.nombre as label
                from cu_categoriasn c
                where c.tipo=3
                and c.codigo_categoria =:codigo_referencia";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_referencia", $item["codigo_referencia"], false);
                $ca->exec();
            } else if ($item["tipo_objeto"] == "4") {
                $sql = "select distinct a.codigo_marca as data, a.nombre_marca as label
                from view_cu_productos_tienda a
                where a.codigo_proveedor_pp=:codigo_proveedor 
	            and a.codigo_marca=:codigo_referencia";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_referencia", $item["codigo_referencia"], false);
                $ca->exec();
            } else if ($item["tipo_objeto"] == "5") {
                $sql = "SELECT distinct c.codigo_categoria as data,c.nombre as label
                from cu_categoriasv c
                where 1=1 and c.codigo_proveedor=:codigo_proveedor
	            and c.codigo_categoria=:codigo_referencia";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_referencia", $item["codigo_referencia"], false);
                $ca->exec();
            } else if ($item["tipo_objeto"] == "6") {
                $sql = "SELECT distinct c.codigo_contenido as data,c.nombre as label
                from cu_contenidos c
                where 1=1 and c.codigo_proveedor_pp=:codigo_proveedor
	            and c.codigo_contenido=:codigo_referencia";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_referencia", $item["codigo_referencia"], false);
                $ca->exec();
            } else if ($item["tipo_objeto"] == "11") {
                $sql = "select distinct p.codigo_producto as data ,p.referencia||'--'||p.codigo_producto||'--'||p.nombre as label
                from matv_cu_productos_tienda p
                where p.codigo_proveedor_pp=:codigo_proveedor 
	            and p.codigo_producto=:codigo_referencia";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":codigo_referencia", $item["codigo_referencia"], false);
                $ca->exec();
            } else {
                $items[$kItem]["codigo_referencia"] = array();
                continue;
            }

            if ($ca->size() == 0) {
                $referencia = array();
            } else {
                $referencia = $ca->fetch();
            }

            $items[$kItem]["codigo_referencia"] = array($referencia);
        }

        return $items;
    }

    public static function loadUbicacionesCanasta($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select
					codigo_ubicacion,
	                codigo_pagina,
	                codigo_referencia,
	                tipo_objeto,
	                codigo_referencia_tipo_ubicacion,
	                orden 
				from cu_ubicaciones_pp where 1=1
	                and tipo_objeto = 10
					and codigo_referencia_tipo_ubicacion =:codigo_canasta";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_canasta", $p["codigo_canasta"], true);
        $ca->exec();
        $ubicaciones = $ca->fetchAll();

        foreach ($ubicaciones as $kUbicacion => $ubicacion) {
            $sql = "";
            if ($ubicacion["codigo_pagina"] == 13) {
                $sql = "select
    	        p.codigo_categoria as data,
    	        p.nombre as label
    	        from cu_categorias_pp p where p.tipo = 1
    	        and codigo_proveedor=:codigo_proveedor
    	        and {$where} ";
            } elseif ($ubicacion["codigo_pagina"] == 26) {
                $sql = "select
    			p.codigo_marca as data,
    			p.nombre as label
    			from  cu_marcas p
    			join cu_proveedores_marcas pp on (pp.codigo_marca=p.codigo_marca)
    			where pp.codigo_proveedor=:codigo_proveedor
    			and {$where}";
            } elseif ($ubicacion["codigo_pagina"] == 35) {
                $sql = "select
    	        p.nombre as label ,
    	        p.codigo_categoria as data
    	        from cu_categoriasv p
    	        where p.codigo_proveedor =:codigo_proveedor
    	        and codigo_categoria =:codigo_referencia";
            } else {
                $ubicaciones[$kUbicacion]["codigo_referencia"] = array();
                continue;
            }

            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":codigo_referencia", $ubicacion["codigo_referencia"], false);
            $ca->exec();

            $ubicaciones[$kUbicacion]["codigo_referencia"] = $ca->fetch();
        }

        return $ubicaciones;
    }

}