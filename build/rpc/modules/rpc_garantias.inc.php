<?php

class Garantias {

    public static function loadPage($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $sql = "select codigo_garantia,nombre 
        	from cu_garantias_especificas
            where codigo_proveedor=:codigo_proveedor and {$where} ";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function save($p) {
        $si = session::info();
        if (!in_array("FGarantiasEspecificasNe_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "codigo_garantia,nombre,contenido,codigo_proveedor";
        $db->transaction();
        if ($p["codigo_garantia"] === "") {
            $ca->prepareInsert("cu_garantias_especificas", $campos);
            $codigoGarantia = $db->nextVal("cu_garantias_especificas_codigo_garantia");
        } 
	else {
            $ca->prepareUpdate("cu_garantias_especificas", $campos, "codigo_garantia=:codigo_garantia");
            $codigoGarantia = $p["codigo_garantia"];
        }

        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_garantia", $codigoGarantia, false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":contenido", $p["contenido"], true);

        $ca->exec();
        $db->commit();

        return;
    }

    public static function loadOne($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_garantias_especificas", "codigo_garantia,nombre,contenido", "codigo_garantia=:codigo_garantia");

        $ca->bindValue(":codigo_garantia", $p["codigo_garantia"], false);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Garantía no localizada");
        }

        $result = $ca->fetch();

        return $result;
    }

}