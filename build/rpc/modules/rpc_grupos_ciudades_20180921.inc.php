<?php
class GruposCiudades {

    public static function loadAll($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select a.codigo_grupo,a.nombre
        from cu_destinos_grupos a
        where a.codigo_proveedor=:codigo_proveedor";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        return $ca->execPage($p);
    }

    public static function save($p) {

        $si = session::info();

        if (!in_array("FGruposCiudadesNe_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }


        if (empty($p["nombre"])) {
            throw new JPublicException("Falta nombre");
        }

        if (empty($p["ciudades"]) || count($p["ciudades"]) == 0) {
            throw new JPublicException("Falta ciudades");
        }

        $p["codigo_grupo"] = isset($p["codigo_grupo"]) ? $p["codigo_grupo"] : "";

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "codigo_grupo,nombre,codigo_proveedor";

        $db->transaction();

        if ($p["codigo_grupo"] === "") {
            $ca->prepareInsert("cu_destinos_grupos", $campos);
            $codigo_grupo = $db->nextVal("cu_destinos_grupos_codigo_grupo");
            $ca->bindValue(":codigo_grupo", $codigo_grupo);
        } else {
            $ca->prepareUpdate("cu_destinos_grupos", $campos, "codigo_grupo=:codigo_grupo");
            $codigo_grupo = $p["codigo_grupo"];
            $ca->bindValue(":codigo_grupo", $codigo_grupo);
        }

        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);

        $ca->exec();

        $ca->prepareDelete("cu_destinos_grupos_ciudades", "codigo_grupo=:codigo_grupo");
        $ca->bindValue(":codigo_grupo", $codigo_grupo, false);
        $ca->exec();


        if (isset($p["ciudades"])) {
            $campos = "codigo_grupo,idciudad";
            // throw new JPublicException(print_r( $p["ciudades"],1));
            foreach ($p["ciudades"] as $r) {

                $ca->prepareInsert("cu_destinos_grupos_ciudades", $campos);
                $ca->bindValue(":codigo_grupo", $codigo_grupo, false);
                $ca->bindValue(":idciudad", $r["data"], false);
                $ca->exec();
            }
        }

        $db->commit();
    }

    public static function load($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select a.codigo_grupo,a.nombre
                from cu_destinos_grupos a
                where a.codigo_proveedor=:codigo_proveedor and a.codigo_grupo=:codigo_grupo";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_grupo", $p["codigo_grupo"], false);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Grupo ciudad no localizado");
        }

        $result = $ca->fetch();

        $ca->prepareSelect("cu_destinos_grupos_ciudades gc join cu_ciudades c on gc.idciudad = c.idciudad ", "c.idciudad ,c.nombre", "gc.codigo_grupo=:codigo_grupo");
        $ca->bindValue(":codigo_grupo", $p["codigo_grupo"], false);
        $ca->exec();

        $result["ciudades"] = array();
        foreach ($ca->fetchAll() as $r) {
            $tmp = array();
            $tmp["data"] = $r["idciudad"];
            $tmp["label"] = $r["nombre"];
            $result["ciudades"][] = $tmp;
        }



        return $result;
    }

    public static function autocompleteCiudades($p) {

        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_proveedores", "codigo_sitio", "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $r = $ca->fetch();


        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.idciudad as data,a.nombre as label
        from cu_ciudades a left join gen_sitios b on (a.codigo_pais=b.codigo_pais)
        where {$where} and a.codigo_pais = 'CO' order by a.nombre";

        $ca->prepare($sql);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteCiudadesPoblacionesDirectas($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_proveedores", "codigo_sitio", "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $r = $ca->fetch();


        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.idciudad as data,a.nombre as label
        from cu_ciudades a left join gen_sitios b on (a.codigo_pais=b.codigo_pais)
        where {$where} and a.codigo_pais = 'CO' and a.tipo_poblacion in ('D','DD') order by a.nombre";

        $ca->prepare($sql);
        $ca->exec();
        return $ca->fetchAll();
    }
}