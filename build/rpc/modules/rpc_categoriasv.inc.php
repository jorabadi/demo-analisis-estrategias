<?php
class Categoriasv {

    public static function loadPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);


        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $sql = "select a.* from ( select a.codigo_categoria,a.nombre,case when a.flags->'google_index'='1' then 'Si' else 'No' end as google_index,
		        a.estado,coalesce(a.fecha_inicial::text,'') as fecha_inicial,
		        coalesce(a.fecha_final::text,'') as fecha_final
		        from cu_categoriasv a
		        where a.codigo_categoria<>0 and codigo_proveedor=:codigo_proveedor ) a where 1=1 and ({$where})";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function loadOne($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $ca->prepareSelect("cu_categoriasv a", "a.*,a.flags->'google_index' as google_index", "codigo_categoria=:codigo_categoria");
        $ca->bindValue(":codigo_categoria", $p["codigo_categoria"], false);
        $ca->exec();
        $result = $ca->fetch();

        $campos = "distinct a.codigo_producto as data,
		          '['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca as label,
		          b.orden,a.inventario,a.estado_revision,a.estado";
		$ca->prepareSelect("matv_cu_productos_tienda a join cu_categoriasv_productos b on (a.codigo_producto=b.codigo_producto and b.codigo_categoria=:codigo_categoria)",$campos);

        $ca->bindValue(":codigo_categoria", $p["codigo_categoria"]);
        $ca->exec();
        $result["productos"] = $ca->fetchAll();

        return $result;
    }

    public static function save($p) {
        $si = session::info();
        $p["codigo_categoria"] = isset($p["codigo_categoria"]) ? $p["codigo_categoria"] : "";

        if ($p["fecha_inicial"] == "") {
            throw new JPublicException("Error, falta fecha inicial");
        }

        if ($p["fecha_final"] == "") {
            throw new JPublicException("Error, falta fecha final");
        }

        $db = JDatabase::database();
        $ca = $db->query();

        $db->transaction();
        $campos = "codigo_categoria,nombre,vpath,categoria_superior,orden,estado,tipo,
		fecha_inicial,fecha_final,meta_title,meta_description,flags,codigo_proveedor";

        //anexo google index
        $hFlags = "hstore('google_index','{$p["google_index"]}')";

        if ($p["codigo_categoria"] === "") {
            $ca->prepareInsert("cu_categoriasv", $campos);
            $codigoCategoria = $db->nextVal("cu_categoriasv_codigo_categoria");
        } else {
            $ca->prepareUpdate("cu_categoriasv", $campos, "codigo_categoria=:codigo_categoria");
            $codigoCategoria = $p["codigo_categoria"];

            $hFlags = "flags||{$hFlags}";
        }

        $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":vpath", "", true);
        $ca->bindValue(":categoria_superior", "-1", false);
        $ca->bindValue(":orden", "10000", false);
        $ca->bindValue(":estado", $p["estado"], true);
        $ca->bindValue(":tipo", "1", false); //producto
        $ca->bindValue(":fecha_inicial", $p["fecha_inicial"], true);
        $ca->bindValue(":fecha_final", $p["fecha_final"], true);
        $ca->bindValue(":meta_title", $p["meta_title"], true);
        $ca->bindValue(":meta_description", $p["meta_description"], true);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);

        //flags
        $ca->bindValue(":flags", $hFlags, false);


        $ca->exec();

        $ca->prepareDelete("cu_categoriasv_productos", "codigo_categoria=:codigo_categoria");
        $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
        $ca->exec();

        $sql = "create temporary table cu_categoriasv_productos_tmp as 
            select codigo_categoria,codigo_producto,orden from cu_categoriasv_productos limit 0";
        $ca->exec($sql);

        foreach ($p["productos"] as $r) {
            $codigoProducto = $r["data"];
            $orden = empty($r["orden"]) ? "10000" : $r["orden"];

            /* $ca->prepareSelect("cu_categoriasv_productos", "*"," codigo_categoria=:codigo_categoria and codigo_producto=:codigo_producto");
              $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
              $ca->bindValue(":codigo_producto", $codigoProducto, false);
              $ca->exec();

              if($ca->size()>0) { //en caso de que ya haya sido relacionado salta el insert y sigue con el otro registro
              continue;
              } */

            $ca->prepareInsert("cu_categoriasv_productos_tmp", "codigo_categoria,codigo_producto,orden");
            $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
            $ca->bindValue(":codigo_producto", $codigoProducto, false);
            $ca->bindValue(":orden", $orden, false);
            $ca->exec();
        }

        $sql = "insert into cu_categoriasv_productos(codigo_categoria,codigo_producto,orden)
            select distinct codigo_categoria,codigo_producto,orden from cu_categoriasv_productos_tmp";
        $ca->exec($sql);

        $db->commit();
        return;
    }

    public static function autocompleteProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre,a.referencia,a.nombre_marca,a.nombre_proveedor";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.codigo_producto as data,
            '['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca as label
            from matv_cu_productos_tienda a
            where 1=1 and {$where} and codigo_proveedor_pp=:codigo_proveedor
            order by a.nombre";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteMarcas($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "m.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select m.codigo_marca as data,m.nombre as label
            from view_cu_marcas m
            where 1=1 and {$where}
            and m.codigo_marca in (select distinct codigo_marca from matv_cu_productos_tienda p where p.codigo_proveedor_pp =:codigo_proveedor_pp)
            order by m.nombre";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteNegocios($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);

        if ($flags["flag_categorizacion"] == 0) {
            $sql = "select a.codigo_categoria as data,a.nombre as label
        			from cu_categoriasn a
        			where 1=1 and a.tipo=1 and {$where}
        			order by a.nombre";
            $ca->prepare($sql);
        }
        if ($flags["flag_categorizacion"] == 1) {
            $sql = "select a.codigo_categoria as data,a.nombre as label
        			from cu_categorias_pp a
        			where 1=1 and a.tipo=1 and {$where}
        			and a.codigo_proveedor=:codigo_proveedor
        			order by a.nombre";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        }
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteCategorias($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);



        if ($flags["flag_categorizacion"] == 0) {

            if (!empty($p["negocio"])) {
                $where .= " and a.codigo_categoria in ( select categoria_superior from cu_categoriasn where {$p["negocio"]} = any(negocios) )";
            }

            $sql = "select a.codigo_categoria as data,a.nombre as label
        			from cu_categoriasn a
        			where 1=1 and a.tipo=2 and {$where}
        			order by a.nombre";
            $ca->prepare($sql);
        }
        if ($flags["flag_categorizacion"] == 1) {

            if (!empty($p["negocio"])) {
                $where .= " and a.categoria_superior = {$p["negocio"]} ";
            }

            $sql = "select a.codigo_categoria as data,a.nombre as label
        				from cu_categorias_pp a
        				where 1=1 and a.tipo=2 and {$where}
        				and a.codigo_proveedor=:codigo_proveedor
        				order by a.nombre";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        }
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteSubcategorias($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);


        if ($flags["flag_categorizacion"] == 0) {

            if (!empty($p["negocio"])) {
                $where .= " and {$p["negocio"]} = any(a.negocios)";
            }

            if (!empty($p["categoria"])) {
                $where .= " and a.categoria_superior = {$p["categoria"]}";
            }

            $sql = "select a.nombre as label,
        			a.codigo_categoria as data
        			from cu_categoriasn a
        			join cu_categoriasn b on (a.categoria_superior=b.codigo_categoria)
        			join cu_categoriasn c on (c.codigo_categoria = any (a.negocios))
        			where 1=1 and a.estado='activo' and b.estado='activo' and c.estado='activo' and a.tipo=3 and ({$where})
        			order by c.nombre,b.nombre,a.nombre";
            $ca->prepare($sql);
        }

        if ($flags["flag_categorizacion"] == 1) {

            if (!empty($p["negocio"])) {
                $where .= "  and c.codigo_categoria = {$p["negocio"]} ";
            }

            if (!empty($p["categoria"])) {
                $where .= " and a.categoria_superior = {$p["categoria"]}";
            }


            $sql = "select a.nombre as label,a.codigo_categoria as data
 					from cu_categorias_pp a 
 					join cu_categorias_pp b on (a.categoria_superior=b.codigo_categoria)
 					join cu_categorias_pp  c on (c.tipo=1 and  b.categoria_superior=c.codigo_categoria)
 					where 1=1 
 					and  a.tipo=3
        			and {$where}
        			and a.codigo_proveedor=:codigo_proveedor
        			order by a.nombre";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        }
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteCategoriaVirtual($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre,a.codigo_categoria";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.codigo_categoria as data,a.nombre  ||' ['||a.codigo_categoria||']'  as label
             from cu_categoriasv  a
             where 1=1 
             and {$where}
             and codigo_proveedor=:codigo_proveedor
             order by a.nombre";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function loadProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        //throw new JPublicException(print_r($p,1));
        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);

        $p["codigo_proveedor"] = empty($p["codigo_proveedor"][0]) ? 0 : $p["codigo_proveedor"][0];

        $rMarcas = array();
        $rProductos = array();
        $rNegocios = array();
        $rCategorias = array();
        $rCategoriasvirtual = array();
        $rSubcategorias = array();
        $oferta = "";
        foreach ($p["marcas"] as $r) {
            $rMarcas[] = $r;
        }
        foreach ($p["productos"] as $r) {
            $rProductos[] = $r;
        }
        foreach ($p["negocios"] as $r) {
            $rNegocios[] = $r;
        }
        foreach ($p["categorias"] as $r) {
            $rCategorias[] = $r;
        }
        foreach ($p["categoria_virtual"] as $r) {
            $rCategoriasvirtual[] = $r;
        }
        foreach ($p["subcategorias"] as $r) {
            $rSubcategorias[] = $r;
        }
        if ($p["oferta"] == "si") {

            $oferta = "and a.codigo_producto in (select p.codigo_producto from  cu_productos_precios p  where  1=1  and current_timestamp < p.fechahora_final)";
        }
        if ($p["oferta"] == "no") {

            $oferta = "  and a.codigo_producto in (select p.codigo_producto from  cu_productos_precios p  where  1=1  and current_timestamp > p.fechahora_final)";
        }


        $codigosMarcas = implode(",", $rMarcas);
        $codigosProductos = implode(",", $rProductos);
        $codigosNegocios = implode(",", $rNegocios);
        $codigosCategorias = implode(",", $rCategorias);
        $codigosCategoriasvirtuales = implode(",", $rCategoriasvirtual);
        $codigosSubcategorias = implode(",", $rSubcategorias);

        $where = "";
        $where .= empty($codigosMarcas) ? "" : " and codigo_marca in (:codigos_marcas)";
        $where .= empty($codigosProductos) ? "" : " and codigo_producto in (:codigos_productos)";
        $where .= empty($p["codigo_proveedor"]) ? "" : " and codigo_proveedor=:codigo_proveedor";

        if ($flags["flag_categorizacion"] == 1) {
            $where .= empty($codigosCategorias) ? "" : " and codigo_categoria2_pp  in (:codigos_categorias)";
            $where .= empty($codigosSubcategorias) ? "" : " and codigo_categoria3_pp in (:codigos_subcategorias)";
            $where .= empty($codigosNegocios) ? "" : " and codigo_categoria1_pp = any (:codigos_negocios)";
        }
        if ($flags["flag_categorizacion"] == 0) {
            $where .= empty($codigosCategorias) ? "" : " and codigo_categoria2  in (:codigos_categorias)";
            $where .= empty($codigosNegocios) ? "" : " and negocios && :codigos_negocios";
            $where .= empty($codigosSubcategorias) ? "" : " and codigo_categoria3 in (:codigos_subcategorias)";
        }
        $where .= empty($codigosCategoriasvirtuales) ? "" : "and a.codigo_producto in (select b.codigo_producto from  cu_categoriasv_productos b where b.codigo_categoria in (:codigos_categoriasv))";



        $where .= empty($codigosProductos) ? "" : " and codigo_producto in (:codigos_productos)";



        if (empty($where)) {
            throw new JPublicException("Falta indicar elemento de seleccion de productos");
        }

        $sql = "select a.codigo_producto as data,
                    '['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca as label,
                    a.inventario,
                    a.estado_revision,
                    a.estado
                from matv_cu_productos_tienda a
                where 1=1 {$where} and a.codigo_proveedor_pp=:codigo_proveedor_pp
                group by a.codigo_producto ,
                    '['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca ,
                    a.inventario,
                    a.estado_revision,
                    a.estado
                order by '['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca ";
        $ca->prepare($sql);
        $ca->bindValue(":codigos_marcas", $codigosMarcas, false);
        $ca->bindValue(":codigos_productos", $codigosProductos, false);
        $ca->bindValue(":codigos_negocios", "{{$codigosNegocios}}", true);
        $ca->bindValue(":codigos_categorias", $codigosCategorias, false);
        $ca->bindValue(":codigos_categoriasv", $codigosCategoriasvirtuales, false);
        $ca->bindValue(":codigos_subcategorias", $codigosSubcategorias, false);
        $ca->bindValue(":codigo_proveedor", $p["codigo_proveedor"], false);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

}