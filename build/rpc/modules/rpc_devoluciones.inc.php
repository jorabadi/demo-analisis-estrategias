<?php
require_once $cfg["rutaModeloLogico"] . "mod_bonos2.inc.php";

class Devoluciones {

    public static function saveNota($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        //se validan los datos
        $iv = array();
        $iv[] = array("name" => "numero_nota", "filter" => JFILTER_VALIDATE_STRING, "min_length" => 1, "max_length" => 100, "label" => "Número de la nota", "error_msg" => 'Valor incorrecto para el campo %1$s');
        $iv[] = array("name" => "documento_aplicar", "filter" => JFILTER_VALIDATE_STRING, "min_length" => 2, "max_length" => 100, "label" => "Documento a aplicar", "error_msg" => 'Valor incorrecto para el campo %1$s');
        $iv[] = array("name" => "nombre_cliente", "filter" => JFILTER_VALIDATE_STRING, "min_length" => 2, "max_length" => 100, "label" => "Nombre del cliente", "error_msg" => 'Valor incorrecto para el campo %1$s');
        $iv[] = array("name" => "identificacion_cliente", "filter" => JFILTER_VALIDATE_STRING, "min_length" => 2, "max_length" => 100, "label" => "Identificación del cliente", "error_msg" => 'Valor incorrecto para el campo %1$s');
        $iv[] = array("name" => "direccion_cliente", "filter" => JFILTER_VALIDATE_STRING, "min_length" => 2, "max_length" => 100, "label" => "Dirección del cliente", "error_msg" => 'Valor incorrecto para el campo %1$s');
        $iv[] = array("name" => "telefono_cliente", "filter" => JFILTER_VALIDATE_STRING, "min_length" => 2, "max_length" => 100, "label" => "Teléfono del cliente", "error_msg" => 'Valor incorrecto para el campo %1$s');
        $iv[] = array("name" => "usuario_elabora", "filter" => JFILTER_VALIDATE_STRING, "min_length" => 2, "max_length" => 100, "label" => "Usuario que elabora", "error_msg" => 'Valor incorrecto para el campo %1$s');
        $iv[] = array("name" => "pin", "filter" => JFILTER_VALIDATE_STRING, "min_length" => 2, "max_length" => 100, "label" => "Pin", "error_msg" => 'Valor incorrecto para el campo %1$s');

        $validation = JInputValidator::validateVars($p, $iv, array('output' => 'string'));
        if ($validation != "") {
            throw new JInputValidatorException($validation);
        }

        $campos = "codigo_nota,numero_nota,documento_aplicar,nombre_cliente,identificacion_cliente,direccion_cliente,
        telefono_cliente,idciudad_cliente,valor_bruto,valor_iva,valor_total,usuario_elabora,usuario_autoriza,observaciones,
        codigo_proveedor_pp,pin,email_cliente";
        $db->transaction();
        if ($p["codigo_nota"] === "") {
            $ca->prepareInsert("cu_notas_credito_ventas_pp_enc", $campos);
            $codigoNota = $db->nextVal("cu_notas_credito_ventas_pp_enc_codigo_nota");
        } 
	else {
            $ca->prepareUpdate("cu_notas_credito_ventas_pp_enc", $campos, "codigo_nota=:codigo_nota");
            $codigoNota = $p["codigo_nota"];
        }

        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_nota", $codigoNota, false);
        $ca->bindValue(":numero_nota", $p["numero_nota"], true);
        $ca->bindValue(":documento_aplicar", $p["documento_aplicar"], true);
        $ca->bindValue(":nombre_cliente", $p["nombre_cliente"], true);
        $ca->bindValue(":identificacion_cliente", $p["identificacion_cliente"], true);
        $ca->bindValue(":direccion_cliente", $p["direccion_cliente"], true);
        $ca->bindValue(":telefono_cliente", $p["telefono_cliente"], true);
        $ca->bindValue(":idciudad_cliente", $p["idciudad_cliente"][0], false);
        $ca->bindValue(":valor_bruto", !empty($p["valor_bruto"]) ? $p["valor_bruto"] : 0, false);
        $ca->bindValue(":valor_iva", !empty($p["valor_iva"]) ? $p["valor_iva"] : 0, false);
        $ca->bindValue(":valor_total", !empty($p["valor_total"]) ? $p["valor_total"] : 0, false);
        $ca->bindValue(":usuario_elabora", $p["usuario_elabora"], true);
        $ca->bindValue(":usuario_autoriza", $p["usuario_autoriza"], true);
        $ca->bindValue(":observaciones", $p["observaciones"], true);
        $ca->bindValue(":pin", $p["pin"], true);
        $ca->bindValue(":email_cliente", $p["email_cliente"], true);
        $ca->exec();

        //elimino los registros del detalle de
        $ca->prepareDelete("cu_notas_credito_ventas_pp_det", "codigo_nota=:codigo_nota");
        $ca->bindValue(":codigo_nota", $codigoNota);
        $ca->exec();

        foreach ($p["detalle"] as $rDetalle) {
            $camposDetalle = "codigo_inventario,codigo_nota,concepto,descripcion,unidades,valor_unitario,valor_total";
            $ca->prepareInsert("cu_notas_credito_ventas_pp_det", $camposDetalle);
            $ca->bindValue(":codigo_nota", $codigoNota, false);
            $codigoInventario = is_array($rDetalle["codigo_inventario"]) ? json_decode(json_encode($rDetalle["codigo_inventario"])) : json_decode($rDetalle["codigo_inventario"]);
            $codigoInventario = !empty($codigoInventario) ? $codigoInventario[0]->data : "null";
            $ca->bindValue(":codigo_inventario", $codigoInventario, false);
            $ca->bindValue(":concepto", $rDetalle["concepto"], true);
            $ca->bindValue(":descripcion", $rDetalle["descripcion"], true);
            $ca->bindValue(":unidades", $rDetalle["unidades"], false);
            $ca->bindValue(":valor_unitario", $rDetalle["valor_unitario"], false);
            $ca->bindValue(":valor_total", $rDetalle["valor_total"], false);
            $ca->exec();
        }

        //busco si existe un bono para la nota
        $ca->prepareSelect("cu_bonos", "codigo_bono", "codigo_proveedor_pp=:codigo_proveedor_pp and codigo_nota=:codigo_nota");
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_nota", $codigoNota, false);
        $ca->exec();
        $tieneBonoNota = $ca->size() > 0;

        //en caso de que este clickeado generar bono y no tenga bono la nota se genera el bono
        if ($p["genera_bono"] && !$tieneBonoNota) {
            $p["codigo_nota"] = $codigoNota; //lleno el dato de la nota
            $p["email_beneficiado"] = $p["email_cliente"]; //lleno el correo al cual se le va a enviar la notificacion
            self::generarBono($p);
        }

        $db->commit();

        return;
    }

    public static function loadNotasPage($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "numero_nota,documento_aplicar,nombre_cliente,identificacion_cliente,pin";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        $sql = "select codigo_nota,numero_nota,fechahora_creacion,documento_aplicar,nombre_cliente,
                identificacion_cliente,valor_total,pin,usuario_elabora,usuario_autoriza
                from cu_notas_credito_ventas_pp_enc
                where codigo_proveedor_pp=:codigo_proveedor_pp and ({$where})";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function loadNotaOne($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $fields = "codigo_nota,numero_nota,documento_aplicar,nombre_cliente,identificacion_cliente,direccion_cliente,
        telefono_cliente,idciudad_cliente,valor_bruto,valor_iva,valor_total,usuario_elabora,usuario_autoriza,observaciones,
        codigo_proveedor_pp,pin,email_cliente";
        $ca->prepareSelect("cu_notas_credito_ventas_pp_enc", $fields, "codigo_nota=:codigo_nota");
        $ca->bindValue(":codigo_nota", $p["codigo_nota"], false);
        $ca->exec();

        $result = $ca->fetch();
        //obtengo la info de la ciudad
        $ca->prepareSelect("cu_ciudades", "nombre as label, idciudad as data", "idciudad=:idciudad");
        $ca->bindValue(":idciudad", $result["idciudad_cliente"], false);
        $ca->exec();
        $rCiudad = $ca->fetch();
        $result["idciudad_cliente"] = array(array("data" => $rCiudad["data"], "label" => $rCiudad["label"]));

        //obtengo el detalle
        $fieldsDetalle = "codigo_nota,concepto,descripcion,unidades,valor_unitario,valor_total";
        $ca->prepareSelect("cu_notas_credito_ventas_pp_det", $fieldsDetalle, "codigo_nota=:codigo_nota");
        $ca->bindValue(":codigo_nota", $p["codigo_nota"], false);
        $ca->exec();
        $result["detalle"] = $ca->fetchAll();


        /*
          //obtengo el dato de los bonos asociados a la nota
          $ca->prepareSelect("cu_bonos","codigo_bono","codigo_proveedor_pp=:codigo_proveedor_pp and codigo_nota=:codigo_nota");
          $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
          $ca->bindValue(":codigo_nota", $p["codigo_nota"], false);
          $ca->exec();
          $result["bono_generado"] = $ca->size()>0; */

        return $result;
    }

    public static function obtenerDatosPin($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $fields = "coalesce(p.nombre_cliente,'') as nombre_cliente,coalesce(p.identificacion_cliente,'') as identificacion_cliente,
        coalesce(p.email_cliente,'') as email_cliente, coalesce(p.direccion_cliente,'') as direccion_cliente,p.idciudad_cliente,
        coalesce(p.telefono_fijo_cliente,'') as telefono_cliente,coalesce(p.factura,'') as factura";
        $ca->prepareSelect("view_cu_pedidos_enc p", $fields, "p.pin=:pin and p.codigo_proveedor_pp=:codigo_proveedor_pp");
        $ca->bindValue(":pin", $p["pin"], true);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();

        if ($ca->size() == 0) {
            throw new JPublicException("No se encontraron datos para el pin.");
        }
        $result = $ca->fetch();

        $ca->prepareSelect("cu_ciudades", "nombre as label, idciudad as data", "idciudad=:idciudad");
        $ca->bindValue(":idciudad", $result["idciudad_cliente"], false);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Error consultando ciudad del cliente");
        }
        $rCiudad = $ca->fetch();
        $result["idciudad_cliente"] = array(array("data" => $rCiudad["data"], "label" => $rCiudad["label"]));

        return $result;
    }

    public static function generarBono($p) {
        $si = session::info();
        $p["valor"] = $p["valor_total"];
        $p["codigo_tipo"] = 4;
        $p["identificacion_tercero"] = $p["identificacion_cliente"];
        $p["valor_minimo_compra"] = 0;
        $p["codigo_cliente"] = null;
        $p["codigo_proveedor_pp"] = $si["codigo_proveedor"];
        $rBono = modBonos2::generar($p);
        //envio la referencia para buscar el bono
        $p["referencia"] = $rBono["referencia"];
        $p["codigo_proveedor_pp"] = $si["codigo_proveedor"];

        //envio el correo del bono
        modMails::beneficiarioNotificarBonoTienda($p);
    }

    public static function autocompleteProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["pin"])) {
            throw new JPublicException("Debe ingrear el PIN para seleccionar producto");
        }

        $sql = "select a.codigo_inventario as data,a.referencia||' / '||a.nombre||' / '||a.talla||'/'||a.color as label
			from view_cu_pedidos_det a 
			where a.pin=:pin";

        $ca->prepare($sql);
        $ca->bindValue(":pin", $p["pin"], true);
        $ca->exec();
        return $ca->fetchAll();
    }

}