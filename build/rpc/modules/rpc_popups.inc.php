<?php
class Popups {

    public static function loadOne($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $result = array();

        $ca->prepareSelect("cu_popups p 
		    join cu_ubicaciones_pp u on u.tipo_objeto = 2 and u.codigo_referencia::integer = p.codigo_popup and p.codigo_proveedor_pp = u.codigo_proveedor_pp
		    join cu_contenidos c on c.codigo_contenido = u.codigo_contenido and u.codigo_proveedor_pp = c.codigo_proveedor_pp", "p.codigo_popup,p.nombre,p.alto,p.ancho,u.codigo_pagina as pagina,p.estado,
		    p.tiempo_permanencia,c.contenido,
		    cast(u.fechahora_inicio as date) as fecha_inicial,
			cast(u.fechahora_final as date) as fecha_final,
			cast(u.fechahora_inicio as time) as hora_inicial,
			cast(u.fechahora_final as time) as hora_final", "codigo_popup=:codigo_popup");
        $ca->bindValue(":codigo_popup", $p["codigo_popup"], false);
        $ca->exec();
        $result = $ca->fetch();

        return $result;
    }

    public static function save($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        if (empty($p["nombre"])) {
            throw new JPublicException("Falta ingresar el nombre");
        }

        if (empty($p["ancho"])) {
            throw new JPublicException("Falta seleccionar el ancho");
        }

        if (empty($p["alto"])) {
            throw new JPublicException("Falta seleccionar el alto");
        }

        /* if(empty($p["pagina"])) {
          throw new JPublicException("Falta seleccionar la pagina");
          } */

        if (empty($p["contenido"])) {
            throw new JPublicException("Falta ingresar el contenido");
        }

        if (empty($p["fecha_inicial"])) {
            throw new JPublicException("Falta seleccionar la fecha inicial de la vigencia");
        }

        if (empty($p["fecha_final"])) {
            throw new JPublicException("Falta seleccionar la fecha final de la vigencia");
        }

        $db->transaction();

        /* $p["fecha_inicial"]= substr($p["fecha_inicial"],0,-14);
          $p["fecha_final"]= substr($p["fecha_final"],0,-14); */
        $duracion = !empty($p["tiempo_permanencia"]) ? $p["tiempo_permanencia"] : "null";

        $campos = "codigo_popup,nombre,ancho,alto,tiempo_permanencia,codigo_proveedor_pp,estado";
        if (empty($p["codigo_popup"])) {

            if ($p["estado"] == "activo") {
                $ca->prepareSelect("cu_popups p 
    		    join cu_ubicaciones_pp u on u.tipo_objeto = 2 and u.codigo_referencia::integer = p.codigo_popup and p.codigo_proveedor_pp = u.codigo_proveedor_pp", "codigo_popup",
    		        "u.codigo_proveedor_pp =:codigo_proveedor_pp 
    		        --and (-1 <> :codigo_pagina or u.codigo_pagina=:codigo_pagina) 
                    and u.codigo_pagina=:codigo_pagina
    		        and estado = 'activo'
                    and (cast(fechahora_inicio as timestamp),cast(fechahora_final as timestamp)) 
    		        overlaps (cast(:fechahora_inicio as timestamp),cast(:fechahora_final as timestamp))");
                $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
                $ca->bindValue(":fechahora_inicio", "{$p["fecha_inicial"]} {$p["hora_inicial"]}", true);
                $ca->bindValue(":fechahora_final", "{$p["fecha_final"]} {$p["hora_final"]}", true);
                $ca->bindValue(":codigo_pagina", !empty($p["pagina"]) ? $p["pagina"] : -1, false);
                $ca->exec();

                if ($ca->size() > 0) {
                    throw new JPublicException("Ya existe un popup vigente para la página indicada.");
                }
            }

            $codigoPopup = $db->nextVal("cu_popups_codigo_popup");
            $ca->prepareInsert("cu_popups", $campos);
        } else {

            if ($p["estado"] == "activo") {
                $ca->prepareSelect("cu_popups p
    		    join cu_ubicaciones_pp u on u.tipo_objeto = 2 and u.codigo_referencia::integer = p.codigo_popup and p.codigo_proveedor_pp = u.codigo_proveedor_pp", "codigo_popup",
    		        "p.codigo_popup<>:codigo_popup and u.codigo_proveedor_pp =:codigo_proveedor_pp 
    		        --and (-1 <> :codigo_pagina or u.codigo_pagina=:codigo_pagina)
                    and u.codigo_pagina=:codigo_pagina
    		        and estado = 'activo'
                    and (cast(fechahora_inicio as timestamp),cast(fechahora_final as timestamp))
    		        overlaps (cast(:fechahora_inicio as timestamp),cast(:fechahora_final as timestamp))");
                $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
                $ca->bindValue(":fechahora_inicio", "{$p["fecha_inicial"]} {$p["hora_inicial"]}", true);
                $ca->bindValue(":fechahora_final", "{$p["fecha_final"]} {$p["hora_final"]}", true);
                $ca->bindValue(":codigo_pagina", !empty($p["pagina"]) ? $p["pagina"] : -1, false);
                $ca->bindValue(":codigo_popup", $p["codigo_popup"], false);
                $ca->exec();

                if ($ca->size() > 0) {
                    throw new JPublicException("Ya existe un popup vigente para la página indicada.");
                }
            }

            $codigoPopup = $p["codigo_popup"];
            $ca->prepareUpdate("cu_popups", $campos, "codigo_popup=:codigo_popup and codigo_proveedor_pp=:codigo_proveedor_pp");
        }



        //cu_popups
        $ca->bindValue(":codigo_popup", $codigoPopup, false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":ancho", $p["ancho"], true);
        $ca->bindValue(":alto", $p["alto"], true);
        $ca->bindValue(":tiempo_permanencia", $duracion, false);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->bindValue(":estado", $p["estado"], true);
        $ca->exec();

        //cu_contenidos
        //busco el contenido 

        $ca->prepareSelect("cu_ubicaciones_pp", "codigo_contenido", "codigo_proveedor_pp=:codigo_proveedor_pp and codigo_referencia=:codigo_referencia
		    and tipo_objeto = 2 ");
        $ca->bindValue(":codigo_referencia", $codigoPopup, true);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        $rContenidos = $ca->fetchAll();

        if (count($rContenidos) > 0) {
            $codigoContenido = $rContenidos[0]["codigo_contenido"];
            $ca->prepareUpdate("cu_contenidos", "nombre,titulo,contenido,fecha_actualizacion", "codigo_contenido=:codigo_contenido and codigo_proveedor_pp=:codigo_proveedor_pp");
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->bindValue(":codigo_contenido", $codigoContenido, false);
            $ca->bindValue(":nombre", "contenido " . $p["nombre"], true);
            $ca->bindValue(":titulo", "contenido " . $p["nombre"], true);
            $ca->bindValue(":contenido", $p["contenido"], true);
            $ca->bindValue(":fecha_actualizacion", "current_timestamp", false);
            $ca->exec();

            $ca->prepareUpdate("cu_ubicaciones_pp", "fechahora_inicio,fechahora_final,codigo_pagina", "codigo_contenido=:codigo_contenido and codigo_proveedor_pp=:codigo_proveedor_pp and codigo_referencia=:codigo_referencia");
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->bindValue(":codigo_contenido", $codigoContenido, false);
            $ca->bindValue(":codigo_referencia", $codigoPopup, true);
            $ca->bindValue(":fechahora_inicio", "{$p["fecha_inicial"]} {$p["hora_inicial"]}", true);
            $ca->bindValue(":fechahora_final", "{$p["fecha_final"]} {$p["hora_final"]}", true);
            $ca->bindValue(":codigo_pagina", !empty($p["pagina"]) ? $p["pagina"] : -1, false);
            $ca->exec();
        } else {
            $codigoContenido = $db->nextVal("cu_contenidos_codigo_contenido");
            $ca->prepareInsert("cu_contenidos", "codigo_contenido,nombre,titulo,contenido,codigo_proveedor_pp");
            $ca->bindValue(":codigo_contenido", $codigoContenido, false);
            $ca->bindValue(":nombre", "contenido " . $p["nombre"], true);
            $ca->bindValue(":titulo", "contenido " . $p["nombre"], true);
            $ca->bindValue(":contenido", $p["contenido"], true);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();

            $camposUb = "codigo_ubicacion,codigo_pagina,codigo_referencia,fechahora_inicio,fechahora_final,
		        codigo_contenido,codigo_proveedor_pp,tipo_objeto";
            $codigoUbicacion = $db->nextVal("cu_ubicaciones_pp_codigo_ubicacion");
            $ca->prepareInsert("cu_ubicaciones_pp", $camposUb);
            $ca->bindValue(":codigo_ubicacion", $codigoUbicacion, false);
            $ca->bindValue(":codigo_pagina", !empty($p["pagina"]) ? $p["pagina"] : -1, false);
            $ca->bindValue(":codigo_referencia", $codigoPopup, true);
            $ca->bindValue(":tipo_objeto", "2", false);
            $ca->bindValue(":fechahora_inicio", "{$p["fecha_inicial"]} {$p["hora_inicial"]}", true);
            $ca->bindValue(":fechahora_final", "{$p["fecha_final"]} {$p["hora_final"]}", true);
            $ca->bindValue(":codigo_contenido", $codigoContenido, false);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();
        }



        $db->commit();

        return;
    }

    public static function loadPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "p.nombre,p.titulo";

        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        $sql = " select p.* from  (select p.codigo_popup,p.nombre,p.ancho,alto,p.tiempo_permanencia,pa.titulo,
	       u.fechahora_inicio ,u.fechahora_final,p.estado
            from cu_popups p
            join cu_ubicaciones_pp u on u.tipo_objeto = 2
                and u.codigo_referencia::integer = p.codigo_popup
                and p.codigo_proveedor_pp = u.codigo_proveedor_pp
            join cu_contenidos c on c.codigo_contenido = u.codigo_contenido
                and u.codigo_proveedor_pp = c.codigo_proveedor_pp
            left join cu_paginas pa on pa.codigo_pagina = u.codigo_pagina
            where 1=1 and u.codigo_proveedor_pp = :codigo_proveedor_pp ) p where 1=1  and ({$where})";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function loadEditRs() {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();

        //Grupos destino
        $sql = "select codigo_pagina as data,titulo as label
		from cu_paginas
		order by titulo";

        $ca->prepare($sql);
        $ca->exec();
        $result["paginas"] = $ca->fetchAll();
        array_unshift($result["paginas"], array("data" => "", "label" => "Seleccione"));

        $result["estados"] = array(
            array("data" => "activo", "label" => "Activo"),
            array("data" => "inactivo", "label" => "Inactivo")
        );

        $result["anchos"] = array(
            array("data" => "wtiny", "label" => "Muy pequeño(30%)"),
            array("data" => "wsmall", "label" => "Pequeño(40%)"),
            array("data" => "wmedium", "label" => "Mediano(60%)"),
            array("data" => "wlarge", "label" => "Largo(70%)"),
            array("data" => "wxlarge", "label" => "Extralargo(95%)"),
            array("data" => "wfull", "label" => "Full(100%)"),
            array("data" => "", "label" => "Seleccione")
        );

        $result["altos"] = array(
            array("data" => "htiny", "label" => "Muy pequeño(30%)"),
            array("data" => "hsmall", "label" => "Pequeño(40%)"),
            array("data" => "hmedium", "label" => "Mediano(60%)"),
            array("data" => "hlarge", "label" => "Largo(70%)"),
            array("data" => "hxlarge", "label" => "Extralargo(95%)"),
            array("data" => "hfull", "label" => "Full(100%)"),
            array("data" => "", "label" => "Seleccione")
        );

        //Defaults
        $result["defaults"] = array(
            "paginas" => "",
            "estados" => "activo",
            "tamanos" => ""
        );


        return $result;
    }

}