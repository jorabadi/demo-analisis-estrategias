<?php

class Transacciones {

    public static function loadPage($p){
		$si = Session::info();
        $db = JDatabase::database();
		$ca = new JDbQuery($db);

        $p["sort"] = $p["sort"] ? $p["sort"]:"fechahora_pedido desc";

        $campos="nombre_cliente,identificacion_cliente,email_cliente";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["cliente"]);

        $filter="";
        if ( !empty($p["filters"]["pin"]) ) {
            $filter .= " ped.pin=:pin and";
        }

        if ( $p["filters"]["forma_pago"] != "todos" ) {
            $filter .= " ped.codigo_forma_pago={$p["filters"]["forma_pago"]} and";
        }

        switch ($p["filters"]["estado"]) {
        	case "pagado_aprobado":
        		$filter.=" ped.estado='pagado' and estado_autorizacion='aprobado' ";
        		break;
        	case "pagado_pendiente":
        		$filter.=" ped.estado='pagado' and estado_autorizacion='pendiente' ";
        		break;
        	case "pin":
        		$filter.=" ped.estado='pin' and current_timestamp < fechahora_limite_reserva ";
        		break;
        	case "pagado_rechazado":
        		$filter.=" ped.estado_autorizacion='rechazado' ";
        		break;
        	case "carritos_creados":
        		$filter.=" ped.estado='carro' and ped.email_cliente<>'' ";
        		if ( !empty($p["filters"]["fecha_desde"]) && !empty($p["filters"]["fecha_hasta"]) ) {
        			$filter .= " and ped.fechahora::date between :desde and :hasta ";
        		}else {
        			$filter .= " and ped.fechahora::date>=current_date-30 ";
        		}
        		break;
        	default:
        		$filter.=" ped.pin<>'' ";
        }

        if ( !empty($p["filters"]["fecha_desde"]) && !empty($p["filters"]["fecha_hasta"]) && $p["filters"]["estado"]<>"carritos_creados") {
            $filter .= " and ped.fechahora_pedido::date between :desde and :hasta ";
        }

        foreach ($p["column_filters"] as $colum  => $valorCulumn){
        	if($valorCulumn != ""){
        		$where .= " and ". $ca->sqlFieldsFilters($colum,$valorCulumn);
        	}
        }


        $sql = "
        select
        	*
        from
        	(
        	select
	            ped.codigo_pedido,
	            ped.pin,
	            ped.fechahora::date::text||'<br/>'||ped.fechahora::time::text as fechahora_carro,
	            ped.fechahora_pedido::date::text||'<br/>'||ped.fechahora_pedido::time::text as fechahora_pedido,
	            ped.fechahora_limite_reserva::date::text||'<br/>'||ped.fechahora_limite_reserva::time::text as fechahora_expiracion,
	            ped.fechahora_pago::date::text||'<br/>'||ped.fechahora_pago::time::text as fechahora_pago,
	            ped.nombre_cliente,
	            ped.identificacion_cliente,
	            ped.email_cliente,
	            'F: '||ped.telefono_fijo_cliente||'<br/>C: '||ped.telefono_celular_cliente as telefono,
	            ped.nombre_destinatario,
	            ped.direccion_destinatario,
	            ped.nombre_ciudad_destinatario,
	            ped.telefono_fijo_destinatario || '<br/>' || ped.telefono_celular_destinatario as telefono_destinatario,
	            func_numfmt(ped.total_coniva,0) as total,
	            CASE
	              WHEN ped.total_bonos >=0 THEN '<b>' || '$' || '</b> ' ||
	               func_numfmt(bonos.valor, 0) ||
	               '<br>  <b> Codigo bono : </b><b style=color' || ':' || 'blue> ' ||
	                bonos.codigo_bono || '</b>' ||
	               '<br>  <b> Referencia : </b><b style=color' || ':' || 'blue> ' ||
	               bonos.referencia || '</b>' ||
	               '<br>  <b> Acuerdo : </b><b style=color' || ':' || 'blue> ' ||
	               COALESCE(bar.nombre, '') || '</b>'
             	  ELSE func_numfmt(bonos.valor, 0)::TEXT
           		END AS total_bonos,
	            ped.nombre_forma_pago,
	            ped.usuario_recaudador,
	            ped.estado,
	            ped.estado_autorizacion,
	            ped.nombre_ciudad_cliente as ciudad,
	            ped.total_transporte,
	            fp.descripcion as forma_pago_elegida,
	            (select array_to_string(array_accum('Unid: '||pd.unidades||'- Nomb :'||pd.nombre||' - Ref: '||pd.referencia||' - Talla-Color: '||pd.talla||'-'||pd.color),' <br/>') as detalle
	              from cu_pedidos_det pd where pd.codigo_pedido = ped.codigo_pedido) as detalle
        	from
        		view_cu_pedidos_enc ped
        		left join cu_formas_pago fp on (ped.forma_pago_elegida = fp.codigo_forma_pago)
        		left join cu_pedidos_bonos pb on (pb.codigo_pedido = ped.codigo_pedido)
        		left join cu_bonos bonos on (bonos.codigo_bono = pb.codigo_bono)
        		left join cu_bonos_acuerdo_redencion bar on (bar.codigo_acuerdo = bonos.codigo_acuerdo)

        	where
        		ped.codigo_proveedor_pp={$si["codigo_proveedor"]}
        		and {$filter}
			) a
        where
        	1=1
			and ({$where})
		";

        $ca->prepare($sql);
        $ca->bindValue(":pin",$p["filters"]["pin"], true);
        $ca->bindValue(":desde",$p["filters"]["fecha_desde"], true);
        $ca->bindValue(":hasta",$p["filters"]["fecha_hasta"], true);
        return $ca->execPage($p);
	}

    public static function loadListadoRs(){
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();

        //Estados
        $result["estados"] = array(
            array("label" => "Pagado y aprobado", "data" => "pagado_aprobado"),
            array("label" => "Pendiente de Pago - Vigentes", "data" => "pin"),
            array("label" => "Pagado y Pendiente de Aprobación", "data" => "pagado_pendiente"),
            array("label" => "Pagado y Rechazado", "data" => "pagado_rechazado"),
        	array("label" => "Carritos creados", "data" => "carritos_creados"),
            array("label" => "Todos", "data" => "todos")
        );

        $ca->prepareSelect("cu_formas_pago","codigo_forma_pago as data,descripcion as label","activo");
        $ca->exec();
        $result["formas_pago"] = $ca->fetchAll();
        $result["formas_pago"][] = array("label"=>"Todos","data"=>"todos");

        $result["defaults"] = array(
            "estado" => "pin",
            "forma_pago" => "todos"
        );

        return $result;
    }

    public static function exportar($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $p["sort"] = $p["sort"] ? $p["sort"]:"fechahora_pedido desc";

        $campos="nombre_cliente,identificacion_cliente,email_cliente";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["cliente"]);

        $filter="";
        if ( !empty($p["filters"]["pin"]) ) {
            $filter .= " ped.pin=:pin and";
        }

        if ( $p["filters"]["forma_pago"] != "todos" ) {
            $filter .= " ped.codigo_forma_pago={$p["filters"]["forma_pago"]} and";
        }

        switch ($p["filters"]["estado"]) {
        	case "pagado_aprobado":
        		$filter.=" ped.estado='pagado' and estado_autorizacion='aprobado' ";
        		break;
        	case "pagado_pendiente":
        		$filter.=" ped.estado='pagado' and estado_autorizacion='pendiente' ";
        		break;
        	case "pin":
        		$filter.=" ped.estado='pin' and current_timestamp < fechahora_limite_reserva ";
        		break;
        	case "pagado_rechazado":
        		$filter.=" ped.estado_autorizacion='rechazado' ";
        		break;
        	case "carritos_creados":
        		$filter.=" ped.estado='carro' and ped.email_cliente<>'' ";
        		if ( !empty($p["filters"]["fecha_desde"]) && !empty($p["filters"]["fecha_hasta"]) ) {
        			$filter .= " and ped.fechahora::date between :desde and :hasta ";
        		}else {
        			$filter .= " and ped.fechahora::date>=current_date-30 ";
        		}
        		break;
        	default:
        		$filter.=" ped.pin<>'' ";
        }

        if ( !empty($p["filters"]["fecha_desde"]) && !empty($p["filters"]["fecha_hasta"]) && $p["filters"]["estado"]<>"carritos_creados") {
            $filter .= " and (ped.fechahora_pedido::date between :desde and :hasta or ped.fechahora_pago::date between :desde and :hasta) ";
        }

        $sql = "select
        	*
                from (
                    select
                    ped.codigo_pedido,
                    ped.pin,
                    ped.fechahora_pedido,
                    ped.fechahora_limite_reserva as fechahora_expiracion,
                    ped.fechahora_pago,
                    ped.fechahora_autorizacion,
                    ped.nombre_cliente,
                    ped.identificacion_cliente,
                    ped.email_cliente,
                    'F: '||ped.telefono_fijo_cliente||' / C: '||ped.telefono_celular_cliente as telefono,
                    ped.nombre_destinatario,
                    ped.direccion_destinatario,
                    ped.telefono_fijo_destinatario || ' / ' || ped.telefono_celular_destinatario as telefono_destinatario,
                    ped.nombre_ciudad_destinatario,
                    ped.email_destinatario,
                    ped.total_coniva as total,
                    ped.total_productos+ped.total_iva as total_productos,
                    ped.total_transporte,
                    ped.total_bonos,
                    coalesce( bonos.referencia,'') as referencia,
                    coalesce(bar.nombre,'') as acuerdo,
                    ped.nombre_forma_pago,
                    ped.usuario_recaudador,
                    ped.estado,
                    ped.estado_autorizacion,
                    ped.nombre_ciudad_cliente as ciudad,
            		fp.descripcion as forma_pago_elegida,
            		(select array_to_string(array_accum('Unid: '||pd.unidades||'- Nomb :'||pd.nombre||' - Ref: '||pd.referencia||' - Talla-Color: '||pd.talla||'-'||pd.color),' \n') as detalle
	              	from cu_pedidos_det pd where pd.codigo_pedido = ped.codigo_pedido) as detalle
        	from
        		view_cu_pedidos_enc ped
        		left join cu_formas_pago fp on (ped.forma_pago_elegida = fp.codigo_forma_pago)
        		left join cu_pedidos_bonos pb on (pb.codigo_pedido = ped.codigo_pedido)
        		left join cu_bonos bonos on (bonos.codigo_bono = pb.codigo_bono)
        		left join cu_bonos_acuerdo_redencion bar on (bar.codigo_acuerdo = bonos.codigo_acuerdo)
        	where
        		ped.codigo_proveedor_pp={$si["codigo_proveedor"]}
        		and {$filter}
			) a
        where
        	1=1
			and ({$where})";

        $ca->prepare($sql);
        $ca->bindValue(":pin",$p["filters"]["pin"], true);
        $ca->bindValue(":desde",$p["filters"]["fecha_desde"], true);
        $ca->bindValue(":hasta",$p["filters"]["fecha_hasta"], true);
        $ca->exec();

        $path = JApp::privateTempPath() . "/transacciones_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
		$wb->setVersion(8);
        $ws =& $wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {

                    if ( in_array($k,array("total","total_productos","total_transporte","total_bonos")) ) {
                        $ws->writeNumber($row, $col, $v);
                        $col++;
                        continue;
                    }

                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }

    public static function exportarDetallado($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $p["sort"] = "fechahora_pedido desc";

        $campos = "nombre_cliente,identificacion_cliente,email_cliente";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["cliente"]);

        $filter = "";
        if (!empty($p["filters"]["pin"])) {
            $filter .= " pe.pin=:pin and";
        }

        if ($p["filters"]["forma_pago"] != "todos") {
            $filter .= " pe.codigo_forma_pago={$p["filters"]["forma_pago"]} and";
        }

        switch ($p["filters"]["estado"]) {
            case "pagado_aprobado":
                $filter.=" pe.estado='pagado' and estado_autorizacion='aprobado' ";
                break;
            case "pagado_pendiente":
                $filter.=" pe.estado='pagado' and estado_autorizacion='pendiente' ";
                break;
            case "pin":
                $filter.=" pe.estado='pin' and current_timestamp < fechahora_limite_reserva ";
                break;
            case "pagado_rechazado":
                $filter.=" pe.estado_autorizacion='rechazado' ";
                break;
            case "carritos_creados":
                $filter.=" pe.estado='carro' and pe.email_cliente<>'' ";
                if (!empty($p["filters"]["fecha_desde"]) && !empty($p["filters"]["fecha_hasta"])) {
                    $filter .= " and pe.fechahora::date between :desde and :hasta ";
                }
                else {
                    $filter .= " and pe.fechahora::date>=current_date-30 ";
                }
                break;
            default:
                $filter.=" pe.pin<>'' ";
        }

        if (!empty($p["filters"]["fecha_desde"]) && !empty($p["filters"]["fecha_hasta"]) && $p["filters"]["estado"] <> "carritos_creados") {
            $filter .= " and (pe.fechahora_pedido::date between :desde and :hasta or pe.fechahora_pago::date between :desde and :hasta) ";
        }

        $sql = "select
                    pe.pin,
                    pe.fechahora::date::text||'<br/>'||pe.fechahora::time::text as fechahora_carro,
                    pe.fechahora_pedido,
                    pe.fechahora_limite_reserva as fecha_hora_expiracion,
                    pe.fechahora_pago,
                    pe.fechahora_autorizacion,
                    pe.factura,
                    pe.nombres_cliente,
                    pe.apellidos_cliente,
                    pe.identificacion_cliente,
                    pe.email_cliente,
                    pe.telefono_fijo_cliente,
                    pe.telefono_celular_cliente,
                    pe.nombres_destinatario,
                    pe.apellidos_destinatario,
                    pe.direccion_destinatario,
                    pe.telefono_fijo_destinatario,
                    pe.telefono_celular_destinatario,
                    pe.nombre_ciudad_destinatario,
                    pe.email_destinatario,
                    pe.total_bonos::integer as total_bonos_pedido,
                    pe.total_iva::integer as total_iva_pedido,
                    pe.total_coniva::integer as total_coniva_pedido,
                    pe.total_productos::integer as total_productos,
                    pe.total_transporte::integer as total_transporte_pedido,
                    pe.estado,
                    pe.estado_autorizacion,
                    pe.nombre_ciudad_cliente,
                    pi.codigo_producto,
                    pd.referencia,
                    pd.nombre as producto,
                    sum(pd.unidades) as unidades,
                    pd.por_iva::integer as por_iva_producto,
                    pd.precio_venta_coniva::integer as precio_venta_coniva_producto_unitario,
                    pd.precio_venta_siniva::integer as precio_venta_siniva_producto_unitario,
                    pd.codigo_inventario,
                    pd.talla,
                    pd.color,
                    coalesce(b.referencia,'') as referencia_bono,
                    coalesce(bar.nombre,'') as acuerdo,
                    fp.descripcion as forma_pago,
                    d.fechahora::date as fecha_despacho,
		    d.fecha_recogida,
		    d.fecha_entrega,
                    d.factura_proveedor
                from
                    cu_pedidos_det as pd
	                join cu_pedidos_enc pe on pe.codigo_pedido = pd.codigo_pedido
	                join cu_productos_inventario pi on pi.codigo_inventario = pd.codigo_inventario
	                left join cu_formas_pago fp on pe.forma_pago_elegida = fp.codigo_forma_pago
	                left join cu_pedidos_bonos pb on pb.codigo_pedido = pe.codigo_pedido
	                left join cu_bonos b on b.codigo_bono = pb.codigo_bono
	                left join cu_bonos_acuerdo_redencion bar on bar.codigo_acuerdo = b.codigo_acuerdo
	                left join cu_despachos_enc d on (pe.codigo_pedido=d.codigo_pedido and d.estado='activo' and d.codigo_proveedor={$si["codigo_proveedor"]})
                where
                    pe.codigo_proveedor_pp={$si["codigo_proveedor"]}
                    and {$filter}
                group by
                    pe.pin,
                    pe.fechahora_pedido,
                    pe.fechahora_limite_reserva,
                    pe.fechahora_pago,
                    pe.fechahora_autorizacion,
                    pe.factura,
                    pe.nombres_cliente,
                    pe.apellidos_cliente,
                    pe.identificacion_cliente,
                    pe.email_cliente,
                    pe.telefono_fijo_cliente,
                    pe.telefono_celular_cliente,
                    pe.nombres_destinatario,
                    pe.apellidos_destinatario,
                    pe.direccion_destinatario,
                    pe.telefono_fijo_destinatario,
                    pe.telefono_celular_destinatario,
                    pe.nombre_ciudad_destinatario,
                    pe.email_destinatario,
                    pe.total_bonos,
                    pe.total_iva,
                    pe.total_coniva,
                    pe.total_productos,
                    pe.total_transporte,
                    pe.estado,
                    pe.estado_autorizacion,
                    pe.nombre_ciudad_cliente,
                    pi.codigo_producto,
                    pd.referencia,
                    pd.nombre,
                    pd.por_iva,
                    pd.precio_venta_coniva,
                    pd.precio_venta_siniva,
                    coalesce(b.referencia,''),
                    coalesce(bar.nombre,''),
                    fp.descripcion,
                    d.fechahora::date,
                    d.factura_proveedor,
                    d.fecha_recogida,
                    d.fecha_entrega,
                    pd.codigo_inventario,
                    pd.talla,
                    pd.color,
                    pe.fechahora::date::text||'<br/>'||pe.fechahora::time::text
                order by pin
        ";

        $ca->prepare($sql);
        $ca->bindValue(":pin", $p["filters"]["pin"], true);
        $ca->bindValue(":desde", $p["filters"]["fecha_desde"], true);
        $ca->bindValue(":hasta", $p["filters"]["fecha_hasta"], true);
        $ca->exec();

        $path = JApp::privateTempPath() . "/transacciones_detalladas_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $wb->setVersion(8);
        $ws = & $wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {
                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }

}
