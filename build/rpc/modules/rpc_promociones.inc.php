<?php
require_once $cfg["rutaModeloLogico"]."mod_tareas_programadas.inc.php";

class Promociones {

    public static function loadPage($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
		$campos = "
			nombre,
			estado_revision,
			referencia,
			marca_proveedor,
			nombre_proveedor,
			origen
		";
        
		$where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
		
        if ($p["filters"]["estado_revision"] != "todos") {
            $where .= " and a.estado_revision='{$p["filters"]["estado_revision"]}' ";
        }
        if ($p["filters"]["vigente"] != "todas") {
            $where .= ($p["filters"]["vigente"] == "1" ? " and (current_timestamp between a.fechahora_inicial and a.fechahora_final or current_timestamp < a.fechahora_inicial)" : " and current_timestamp > a.fechahora_final ");
        }
        if (isset($p["filters"]["fecha_desde"]) && $p["filters"]["fecha_desde"] != "" && isset($p["filters"]["fecha_hasta"]) && $p["filters"]["fecha_hasta"] != "") {
            $where .= " and (cast(a.fechahora_inicial as date),cast(a.fechahora_final as date)) overlaps ('{$p["filters"]["fecha_desde"]}'::date,'{$p["filters"]["fecha_hasta"]}'::date) ";
        }
        
        foreach ($p["column_filters"] as $colum  => $valorCulumn){
        	if($valorCulumn != ""){
        		$where .= " and ". $ca->sqlFieldsFilters($colum,$valorCulumn);
        	}
        }


        //se comenta el query original para que no se pierda lo de aliados
      /*  $sql = "
        select
        *
        from (


        -- trae las promociones del aliado de los productos que NO tienen precio por variacion
        SELECT min(a.idprecio) AS idprecio,
        a.codigo_producto,
        a.estado_revision,
        a.origen,
        a.fechahora_inicial::TEXT || '<br>' || a.fechahora_final::TEXT AS fechahora_inicial_final,
        func_numfmt(e.precio_coniva, 2) || '<br/>' || func_numfmt(a.precio_coniva, 2) AS precio_coniva,
        func_numfmt(e.precio_compra, 2) || '<br/>' || func_numfmt(a.precio_compra, 2) AS precio_compra,
        e.descuento_proveedor || '<br/>' || a.descuento_proveedor AS descuento_proveedor,
        b.nombre,
        b.nombre || '<br/>' || b.referencia AS nombre_referencia,
        d.nombre || '<br/>' || c.nombre AS marca_proveedor,
        b.orden_oferta AS orden,
        sum(b.inventario) AS inventario,
        b.referencia,
        c.nombre AS nombre_proveedor,
        a.fechahora_inicial,
        a.fechahora_final,
        CASE WHEN a.codigo_tipo = 2 THEN 'Coordiutil' WHEN a.codigo_tipo = 4 THEN 'P/Personalizado' WHEN a.codigo_tipo = 5 THEN 'Recuadador' END AS tipo
        FROM cu_productos_precios a
        INNER JOIN cu_productos b ON (a.codigo_producto = b.codigo_producto)
        INNER JOIN cu_proveedores c ON (b.codigo_proveedor = c.codigo_proveedor)
        INNER JOIN cu_marcas d ON (d.codigo_marca = b.codigo_marca)
        INNER JOIN cu_productos_precios e ON (
        a.codigo_producto = e.codigo_producto
        AND e.codigo_inventario = a.codigo_inventario
        AND e.codigo_tipo = (
        SELECT pa.codigo_tipo_precio
        FROM cu_proveedores_asociados_pp pa
        WHERE a.codigo_proveedor_pp = pa.codigo_proveedor
        AND c.codigo_proveedor = pa.codigo_proveedor_asociado
        )
        )
        WHERE a.codigo_proveedor_pp <> 0
        AND c.codigo_proveedor = b.codigo_proveedor
        AND a.codigo_tipo IN (2, 4)
        AND b.flags -> 'precio_por_variaciones' = '0'
        AND a.codigo_proveedor_pp=:codigo_proveedor
        GROUP BY a.codigo_producto,
        a.estado_revision,
        a.origen,
        a.fechahora_inicial::TEXT || '<br>' || a.fechahora_final::TEXT,
        e.precio_coniva,
        a.precio_coniva,
        e.precio_compra,
        a.precio_compra,
        e.descuento_proveedor || '<br/>' || a.descuento_proveedor,
        b.nombre,
        b.nombre || '<br/>' || b.referencia,
        d.nombre || '<br/>' || c.nombre,
        b.orden_oferta,
        b.referencia,
        c.nombre,
        a.fechahora_inicial,
        a.fechahora_final,
        a.codigo_tipo

        UNION ALL

        -- trae las promociones del aliado de los productos que tienen precio por variacion
        SELECT a.idprecio,
        a.codigo_producto,
        a.estado_revision,
        a.origen,
        a.fechahora_inicial::TEXT || '<br>' || a.fechahora_final::TEXT AS fechahora_inicial_final,
        func_numfmt(e.precio_coniva, 2) || '<br/>' || func_numfmt(a.precio_coniva, 2) AS precio_coniva,
        func_numfmt(e.precio_compra, 2) || '<br/>' || func_numfmt(a.precio_compra, 2) AS precio_compra,
        e.descuento_proveedor || '<br/>' || a.descuento_proveedor AS descuento_proveedor,
        b.nombre,
        b.nombre || '<br/>' || b.referencia || '<br/>' || 'Talla:' || b.talla || ' Color:' || b.color AS nombre_referencia,
        d.nombre || '<br/>' || c.nombre AS marca_proveedor,
        b.orden_oferta AS orden,
        b.inventario,
        b.referencia,
        c.nombre AS nombre_proveedor,
        a.fechahora_inicial,
        a.fechahora_final,
        CASE WHEN a.codigo_tipo = 2 THEN 'Coordiutil' WHEN a.codigo_tipo = 4 THEN 'P/Personalizado' WHEN a.codigo_tipo = 5 THEN 'Recuadador' END AS tipo
        FROM cu_productos_precios a
        INNER JOIN view_cu_productos_inventario b ON (
        a.codigo_producto = b.codigo_producto
        AND a.codigo_inventario = b.codigo_inventario
        )
        INNER JOIN cu_proveedores c ON (b.codigo_proveedor = c.codigo_proveedor)
        INNER JOIN cu_marcas d ON (d.codigo_marca = b.codigo_marca)
        INNER JOIN cu_productos_precios e ON (
        a.codigo_producto = e.codigo_producto
        AND e.codigo_inventario = b.codigo_inventario
        AND e.codigo_tipo = (
        SELECT pa.codigo_tipo_precio
        FROM cu_proveedores_asociados_pp pa
        WHERE a.codigo_proveedor_pp = pa.codigo_proveedor
        AND c.codigo_proveedor = pa.codigo_proveedor_asociado
        )
        )
        WHERE a.codigo_proveedor_pp <> 0
        AND c.codigo_proveedor = b.codigo_proveedor
        AND a.codigo_tipo IN (2, 4)
        AND b.flags -> 'precio_por_variaciones' = '1'
        AND a.codigo_proveedor_pp=:codigo_proveedor


        ) a
        where
        1=1
        and ({$where})
        ";*/

        $sql = "
			select 
				* 
			from (
				select 
					a.idprecio,
					a.codigo_producto,
					a.estado_revision,
					a.origen,
					a.fechahora_inicial::text||'<br>'||a.fechahora_final::text as fechahora_inicial_final,
					func_numfmt(e.precio_coniva,2)||'<br/>'||func_numfmt(a.precio_coniva,2) as precio_coniva,
					func_numfmt(e.precio_compra,2)||'<br/>'||func_numfmt(a.precio_compra,2) as precio_compra,
					e.descuento_proveedor||'<br/>'||a.descuento_proveedor as descuento_proveedor,
					b.nombre,
					b.nombre||'<br/>'||b.referencia||'<br/>'||'Talla:'||b.talla||' Color:'||b.color as nombre_referencia,
					d.nombre||'<br/>'||c.nombre as marca_proveedor,
					b.orden_oferta as orden,
					b.inventario,
					b.referencia,
					c.nombre as nombre_proveedor,
					a.fechahora_inicial,
					a.fechahora_final,
					case when a.codigo_tipo = 2 then 'Coordiutil'
						when a.codigo_tipo = 4 then 'P/Personalizado' end as tipo
				from 
					cu_productos_precios a
					join view_cu_productos_inventario_base b on (a.codigo_producto=b.codigo_producto and a.codigo_inventario=b.codigo_inventario)
					join cu_proveedores c on (b.codigo_proveedor=c.codigo_proveedor)
					join cu_marcas d on (d.codigo_marca=b.codigo_marca)
					join cu_productos_precios e on (a.codigo_producto=e.codigo_producto and e.codigo_inventario=b.codigo_inventario and e.codigo_tipo=(case when a.codigo_tipo=2 then 1 when a.codigo_tipo=4 then 3 end))
				where 
					1=1 
					and a.codigo_proveedor_pp=0 
					and c.codigo_proveedor=:codigo_proveedor
					and a.codigo_tipo in (2,4) 
					and b.flags->'precio_por_variaciones'='1'
				union all
				select 
					min(a.idprecio) as idprecio,
					a.codigo_producto,
					a.estado_revision,
					a.origen,
					a.fechahora_inicial::text||'<br>'||a.fechahora_final::text as fechahora_inicial_final,
					func_numfmt(e.precio_coniva,2)||'<br/>'||func_numfmt(a.precio_coniva,2) as precio_coniva,
					func_numfmt(e.precio_compra,2)||'<br/>'||func_numfmt(a.precio_compra,2) as precio_compra,
					e.descuento_proveedor||'<br/>'||a.descuento_proveedor as descuento_proveedor,
					b.nombre,
					b.nombre||'<br/>'||b.referencia as nombre_referencia,
					d.nombre||'<br/>'||c.nombre as marca_proveedor,
					b.orden_oferta as orden,
					sum(b.inventario) as inventario,
					b.referencia,
					c.nombre as nombre_proveedor,
					a.fechahora_inicial,
					a.fechahora_final,
					case when a.codigo_tipo = 2 then 'Coordiutil'
						when a.codigo_tipo = 4 then 'P/Personalizado' end as tipo
				from 
					cu_productos_precios a
					join view_cu_productos_inventario_base b on (a.codigo_producto=b.codigo_producto and a.codigo_inventario=b.codigo_inventario)
					join cu_proveedores c on (b.codigo_proveedor=c.codigo_proveedor)
					join cu_marcas d on (d.codigo_marca=b.codigo_marca)
					join cu_productos_precios e on (a.codigo_producto=e.codigo_producto and e.codigo_inventario=b.codigo_inventario and e.codigo_tipo=(case when a.codigo_tipo=2 then 1 when a.codigo_tipo=4 then 3 end))
				where 
					1=1 
					and a.codigo_proveedor_pp=0 
					and c.codigo_proveedor=:codigo_proveedor
					and a.codigo_tipo in (2,4) 
					and b.flags->'precio_por_variaciones'='0'
				group by 
					a.codigo_producto,
					a.estado_revision,
					a.origen,
					a.fechahora_inicial::text||'<br>'||a.fechahora_final::text,
					e.precio_coniva,a.precio_coniva,
					e.precio_compra,a.precio_compra,
					e.descuento_proveedor||'<br/>'||a.descuento_proveedor,
					b.nombre,
					b.nombre||'<br/>'||b.referencia,
					d.nombre||'<br/>'||c.nombre,
					b.orden_oferta,
					b.referencia,
					c.nombre,
					a.fechahora_inicial,
					a.fechahora_final,
					a.codigo_tipo


				) a
			where 
				1=1
				and ({$where}) 
		";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $result = $ca->execPage($p);
        for ($i = 0; $i < count($result["records"]); $i++) {
            $tmp = modRs::load(rs::cuProductos, $result["records"][$i]["codigo_producto"], array(rs::cuProductosImagen1));
            if (isset($tmp["imagen_1"])) {
                $result["records"][$i]["imagen_1"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }
        return $result;
    }
   
    public static function loadOne($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $sql = "
			select 
				a.idprecio,
				a.codigo_producto,
				a.fechahora_inicial,
				a.fechahora_final,
				a.precio_coniva,
				a.descuento_proveedor,
				a.descripcion,
				a.estado_revision,
				a.codigo_estado_revision,
				a.origen,
				a.tipo,
				a.codigo_tipo,
				a.codigo_recaudador,
				a.codigos_forma_pago,
				a.fechahora,
				a.codigo_proveedor_pp,
				a.codigos_franquicia,
				a.codigos_banco,
				a.last_user,
				a.valor_transporte,
				a.val_manejo_inter,
				a.codigo_inventario,
        		cast(a.fechahora_inicial as date) as fecha_inicial,
				cast(a.fechahora_final as date) as fecha_final,
				cast(a.fechahora_inicial as time) as hora_inicial,
				cast(a.fechahora_final as time) as hora_final,
				c.nombre||', '||c.referencia||' ['||c.nombre_marca||', '||c.vpath||']' as nombre_producto,
				c.moneda as moneda_l,
				c.por_iva as por_iva_l,
				b.descripcion as descripcion_estado_revision,
				d.precio_compra as precio_compra_l,
				d.precio_coniva as precio_coniva_l,
				d.descuento_proveedor as descuento_proveedor_l,
				d.precio_coniva as precio_coniva_sinpromocion,
        		d.precio_coniva as precio_compra, /* precio coniva sin promocion  que va en el con nombre precio_compra momentaniamente    */
        		c.flags->'precio_por_variaciones' as precio_por_variaciones,
        		coalesce(a.valor_transporte,0) as valor_transporte,
        		
        		f.talla,
        		f.color
			from 
				cu_productos_precios a
				join cu_estados_revision b on (a.codigo_estado_revision=b.codigo_estado)
				join view_cu_productos_base c on (a.codigo_producto=c.codigo_producto and c.codigo_proveedor=:codigo_proveedor)
        		join cu_productos_inventario f on (a.codigo_inventario=f.codigo_inventario)
        		join cu_productos_precios d on (d.codigo_producto=a.codigo_producto and d.codigo_tipo=(case when a.codigo_tipo=2 then 1 when a.codigo_tipo=4 then 3 end) and d.codigo_inventario=a.codigo_inventario)	
        	where 
				a.idprecio=:idprecio
		";

        $ca->prepare($sql);
        $ca->bindValue(":idprecio", $p["idprecio"], false);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $result = $ca->fetch();
        $result["codigo_producto"] = array( array("label" => $result["nombre_producto"], "data" => $result["codigo_producto"]));
        
        
        $tmp = modRs::load(rs::cuProductos, $result["codigo_producto"][0]["data"], array(rs::cuProductosImagen1));
        if (isset($tmp["imagen_1"])) {
        	$result["imagen_1"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 140, "h" => 110, "frm" => 1));
        }
        $logo = modRs::load(rs::cuProveedores, $si["codigo_proveedor"], array(rs::cuProveedoresLogo));
        
        foreach ($logo as $kl => $v) {
        	$result["logo"] = JDbFile::url($v["id"], array("w" => 200, "h" => 110, "frm" => 1));
        }
        if(!isset($result["logo"])){
        	$result["logo"] = "resource/com/coordiutil/proveedores/icon/Oxygen/128/coordiutil/vendes-facil-V2.png";
        }
        return $result;
    }

    public static function loadEditRs() {
        $si = Session::info();
        $result = array();
        $result["flag_tienda"] = $si["flag_tienda"];
        
        $flagsPp=  JUtils::pgsqlHStoreToArray($si['flags_pp']);
        
        if(isset($flagsPp["flag_flete_incluido_precio"])){
         
        	$result["flag_flete_incluido_precio"]=$flagsPp["flag_flete_incluido_precio"];
        }
        else{
        	$result["flag_flete_incluido_precio"]= "0";
        	
        }
        if ($si["flag_tienda"] == "1") {
            $result["codigos_tipo"] = array(
				array("data" => "2", "label" => "Coordiutil","icon"=>"com/coordiutil/proveedores/icon/Oxygen/128/coordiutil/CU.png"), 
				array("data" => "4", "label" => "Portal personalizado","icon"=>"com/coordiutil/proveedores/icon/Oxygen/128/coordiutil/VF.png")
			);
            $result["defaults"] = array("codigo_tipo" => "4");
        } else {
            $result["codigos_tipo"] = array( array("data" => "2", "label" => "Coordiutil"));
            $result["defaults"] = array("codigo_tipo" => "2");
        }
        $result["hora_inicial"] = "00:00:00";
        $result["hora_final"] = "23:59:59";
        
        return $result;
    }

    public static function save($p) {
    	
    	$si = Session::info();
        if (!in_array("FPromocionesNe_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }
        
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        if (!isset($p["codigo_producto"]["0"])) {
            throw new JPublicException("Error, falta seleccionar producto");
        }
        
        if($p['varaciones']==0){
        	
        	$promocion= array();
        	$promocion['precio_coniva_promocion']=$p['precio_coniva'];
        	$promocion['fecha_inicial']=$p['fecha_inicial'];
        	$promocion['hora_inicial']=$p['hora_inicial'];
        	$promocion['fecha_final']=$p['fecha_final'];
        	$promocion['hora_final']=$p['hora_final'];
        	$promocion['precio_compra']=$p['precio_compra'];
        	$promocion['descuento_proveedor']=$p['descuento_proveedor'];
        	$promocion['valor_transporte']=$p['valor_transporte'];
        	$promocion["descuento_cliente"] = empty($p['precios_variacion']["descuento_cliente"])?0:$p['precios_variacion']["descuento_cliente"];
        	$p['precios_variacion']['0']=$promocion;
        	
        }
        $estadoRevision = "pendiente";
		
		foreach ($p['precios_variacion'] as $promocion ){
			
			if($promocion['precio_coniva_promocion']<=0){
				
				throw new JPublicException("Error, Precio promocion  debe ser mayor a 0 ");
			}
			
			
			$promocion["codigo_inventario"] = empty($promocion["codigo_inventario"])?0:$promocion["codigo_inventario"];
			$sqlCodigoInventario = "";
			if ( !empty($promocion["codigo_inventario"]) ) {
				$sqlCodigoInventario = " and codigo_inventario=:codigo_inventario";
			}
			
	        
			/*Se valida que el precio de la promocion no sea superior al precio normal del producto*/
			/*2 y 4 son los codigos de los tipos de precios para promociones*/
	        if( in_array($p["codigo_tipo"],array(2,4)) ){
	            $sql = "
					select 
						a.precio_coniva 
					from 
						cu_productos_precios a 
					where 
						a.codigo_producto=:codigo_producto 
						and :precio_coniva>=a.precio_coniva 
						and a.codigo_tipo=:codigo_tipo
						{$sqlCodigoInventario}
				";
	            $ca->prepare($sql);
	            $ca->bindValue(":precio_coniva", $promocion["precio_coniva_promocion"], false);
	            $ca->bindValue(":codigo_producto", $p["codigo_producto"][0], false);
				$ca->bindValue(":codigo_inventario", $promocion["codigo_inventario"], false);
				/*Se resta 1 al codigo_tipo para hacer referencia al codigo_tipo del precio normal del producto*/
				$ca->bindValue(":codigo_tipo", $p["codigo_tipo"]-1, false);
	            $ca->exec();
	            if($ca->size()>0){
	                throw new JPublicException("Error, el precio de la promocion debe de ser mas bajo que el precio corriente.");
	            }
				
				/*Si es promocion de tipo 4 (de tienda VendesFacil) queda aprobada de una vez la promocion*/
				if($p["codigo_tipo"]==4){
					$estadoRevision = "aprobado";
					$promocion["descuento_proveedor"]=$promocion['descuento_cliente'];
					$promocion['precio_compra']=$promocion["precio_coniva_promocion"];
				}
				/* si la promocion es creada por unos de los 'proveedores' de novaventa aliado 457  por defecto estado_revision aprobado*/
				if($p["codigo_tipo"]==2 && in_array($si["codigo_proveedor"],array(599,600,601))){
					$estadoRevision = "aprobado";
				}
	        }
	        $db->transaction();
	        $campos = "
				fechahora_inicial,
				fechahora_final,
				precio_coniva,
				descuento_proveedor,
				descripcion,
				codigo_estado_revision,
				estado_revision,
				origen,
				precio_compra,
				codigo_tipo,
				last_user,
	        	valor_transporte
			";
			
	        if ($p["idprecio"] === "") {
	            $ca->prepareSelect(
					"cu_productos_precios", 
					"codigo_producto", 
	                "codigo_producto=:codigo_producto 
					and codigo_tipo=:codigo_tipo 
					and codigo_proveedor_pp=0
	                and (cast(fechahora_inicial as timestamp),cast(fechahora_final as timestamp)) 
						overlaps (cast(:fechahora_inicial as timestamp),cast(:fechahora_final as timestamp))
					{$sqlCodigoInventario}
					"
				);
	            $ca->bindValue(":codigo_producto", $p["codigo_producto"]["0"], false);
				$ca->bindValue(":codigo_inventario", $promocion["codigo_inventario"], false);
	            $ca->bindValue(":fechahora_inicial", "{$promocion["fecha_inicial"]} {$promocion["hora_inicial"]}", true);
	            $ca->bindValue(":fechahora_final", "{$promocion["fecha_final"]} {$promocion["hora_final"]}", true);
	            $ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
	            $ca->exec();
	            
	            $campos .= ",codigo_producto,codigo_inventario";
	            
	            $promocion["valor_transporte"] = !empty($promocion["valor_transporte"]) ? $promocion["valor_transporte"] : "0";
	            if ($ca->size() > 0) {
	                throw new JPublicException("Error, ya existe una promoción en estas fechas para el producto seleccionado.");
	            }
	            
	            if ( !empty($promocion["codigo_inventario"]) ) {
	            	$ca->prepareInsert("cu_productos_precios", $campos);
	            }else {
	            	$sqlCampos = "";
	            	$lCampos = explode(",",$campos);
	            	for ($i = 0; $i < count($lCampos); $i++) {
	            		$lCampos[$i] = trim($lCampos[$i]);
	            		/*No se incluye el codigo inventario en los campos*/
	            		if ( $lCampos[$i] == "codigo_inventario" ) continue;
	            		$sqlCampos .= ":" . $lCampos[$i] . ",";
	            	}
	            	$sqlCampos = substr(trim($sqlCampos), 0, -1);
	            	$sql = "
	            	insert into cu_productos_precios({$campos})
	            	select
		            	{$sqlCampos},
		            	a.codigo_inventario
	            	from
		            	cu_productos_inventario a
	            	where
	            		a.codigo_producto=:codigo_producto
	            	";
	            	$ca->prepare($sql);
	            }
	        } else {
				$sqlCodigoInventarioValidacion = "";
	        	if ( empty($promocion["codigo_inventario"]) ) {
	        		$sqlCodigoInventarioValidacion = "and codigo_inventario in (select codigo_inventario from cu_productos_precios where idprecio=:idprecio)";
	        	}
	            $ca->prepareSelect(
					"cu_productos_precios", 
					"codigo_producto", 
	                "idprecio<>:idprecio 
					and codigo_producto=:codigo_producto 
					{$sqlCodigoInventarioValidacion}
					and codigo_tipo=:codigo_tipo 
					and codigo_proveedor_pp=0
	                and (cast(fechahora_inicial as timestamp),cast(fechahora_final as timestamp)) 
						overlaps (cast(:fechahora_inicial as timestamp),cast(:fechahora_final as timestamp))
					{$sqlCodigoInventario}
					"
				);
	            $ca->bindValue(":codigo_producto", $p["codigo_producto"]["0"], false);
				$ca->bindValue(":codigo_inventario", $promocion["codigo_inventario"], false);
	            $ca->bindValue(":fechahora_inicial", "{$promocion["fecha_inicial"]} {$promocion["hora_inicial"]}", true);
	            $ca->bindValue(":fechahora_final", "{$promocion["fecha_final"]} {$promocion["hora_final"]}", true);
	            $ca->bindValue(":idprecio", $p["idprecio"], false);
	            $ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
	            $ca->exec();
				
	            if ($ca->size() > 0) {
	                throw new JPublicException("Error, ya existe una promoción en estas fechas para el producto seleccionado.");
	            }
	            
	            $p["valor_transporte"] = isset($p["valor_transporte"]) ? $p["valor_transporte"] : "valor_transporte";
	            
	            
				if ( !empty($promocion["codigo_inventario"]) ) {
					$ca->prepareUpdate("cu_productos_precios", $campos, "idprecio=:idprecio {$sqlCodigoInventario}");
					$ca->bindValue(":idprecio", $p["idprecio"], false);
				}else {
					$sqlCampos = "";
					$lCampos = explode(",",$campos);
					for ($i = 0; $i < count($lCampos); $i++) {
						$lCampos[$i] = trim($lCampos[$i]);
						$sqlCampos .= $lCampos[$i] . " = :" . $lCampos[$i] . ",";
					}
					$sqlCampos = substr(trim($sqlCampos), 0, -1);
					
					/*Se debe actualizar todos los registros del producto para esa promocion (todos los codigos de inventario por igual)*/
					$sql = "
						update 
							cu_productos_precios a
						set 
							{$sqlCampos}
						from 
							cu_productos_precios b
						where 
							b.idprecio=:idprecio 
							and b.codigo_producto=a.codigo_producto 
							and b.fechahora_inicial=a.fechahora_inicial
							and b.fechahora_final=a.fechahora_final 
							and b.codigo_tipo=a.codigo_tipo
					";
					$ca->prepare($sql);
					$ca->bindValue(":idprecio", $p["idprecio"], false);
				}
	        }
	       
	        $ca->bindValue(":codigo_producto", $p["codigo_producto"]["0"], false);
			$ca->bindValue(":codigo_inventario", $promocion["codigo_inventario"], false);
	        $ca->bindValue(":fechahora_inicial", "{$promocion["fecha_inicial"]} {$promocion["hora_inicial"]}", true);
	        $ca->bindValue(":fechahora_final", "{$promocion["fecha_final"]} {$promocion["hora_final"]}", true);
	        $ca->bindValue(":descuento_proveedor", $promocion["descuento_proveedor"], false);
	        $ca->bindValue(":precio_coniva", $promocion["precio_coniva_promocion"], false);
	        $ca->bindValue(":precio_compra", $promocion["precio_compra"], false);
	        $ca->bindValue(":descripcion", $p["descripcion"], true);
	        $ca->bindValue(":estado_revision", $estadoRevision, true);
	        $ca->bindValue(":codigo_estado_revision", "0", false);
	        $ca->bindValue(":origen", "proveedor", true);
	        $ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
	        $ca->bindValue(":last_user",$si["usuario"], true);
	        $ca->bindValue(":valor_transporte",$promocion["valor_transporte"], false);
	        $ca->exec();
	        
	        $db->commit();
		}
		$flags = JUtils::pgsqlHStoreToArray($si["flags"]);
		 
		if ( !empty($flags["flag_tienda_externa"]) && $flags["flag_tienda_externa"] == "1" ) {
			$sql = "update cu_productos set fechahora_sync_proveedor=null where codigo_producto=:codigo_producto";
			$ca->prepare($sql);
			$ca->bindValue(":codigo_producto", $p["codigo_producto"]['0'], false);
			$ca->exec();
		}
			
		if( !in_array($p["codigo_tipo"],array(1,3)) ){
			/*Se progrma la ejecucion de la tarea que va a actualizar la informacion de precios menores del producto una vez la promocion entre en vigencia (1 minuto despues de la fecha inicial)*/
			$fechaProgramadaInicial = DateTime::createFromFormat('Y-m-d H:i:s',"{$promocion["fecha_inicial"]} {$promocion["hora_inicial"]}")->add(date_interval_create_from_date_string('1 minute'))->format('Y-m-d H:i:s');
			$fechaProgramadaFinal = DateTime::createFromFormat('Y-m-d H:i:s',"{$promocion["fecha_final"]} {$promocion["hora_final"]}")->add(date_interval_create_from_date_string('1 minute'))->format('Y-m-d H:i:s');
		}
		if($fechaProgramadaInicial< date("Y-m-d H:i:s")){
			$fechaProgramadaInicial = date("Y-m-d H:i:s");
		}
		modTareasProgramdas::programar(
				__CLASS__,
				__METHOD__,
				"/tasks/actualizar_productos_precios_menores.php",
				array("codigo_producto"=>$p["codigo_producto"]["0"],"origen"=>"proveedores"),
				empty($fechaProgramadaInicial)?array():array("fechahora"=>$fechaProgramadaInicial,"prioridad"=>1)
				);
			
		modTareasProgramdas::programar(
				__CLASS__,
				__METHOD__,
				"/tasks/actualizar_productos_precios_menores.php",
				array("codigo_producto"=>$p["codigo_producto"]["0"],"origen"=>"proveedores"),
				empty($fechaProgramadaFinal)?array():array("fechahora"=>$fechaProgramadaFinal,"prioridad"=>1)
				);
		 
		
        
        return;
    }

    public static function autocompleteProductos($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
		
        if (empty($p["codigo_tipo"])) {
            throw new JPublicException("Error, debe seleccionar Tipo promoción.");
        }
		
        $campos = "
			a.nombre,
			a.referencia,
			a.nombre_marca
		";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
		$sql = "
			select 
				a.nombre,
				a.codigo_producto as data,
    			a.nombre||', '||a.referencia||' ['||a.nombre_marca||', '||a.vpath||']' as label,
    			a.por_iva,
    			a.moneda,
				flags->'precio_por_variaciones' as precio_por_variaciones
			from 
				view_cu_productos_base a
			where 
				1=1 
				and a.codigo_proveedor=:codigo_proveedor 
				and a.estado= 'activo'
				and {$where}
			order by 
				a.nombre
			";
		$ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
		$rProductos = $ca->fetchAll();
		
		
		foreach( $rProductos as $k => $r ) {
			
			$sql = "
					select 
					 min(pp.descuento_proveedor) as descuento_proveedor,
					func_numfmt(pp.precio_coniva, 2) as precio_venta_coniva,
					func_numfmt(min(pp.precio_compra), 2) as precio_cu_siniva,
					pp.precio_coniva as precio_coniva_sinpromocion
				from cu_productos_precios_menores ppm
					join cu_productos_precios pp on (ppm.codigo_producto=pp.codigo_producto and pp.codigo_tipo=:codigo_tipo -1 )
				where 
					ppm.codigo_tipo=pp.codigo_tipo
					and ppm.codigo_producto=:codigo_producto
				group by ppm.precio,pp.precio_coniva
			";
			$ca->prepare($sql);
			$ca->bindValue(":codigo_producto", $r["data"], false);
			$ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
			$ca->exec();
			if ( $ca->size()>0 ) {
				$res = $ca->fetch();
				$tmp = modRs::load(rs::cuProductos, $r["data"], array(rs::cuProductosImagen1));
				if (isset($tmp["imagen_1"])) {
					$res["imagen_1"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 140, "h" => 110, "frm" => 1));
				}
				$logo = modRs::load(rs::cuProveedores, $si["codigo_proveedor"], array(rs::cuProveedoresLogo));
				
				foreach ($logo as $kl => $v) {
					$res["logo"] = JDbFile::url($v["id"], array("w" => 200, "h" => 110, "frm" => 1));
				}
				
				if(!isset($res["logo"])){
					$res["logo"] = "resource/com/coordiutil/proveedores/icon/Oxygen/128/coordiutil/vendes-facil-V2.png";
				}
				
				
				
				
				$rProductos[$k] = array_merge($r,$res);
			}	
		}
		
		return $rProductos;
    }
	
	public static function autocompleteVariaciones($p) {
		$si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
		
		if (empty($p["codigo_producto"])) {
            throw new JPublicException("Error, debe seleccionar producto.");
        }
		
		$sql = "
			select 
				b.*,
				a.codigo_inventario,
				talla,
				color,
				peso,
				alto::integer as alto,
				largo::integer as largo,
				ancho::integer as ancho,
				refinventario
				
			from 
				cu_productos_inventario a
				join cu_productos_precios b on  (b.codigo_inventario=a.codigo_inventario and codigo_tipo=3)
			where 
				a.codigo_producto=:codigo_producto
				and a.estado='activo'
		";
		$ca->prepare($sql);
        $ca->bindValue(":codigo_producto", $p["codigo_producto"], false);
        $ca->exec();
        $result["varciaciones"] = $ca->fetchAll();
		return $result;
	}

	public static function exportarPromociones($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
		$campos = "
			nombre,
			estado_revision,
			referencia,
			marca_proveedor,
			nombre_proveedor,
			origen
		";
        
		$where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
		
        if ($p["filters"]["estado_revision"] != "todos") {
            $where .= " and a.estado_revision='{$p["filters"]["estado_revision"]}' ";
        }
        if ($p["filters"]["vigente"] != "todas") {
            $where .= ($p["filters"]["vigente"] == "1" ? " and (current_timestamp between a.fechahora_inicial and a.fechahora_final or current_timestamp < a.fechahora_inicial)" : " and current_timestamp > a.fechahora_final ");
        }
        if (isset($p["filters"]["fecha_desde"]) && $p["filters"]["fecha_desde"] != "" && isset($p["filters"]["fecha_hasta"]) && $p["filters"]["fecha_hasta"] != "") {
            $where .= " and (cast(a.fechahora_inicial as date),cast(a.fechahora_final as date)) overlaps ('{$p["filters"]["fecha_desde"]}'::date,'{$p["filters"]["fecha_hasta"]}'::date) ";
        }
        $sql = "
			select 
				* 
			from (
				select 
					a.idprecio,
					a.codigo_producto,
					a.estado_revision,
					a.origen,
					a.fechahora_inicial,
					a.fechahora_final,
					func_numfmt(e.precio_coniva,2) as precio_coniva_sin_promoccion,
					func_numfmt(a.precio_coniva,2) as precio_coniva_promoccion,
					func_numfmt(e.precio_compra,2) as precio_compra_coordiutil_sin_promoccion,
					func_numfmt(a.precio_compra,2) as precio_compra_coordiutil_promoccion,
					e.descuento_proveedor,
					b.nombre,
					b.nombre||'-'||b.referencia||'-'||'Talla:'||b.talla||' Color:'||b.color as nombre_referencia,
					d.nombre||'-'||c.nombre as marca_proveedor,
					b.orden_oferta as orden,
					b.inventario,
					b.referencia,
					c.nombre as nombre_proveedor,
					
					case when a.codigo_tipo = 2 then 'Coordiutil'
						when a.codigo_tipo = 4 then 'P/Personalizado' end as tipo
				from 
					cu_productos_precios a
					join view_cu_productos_inventario_base b on (a.codigo_producto=b.codigo_producto and a.codigo_inventario=b.codigo_inventario)
					join cu_proveedores c on (b.codigo_proveedor=c.codigo_proveedor)
					join cu_marcas d on (d.codigo_marca=b.codigo_marca)
					join cu_productos_precios e on (a.codigo_producto=e.codigo_producto and e.codigo_inventario=b.codigo_inventario and e.codigo_tipo=(case when a.codigo_tipo=2 then 1 when a.codigo_tipo=4 then 3 end))
				where 
					1=1 
					and a.codigo_proveedor_pp=0 
					and c.codigo_proveedor=:codigo_proveedor
					and a.codigo_tipo in (2,4) 
					and b.flags->'precio_por_variaciones'='1'
				union all
				
				select 
					min(a.idprecio) as idprecio,
					a.codigo_producto,
					a.estado_revision,
					a.origen,
					a.fechahora_inicial,
					a.fechahora_final,					
					func_numfmt(e.precio_coniva,2) as precio_coniva_sin_promoccion,
					func_numfmt(a.precio_coniva,2) as precio_coniva_promoccion,
					func_numfmt(e.precio_compra,2) as precio_compra_coordiutil_sin_promoccion,
					func_numfmt(a.precio_compra,2) as precio_compra_coordiutil_promoccion,
					e.descuento_proveedor,
					b.nombre,
					b.nombre||'-'||b.referencia as nombre_referencia,
					d.nombre||'-'||c.nombre as marca_proveedor,
					b.orden_oferta as orden,
					sum(b.inventario) as inventario,
					b.referencia,
					c.nombre as nombre_proveedor,
					case when a.codigo_tipo = 2 then 'Coordiutil'
						when a.codigo_tipo = 4 then 'P/Personalizado' end as tipo
				from 
					cu_productos_precios a
					join view_cu_productos_inventario_base b on (a.codigo_producto=b.codigo_producto and a.codigo_inventario=b.codigo_inventario)
					join cu_proveedores c on (b.codigo_proveedor=c.codigo_proveedor)
					join cu_marcas d on (d.codigo_marca=b.codigo_marca)
					join cu_productos_precios e on (a.codigo_producto=e.codigo_producto and e.codigo_inventario=b.codigo_inventario and e.codigo_tipo=(case when a.codigo_tipo=2 then 1 when a.codigo_tipo=4 then 3 end))
				where 
					1=1 
					and a.codigo_proveedor_pp=0 
					and c.codigo_proveedor=:codigo_proveedor
					and a.codigo_tipo in (2,4) 
					and b.flags->'precio_por_variaciones'='0'
				group by 
					a.codigo_producto,
					a.estado_revision,
					a.origen,
					a.fechahora_inicial::text||'<br>'||a.fechahora_final::text,
					e.precio_coniva,a.precio_coniva,
					e.precio_compra,a.precio_compra,
					e.descuento_proveedor||'-'||a.descuento_proveedor,
					b.nombre,
					b.nombre||'-'||b.referencia,
					d.nombre||'-'||c.nombre,
					b.orden_oferta,
					b.referencia,
					c.nombre,
					a.fechahora_inicial,
					a.fechahora_final,
					a.codigo_tipo,
					e.descuento_proveedor
					
					
					
 UNION ALL
			
                -- trae las promociones del aliado de los productos que NO tienen precio por variacion
                    SELECT min(a.idprecio) AS idprecio,
                        a.codigo_producto,
                        a.estado_revision,
                        a.origen,
                        a.fechahora_inicial,
                        a.fechahora_final,                        
                        func_numfmt(e.precio_coniva,2) as precio_coniva_sin_promoccion,
						func_numfmt(a.precio_coniva,2) as precio_coniva_promoccion,
						func_numfmt(e.precio_compra,2) as precio_compra_coordiutil_sin_promoccion,
						func_numfmt(a.precio_compra,2) as precio_compra_coordiutil_promoccion,
						e.descuento_proveedor,
                        b.nombre,
                        b.nombre || '-' || b.referencia AS nombre_referencia,
                        d.nombre || '-' || c.nombre AS marca_proveedor,
                        b.orden_oferta AS orden,
                        sum(b.inventario) AS inventario,
                        b.referencia,
                        c.nombre AS nombre_proveedor,
                        CASE WHEN a.codigo_tipo = 2 THEN 'Coordiutil' WHEN a.codigo_tipo = 4 THEN 'P/Personalizado' WHEN a.codigo_tipo = 5 THEN 'Recuadador' END AS tipo
                    FROM cu_productos_precios a
                    INNER JOIN cu_productos b ON (a.codigo_producto = b.codigo_producto)
                    INNER JOIN cu_proveedores c ON (b.codigo_proveedor = c.codigo_proveedor)
                    INNER JOIN cu_marcas d ON (d.codigo_marca = b.codigo_marca)
                    INNER JOIN cu_productos_precios e ON (
                        a.codigo_producto = e.codigo_producto
                        AND e.codigo_inventario = a.codigo_inventario
                        AND e.codigo_tipo = (
                            SELECT pa.codigo_tipo_precio
                            FROM cu_proveedores_asociados_pp pa
                            WHERE a.codigo_proveedor_pp = pa.codigo_proveedor
                            AND c.codigo_proveedor = pa.codigo_proveedor_asociado
                        )
                    )
                    WHERE a.codigo_proveedor_pp <> 0
                        AND c.codigo_proveedor = b.codigo_proveedor
                        AND a.codigo_tipo IN (2, 4)
                        AND b.flags -> 'precio_por_variaciones' = '0'
                        AND a.codigo_proveedor_pp=:codigo_proveedor
                    GROUP BY a.codigo_producto,
                        a.estado_revision,
                        a.origen,
                        a.fechahora_inicial::TEXT || '<br>' || a.fechahora_final::TEXT,
                        e.precio_coniva,
                        a.precio_coniva,
                        e.precio_compra,
                        a.precio_compra,
                        e.descuento_proveedor || '-' || a.descuento_proveedor,
                        b.nombre,
                        b.nombre || '-' || b.referencia,
                        d.nombre || '-' || c.nombre,
                        b.orden_oferta,
                        b.referencia,
                        c.nombre,
                        a.fechahora_inicial,
                        a.fechahora_final,
                        a.codigo_tipo,
                        e.descuento_proveedor
				
                    UNION ALL
				
                -- trae las promociones del aliado de los productos que tienen precio por variacion
                    SELECT a.idprecio,
                        a.codigo_producto,
                        a.estado_revision,
                        a.origen,
                        a.fechahora_inicial,
                        a.fechahora_final,                        
                        func_numfmt(e.precio_coniva,2) as precio_coniva_sin_promoccion,
						func_numfmt(a.precio_coniva,2) as precio_coniva_promoccion,
						func_numfmt(e.precio_compra,2) as precio_compra_coordiutil_sin_promoccion,
						func_numfmt(a.precio_compra,2) as precio_compra_coordiutil_promoccion,
						e.descuento_proveedor,
                        b.nombre,
                        b.nombre || '-' || b.referencia || '-' || 'Talla:' || b.talla || ' Color:' || b.color AS nombre_referencia,
                        d.nombre || '-' || c.nombre AS marca_proveedor,
                        b.orden_oferta AS orden,
                        b.inventario,
                        b.referencia,
                        c.nombre AS nombre_proveedor,
                        CASE WHEN a.codigo_tipo = 2 THEN 'Coordiutil' WHEN a.codigo_tipo = 4 THEN 'P/Personalizado' WHEN a.codigo_tipo = 5 THEN 'Recuadador' END AS tipo
                    FROM cu_productos_precios a
                    INNER JOIN view_cu_productos_inventario b ON (
                        a.codigo_producto = b.codigo_producto
                        AND a.codigo_inventario = b.codigo_inventario
                    )
                    INNER JOIN cu_proveedores c ON (b.codigo_proveedor = c.codigo_proveedor)
                    INNER JOIN cu_marcas d ON (d.codigo_marca = b.codigo_marca)
                    INNER JOIN cu_productos_precios e ON (
                        a.codigo_producto = e.codigo_producto
                        AND e.codigo_inventario = b.codigo_inventario
                        AND e.codigo_tipo = (
                            SELECT pa.codigo_tipo_precio
                            FROM cu_proveedores_asociados_pp pa
                            WHERE a.codigo_proveedor_pp = pa.codigo_proveedor
                            AND c.codigo_proveedor = pa.codigo_proveedor_asociado
                        )
                    )
                    WHERE a.codigo_proveedor_pp <> 0
                        AND c.codigo_proveedor = b.codigo_proveedor
                        AND a.codigo_tipo IN (2, 4)
                        AND b.flags -> 'precio_por_variaciones' = '1'
                        AND a.codigo_proveedor_pp=:codigo_proveedor
                       
					
				) a
			where 
				1=1
				and ({$where}) 
		";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
       // throw new JPublicException(print_r($ca->preparedQuery()));
        $ca->exec();
        
        $path = JApp::privateTempPath() . "/promociones_{$si["codigo_proveedor"]}.xls";
         
         $wb = new Spreadsheet_Excel_Writer_Workbook($path);
         $wb->setVersion(8);
         $ws =& $wb->addWorksheet('Hoja1');

         if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
             $row = 0;
             foreach ($ca->fetchAll() as $r) {
                    $col = 0;
                    if ($row == 0) {
                            foreach ($r as $k => $v) {
                                    $k = str_replace("_", " ", $k);
                                    $k = ucfirst($k);

                                    $ws->writeString($row, $col, $k);
                                    $col++;
                            }
                            $row += 1;
                            $col = 0;
                    }

                    foreach ($r as $k => $v) {

                            if ( in_array($k,array("")) ) {
                                    $ws->writeNumber($row, $col, $v);
                                    $col++;
                                    continue;
                            }

                            $ws->writeString($row, $col, utf8_decode($v));
                            $col++;
                    }
                    $row++;
             }  
        }

         $wb->close();
         return basename($path);
	}
}