<?php

class CategoriasPp {

    public static function load($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $rCategorias = array();

        $sql = ("select c.* from (
            select c1.codigo_categoria,c1.nombre,c1.vpath,c1.tipo,
            0 as codigo_categoria2, '' as nombre_categoria2, '' as vpath2,
            0 as codigo_categoria3, '' as nombre_categoria3, '' as vpath3
            from cu_categorias_pp  c1
            where c1.codigo_proveedor = :codigo_proveedor and c1.tipo = 0
            union all
            --nivel 1
            select c1.codigo_categoria,c1.nombre,c1.vpath,c1.tipo,
            0 as codigo_categoria2, '' as nombre_categoria2, '' as vpath2,
            0 as codigo_categoria3, '' as nombre_categoria3, '' as vpath3
            from cu_categorias_pp  c1
            where c1.codigo_proveedor = :codigo_proveedor and c1.tipo = 1
            union all
            select c1.codigo_categoria,c1.nombre,c1.vpath,c2.tipo,
            c2.codigo_categoria as codigo_categoria2,c2.nombre as nombre_categoria2,c2.vpath as vpath2,
            0 as codigo_categoria3, '' as nombre_categoria3, '' as vpath3
            from cu_categorias_pp  c1
            join cu_categorias_pp  c2 on c2.categoria_superior = c1.codigo_categoria and c2.tipo = 2
            where c1.codigo_proveedor = :codigo_proveedor and c1.tipo = 1
            union all
            select c1.codigo_categoria,c1.nombre,c1.vpath,c3.tipo,
            c2.codigo_categoria as codigo_categoria2,c2.nombre as nombre_categoria2,c2.vpath as vpath2,
            c3.codigo_categoria as codigo_categoria3,c3.nombre as nombre_categoria3,c3.vpath as vpath3
            from cu_categorias_pp  c1
            join cu_categorias_pp  c2 on c2.categoria_superior = c1.codigo_categoria and c2.tipo = 2
            join cu_categorias_pp  c3 on c3.categoria_superior = c2.codigo_categoria and c3.tipo = 3
            where c1.codigo_proveedor = :codigo_proveedor and c1.tipo = 1
            )t
            join cu_categorias_pp c on case when t.tipo = 1 then c.codigo_categoria = t.codigo_categoria
            when t.tipo=2 then c.codigo_categoria = t.codigo_categoria2
            when t.tipo=3 then c.codigo_categoria = t.codigo_categoria3
            when t.tipo=0 then c.codigo_categoria = t.codigo_categoria
            end
            order by t.codigo_categoria ,t.codigo_categoria2,t.codigo_categoria3");

        $ca->prepare($sql);

        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $rCategorias = $ca->fetchAll();

        return $rCategorias;

    }

    public static function save($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = $db->query();

        $rCategorias = array();
        $raiz = 1;
        foreach ($p[$raiz] as $k1 => $v1) {
            if ($v1 === null) {
                continue;
            }
            if (!is_array($v1)) {
                $rCategorias[$raiz][$k1] = $v1;
            } else {
                foreach ($v1 as $k2 => $v2) {
                    if ($v2 === null) {
                        continue;
                    }
                    if (!is_array($v2)) {
                        $rCategorias[$k1][$k2] = $v2;
                    } else {
                        foreach ($v2 as $k3 => $v3) {
                            if ($v3 === null) {
                                continue;
                            }
                            if (!is_array($v3)) {
                                $rCategorias[$k2][$k3] = $v3;
                            } else {
                                foreach ($v3 as $k4 => $v4) {
                                    if ($v4 === null) {
                                        continue;
                                    }
                                    $rCategorias[$k3][$k4] = $v4;
                                }
                            }
                        }
                    }
                }
            }
        }

        $db->transaction();
        $campos = "codigo_proveedor,codigo_categoria,nombre,categoria_superior,tipo,orden,meta_description,meta_title,meta_keywords";
        //for ($i=1; $i <= count($rCategorias); $i++) {
        //$i = 0;

        $rCategorias[1]["categoria_superior"] = null;

        if (empty($rCategorias[1]["codigo_categoria"])) {
            $ca->prepareInsert("cu_categorias_pp", $campos);
            $codigoCategoria = $db->nextVal("cu_categorias_pp_codigo_categoria");
            $rCategorias[1]["codigo_categoria"] = $codigoCategoria;
            $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":nombre", "/", true);
            $ca->bindValue(":categoria_superior", "null", false);
            $ca->bindValue(":tipo", "0", false);
            $ca->bindValue(":orden", "0", false);
            $ca->bindValue(":meta_description", "", true);
            $ca->bindValue(":meta_title", "", true);
            $ca->bindValue(":meta_keywords", "", true);

            $ca->exec();

        }

        //foreach ($rCategorias as $r) {
        $ind = 2;
        for ($i = 2; $i <= count($rCategorias); $i++) {
            if (!isset($rCategorias[$ind])) {
                $ind++;
                $i--;
                continue;
            }
            $r = $rCategorias[$ind];
            if (empty($r["categoria_superior"])) {
                $r["categoria_superior"] = $rCategorias[$r["node_parent"]]["codigo_categoria"];
            }
            if (empty($r["codigo_categoria"])) {
                $ca->prepareInsert("cu_categorias_pp", $campos);
                $codigoCategoria = $db->nextVal("cu_categorias_pp_codigo_categoria");
                $rCategorias[$r["node_id"]]["codigo_categoria"] = $codigoCategoria;

            } else {
                $ca->prepareUpdate("cu_categorias_pp", $campos, "codigo_categoria=:codigo_categoria");
                $codigoCategoria = $r["codigo_categoria"];

            }
            $r["orden"] = empty($r["orden"])?"0":$r["orden"];
            $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":nombre", $r["nombre"], true);
            $ca->bindValue(":categoria_superior", $r["categoria_superior"], false);
            $ca->bindValue(":tipo", $r["tipo"], false);
            $ca->bindValue(":orden", $r["orden"], false);
            $ca->bindValue(":meta_description", !empty($r["meta_description"]) ? $r["meta_description"] : "", true);
            $ca->bindValue(":meta_title", !empty($r["meta_title"]) ? $r["meta_title"] : "", true);
            $ca->bindValue(":meta_keywords", !empty($r["meta_keywords"]) ? $r["meta_keywords"] : "", true);

            $ca->exec();

            $ind++;

        }
        $sql = "select func_categorias_pp_actualizar({$si["codigo_proveedor"]})";
        $ca->exec($sql);

        //modRs::save(rs::cuCategorias, $codigoCategoria, rs::cuCategoriasImagen1, $p["imagen1"]);
        $db->commit();
        return;
    }

    public static function autocomplete($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = $db->query();

        $campos = "a.vpath";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.codigo_categoria as data,a.vpath as label
            from cu_categorias_pp a
            where a.codigo_proveedor=:codigo_proveedor and a.tipo = 3  and {$where} order by a.vpath";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteAllCategories($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = $db->query();

        $campos = "a.vpath";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.codigo_categoria as data,a.vpath as label
            from cu_categorias_pp a
            where a.codigo_proveedor=:codigo_proveedor and a.tipo in (3,2,1)  and {$where} order by a.vpath";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function eliminarCategoria($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = $db->query();

        $codigoCategoria = $p["codigo_categoria"];

        $sql = "SELECT c.tipo,c.codigo_categoria from cu_categorias_pp c WHERE c.codigo_categoria=:codigo_categoria and codigo_proveedor=:codigo_proveedor";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Error");
        }

        $categoriaActual = $ca->fetch();
        $db->transaction();
        self::eliminarArbolCategoria($categoriaActual);
        $db->commit();

    }

    public static function eliminarArbolCategoria($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = $db->query();
        $codigoCategoria = $p["codigo_categoria"];
        if ($p["tipo"] != "3") {
            $sql = "SELECT c.tipo,c.codigo_categoria from cu_categorias_pp c WHERE categoria_superior=:codigo_categoria ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
            $ca->exec();
            foreach ($ca->fetchAll() as $rCategoriaHijo) {
                self::eliminarArbolCategoria(array("codigo_categoria" => $rCategoriaHijo["codigo_categoria"], "tipo" => $rCategoriaHijo["tipo"]));
            }
        }

        $sql = "update cu_productos p  set codigo_categoria_pp = null where p.codigo_categoria_pp=:codigo_categoria and p.codigo_proveedor=:codigo_proveedor";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $sql = "delete from cu_categorias_pp c where c.codigo_categoria=:codigo_categoria  and codigo_proveedor=:codigo_proveedor ";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_categoria", $codigoCategoria, false);
        $ca->exec();

    }

}