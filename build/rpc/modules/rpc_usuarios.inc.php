<?php
require_once $cfg["rutaModeloLogico"] . "mod_utils.inc.php";

class Usuarios {

    public static function loadAll($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "usuario";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if (!empty($p["column_filters"])) {
            foreach ($p["column_filters"] as $colum => $valorCulumn) {
                if ($valorCulumn != "") {
                    $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
                }
            }
        }

        if ($p['filters']['estado'] != "todos") {
            $where .= " and estado='{$p['filters']['estado']}' ";
        }

        $sql = "select usuario,clave,nombre,estado from
            cu_proveedores_usuarios
            where codigo_proveedor=:codigo_proveedor and primario=false and {$where} ";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function save($p) {
        $si = session::info();
        if (!in_array("FUsuariosNe_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }
        $tmp = modUtils::validarClave($p["clave"]);
        if ($tmp !== true) {
            throw new JPublicException($tmp);
        }

        if (strpos($p["usuario"], ".") !== false) {
            throw new JPublicException("El usuario no puede contener puntos");
        }

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "codigo_proveedor,usuario,clave,nombre,primario,permisos";

        $arrUsuario = explode(".", $si["usuario"]);
        $usuario = "{$arrUsuario[0]}.{$p["usuario"]}";

        $rPermisos = array();
        foreach ($p["permisos"] as $r) {
            if ($r["data"] == "1") {
                $rPermisos[] = $r["codigo"];
            }
        }

        if ($p["codigo_usuario"] === "") {
            $ca->prepareInsert("cu_proveedores_usuarios", $campos);
        } else {
            $ca->prepareUpdate("cu_proveedores_usuarios", $campos, "codigo_proveedor=:codigo_proveedor and usuario=:codigo_usuario");

            $ca->bindValue(":codigo_usuario", "{$usuario}", true);
        }

        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);

        //   throw new JPublicException(print_r($usuario,1));

        $ca->bindValue(":usuario", $usuario, true);
        $ca->bindValue(":clave", $p["clave"], true);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":primario", "false", true);
        $ca->bindValue(":permisos", implode(",", $rPermisos), true);
        $db->transaction();
        $ca->exec();
        $db->commit();
    }

    public static function loadEditRs() {
        $si = session::info();
        $result = array();

        $refl = new ReflectionClass('ProveedoresAcl');

        /* $result["keys"] = array_keys($refl->getConstants());
          $result["assoc"] = $refl->getConstants(); */

        $keys = array_keys($refl->getConstants());
        $assocs = $refl->getConstants();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $ca->prepareSelect("cu_proveedores_usuarios", "permisos", "codigo_proveedor=:codigo_proveedor and usuario=:usuario");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->bindValue(":usuario", $si["usuario"], true);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Usuario no localizado");
        }

        $arrPermisos = $ca->fetch();
        $permisos = explode(",", $arrPermisos["permisos"]);


        $result = array();
        $keysTemp = array();
        $assocTemp = array();
        $i = 0;
        if (!empty($arrPermisos["permisos"])) {
            foreach ($keys as $key) {
                $j = 0;
                foreach ($permisos as $permiso) {
                    if ($permisos[$j] == $key) {
                        $keysTemp[] = $key;
                        $assocTemp[$key] = $assocs[$key];
                        break;
                    }
                    $j++;
                }
                $i++;
            }
        } else {
            $keysTemp = $keys;
            $assocTemp = $assocs;
        }

        $result["keys"] = $keysTemp;
        $result["assoc"] = $assocTemp;

        return $result;
    }

    public static function loadRsRestriciones($p) {
        $si = session::info();
        $restricciones = array();
        $usuarios = self::loadAll($p);
        $restricciones = $si['restricciones_paquetes'][get_class()];

        return array("restricciones" => $restricciones, "data" => array("cantidad_usuarios_registrados" => $usuarios['recordCount']));
    }

    public static function load($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_proveedores_usuarios", "*", "codigo_proveedor=:codigo_proveedor and usuario=:usuario");

        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->bindValue(":usuario", $p["usuario"], true);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Usuario no localizado");
        }

        $result = $ca->fetch();
        $result["usuario"] = explode(".", $result["usuario"]);
        $result["usuario"] = $result["usuario"][1];
        $result["permisos"] = explode(",", $result["permisos"]);

        return $result;
    }

    public static function delete($p) {
        //   throw new JPublicException(print_r($p,1));
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $db->transaction();

        $sql = "delete from cu_proveedores_usuarios
            where codigo_proveedor=:codigo_proveedor and usuario=:usuario";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->bindValue(":usuario", $p["usuario"], true);

        $ca->exec();

        $db->commit();

        //    JJSonRpcServer::sucess("Registro eliminado correctamente");
    }

    public static function cambiarClave($p) {
        //   throw new JPublicException(print_r($p,1));
        $si = session::info();

        if ($p["clave_nueva1"] != $p["clave_nueva2"]) {
            throw new JPublicException("La nueva clave y su confirmación no coinciden");
        }

        $tmp = modUtils::validarClave($p["clave_nueva1"]);
        if ($tmp !== true) {
            throw new JPublicException($tmp);
        }

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_proveedores_usuarios", "*", "usuario=:usuario");
        $ca->bindValue(":usuario", $si["usuario"], true);
        $ca->exec();

        if ($ca->size() == 0) {
            throw new JPublicException("Usuario invalido");
        }

        $r = $ca->fetch();
        if ($r["clave"] != $p["clave_actual"]) {
            throw new JPublicException("Clave actual invalida");
        }

        $ca->prepareUpdate("cu_proveedores_usuarios", "clave,nueva_clave", "usuario=:usuario  and codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":usuario", $si["usuario"], true);
        $ca->bindValue(":clave", $p["clave_nueva1"], true);
        $ca->bindValue(":nueva_clave", "0", true);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);

        $db->transaction();
        $ca->exec();
        $db->commit();
    }

    public static function inactivar($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "update cu_proveedores_usuarios set estado='inactivo'
            where codigo_proveedor=:codigo_proveedor and usuario=:usuario";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":usuario", $p["usuario"], true);

        $db->transaction();
        $ca->exec();
        $db->commit();
    }

}