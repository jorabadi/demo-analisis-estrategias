<?php

class Contenidos{

    public static function loadPage($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $campos = "codigo_contenido,nombre,titulo,ubicaciones";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }
        
        $sql = " select * from ( 
                        select     a. codigo_contenido,
                                a.nombre,
                                a.titulo,
                                cast(a.fecha_creacion as date) as fecha_creacion,
                    array_to_string(array_agg(distinct(c.titulo)),', ') as ubicaciones 
                                from cu_contenidos a 
                    left join cu_ubicaciones_pp b on (b.codigo_contenido= a.codigo_contenido)
                    left join cu_paginas c on (b.codigo_pagina = c.codigo_pagina)
                    where 1=1 and a.codigo_proveedor_pp=:codigo_proveedor
                    group by a. codigo_contenido,a.nombre,a.titulo,a.fecha_creacion
                    ) tbl where 1=1  and {$where}
                ";
        
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $result = $ca->execPage($p);
        
        return $result;
    }

    public static function loadOne($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $sql = "select
                    nombre,
                    titulo,
                    descripcion_seo,
                    contenido		
                from cu_contenidos where codigo_contenido = :codigo_contenido
                    and codigo_proveedor_pp=:codigo_proveedor";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->bindValue(":codigo_contenido", $p["codigo_contenido"]);
        $ca->exec();
        $result = $ca->fetch();
        
        $sql = "select
                    codigo_contenido,
                    codigo_pagina,
                    codigo_proveedor_pp,
                    codigo_referencia,
                    codigo_ubicacion,
                    seccion_ubicacion,
                    cast(fechahora_inicio as date) as fecha_inicio,
                    cast(fechahora_final as date) as fecha_final,
                    cast(fechahora_inicio as time) as hora_inicio,
                    cast(fechahora_final as time) as hora_final,
                    orden
                from cu_ubicaciones_pp where codigo_contenido = :codigo_contenido
                    and codigo_proveedor_pp=:codigo_proveedor		
                ";
        
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->bindValue(":codigo_contenido", $p["codigo_contenido"]);
        $ca->exec();
        $rUbicaciones = $ca->fetchAll();
        $flagsProv = JUtils::pgsqlHStoreToArray($si["flags"]);
        
        foreach ($rUbicaciones as $ubicaciones) {
            /*
             * tipo pagina ubicacion
             * 47 = negocio
             * 48 = marca
             * 37 = categoria virtual
             * 53 => correos
             */
            
            if ($ubicaciones["codigo_pagina"] != "1" && $ubicaciones["codigo_referencia"] != "0" && $ubicaciones["codigo_pagina"] != "53") {
                
                if ($ubicaciones["codigo_pagina"] == 47) {
                    if ($flagsProv["flag_categorizacion"] == 0) {
                        $sql = "select
                                                          p.codigo_categoria as data,
                                                          p.nombre as label
                                                 from cu_categoriasn p where p.estado = 'activo'
                                                      and codigo_categoria=:codigo_categoria ";
                    } else {
                        $sql = "select
                                                          p.codigo_categoria as data,
                                                          p.nombre as label
                                                 from cu_categorias_pp p where codigo_proveedor=:codigo_proveedor
                                                      and codigo_categoria=:codigo_categoria ";
                    }
                    
                    $ca->prepare($sql);
                    $ca->bindValue(":codigo_categoria", $ubicaciones["codigo_referencia"]);
                } elseif ($ubicaciones["codigo_pagina"] == 48) {
                    $sql = "select
                                                          p.codigo_marca as data,
                                                          p.nombre as label
                                                 from cu_marcas p
                                                          join cu_proveedores_marcas pp on (pp.codigo_marca=p.codigo_marca)
                                                          where pp.codigo_proveedor=:codigo_proveedor 
                                                          and p.codigo_marca=:codigo_marca";
                    
                    $ca->prepare($sql);
                    $ca->bindValue(":codigo_marca", $ubicaciones["codigo_referencia"]);
                } elseif ($ubicaciones["codigo_pagina"] == 37) {
                    $sql = "select
                                                          p.nombre as label ,
                                                          p.codigo_categoria as data
                                                 from cu_categoriasv p
                                                          where p.codigo_proveedor =:codigo_proveedor
                                                          and codigo_categoria=:codigo_categoria ";
                    
                    $ca->prepare($sql);
                    $ca->bindValue(":codigo_categoria", $ubicaciones["codigo_referencia"]);
                }
                
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
                $ca->exec();
                $ubicaciones["codigo_referencia"] = $ca->fetchall();
            } elseif ($ubicaciones["codigo_pagina"] == "1") {
                $ubicaciones["codigo_referencia"] = array(
                    array(
                        "data" => 0,
                        "label" => "Home"
                    )
                );
            } elseif ($ubicaciones["codigo_referencia"] == "0") {
                $ubicaciones["codigo_referencia"] = array(
                    array(
                        "data" => 0,
                        "label" => ""
                    )
                );
            } elseif ($ubicaciones["codigo_pagina"] == "53") { // correos
                $correos = array(
                    "cliente_notificar_nuevo_registro" => array(
                        "label" => "Nuevo registro cliente",
                        "data" => "cliente_notificar_nuevo_registro"
                    ),
                    "cliente_notificacion_actualizacion_datos" => array(
                        "label" => "Actualizacion de datos cliente",
                        "data" => "cliente_notificacion_actualizacion_datos"
                    ),
                    "cliente_recuperar_clave" => array(
                        "label" => "Recuperar clave",
                        "data" => "cliente_recuperar_clave"
                    ),
                    "cliente_notificar_aprobacion_pin_pce" => array(
                        "label" => "Aprobacion pines contraentrega",
                        "data" => "cliente_notificar_aprobacion_pin_pce"
                    ),
                    "cliente_notificar_mediacion_servicio" => array(
                        "label" => "Mediación comentarios",
                        "data" => "cliente_notificar_mediacion_servicio"
                    ),
                    "cliente_notificar_pago_rechazado" => array(
                        "label" => "Pago rechazado",
                        "data" => "cliente_notificar_pago_rechazado"
                    ),
                    "cliente_notificar_pin_pagado" => array(
                        "label" => "Pin pagado aprobado",
                        "data" => "cliente_notificar_pin_pagado"
                    ),
                    "cliente_notificar_pin_pagado_pendiente" => array(
                        "label" => "Pin pagado pendiente",
                        "data" => "cliente_notificar_pin_pagado_pendiente"
                    ),
                    "cliente_notificar_pin_pce_despachado" => array(
                        "label" => "Notificacion transporte pines contraentrega",
                        "data" => "cliente_notificar_pin_pce_despachado"
                    ),
                    "cliente_notificar_tracking" => array(
                        "label" => "Notificacion transporte pines",
                        "data" => "cliente_notificar_tracking"
                    ),
                    "cliente_notificar_pin_reservado" => array(
                        "label" => "Pines reservados",
                        "data" => "cliente_notificar_pin_reservado"
                    ),
                    "cliente_notificar_pin_pce" => array(
                        "label" => "Pines reservados contraentrega",
                        "data" => "cliente_notificar_pin_pce"
                    ),
                    "cliente_notificar_pin_reservado_cooperativa" => array(
                        "label" => "Pines reservados cooperativas",
                        "data" => "cliente_notificar_pin_reservado_cooperativa"
                    ),
                    "coordiutil_notificar_pqr" => array(
                        "label" => "Notificacion pqr",
                        "data" => "coordiutil_notificar_pqr"
                    ),
                    "coordiutil_enviar_respuesta_pqr" => array(
                        "label" => "Respuesta pqr",
                        "data" => "coordiutil_enviar_respuesta_pqr"
                    ),
                    "coordiutil_notificar_contactenos" => array(
                        "label" => "Notificacion contactenos",
                        "data" => "coordiutil_notificar_contactenos"
                    ),
                    "cliente_notificar_datos_faltantes_validacion" => array(
                        "label" => "Datos faltantes validacion",
                        "data" => "cliente_notificar_datos_faltantes_validacion"
                    ),
                    "beneficiario_notificar_bono" => array(
                        "label" => "Bonos",
                        "data" => "beneficiario_notificar_bono"
                    )
                );
                $ubicaciones["codigo_referencia"] = array(
                    $correos[$ubicaciones["codigo_referencia"]]
                );
            }
            
            $result["ubicaciones"][] = $ubicaciones;
        }
        
        return $result;
    }

    public static function autocompleteReferecia($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $sql = "";
        
        $flagsProv = JUtils::pgsqlHStoreToArray($si["flags"]);
        
        $campos = "p.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        /*
         * tipo pagina ubicacion
         * 47 = negocio
         * 48 = marca
         * 37 = categoria virtual
         * 53 = correos
         */
        if ($p["codigo_pagina"] == 47) {
            if ($flagsProv["flag_categorizacion"] == 1) {
                $sql = "select
                                  p.codigo_categoria as data,
                                  p.nombre as label
                                from cu_categorias_pp p where codigo_proveedor=:codigo_proveedor
                                  and {$where} ";
            } else {
                $sql = "select
                                  p.codigo_categoria as data,
                                  p.nombre as label
                                from cu_categoriasn p
                where p.estado = 'activo'
                             and {$where} ";
            }
        } elseif ($p["codigo_pagina"] == 48) {
            $sql = "select 
                                  p.codigo_marca as data,
                                  p.nombre as label
                                from  cu_marcas p
                                  join cu_proveedores_marcas pp on (pp.codigo_marca=p.codigo_marca)
                                  where pp.codigo_proveedor=:codigo_proveedor
                                  and {$where}";
        } elseif ($p["codigo_pagina"] == 37) {
            $sql = "select 
                                  p.nombre as label ,
                                  p.codigo_categoria as data 
                                from cu_categoriasv p 
                                  where p.codigo_proveedor =:codigo_proveedor
                                  and {$where}";
        } elseif ($p["codigo_pagina"] == 53) { // correos
            $result = array(
                array(
                    "label" => "Nuevo registro cliente",
                    "data" => "cliente_notificar_nuevo_registro"
                ),
                array(
                    "label" => "Actualizacion de datos cliente",
                    "data" => "cliente_notificacion_actualizacion_datos"
                ),
                array(
                    "label" => "Recuperar clave",
                    "data" => "cliente_recuperar_clave"
                ),
                array(
                    "label" => "Aprobacion pines contraentrega",
                    "data" => "cliente_notificar_aprobacion_pin_pce"
                ),
                array(
                    "label" => "Mediación comentarios",
                    "data" => "cliente_notificar_mediacion_servicio"
                ),
                array(
                    "label" => "Pago rechazado",
                    "data" => "cliente_notificar_pago_rechazado"
                ),
                array(
                    "label" => "Pin pagado aprobado",
                    "data" => "cliente_notificar_pin_pagado"
                ),
                array(
                    "label" => "Pin pagado pendiente",
                    "data" => "cliente_notificar_pin_pagado_pendiente"
                ),
                array(
                    "label" => "Notificacion transporte pines contraentrega",
                    "data" => "cliente_notificar_pin_pce_despachado"
                ),
                array(
                    "label" => "Notificacion transporte pines",
                    "data" => "cliente_notificar_tracking"
                ),
                array(
                    "label" => "Pines reservados",
                    "data" => "cliente_notificar_pin_reservado"
                ),
                array(
                    "label" => "Pines reservados contraentrega",
                    "data" => "cliente_notificar_pin_pce"
                ),
                array(
                    "label" => "Pines reservados cooperativas",
                    "data" => "cliente_notificar_pin_reservado_cooperativa"
                ),
                array(
                    "label" => "Notificacion pqr",
                    "data" => "coordiutil_notificar_pqr"
                ),
                array(
                    "label" => "Respuesta pqr",
                    "data" => "coordiutil_enviar_respuesta_pqr"
                ),
                array(
                    "label" => "Notificacion contactenos",
                    "data" => "coordiutil_notificar_contactenos"
                ),
                array(
                    "label" => "Datos faltantes validacion",
                    "data" => "cliente_notificar_datos_faltantes_validacion"
                ),
                array(
                    "label" => "Bonos",
                    "data" => "beneficiario_notificar_bono"
                )
            );
            
            return $result;
        }
        
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function save($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $db->transaction();
        
        if (empty($p["nombre"])) {
            throw new JPublicException(print_r("por favor ingrese el nombre del contenido", 1));
        }
        if (empty($p["titulo"])) {
            throw new JPublicException(print_r("por favor ingrese el titulo del contenido", 1));
        }
        if (empty($p["contenido"])) {
            throw new JPublicException(print_r("por favor ingrese el contenido ", 1));
        }
        
        $p["nombre"] = trim($p["nombre"]);
        
        $campos = "codigo_contenido, nombre,titulo, contenido, codigo_proveedor_pp, descripcion_seo";
        if ($p["codigo_contenido"] === "") {
            $codigo_contenido = $db->nextVal("cu_contenidos_codigo_contenido");
            $ca->prepareInsert("cu_contenidos", $campos);
        }
        else {
            $codigo_contenido = $p["codigo_contenido"];
            $ca->prepareUpdate("cu_contenidos", $campos, "codigo_contenido = :codigo_contenido and codigo_proveedor_pp=:codigo_proveedor_pp");
        }
        
        $ca->bindValue(":codigo_contenido", $codigo_contenido, false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":titulo", $p["titulo"], true);
        $ca->bindValue(":contenido", $p["contenido"], true);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"]);
        $ca->bindValue(":descripcion_seo", $p["descripcion_seo"], true);
        $ca->exec();
        
        if ($p["codigo_contenido"] != "") {
            $sql = "delete from cu_ubicaciones_pp
            where codigo_contenido=:codigo_contenido and codigo_proveedor_pp=:codigo_proveedor_pp and tipo_objeto = 1 ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_contenido", $codigo_contenido);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"]);
            $ca->exec();
        }
        
        foreach ($p["ubicaciones"] as $ubicaciones) {
            if (! empty($p["ubicaciones"])) {
                if(!isset($ubicaciones["codigo_referencia"]) || !isset($ubicaciones["codigo_referencia"][0])  || !isset($ubicaciones["codigo_referencia"][0]["data"])){
                    $ubicaciones["codigo_referencia"] = array(); 
                    $ubicaciones["codigo_referencia"][] = array("data" => 0);
                }
                
                $ubicaciones["codigo_referencia"][0]["data"] = !empty($ubicaciones["codigo_referencia"][0]["data"]) ? $ubicaciones["codigo_referencia"][0]["data"] : 0;
                
                $campos = "codigo_ubicacion,codigo_pagina,codigo_referencia,orden,fechahora_inicio,fechahora_final,codigo_contenido,codigo_proveedor_pp,seccion_ubicacion,tipo_objeto";
                
                if ($ubicaciones["codigo_pagina"] != "1" && $ubicaciones["codigo_referencia"][0]["data"] == "1") {
                    throw new JPublicException(print_r("referencia Home invalidad para el tipo de pagina selecionada por favor elija una refrencia correcta ", 1));
                }
                
                if (empty($ubicaciones["codigo_pagina"])) {
                    throw new JPublicException(print_r("Se debe selecionar el nombre de la  pagina en la tabla ubicaciones ", 1));
                }
                /*if (empty($ubicaciones["fecha_inicio"])) {
                    throw new JPublicException(print_r("Se debe selecionar la fecha inicial en la tabla ubicaciones ", 1));
                }
                if (empty($ubicaciones["fecha_final"])) {
                    throw new JPublicException(print_r("Se debe selecionar la fecha final en la tabla ubicaciones ", 1));
                }*/
                if (empty($ubicaciones["orden"])) {
                    throw new JPublicException(print_r("Se debe indicar el orden en la tabla ubicaciones ", 1));
                }
                if (empty($ubicaciones["seccion_ubicacion"])) {
                    throw new JPublicException(print_r("Se debe selecionar la seccion de la  pagina en la tabla ubicaciones ", 1));
                }
                
                // throw new JPublicException (print_r($ubicaciones["codigo_referencia"][0]["data"], 1 ));
                
                self::validacionCamposVacion($ubicaciones);
                
                $horaInicio = ! empty($ubicaciones["hora_inicio"]) ? $ubicaciones["hora_inicio"] : "00:00";
                $horaFinal = ! empty($ubicaciones["hora_final"]) ? $ubicaciones["hora_final"] : "13:59";
                
                $fechaInicio = !empty($ubicaciones["fecha_inicio"]) ? $ubicaciones["fecha_inicio"]:"1900-01-01";
                $fechaFin = !empty($ubicaciones["fecha_final"]) ? $ubicaciones["fecha_final"]:"3000-12-31";
                
                $fechaHoraInicio = $fechaInicio. " " . $horaInicio;
                $fechaHoraFinal = $fechaFin. " " . $horaFinal;
                
                $codigo_ubicacion = $db->nextVal("cu_ubicaciones_pp_codigo_ubicacion");
                $ca->prepareInsert("cu_ubicaciones_pp", $campos);
                $ca->bindValue(":codigo_ubicacion", $codigo_ubicacion, false);
                $ca->bindValue(":codigo_pagina", $ubicaciones["codigo_pagina"], true);
                $ca->bindValue(":seccion_ubicacion", $ubicaciones["seccion_ubicacion"], true);
                $ca->bindValue(":codigo_referencia", $ubicaciones["codigo_referencia"][0]["data"], true);
                $ca->bindValue(":orden", $ubicaciones["orden"], true);
                $ca->bindValue(":fechahora_inicio", $fechaHoraInicio, true);
                $ca->bindValue(":fechahora_final", $fechaHoraFinal, true);
                $ca->bindValue(":codigo_contenido", $codigo_contenido, false);
                $ca->bindValue(":tipo_objeto", 1, false);
                $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
                $ca->exec();
                
                if(!empty($ubicaciones["codigo_pagina"]) && $ubicaciones["codigo_pagina"]!="-1") {
                    $ca->prepare("select * from cu_paginas where codigo_pagina =:codigo_pagina");
                    $ca->bindValue(":codigo_pagina", $ubicaciones["codigo_pagina"], false);
                    $ca->exec();
                    $rPagina = $ca->fetch();
                    global $manejadorCache;
                    $cacheKey = "ContenidosPP_loadContenidoPorUbicacion_{$si["codigo_proveedor"]}_{$rPagina["url_request"]}_{$ubicaciones["seccion_ubicacion"]}_";
                    $manejadorCache->set($cacheKey, false, 900);
                }
            }
        }
        $db->commit();

        if ($codigo_contenido  > 0){
            return $codigo_contenido;
        }
    }

    public static function loadEditRs(){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $sql = "select codigo_pagina as data,titulo as label 
                from cu_paginas where titulo <>''";
        $ca->prepare($sql);
        $ca->exec();
        
        $paginas = $ca->fetchAll();
        $encabezado = array(array(
            "data" => "-1",
            "label" => "Todas"
        ));
        $result["tipos_ubicaciones"] = array_merge($encabezado,$paginas);
        
        $sql = "select identificador as data,nombre as label
                from cu_ubicaciones_secciones";
        $ca->prepare($sql);
        $ca->exec();
        $result["secciones"] = $ca->fetchAll();
        
        return $result;
    }

    public static function validacionCamposVacion($p){
        foreach ($p as $campo => $valor) {
            if ($valor == "") {
                throw new JPublicException(" {$campo} vacio, por favor complete la informacion");
            }
        }
        return;
    }   
    
    public static function exportarContenidos($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $campos = "codigo_contenido,nombre,titulo";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }
        
        $sql = "select * from ( 
                    select a.codigo_contenido,
                            a.nombre,
                            a.titulo,
                            a.contenido,
                            a.descripcion_seo
                            from cu_contenidos a 
                    where a.codigo_proveedor_pp = :codigo_proveedor
                ) tbl where {$where}
                ";
        
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();
        $contenidos = $ca->fetchAll();
        
//        $txt = '';
        $path = JApp::privateTempPath() . "/contenidos_{$si["codigo_proveedor"]}.csv";
        $f = fopen($path, 'w');

        
        foreach ($contenidos as $c) {
//            $txt .= $c['codigo_contenido'].','.$c['nombre'].','.$c['titulo'].','.$c['contenido'].','.$c['descripcion_seo'].'
//';
            
            fputcsv($f, $c, ',');

        }
        
        fclose($f);
        
        
//        file_put_contents ($path,$txt);
        return basename($path);
        
        
        // Este bloque es para exportar en excel
//        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
//        $ws = &$wb->addWorksheet('Hoja1');
//
//        $row = 0;
//        foreach ($datos as $r) {
//            $col = 0;
//            if ($row == 0) {
//                foreach ($r as $k => $v) {
//                    $k = str_replace("_", " ", $k);
//                    $k = ucfirst($k);
//
//                    $ws->writeString($row, $col, $k);
//                    $col++;
//                }
//                $row += 1;
//                $col = 0;
//            }
//
//            $rCampos = array();
//            foreach ($r as $k => $v) {
//
//                if (in_array($k, $rCampos)) {
//                    $ws->writeNumber($row, $col, $v);
//                    $col++;
//                    continue;
//                }
//
//                $ws->writeString($row, $col, utf8_decode($v));
//                $col++;
//            }
//            $row++;
//        }
//
//        $wb->close();
//        return basename($path);
    }
}