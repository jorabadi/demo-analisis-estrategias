<?php
require_once $cfg["rutaModeloLogico"]."mod_utils.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_proveedores.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_temas.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_fuentes.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_menu.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_vendesfacil_ventas.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_ciudades.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_inbounds_empresas.inc.php";
class Proveedor {

    public static function load($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_proveedores", "nit,nombre,direccion,telefono,callcenter_telefono,callcenter_contacto,callcenter_email,email_notificaciones,url", "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Proveedor no localizado");
        }

        return $ca->fetch();
    }

    public static function save($p) {
        $si = Session::info();

        if (empty($p["nombre"])) {
            throw new JPublicException("Nombre inválido");
        }

        if (empty($p["nit"])) {
            throw new JPublicException("Nit inválido");
        }

        if (empty($p["direccion"])) {
            throw new JPublicException("Dirección inválido");
        }

        if (empty($p["telefono"])) {
            throw new JPublicException("Teléfono inválido");
        }

        $campos = "nit,nombre,direccion,telefono,callcenter_telefono,callcenter_contacto,callcenter_email,email_notificaciones,url";

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareUpdate("cu_proveedores", $campos, "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":nit", $p["nit"], true);
        $ca->bindValue(":direccion", $p["direccion"], true);
        $ca->bindValue(":telefono", $p["telefono"], true);
        $ca->bindValue(":url", $p["url"], true);
        $ca->bindValue(":callcenter_telefono", $p["callcenter_telefono"], true);
        $ca->bindValue(":callcenter_contacto", $p["callcenter_contacto"], true);

        $callcenterEmail = "";
        $i = 0;
        foreach ($p["correos_atencion"] as $v) {
            if (!JInputValidator::isEmail($v["correo"])) {
                throw new JPublicException("Correo invalido de atención al cliente: {$v["correo"]}");
            }
            if ($i != 0) {
                $callcenterEmail .= "," . $v["correo"];
            } else {
                $callcenterEmail .= $v["correo"];
            }
            $i++;
        }
        $ca->bindValue(":callcenter_email", $callcenterEmail, true);

        $emailNotificaciones = "";
        $i = 0;
        foreach ($p["correos_notificaciones"] as $v) {
            if (!JInputValidator::isEmail($v["correo"])) {
                throw new JPublicException("Correo invalido de notificaciones: {$v["correo"]}");
            }
            if ($i != 0) {
                $emailNotificaciones .= "," . $v["correo"];
            } else {
                $emailNotificaciones .= $v["correo"];
            }
            $i++;
        }
        $ca->bindValue(":email_notificaciones", $emailNotificaciones, true);

        $db->transaction();
        $ca->exec();
        $db->commit();
        return;
    }

    public static function savePreferencias($p) {
        global $cfg;
        $si = Session::info();
        
        if (!in_array("FPreferenciasNe_Save", $si["permisos"]) && $si["tipo"] != "admin") { 
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }

        $db = JDatabase::database();
        $ca = new JDbQuery($db);
       


        $coloresNew = array();
        foreach ($p["colores_new"] as $r) {
            if (empty($r["key"]))
                continue;
            $coloresNew[$r["key"]] = $r["color"];
        }
		
		$p["condiciones_uso_importados"] = empty($p["condiciones_uso_importados"])?"":$p["condiciones_uso_importados"];

        $personalizacion = array("colores_new" => $coloresNew, "general" => $p["general"]);
    
        $regexFacebook = '/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/';
        $regexInstagram = '/^(https?:\/\/)?(www\.)?instagram.com\/[a-zA-Z0-9(\.\?)?]/';
        $regexYoutube = '/^(https?:\/\/)?(www\.)?youtube.com\/[a-zA-Z0-9(\.\?)?]/';
        $regexTwitter = '/^(https?:\/\/)?(www\.)?twitter.com\/[a-zA-Z0-9(\.\?)?]/';
        
        $social_plugins = $p["social_plugins"];
        
        if(isset($social_plugins['Facebook']['url'])){
            $urlFacebook = $social_plugins['Facebook']['url'];
            if(!empty($urlFacebook)){
                $urlValida = self::validarUrl($urlFacebook, $regexFacebook);
                if($urlValida){
                    $social_plugins['Facebook']['url'] = self::convertirUrlHttps($urlFacebook);
                }else{
                    throw new JPublicException("URL de facebook incorrecta.");
                }
            }
        }
        
        if(isset($social_plugins['Instagram']['url'])){
            $urlInstagram = $social_plugins['Instagram']['url'];
            if(!empty($urlInstagram)){
                $urlValida = self::validarUrl($urlInstagram, $regexInstagram);
                if($urlValida){
                    $social_plugins['Instagram']['url'] = self::convertirUrlHttps($urlInstagram);
                }else{
                    throw new JPublicException("URL de Instagram incorrecta.");
                }
            }
        }
        
        if(isset($social_plugins['Twitter']['url'])){
            $urlTwitter = $social_plugins['Twitter']['url'];
            if(!empty($urlTwitter)){
                $urlValida = self::validarUrl($urlTwitter, $regexTwitter);
                if($urlValida){
                    $social_plugins['Twitter']['url'] = self::convertirUrlHttps($urlTwitter);
                }else{
                    throw new JPublicException("URL de twitter incorrecta.");
                }
            }
        }
        
        if(isset($social_plugins['Youtube']['url'])){
            $urlYoutube = $social_plugins['Youtube']['url'];
            if(!empty($urlYoutube)){
                $urlValida = self::validarUrl($urlYoutube, $regexYoutube);
                if($urlValida){
                    $social_plugins['Youtube']['url'] = self::convertirUrlHttps($urlYoutube);
                }else{
                    throw new JPublicException("URL de youtube incorrecta.");
                }
            }
        }
        
        $p["social_plugins"] = json_encode($social_plugins);

        $codigoProveedor = $si["codigo_proveedor"];
        $db->transaction();

        $campos = "personalizacion,tienda_condiciones_uso";
        $ca->prepareUpdate("cu_proveedores", $campos, "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $codigoProveedor, false);
        $ca->bindValue(":personalizacion", json_encode($personalizacion), true);
        $ca->bindValue(":tienda_condiciones_uso", $p["tienda_condiciones_uso"], true);
        $ca->exec();

        $ca->prepareSelect("cu_proveedores_pp ", "*", "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $r = $ca->fetchAll();
        
        /* No  dejar guardar  si viene el listado de formas de pago vacio  */
        if(empty($p['formas_pago'])){
            throw new JPublicException("No se pueden guardar con las formas de pago vacias");
        }
        
        
        $rFormasPago = array();
        foreach ($p['formas_pago'] as $formasPago){
            if($formasPago['estado']==1){
                $rFormasPago[]=$formasPago['codigo_forma_pago'];
            }
        }
        $codigosFormaPago= implode(",", $rFormasPago);
        $campos = "seo_sufijo,google_analytics_code,codigo_proveedor,meta_title,meta_description,texto_linea_callcenter_home,condiciones_uso_importados,scripts_header,scripts_body,social_plugins,codigos_forma_pago";
        if (!empty($r)) {
            $ca->prepareUpdate("cu_proveedores_pp", $campos, "codigo_proveedor=:codigo_proveedor");
        } else {
            $ca->prepareInsert("cu_proveedores_pp", $campos);
        }
        

        $ca->bindValue(":codigo_proveedor", $codigoProveedor, false);
        $ca->bindValue(":seo_sufijo", $p["seo_sufijo"], true);
        $ca->bindValue(":google_analytics_code", $p["google_analytics_code"], true);
        $ca->bindValue(":meta_title", $p["meta_title"], true);
        $ca->bindValue(":meta_description", $p["meta_description"], true);
        $ca->bindValue(":texto_linea_callcenter_home", $p["texto_linea_callcenter_home"], true);
        $ca->bindValue(":condiciones_uso_importados", $p["condiciones_uso_importados"], true);
        $ca->bindValue(":scripts_header", $p["scripts_header"], true);
        $ca->bindValue(":scripts_body", $p["scripts_body"], true);
        $ca->bindValue(":social_plugins", $p["social_plugins"], true);
        $ca->bindValue(":codigos_forma_pago", "{{$codigosFormaPago}}", true);
	//	throw new JPublicException(print_r($ca->preparedQuery(),1));
        $ca->exec();
        
        global $manejadorCache;
        $manejadorCache->set("FormasPagoPP_consultarFormasPago_$codigoProveedor", false, 1);

        //guardo la barra menu
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        modMenu::guardarBarraMenu($p);
        $manejadorCache->set("CategoriasPP_menu_$codigoProveedor", false, 900);

        // flags  administrables =>  opciones administrables  por el proveedor

        foreach ($p["administracion"] as $rFlags) {

            if ($rFlags["flag"] != "flag_grupos_ciudades") {
                $sql = "UPDATE cu_proveedores_pp p set flags=flags || hstore('{$rFlags["flag"]}','{$rFlags["estado"]}')
        			  where codigo_proveedor=:codigo_proveedor";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->exec();
            }
            if ($rFlags["flag"] == "flag_grupos_ciudades") {
                $sql = "UPDATE cu_proveedores p set flag_grupos_ciudades ='{$rFlags["estado"]}' where codigo_proveedor=:codigo_proveedor ";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->exec();

            }
        }
        
        
        $ca->prepareSelect("mod_ssl", "*", "codigo_proveedor_pp=:codigo_proveerdor and primary_domain=:primary_domain");
        $ca->bindValue(":codigo_proveerdor", $si['codigo_proveedor'], false);
        $ca->bindValue(":primary_domain", $p['dominio_principal'], true);
        $ca->exec();
        if ($ca->size() == 0) {
            
            if($p['primary_certificate'] !="" || $p['root_certificate'] !="" || $p['private_key'] !=""  || $p['emision_certificado'] !=""  ){
                
                if($p['emision_certificado']==""){
                    throw new JPublicException("Por favor ingrese una fecha de emisión de certificado");
                }
                
                $privatekey_encrypt = self::sencrypt_decrypt('encrypt', $p['private_key']);
                $raliasDominios= array();
                
                if(empty($p['alias_dominios'])){
                    $p['alias_dominios']= array(array("url_alias"=>$p["dominio_principal"]));
                }
                foreach ($p['alias_dominios'] as $alias ){
                    $raliasDominios[]=$alias['url_alias'];
                }
                
                $aliasDominio= json_encode($raliasDominios); 
                
                $campos='codigo_proveedor_pp,type,primary_domain,aliases,certificate_ca,certificate_crt,certificate_key,issued_date';
                $ca->prepareInsert("mod_ssl", $campos);
                $ca->bindValue(":codigo_proveedor_pp",$si['codigo_proveedor'],false);
                $ca->bindValue(":type",'manual',true);
                $ca->bindValue(":primary_domain", $p["dominio_principal"], true);
                $ca->bindValue(":aliases",  $aliasDominio, true);
                $ca->bindValue(":certificate_ca", $p["root_certificate"], true);
                $ca->bindValue(":certificate_crt", $p["primary_certificate"], true);
                $ca->bindValue(":certificate_key", $privatekey_encrypt, true);
                $ca->bindValue(":issued_date", $p['emision_certificado'],true);
                //throw new JPublicException(print_r($ca->preparedQuery()));
                $ca->exec();
            }
        }else{
            
            
            $privatekey_encrypt = self::sencrypt_decrypt('encrypt', $p['private_key']);
            $ca->prepareSelect("mod_ssl", "*", "codigo_proveedor_pp=:codigo_proveerdor and primary_domain=:primary_domain 
              and certificate_ca=:certificate_ca and certificate_crt=:certificate_crt and  certificate_key=:certificate_key and issued_date=:issued_date");
            $ca->bindValue(":codigo_proveerdor", $si['codigo_proveedor'], false);
            $ca->bindValue(":primary_domain", $p['dominio_principal'], true);
            $ca->bindValue(":certificate_ca", $p["root_certificate"], true);
            $ca->bindValue(":certificate_crt", $p["primary_certificate"], true);
            $ca->bindValue(":certificate_key", $privatekey_encrypt, true);
            $ca->bindValue(":issued_date", $p['emision_certificado'],true);
            $ca->exec();
            if ($ca->size() ==0) {
                
                if($p['emision_certificado']==""){
                    throw new JPublicException("Por favor ingrese una fecha de emisión de certificado");
                }
                
                foreach ($p['alias_dominios'] as $alias ){
                    $raliasDominios[]=$alias['url_alias'];
                }
                $aliasDominio= json_encode($raliasDominios); 
                
                $campos='codigo_proveedor_pp,primary_domain,aliases,certificate_ca,certificate_crt,certificate_key,issued_date,status';
                $ca->prepareUpdate("mod_ssl", $campos, "codigo_proveedor_pp=:codigo_proveedor_pp and primary_domain=:primary_domain");
                $ca->bindValue(":codigo_proveedor_pp", $si['codigo_proveedor'], false);
                $ca->bindValue(":primary_domain", $p['dominio_principal'], true);
                $ca->bindValue(":aliases",  $aliasDominio, true);
                $ca->bindValue(":certificate_ca", $p["root_certificate"], true);
                $ca->bindValue(":certificate_crt", $p["primary_certificate"], true);
                $ca->bindValue(":certificate_key", $privatekey_encrypt, true);
                $ca->bindValue(":issued_date", $p['emision_certificado'],true);
                $ca->bindValue(":status",'pending',true);
                $ca->exec();
            }
            
        }
        
        

        if (!empty($p["logo"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresLogo, $p["logo"]);
        }

        if (!empty($p["imagen_fondo_interno"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresFondoInterno, $p["imagen_fondo_interno"]);
        }

        if (!empty($p["imagen_fondo_externo"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresFondoExterno, $p["imagen_fondo_externo"]);
        }

        //insertar recursos banners

        if (!empty($p["imagen_pp_banner1_new"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresPpBanner1_new, $p["imagen_pp_banner1_new"]);
        }
        //throw new JPublicException(print_r($p["imagen_pp_banner1_new"],1));

        if (!empty($p["imagen_pp_banner2_new"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresPpBanner2_new, $p["imagen_pp_banner2_new"]);
        }

        if (!empty($p["imagen_pp_banner3_new"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresPpBanner3_new, $p["imagen_pp_banner3_new"]);
        }

        if (!empty($p["imagen_pp_banner4_new"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresPpBanner4_new, $p["imagen_pp_banner4_new"]);
        }

        if (!empty($p["imagen_pp_header_correo"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresPpHeaderCorreo, $p["imagen_pp_header_correo"]);
        }

        if (!empty($p["imagen_pp_footer_correo"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresPpFooterCorreo, $p["imagen_pp_footer_correo"]);
        }
        
        if (!empty($p["imagen_pp_heander_correo_importados"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresPpHeaderCorreoImportados, $p["imagen_pp_heander_correo_importados"]);
        }
        
       if(!empty($p["imagen_pp_footer_correo_importados"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuProveedoresPpFooterCorreoImportados, $p["imagen_pp_footer_correo_importados"]);
        }

        if (!empty($p["pp_archivo_webmastertool"]) && isset($p["pp_archivo_webmastertool"]["tmp_name"])) {
            $txt = file_get_contents($cfg["privateTempPath"] . '/' . $p["pp_archivo_webmastertool"]["tmp_name"]);
            $sql = "UPDATE cu_proveedores_pp set webmastertools = :txt where codigo_proveedor = :codigo_proveedor";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":txt", $txt, true);
            $ca->exec();
        }

        if (!empty($p["imagen_pp_fondo_popup_mayoria_edad"])) {
            modRs::save(rs::cuProveedores,$codigoProveedor, rs::cuProveedoresPpFondoPopupMayoriaEdad, $p["imagen_pp_fondo_popup_mayoria_edad"]);
        }
        if (!empty($p["favicon"])) {
            modRs::save(rs::cuProveedores,$codigoProveedor, rs::cuProveedoresPpFavicon, $p["favicon"]);
        }
        
        if (!empty($p["upload_file_fiel_private_key"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuTiendaCertificadoPrivateKey, $p["upload_file_fiel_private_key"]);
        }
        if (!empty($p["upload_file_fiel_primary_certificate"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuTiendaCertificadoPrimary, $p["upload_file_fiel_primary_certificate"]);
        }
        if (!empty($p["upload_file_fiel_root_certificate"])) {
            modRs::save(rs::cuProveedores, $codigoProveedor, rs::cuTiendaCertificadoRoot, $p["upload_file_fiel_root_certificate"]);
        }
        
        
        
        
        $db->commit();

        $sql = "select nombre_url from cu_proveedores where codigo_proveedor=:codigo_proveedor";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $rProv = $ca->fetch();
        
        
        //GENERO EL ARCHIVO CON LA CACHE DE LOS ESTILOS PARA MOSTRAR EN LA PAGINA
        
        

        $result["colores_new"] = json_decode(file_get_contents("{$cfg["rutaArchivoJson"]}personalizacion.json"), true);
        
        
        foreach (array_keys($result["colores_new"]) as $k) {
        	foreach (array_keys($result["colores_new"][$k]) as $k1) {
        		$result["colores_new"][$k][$k1]["default"] = $result["colores_new"][$k][$k1]["default"];
        		
        		if (isset($personalizacion["colores_new"][$k1])) {
        			$result["colores_new"][$k][$k1]["default"] = $personalizacion["colores_new"][$k1];
        		}
        	}
        }
        
        
        $pGenerarCache = array(
        	"nombre_url" => $rProv["nombre_url"],
        	"json" => json_encode($result["colores_new"])
        );
        self::generarCacheEstiloTienda($pGenerarCache);
        
        
        return;
    }

    public static function loadPreferences() {
        global $cfg;
        $result = array("colores" => array(), "colores_new" => array(), "general" => array(), "rs" => array(), "tienda_condiciones_uso" => "");

        $si = Session::info();
        $ca = new JDbQuery(JDatabase::database());

        $campos = "p.personalizacion,		
                    p.resources,
                    p.tienda_condiciones_uso,
                    p1.condiciones_uso_importados,
                    p1.flags->'flag_importado' as flag_importado,
                    coalesce(p1.seo_sufijo,'') as seo_sufijo,
                    coalesce(p1.google_analytics_code,'') as google_analytics_code,
                    meta_title,
                    flag_tienda,		
                    meta_description,
                    p1.scripts_header,
                    p1.scripts_body,
                    p1.certificado_ssl_key,
                    p1.certificado_ssl_primary,
                    p1.certificado_ssl_root,
                    p1.texto_linea_callcenter_home,
                    p.tienda_banner_urls->'" . rs::cuProveedoresPpBanner1_new . "' as url_banner1_new, 
                    p.tienda_banner_urls->'" . rs::cuProveedoresPpBanner2_new . "' as url_banner2_new, 
                    p.tienda_banner_urls->'" . rs::cuProveedoresPpBanner3_new . "' as url_banner3_new, 
                    p.tienda_banner_urls->'" . rs::cuProveedoresPpBanner4_new . "' as url_banner4_new,
                    p1.social_plugins,
                    p1.webmastertools,
                    p1.google_analytics_account_id,
                    p1.tag_manager_account_id,
                    p.codigo_tema";
        
        $ca->prepareSelect("cu_proveedores p left join cu_proveedores_pp p1 on p.codigo_proveedor = p1.codigo_proveedor", $campos, "p.codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $r = $ca->fetch();

        $result["codigo_tema"] = $r["codigo_tema"];
        $result["tienda_condiciones_uso"] = $r["tienda_condiciones_uso"];
        $result["condiciones_uso_importados"] = $r["condiciones_uso_importados"];
        $result["flag_importado"] = $r["flag_importado"];
        $result["codigo_proveedor"] = $si["codigo_proveedor"];
        $result["google_analytics_account_id"] = $r["google_analytics_account_id"];
        $result["tag_manager_account_id"] = $r["tag_manager_account_id"];

        if ($r["personalizacion"] != "") {
            $personalizacion = json_decode($r["personalizacion"], true);
            $result["general"] = $personalizacion["general"];
        }

        $result["seo_sufijo"] = $r["seo_sufijo"];
        $result["google_analytics_code"] = $r["google_analytics_code"];
        $result["meta_title"] = $r["meta_title"];
        $result["meta_description"] = $r["meta_description"];
        $result["texto_linea_callcenter_home"] = $r["texto_linea_callcenter_home"];

        //url banners
        $result["url_banner1_new"] = $r["url_banner1_new"];
        $result["url_banner2_new"] = $r["url_banner2_new"];
        $result["url_banner3_new"] = $r["url_banner3_new"];
        $result["url_banner4_new"] = $r["url_banner4_new"];

        $result["flag_tienda"] = $r["flag_tienda"];
        $result['scripts_header'] = $r['scripts_header'];
        $result['scripts_body'] = $r['scripts_body'];
        $result['certificado_ssl_key'] = $r['certificado_ssl_key'];
        $result['certificado_ssl_primary'] = $r['certificado_ssl_primary'];
        $result['certificado_ssl_root'] = $r['certificado_ssl_root'];
        $result["social_plugins"] = json_decode($r["social_plugins"], true);
        $result["colores_new"] = json_decode(file_get_contents("{$cfg["rutaArchivoJson"]}personalizacion.json"), true);
        $result['webmastertools'] = $r['webmastertools'];

        foreach (array_keys($result["colores_new"]) as $k) {
            foreach (array_keys($result["colores_new"][$k]) as $k1) {
                $result["colores_new"][$k][$k1]["color"] = $result["colores_new"][$k][$k1]["default"];

                if (isset($personalizacion["colores_new"][$k1])) {
                    $result["colores_new"][$k][$k1]["color"] = $personalizacion["colores_new"][$k1];
                }
            }
        }

        $result["rs"] = modRs::load(rs::cuProveedores, $si["codigo_proveedor"], array(rs::cuProveedoresLogo, rs::cuProveedoresFondoInterno, rs::cuProveedoresFondoExterno, rs::cuProveedoresPpBanner1, rs::cuProveedoresPpBanner2, rs::cuProveedoresPpBanner3, rs::cuProveedoresPpBanner4, rs::cuProveedoresPpBanner1_new, rs::cuProveedoresPpBanner2_new, rs::cuProveedoresPpBanner3_new, rs::cuProveedoresPpBanner4_new, rs::cuProveedoresPpHeaderCorreo, rs::cuProveedoresPpHeaderCorreoImportados, rs::cuProveedoresPpFooterCorreo, rs::cuProveedoresPpFooterCorreoImportados, rs::cuProveedoresPpArchivoWebmastertool, rs::cuProveedoresPpFondoPopupMayoriaEdad, rs::cuProveedoresPpFavicon,rs::cuTiendaCertificadoPrivateKey, rs::cuTiendaCertificadoPrimary, rs::cuTiendaCertificadoRoot));
        foreach ($result["rs"] as $k => $v) {
            $result["rs"][$k]["preview"] = JDbFile::url($v["id"], array("w" => 260, "h" => 120, "frm" => 1));
        }

        //Barra menu
        $sql = "select 
                a.codigo1,
                a.codigo2,
                a.codigo3,
                a.tipo as codigo_tipo,
                coalesce(a.etiqueta_menu,'') as etiqueta_menu,
                a.orden,
                case when a.tipo=3 then coalesce(e.nombre,'')
                when b.flags->'flag_categorizacion'='0' and a.tipo=2 then 
                    (select coalesce(nombre,'') from cu_categoriasn where codigo_categoria=a.codigo2)||'/'||c.nombre 
                when b.flags->'flag_categorizacion'='1' and a.tipo=2 then 
                    (select coalesce(nombre,'') from cu_categorias_pp where codigo_categoria=a.codigo2)||'/'||d.nombre
                when b.flags->'flag_categorizacion'='0' and a.tipo=5 then 
                    (select coalesce(nombre,'') from cu_categoriasn where codigo_categoria=a.codigo3)||'/'||c.nombre 
                when b.flags->'flag_categorizacion'='1' and a.tipo=5 then 
                    (select coalesce(nombre,'') from cu_categorias_pp where codigo_categoria=a.codigo3)||'/'||d.nombre
                when b.flags->'flag_categorizacion'='0' and a.tipo=1 then 
                    coalesce(c.nombre,'') 
                when b.flags->'flag_categorizacion'='1' and a.tipo=1 then 
                    coalesce(d.nombre,'') 
                when a.tipo=4 then 
                    coalesce(cv.nombre,'') 
                end as nombre
            from cu_barramenu_pp a 
            join cu_proveedores b on (a.codigo_proveedor=b.codigo_proveedor)
            left join cu_categoriasn c on (a.codigo1 = c.codigo_categoria and (case when a.tipo = 5 then c.tipo = 3 else a.tipo=c.tipo end))
            left join cu_categorias_pp d on (a.codigo1=d.codigo_categoria and (case when a.tipo = 5 then d.tipo = 3 else a.tipo=d.tipo end))
            left join view_cu_marcas e on (case when a.tipo=3 then a.codigo1 = e.codigo_marca end )
            left join cu_categoriasv cv on (a.codigo1 = cv.codigo_categoria and cv.codigo_proveedor = b.codigo_proveedor)
            where a.codigo_proveedor=:codigo_proveedor order by a.codigo1";

        /*  $sql = "select
          1 as codigo1,1 as codigo2,1 as codigo_tipo, 'et' as etiqueta_menu,1 as orden"; */
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $result["barra_menu"] = $ca->fetchAll();

        
        if($result["flag_tienda"]=="1"){
        	$sql = "SELECT column_name FROM information_schema.columns WHERE table_name ='cu_proveedores' and column_name like '%flag_%';";
	        $ca->prepare($sql);
	        $ca->exec();
	        $rflag = $ca->fetchAll();
	        $campos_flags = "";
	        
	        foreach ($rflag as $nombre_flag){
	        	$campos_flags.= "p.".$nombre_flag["column_name"].",";
	        	
	        	
	        }
	        $campos_flags= substr($campos_flags, 0, -1);
	        
	        $sql="select ".$campos_flags.",pp.flags  as flags_proveedores_pp from cu_proveedores p join cu_proveedores_pp pp on ( p.codigo_proveedor=pp.codigo_proveedor) where p.codigo_proveedor=:codigo_proveedor";
	        $ca->prepare($sql);
	        $ca->bindValue(":codigo_proveedor",$si["codigo_proveedor"], false);
	        $ca->exec();
	        
	        $flagsProveedores = $ca->fetchAll();
	        
	        $flagsProvPp = JUtils::pgsqlHStoreToArray($flagsProveedores["0"]["flags_proveedores_pp"]);
	        
            $result["flags_pp"] = $flagsProvPp;
	        $result["flags_administracion"] = array(
		    array("nombre" => "Grupo ciudad", "flag" => "flag_grupos_ciudades", "valor" => $flagsProveedores["0"]["flag_grupos_ciudades"], "descripcion" => "Grupo de ciudad donde aplica el producto"),
		    array("nombre" => "Popup mayor edad", "flag" => "flag_popup_mayoredad", "valor" => $flagsProvPp["flag_popup_mayoredad"], "descripcion" => "Aplica para productos unicos para personas mayores de edad"),
		    array("nombre" => "Popups generales", "flag" => "flag_popup", "valor" => $flagsProvPp["flag_popup"], "descripcion" => "Popups activos (Ventanas emergentes)"),
		    array("nombre" => "Barra marca", "flag" => "flag_barra_marca", "valor" => $flagsProvPp["flag_barra_marca"], "descripcion" => "Barra de Marcas"),
		    //array("nombre"=>  "Plantillas","flag"=>"flag_plantilla","valor"=>$flagsProvPp["flag_plantilla"],"descripcion"=>"Permite el uso de las plantillas en el sitio"),
		    array("nombre" => "Afiliado a cámara de comercio electrónico", "flag" => "afiliado_camara_comercio", "valor" => $flagsProvPp["afiliado_camara_comercio"], "descripcion" => "Cuando el proveedor esta afiliado a la camara de comercio"),
		    array("nombre" => "Linea callcenter home", "flag" => "flag_linea_callcenter_home", "valor" => $flagsProvPp["flag_linea_callcenter_home"], "descripcion" => "Mostrar linea call center"),
		    array("nombre" => "Boton categorias ", "flag" => "flag_boton_categorias", "valor" => $flagsProvPp["flag_boton_categorias"], "descripcion" => "Mostrar el la barra el boton Categocias"),
		    array("nombre" => "Comentarios de productos", "flag" => "flag_comentarios", "valor" => $flagsProvPp["flag_comentarios"], "descripcion" => "Activa el modulo de comentarios de los clientes en la tienda"),
		    array("nombre" => "Link Quienes somos", "flag" => "flag_link_quienes_somos", "valor" => $flagsProvPp["flag_link_quienes_somos"], "descripcion" => "Cambia el link de \"Ayuda\" del header del sitio por uno a \"Quienes somos\". Se debe crear un contenido con el nombre \"quienessomos\""),
		    array("nombre" => "Link preguntas frecuentes", "flag" => "flag_link_faq", "valor" => $flagsProvPp["flag_link_faq"], "descripcion" => "Muestra u oculta el link de  \"Preguntas frecuentes\" del footer del sitio. Se debe crear un contenido con el nombre \"ns_preguntasFrecuentes\""),
		    array("nombre" => "Notificaciones dominio propio", "flag" => "flag_notificaciones_correopropio", "valor" => $flagsProvPp["flag_notificaciones_correopropio"], "descripcion" => "Envia las notificaciones con un remitente de su dominio"),
		    array("nombre" => "Formato generico factura PDF de la tienda", "flag" => "flag_facturacion_tienda", "valor" => $flagsProvPp["flag_facturacion_tienda"], "descripcion" => "Permite que la tienda genere un PDF con el #factura indicado por ellos en despachos y la resolucion de factura vigente configurada."),
		    array("nombre" => "Notificar copia pin pce a correos de tienda", "flag" => "notificar_pce_copia_correo", "valor" => $flagsProvPp["notificar_pce_copia_correo"], "descripcion" => "Indica que se debe enviar copia del correo de notificacion de pce a los correos almacenados."),
		    array("nombre" => "La tienda utiliza vendedores", "flag" => "usa_vendedores", "valor" => $flagsProvPp["usa_vendedores"], "descripcion" => "Indica si la tienda utiliza personal propio para registrar las compras."),
		    array("nombre" => "La tienda permite comprar sin registrarse", "flag" => "compra_sin_registro", "valor" => $flagsProvPp["compra_sin_registro"], "descripcion" => "flag para controlar si la tienda permite comprar sin registrarse o no."),
		    array("nombre" => "Imprimir los productos en el rótulo", "flag" => "mostrar_contenido_en_rotulo", "valor" => $flagsProvPp["mostrar_contenido_en_rotulo"], "descripcion" => "Indica si la tienda permite o no que salgan los productos impresos en el rotulo."),
		    array("nombre" => "Mostrar lista de deseos", "flag" => "lista_deseos", "valor" => $flagsProvPp["lista_deseos"], "descripcion" => "Indica si la tienda permite o no que los clientes tengan lista de deseos."),
                   /* array("nombre" => "Modulo  email Marketing", "flag" => "email_marketing", "valor" => $flagsProvPp["email_marketing"], "descripcion" => "Modulo para creacion de campañas de Email marketing (Tiene costo adicional)."),
                    array("nombre" => "Modulo sms Marketing", "flag" => "sms_marketing", "valor" => $flagsProvPp["sms_marketing"], "descripcion" => "Modulo para creacion de campañas de Sms marketing (Tiene costo adicional)."),*/
                    array("nombre" => "Modulo para configurar Google Analytics", "flag" => "flag_google_analytics_configurable", "valor" => $flagsProvPp["flag_google_analytics_configurable"], "descripcion" => "Modulo para configurar la cuenta de google analytics."),
                    array("nombre" => "Inventario por bodegas", "flag" => "gestion_inventario_por_bodega", "valor" => $flagsProvPp["gestion_inventario_por_bodega"], "descripcion" => "Permite que la tienda gestione el inventario en diferentes bodegas"),
                    array("nombre" => "Enlace para publicar en redes sociales", "flag" => "agregar_carro_desde_enlace", "valor" => $flagsProvPp["agregar_carro_desde_enlace"], "descripcion" => "Permite generar enlaces de los productos para agregarlos al carrito desde redes sociales"),
                    array("nombre" => " Hace que se muestre la descripcion corta de los productos en el carrito", "flag" => "descripcion_en_carro", "valor" => $flagsProvPp["descripcion_en_carro"], "descripcion" => " Hace que se muestre la descripcion corta de los productos en el carrito")
		);
	}
		$formasPagoPorDefectoTiendas = self::formasPagoPorDefectoTiendas();
		$formasPagoTienda =self::formasPagoTienda();
		$result['formas_pago']= array();
		
		$rFormas_pago = array_map("unserialize", array_unique(array_map("serialize",array_merge_recursive($formasPagoPorDefectoTiendas,$formasPagoTienda))));
		foreach ($rFormas_pago as $k){
			if (JUtils::in_array_r($k['codigo_forma_pago'], $formasPagoTienda)){
				$k['estado']="1";
			}else{
				$k['estado']="0";
			}
			$result['formas_pago'][]=$k;
		}
		/*  consultar informacion de los dominios para el manejo de los dominios   
		 *   Fecha: mayo -30- 2018   Desarrollador: Sebastian Montoya 
         */
		
		$result['certificado_ssl'] = self::consultarCertificadoSSLTienda();
		
		return $result;
    }

    public static function loadInfoSesion($p) {
        $si = session::info();

        $si["flags"]= JUtils::pgsqlHStoreToArray($si["flags"]);
        $si["flags_pp"]= JUtils::pgsqlHStoreToArray($si["flags_pp"]);
        return $si;
    }

    public static function loadInfoProveedorOne($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect(
            "view_cu_proveedores a
        left join cu_contratos_tienda b on (a.codigo_contrato=b.codigo_contrato)",
            "codigo_proveedor,nit,nombre,direccion,telefono,callcenter_telefono,callcenter_contacto,callcenter_email,email_notificaciones,url,
            flag_tienda,nombre_tienda,nombre_representante_legal,correo_representante_legal,b.contrato,correo_notificacion_facturacion,
            aceptacion_contrato,a.codigo_contrato,a.fecha_aceptacion_contrato,
         google_analytics_account_id,tag_manager_account_id,flags_pp",
            "codigo_proveedor=:codigo_proveedor"
        );
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();
        if ($ca->size() == 0) {
            throw new JPublicException("Proveedor no localizado");
        }
        $resp = $ca->fetch();
        $resp['session']=$si;
        
        $resp["flags_pp"]= JUtils::pgsqlHStoreToArray($resp["flags_pp"]);
        
        $ca->prepareSelect("cu_documentos_contratacion_tienda", "*", "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        
        if($ca->size()>0){
            $rDatosDocumentos=$ca->fetch();
            $resp['flags']=JUtils::pgsqlHStoreToArray($rDatosDocumentos['flags']);
        }
        $resp["rs"] = modRs::load(rs::cuDocumentosContratoTienda, $si["codigo_proveedor"]);
        foreach ( $resp["rs"] as $k => $v) {
            $resp["rs"][$k]["preview"] = JDbFile::url($v["id"], array("w" => 260, "h" => 120, "frm" => 1));
        }
        
        
        
        if ( !empty($resp['codigo_contrato']) ) {
            
            $ca->prepareSelect("cu_contratos_tienda", "*", "codigo_contrato=:codigo_contrato");
            $ca->bindValue(":codigo_contrato", $resp['codigo_contrato'], false);
            $ca->exec();
            $rContrato= $ca->fetch();
            $resp['contrato']=$rContrato['contrato'];
            $resp["contrato_pdf"] = modRs::load(rs::cuContratoTiendaPP,$resp['codigo_contrato']);
            
            
        }else{
            
            $ca->prepareSelect("cu_contratos_tienda", "*", "CURRENT_DATE   BETWEEN fecha_inicial AND fecha_final");
            $ca->exec();
            $rContrato= $ca->fetch();
            $resp['contrato']=$rContrato['contrato'];
            $resp["contrato_pdf"] = modRs::load(rs::cuContratoTiendaPP,$rContrato['codigo_contrato']);
            $resp["codigo_contrato"] = $rContrato['codigo_contrato'];
            
            
        } 
        
        return $resp;
    }
    
    public static function guardarInformacionTienda($p){
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $iv = array();
        $iv[] = array('name' => 'nombre', "filter" => JFILTER_VALIDATE_STRING, 'min_length' => 3, 'max_length' => 100);
        $validationErrors = JInputValidator::validateVars($p, $iv, array('output' => 'string'));
        
        if ($validationErrors) {
            throw new JPublicException($validationErrors);
        }
        
        $callcenterEmail = "";
        $i = 0;
        foreach ($p["correos_atencion"] as $val) {
            if (empty($val["correo"])) {
                continue;
            }
            if (!JInputValidator::isEmail($val["correo"])) {
                throw new JPublicException("Correo invalido de atención al cliente: {$val["correo"]}");
            }
            if ($i != 0) {
                $callcenterEmail .= "," . $val["correo"];
            } else {
                $callcenterEmail .= $val["correo"];
            }
            $i++;
        }
        
        $emailNotificaciones = "";
        $i = 0;
        foreach ($p["correos_notificaciones"] as $val) {
            if (empty($val["correo"])) {
                continue;
            }
            if (!JInputValidator::isEmail($val["correo"])) {
                throw new JPublicException("Correo invalido de notificaciones: {$val["correo"]}");
            }
            if ($i != 0) {
                $emailNotificaciones .= "," . $val["correo"];
            } else {
                $emailNotificaciones .= $val["correo"];
            }
            $i++;
        }
        
        $campos = "nombre,nit,direccion,telefono,callcenter_telefono,callcenter_contacto,callcenter_email,email_notificaciones,url,nombre_tienda,nombre_representante_legal,correo_representante_legal,correo_notificacion_facturacion";
        
        $db->transaction();
        $ca->prepareUpdate("cu_proveedores", $campos, "codigo_proveedor=:codigo_proveedor");
        
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":nit", $p["nit"], true);
        $ca->bindValue(":direccion", $p["direccion"], true);
        $ca->bindValue(":telefono", $p["telefono"], true);
        $ca->bindValue(":url", $p["url"], true);
        $ca->bindValue(":nombre_tienda", $p["nombre_tienda"], true);
        $ca->bindValue(":callcenter_telefono", $p["callcenter_telefono"], true);
        $ca->bindValue(":callcenter_contacto", $p["callcenter_contacto"], true);
        $ca->bindValue(":callcenter_email", $callcenterEmail, true);
        $ca->bindValue(":email_notificaciones", $emailNotificaciones, true);
        $ca->bindValue(":nombre_representante_legal", $p['nombre_representante_legal'], true);
        $ca->bindValue(":correo_representante_legal", $p['correo_representante_legal'], true);
        $ca->bindValue(":correo_notificacion_facturacion", $p['correo_notificacion_facturacion'], true);
        $ca->exec();
        
        $db->commit();
    }
    
    public static function guardarInformacionEmpresa($p){
        $si = session::info();
        
        $inboundEmpresa = $p['inboundEmpresa'];
        $inboundEmpresa["codigo_proveedor"] = $si["codigo_proveedor"];
       
        return modVendesfacilVentas::actualizarInboundsAdminTienda($inboundEmpresa);
    }
    
    public static function guardarDocumentosContrato($p){
        $si = session::info();
        $db = JDatabase::database();
        
        $ca = new JDbQuery($db);
        $ca->prepareSelect("cu_documentos_contratacion_tienda", "*", "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        
        if($ca->size()==0){
            $ca->prepareInsert("cu_documentos_contratacion_tienda ", "codigo_proveedor","RETURNING *");
            $ca->bindValue(":codigo_proveedor", $si['codigo_proveedor'], false);
            $ca->exec();
        }
        $rDocumentos =$ca->fetch();
        $flags = JUtils::pgsqlHStoreToArray($rDocumentos['flags']);
       
        $documentos =array();
        
        if(!empty($p['upload_file_fiel_rut']) &&!empty($p['upload_file_fiel_rut']['name']) ){
            $p['upload_file_fiel_rut']['nombre_documento']='Documento rut';
            $p['upload_file_fiel_rut']['estado_revision']=$flags['revision_rut_pdf'];
            $documentos['upload_file_fiel_rut']=$p['upload_file_fiel_rut'];
        
        }else{
            $sql=" UPDATE cu_documentos_contratacion_tienda
                       SET flags = flags || hstore('carga_rut_pdf', '0') || hstore('fecha_carga_rut_pdf','')  || hstore('revision_rut_pdf', '')
                            WHERE codigo_proveedor =:codigo_proveedor
                      ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            modRs::delete(rs::cuDocumentosContratoTienda,$si['codigo_proveedor'], rs::cuProveedoresRutPDF);
        } 
        if(!empty($p['upload_file_fiel_camara_comercio'])&&!empty($p['upload_file_fiel_camara_comercio']['name'])){
            $p['upload_file_fiel_camara_comercio']['nombre_documento']='Documento camara de comercio';
            $p['upload_file_fiel_camara_comercio']['estado_revision']=$flags['revision_camara_comercio_pdf'];
           $documentos['upload_file_fiel_camara_comercio']=$p['upload_file_fiel_camara_comercio'];
        }else{
            
            modRs::delete(rs::cuDocumentosContratoTienda,$si['codigo_proveedor'], rs::cuProveedoresCamaraComerecioPDF);
            $sql=" UPDATE cu_documentos_contratacion_tienda
                       SET flags = flags || hstore('carga_camara_comercio_pdf', '0') || hstore('fecha_carga_camara_comercio_pdf', '') || hstore('revision_camara_comercio_pdf', '')
                            WHERE codigo_proveedor =:codigo_proveedor
                      ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            
        } 
        
        
        if(!empty( $p['upload_file_fiel_certificacion_bancaria'])&&!empty($p['upload_file_fiel_certificacion_bancaria']['name'])){
            $p['upload_file_fiel_certificacion_bancaria']['nombre_documento']='Documento certificacion bancaria';
            $p['upload_file_fiel_certificacion_bancaria']['estado_revision']=$flags['revision_certificacion_bancaria_pdf'];
            $documentos['upload_file_fiel_certificacion_bancaria']=$p['upload_file_fiel_certificacion_bancaria'];
        }else{
            modRs::delete(rs::cuDocumentosContratoTienda,$si['codigo_proveedor'], rs::cuProveedoresCertificadioBancarioPDF);
            $sql=" UPDATE cu_documentos_contratacion_tienda
                       SET flags = flags || hstore('carga_certificacion_bancaria_pdf', '') || hstore('fecha_carga_certificacion_bancaria_pdf','')  || hstore('revision_certificacion_bancaria_pdf', '')
                            WHERE codigo_proveedor =:codigo_proveedor
                      ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            
            
        } 
        
        if(!empty( $p['upload_file_fiel_cedula_representante_legal'])&&!empty($p['upload_file_fiel_cedula_representante_legal']['name'])){
            $p['upload_file_fiel_cedula_representante_legal']['nombre_documento']='Documento cedula representado legal';
            $p['upload_file_fiel_cedula_representante_legal']['estado_revision']=$flags['revision_cedula_representante_legal_pdf'];
            $documentos['upload_file_fiel_cedula_representante_legal']=$p['upload_file_fiel_cedula_representante_legal'];
            
            
            
            if(empty($p['nombre_representante_legal'])){
                
                throw new JPublicException(" Por favor ingrese un nombre de representante legal valido",1);
            }
            if(empty($p['correo_representante_legal'])){
                
                throw new JPublicException(" Por favor ingrese un correo eletronico de representante legal valido",1);
            }
            if (!filter_var($p['correo_representante_legal'], FILTER_VALIDATE_EMAIL)) {
                
                throw new JPublicException(" Por favor ingrese un correo eletronico de representante legal valido",1);
            }
            
            Proveedor::validarInfoProveedor($p);
            
            
        } else {
            
            modRs::delete(rs::cuDocumentosContratoTienda,$si['codigo_proveedor'], rs::cuProveedoresCedulaRepresentanteLegalPDF );
            
            $sql=" UPDATE cu_documentos_contratacion_tienda
                       SET flags = flags || hstore('carga_cedula_representante_legal_pdf', '0') || hstore('fecha_carga_cedula_representante_legal_pdf', '')  || hstore('revision_cedula_representante_legal_pdf', '')
                            WHERE codigo_proveedor =:codigo_proveedor
                      ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
        }
        
       
        
        foreach ($documentos as $documento){
            
            if ($documento['estado_revision']!='aprobado'){
                self::validarDocumentos($documento);
                self::GuardarDocumentos($documento);
               
            }
        }
        
        
    }

    public static function saveInfoProveedor($p)
    {

        $si = session::info();

        if (!in_array("FInformacionProveedorNe_Save", $si["permisos"]) && $si["tipo"] != "admin") {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }
        
        $db = JDatabase::database();
        $db->transaction();
        
        try {
             if($p['tab']=='informacionTienda'){
                self::guardarInformacionTienda($p);
            }if($p['tab']=='informacionEmpresa'){
                self::guardarInformacionEmpresa($p);
            }if($p['tab']=='tabCargarDocumentos'){
                self::guardarDocumentosContrato($p);
            }
            $db->commit();
        } catch(Exception $e) {
            $db->rollback();
            throw new JPublicException($e->getMessage());
        }
        
        
        
       
    }

    public static function validarDocumentos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        if($p['type'] !='application/pdf'){
            throw new JPublicException("{$p["nombre_documento"]} debe ser en formato pdf");
        }
    }

    public static function GuardarDocumentos($p) {
            $si = session::info();
            $db = JDatabase::database();
            $ca = new JDbQuery($db);
            
           
            
            $ca->prepareSelect("cu_documentos_contratacion_tienda", "resources", "codigo_proveedor=:codigo_proveedor");
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            
            if($ca->size()==0){
                $resources= array();
                $resources['rut_pdf']['id']=0;
                $resources['camara_comercio_pdf']['id']=0;
                $resources['certificado_bancario_pdf']['id']=0;
                $resources['cedula_representante_legal_pdf']['id']=0;
                
            }else{
                $resources =$ca->fetch();
                $resources= json_decode($resources['resources'],true);
            }
            
            
            
            if($p['nombre_documento']=="Documento rut"){
                
                if($p['id']==0 || $p['id']!=$resources['rut_pdf']['id']){
                    
                 //   modMails::clienteNotificarDocumentoCargado (array("codigo_proveedor"=>$si["codigo_proveedor"],"tipo_documento"=>"Rut"));
                }
                
                $sql=" UPDATE cu_documentos_contratacion_tienda
                       SET flags = flags || hstore('carga_rut_pdf', '1') || hstore('fecha_carga_rut_pdf', :fechahora)  || hstore('revision_rut_pdf', 'pendiente') 
                            WHERE codigo_proveedor =:codigo_proveedor 
                      ";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":fechahora", date("d-m-Y g:i A"), true);
                $ca->exec();
                
                modRs::save(rs::cuDocumentosContratoTienda,$si['codigo_proveedor'], rs::cuProveedoresRutPDF, $p);
            }
            
            if($p['nombre_documento']=="Documento camara de comercio"){
                
                 if($p['id']==0 || $p['id']!=$resources['camara_comercio_pdf']['id']){
                    
                 //    modMails::clienteNotificarDocumentoCargado (array("codigo_proveedor"=>$si["codigo_proveedor"],"tipo_documento"=>"Camara Comercio"));
                    
                }
                
                
                modRs::save(rs::cuDocumentosContratoTienda,$si['codigo_proveedor'], rs::cuProveedoresCamaraComerecioPDF, $p);
                $sql=" UPDATE cu_documentos_contratacion_tienda
                       SET flags = flags || hstore('carga_camara_comercio_pdf', '1') || hstore('fecha_carga_camara_comercio_pdf', :fechahora) || hstore('revision_camara_comercio_pdf', 'pendiente') 
                            WHERE codigo_proveedor =:codigo_proveedor
                      ";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":fechahora", date("d-m-Y g:i A"), true);
                $ca->exec();
               
            }
            if($p['nombre_documento']=="Documento certificacion bancaria"){
                
                   if($p['id']==0 || $p['id']!=$resources['certificado_bancario_pdf']['id']){
                    
                    // modMails::clienteNotificarDocumentoCargado (array("codigo_proveedor"=>$si["codigo_proveedor"],"tipo_documento"=>"Certificacion_bancaria"));
                    
                }
                
                
            modRs::save(rs::cuDocumentosContratoTienda,$si['codigo_proveedor'], rs::cuProveedoresCertificadioBancarioPDF, $p);
            $sql=" UPDATE cu_documentos_contratacion_tienda
                       SET flags = flags || hstore('carga_certificacion_bancaria_pdf', '1') || hstore('fecha_carga_certificacion_bancaria_pdf', :fechahora)  || hstore('revision_certificacion_bancaria_pdf', 'pendiente')
                            WHERE codigo_proveedor =:codigo_proveedor
                      ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":fechahora", date("d-m-Y g:i A"), true);
            $ca->exec();
             
            
            }
            if($p['nombre_documento']=="Documento cedula representado legal"){
                
                
                if($p['id']==0 || $p['id']!=$resources['cedula_representante_legal_pdf']['id']){
                    
                    // modMails::clienteNotificarDocumentoCargado (array("codigo_proveedor"=>$si["codigo_proveedor"],"tipo_documento"=>"cedula_representante_legal_pdf"));
                    
                }
                
            $sql = " SELECT flags from cu_documentos_contratacion_tienda
                            WHERE codigo_proveedor =:codigo_proveedor and
                            flags->'carga_cedula_representante_legal_pdf' = '0'
                      ";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();

            if($ca->size() > 0)
            {
                modMails::notificarRepresentanteLegalInicioContratacion(array("codigo_proveedor" => $si["codigo_proveedor"]));
            }
                modRs::save(rs::cuDocumentosContratoTienda,$si['codigo_proveedor'], rs::cuProveedoresCedulaRepresentanteLegalPDF, $p);
                
                $sql=" UPDATE cu_documentos_contratacion_tienda
                       SET flags = flags || hstore('carga_cedula_representante_legal_pdf', '1') || hstore('fecha_carga_cedula_representante_legal_pdf', :fechahora)  || hstore('revision_cedula_representante_legal_pdf', 'pendiente')
                            WHERE codigo_proveedor =:codigo_proveedor
                      ";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
                $ca->bindValue(":fechahora", date("d-m-Y g:i A"), true);
                $ca->exec();
                
            }
            
        
    
    }

    public static function loadPageEstadisticas($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "nombre_referencia";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro_producto_referencia"]);
        $campos = "nombre_marca";
        $where .= " and " . $ca->sqlFieldsFilters($campos, $p["filters"]["filtro_marca"]);
        
        foreach ($p["column_filters"] as $colum  => $valorCulumn){
        	if($valorCulumn != ""){
        		$where .= " and ". $ca->sqlFieldsFilters($colum,$valorCulumn);
        	}
        }

        $sql = "select  
        			nombre_referencia,
        			codigo_producto,
        			nombre_marca,
        			sum(st.ventas_cu) as ventas_cu,sum(st.reservas_cu) as reservas_cu,sum(st.visitas_cu) as visitas_cu,
                    sum(st.ventas_pp) as ventas_pp,sum(st.reservas_pp) as reservas_pp,sum(st.visitas_pp) as visitas_pp
                    from
                    (
                       --portal personalizado proveedor coordiutil
                      select a.codigo_producto,
                            b.nombre||'<br/>'||b.referencia as nombre_referencia,
                            b.nombre_marca,
                            0 as ventas_cu,
                            0 as reservas_cu,
                            0 as visitas_cu,
                            (a.estadisticas->'ventas')::numeric as ventas_pp,
                            (a.estadisticas->'reservas')::numeric as reservas_pp,
                            (a.estadisticas->'visitas')::numeric as visitas_pp
                      from cu_productos_estadisticas_pp a
                      left join matv_cu_productos_tienda b on a.codigo_producto = b.codigo_producto and a.codigo_proveedor = b.codigo_proveedor_pp
                      where b.codigo_proveedor=:codigo_proveedor 
                      -- and ({$where})
                      and b.codigo_proveedor<>a.codigo_proveedor
                      union all
                      --portal personalizado provedor tienda
                      select a.codigo_producto,
                            b.nombre||'<br/>'||b.referencia as nombre_referencia,
                            b.nombre_marca,
                            0 as ventas_cu,
                            0 as reservas_cu,
                            0 as visitas_cu,
                            (a.estadisticas->'ventas')::numeric as ventas_pp,
                            (a.estadisticas->'reservas')::numeric as reservas_pp,
                            (a.estadisticas->'visitas')::numeric as visitas_pp
                      from cu_productos_estadisticas_pp a
                      left join matv_cu_productos_tienda b on a.codigo_producto = b.codigo_producto and a.codigo_proveedor = b.codigo_proveedor_pp
                      where a.codigo_proveedor=:codigo_proveedor 
                ) as st
                where 1=1 
                and ({$where})
             group by st.codigo_producto,st.nombre_referencia,st.nombre_marca";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $result = $ca->execPage($p);

        for ($i = 0; $i < count($result["records"]); $i++) {
            $result["records"][$i]["rs"] = modRs::load(rs::cuProductos, $result["records"][$i]["codigo_producto"], array(rs::cuProductosImagen1));
            $result["records"][$i]["imagen_1"] = "";
            if (isset($result["records"][$i]["rs"]["imagen_1"])) {
                $result["records"][$i]["imagen_1"] = JDbFile::url($result["records"][$i]["rs"]["imagen_1"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }

        return $result;
    }

    public static function exportarEstadisticas($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $camposProductoReferencia = "b.referencia,b.nombre";
        $where = $ca->sqlFieldsFilters($camposProductoReferencia, $p["filtro_producto_referencia"]);
        $campoMarca = "b.nombre_marca";
        $where .= " and " . $ca->sqlFieldsFilters($campoMarca, $p["filtro_marca"]);

        $sql = "select st.codigo_producto,st.nombre,st.referencia,st.nombre_marca,
                    sum(st.ventas_cu) as ventas_cu,sum(st.reservas_cu) as reservas_cu,sum(st.visitas_cu) as visitas_cu,
                    sum(st.ventas_pp) as ventas_pp,sum(st.reservas_pp) as reservas_pp,sum(st.visitas_pp) as visitas_pp
                    from
                    (
                    -- coordiutil
                      select b.codigo_producto,
                            b.nombre,
                            b.referencia,
                            b.nombre_marca,
                            (b.estadisticas_cu->'ventas')::numeric as ventas_cu,
                            (b.estadisticas_cu->'reservas')::numeric as reservas_cu,
                            (b.estadisticas_cu->'visitas')::numeric as visitas_cu,
                            0 as ventas_pp,
                            0 as reservas_pp,
                            0 as visitas_pp
                      from  view_cu_productos b 
                      where b.codigo_proveedor=:codigo_proveedor and ({$where})
                      union all
                       --portal personalizado proveedor coordiutil
                      select a.codigo_producto,
                            b.nombre,
                            b.referencia,
                            b.nombre_marca,
                            0 as ventas_cu,
                            0 as reservas_cu,
                            0 as visitas_cu,
                            (a.estadisticas->'ventas')::numeric as ventas_pp,
                            (a.estadisticas->'reservas')::numeric as reservas_pp,
                            (a.estadisticas->'visitas')::numeric as visitas_pp
                      from cu_productos_estadisticas_pp a
                      left join view_cu_productos_tienda b on a.codigo_producto = b.codigo_producto and a.codigo_proveedor = b.codigo_proveedor_pp
                      where b.codigo_proveedor=:codigo_proveedor and ({$where})
                      and b.codigo_proveedor<>a.codigo_proveedor
                      union all
                      --portal personalizado provedor tienda
                      select a.codigo_producto,
                            b.nombre,
                            b.referencia,
                            b.nombre_marca,
                            0 as ventas_cu,
                            0 as reservas_cu,
                            0 as visitas_cu,
                            (a.estadisticas->'ventas')::numeric as ventas_pp,
                            (a.estadisticas->'reservas')::numeric as reservas_pp,
                            (a.estadisticas->'visitas')::numeric as visitas_pp
                      from cu_productos_estadisticas_pp a
                      left join view_cu_productos_tienda b on a.codigo_producto = b.codigo_producto and a.codigo_proveedor = b.codigo_proveedor_pp
                      where a.codigo_proveedor=:codigo_proveedor and ({$where})
                ) as st
               group by st.codigo_producto,st.nombre,st.referencia,st.nombre_marca
        ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/estadisticas_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = &$wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {

                    if ($k == "ventas_cu" || $k == "reservas_cu" || $k == "visitas_cu" || $k == "ventas_pp" || $k == "reservas_pp" || $k == "visitas_pp") {
                        $ws->writeNumber($row, $col, $v);
                        $col++;
                        continue;
                    }

                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }

    public static function loadEstadisticasRs($p) {
        $si = session::info();
        return $si;
    }

    public static function loadOpcionesBarraMenu($p)
    {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $flagsProv = JUtils::pgsqlHStoreToArray($si["flags"]);
        $result = array();
        
        if ($p["tipo"] == "1" && $flagsProv["flag_categorizacion"] == 0) { // optimizada
            $sql = "select a.codigo_categoria as codigo1, null as codigo2, a.nombre, :tipo as tipo
                    from cu_categoriasn a
                    left join cu_barramenu_pp b on (a.codigo_categoria = b.codigo1 and b.tipo = 1 and b.codigo_proveedor = :codigo_proveedor)
                    where a.tipo = 1
                       and b.codigo1 is null";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":tipo", $p["tipo"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        }
        
        if ($p["tipo"] == "1" && $flagsProv["flag_categorizacion"] == 1) { // optimizada
            $sql = "select a.codigo_categoria as codigo1, null as codigo2, a.nombre, 1 as tipo
                    from cu_categorias_pp a
                    left join cu_barramenu_pp b on (a.codigo_categoria = b.codigo1 and b.tipo = 1 and b.codigo_proveedor = :codigo_proveedor)
                    where a.tipo = 1
                        and b.codigo1 is null
                        and a.codigo_proveedor = :codigo_proveedor";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":tipo", $p["tipo"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        }
        
        if ($p["tipo"] == "2" && $flagsProv["flag_categorizacion"] == 0) { // optimizada
            $sql = "select distinct a.codigo_categoria2 as codigo1, a.codigo_categoria1 as codigo2, a.nombre1||'/'||a.nombre2 as nombre, :tipo as tipo
                    from view_cu_categoriasn a
                    left join cu_barramenu_pp b on (a.codigo_categoria2 = b.codigo1 and b.tipo = 2 and b.codigo_proveedor = :codigo_proveedor)
                    where b.codigo1 is null";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":tipo", $p["tipo"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        }
        
        if ($p["tipo"] == "2" && $flagsProv["flag_categorizacion"] == 1) { // optimizada
            $sql = "select distinct a.codigo_categoria2 as codigo1, a.codigo_categoria1 as codigo2, a.nombre1||'/'||a.nombre2 as nombre, :tipo as tipo
                    from view_cu_categorias_pp a
                    left join cu_barramenu_pp b on (a.codigo_categoria2 = b.codigo1 and b.tipo = 2 and b.codigo_proveedor = :codigo_proveedor)
                    where  b.codigo1 is null
                        and a.codigo_proveedor = :codigo_proveedor";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":tipo", $p["tipo"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        }
        
        if ($p["tipo"] == "3") {
            $ca->prepareSelect(
                "matv_cu_productos_tienda a
				left join cu_barramenu_pp b on (a.codigo_marca=b.codigo1 and b.tipo=3 and b.codigo_proveedor=:codigo_proveedor)",
                "distinct a.codigo_marca as codigo1,null as codigo2, a.nombre_marca as nombre, :tipo as tipo ",
                "a.codigo_proveedor_pp=:codigo_proveedor and a.estado_marca='activo' and a.estado='activo'
				and a.estado_revision='aceptado'
                                and a.inventario > 0
                                and ((a.estado_tienda='activo' and a.codigo_proveedor=a.codigo_proveedor_pp) or (a.codigo_proveedor<>a.codigo_proveedor_pp))
				and b.codigo1 is null",
                "a.nombre_marca"
                );
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":tipo", $p["tipo"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        }
        
        if ($p["tipo"] == "4") { // optimizada
            $sql = "SELECT a.codigo_categoria as codigo1, null as acodigo2, a.nombre, :tipo as tipo
                    from cu_categoriasv a
                    left join cu_barramenu_pp b on (a.codigo_categoria = b.codigo1 and b.codigo_proveedor = :codigo_proveedor and b.tipo = 4)
                    where b.codigo1 IS NULL
                        and a.estado = 'activo'
                        and a.codigo_proveedor = :codigo_proveedor";
            
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":tipo", $p["tipo"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        }
        
        if ($p["tipo"] == "5" && $flagsProv["flag_categorizacion"] == 0) { //optimizada
            $sql = "select distinct a.codigo_categoria3 as codigo1, a.codigo_categoria2 as codigo2, a.codigo_categoria1 as codigo3, a.nombre1||'\/'||a.nombre2||'\/'||a.nombre3 as nombre, :tipo as tipo
                    from view_cu_categoriasn a
                    left join cu_barramenu_pp b on (a.codigo_categoria3 = b.codigo1 and b.tipo = 5 and b.codigo_proveedor = :codigo_proveedor)
                    where b.codigo1 is null";
            
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":tipo", $p["tipo"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        }
        
        if ($p["tipo"] == "5" && $flagsProv["flag_categorizacion"] == 1) { //optimizada
            $sql = "select distinct a.codigo_categoria3 as codigo1, a.codigo_categoria2 as codigo2, a.codigo_categoria1 as codigo3, a.nombre1||'\/'||a.nombre2||'\/'||a.nombre3 as nombre, :tipo as tipo
                    from view_cu_categorias_pp a
                    left join cu_barramenu_pp b on (a.codigo_categoria3 = b.codigo1 and b.tipo = 5 and b.codigo_proveedor = :codigo_proveedor)
                    where b.codigo1 is null
                        and a.codigo_proveedor = :codigo_proveedor";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":tipo", $p["tipo"], false);
            $ca->exec();
            $result = $ca->fetchAll();
        }
        
        return $result;
    }

    public static function eliminarElementoMenu($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $is = "=:codigo2";
        if ($p["codigo2"] == "null") {

            $is = "is null";
        }

        $db->transaction();
        $sql = "delete from cu_barramenu_pp where codigo1=:codigo1 and codigo2 $is and codigo_proveedor=:codigo_proveedor";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo1", $p["codigo1"], false);
        $ca->bindValue(":codigo2", $p["codigo2"], false);
        $ca->exec();
         //throw new JPublicException(print_r($ca->preparedQuery(),1));
        $db->commit();

    }
    
    public static function loadEditRs() {
    	$si = Session::info();
    	return array("session"=>$si);
    }
    
    /**
     * Funcion que se encarga de generar la cache o el archivo donde van a estar los estilos de la tienda en formato json 
     * con la siguiente estructura
     * ruta => /ecommerce/demo/proveedores/rpc/modules/../../../store/controllers/styles
     * nombre_archivo => personalizacion_{nombre_url_tienda}.json
     * $p => array(
     * 	nombre_url => text => nombre url de la tienda => ejemplo agaval , une, exito
     *	json => text=> contiene el json con la personalizacion de los colores
     * retorna vacio
     *)
     */
    public static function generarCacheEstiloTienda($p) {
        global $cfg;
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
    	$fileName = "{$cfg["rutaArchivoJson"]}personalizacion_".$p["nombre_url"].".json";
    	file_put_contents($fileName,$p["json"]);
    	
    	//reseteo el cache de la info del proveedor
        global $manejadorCache;
    	$manejadorCache->set("PP_informacionProveedor_{$si["codigo_proveedor"]}",false,900);
    	
    	$ca->prepare("select func_next_version_personalizacion(:codigo_proveedor_pp)");
    	$ca->bindValue(":codigo_proveedor_pp",$si["codigo_proveedor"], false);
    	$ca->exec();
    }
    
    public static function optenerHtmlTutoriales($p) {
    	$si = Session::info();
    	$db = JDatabase::database();
    	$ca = new JDbQuery($db);
    	
    	$sql = "select
					contenido
				from cu_contenidos where 1=1
					and codigo_contenido =:codigo_contenido";
    	$ca->prepare($sql);
    	$ca->bindValue(":codigo_contenido",103,false);
    	$ca->exec();
    	$result = $ca->fetch();
    	
    	
    
    	return $result;
    }
    
    public static function saveTutorialVisto($p) {
    	$si = Session::info();
    	$db = JDatabase::database();
    	$ca = new JDbQuery($db);
    	
    	$sql = "update cu_proveedores_usuarios set tutorial_visto=true
    			where codigo_proveedor=:codigo_proveedor
    			and usuario=:usuario";
    	
    	$ca->prepare($sql);
    	$ca->bindValue(":codigo_proveedor",$p["codigo_proveedor"],false);
    	$ca->bindValue(":usuario",$p["usuario"],true);
    	$ca->exec();
    	
    	return ;
    	
    }
    
    public static function saveViedeoBienvenidaVisto($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $sql = "update cu_proveedores_usuarios set video_bienvenida_visto=true
    			where codigo_proveedor=:codigo_proveedor
    			and usuario=:usuario";
        
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor",$p["codigo_proveedor"],false);
        $ca->bindValue(":usuario",$p["usuario"],true);
        $ca->exec();
        
        return ;
        
    }
    
    public static function nuevaActualizacion($p) {
    	$si = Session::info();
    	$db = JDatabase::database();
    	$ca = new JDbQuery($db);
    	
    	$sql = "update cu_proveedores_usuarios set flags=flags || hstore('flag_nuevo_actualizacion','0')
    			where codigo_proveedor=:codigo_proveedor
    			and usuario=:usuario";
    	
    	$ca->prepare($sql);
    	$ca->bindValue(":codigo_proveedor",$p["codigo_proveedor"],false);
    	$ca->bindValue(":usuario",$p["usuario"],true);
    	$ca->exec();
    	 
    	return ;
    }
    
    public static function loadResolucionesAll($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "nombre, descripcion, fechahora_inicio, fechahora_fin";
        $where = $ca -> sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $sql = "select * from cu_resolucion_factura where codigo_proveedor={$si["codigo_proveedor"]} and ({$where})";

        $ca->prepare($sql);
        $result = $ca->execPage($p);

        for ($i = 0; $i < count($result["records"]); $i++) {
            $tmp = modRs::load(rs::cuResoluciones, $result["records"][$i]["codigo_resolucion"], array(rs::cuResolucionesLogo));
            if (isset($tmp["logo"])) {
                $result["records"][$i]["logo"] = JDbFile::url($tmp["logo"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }

        return $result;
        
    }
    
    public static function loadResolucionOne($p) {
    	$db = JDatabase::database();
    	$ca = new JDbQuery($db);
        
        $sql = "select * from cu_resolucion_factura where codigo_resolucion={$p['codigo_resolucion']}";
    
        $ca->prepare($sql);
        $ca->exec();
        
        if($ca->size() == 0){
            throw new JPublicException("Resolución no localizada");
        }
        else{
            $result = $ca->fetch();
            $result["rs"] = modRs::load(rs::cuResoluciones, $p["codigo_resolucion"], rs::cuResolucionesLogo);

            foreach ($result["rs"] as $k => $v) {
                $result["rs"][$k]["preview"] = JDbFile::url($v["id"], array("w" => 130, "h" => 60, "frm" => 1));
            }
            
            return $result;
        }
    }
    
    public static function saveResolucion($p) {
        if (empty($p["nombre"])) {
            throw new JPublicException("Nombre inválido");
        }
        if (empty($p["fecha_inicio"])) {
            throw new JPublicException("Fecha inicial inválida");
        }
        if (empty($p["hora_inicio"])) {
            throw new JPublicException("Hora inicial inválido");
        }
        if (empty($p["fecha_fin"])) {
            throw new JPublicException("Fecha final inválida");
        }
        if (empty($p["hora_fin"])) {
            throw new JPublicException("Hora final inválido");
        }
        if (empty($p["descripcion"])) {
            throw new JPublicException("Texto inválido");
        }
        
        $fechaIni = $p["fecha_inicio"].' '.$p["hora_inicio"];
        $fechaFin = $p["fecha_fin"].' '.$p["hora_fin"];
        
        if(!modUtils::validarRangoFechas($fechaIni, $fechaFin)){
            throw new JPublicException("El rango de fechas es inválido");
        }
        
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        if (empty($p["codigo_resolucion"])) { // es un registro nuevo
            $sql = "select * from cu_resolucion_factura where 
                    ((fechahora_inicio < '$fechaFin' and fechahora_inicio > '$fechaIni') or
                    (fechahora_fin < '$fechaIni' and fechahora_fin > '$fechaFin') or
                    (fechahora_inicio < '$fechaIni' and fechahora_fin > '$fechaFin')) and
                    codigo_proveedor = :codigo_proveedor";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            if ($ca->size() > 0){ // verificar que no exista otro con esa fecha
                throw new JPublicException("Ya hay otras resoluciones en este rango de fechas");
            }
            
            $codigoResolucion = $db->nextVal ( "cu_resolucion_factura_codigo_resolucion" );
            $ca->prepareInsert("cu_resolucion_factura", "codigo_resolucion, descripcion, nombre, fechahora_inicio, fechahora_fin, codigo_proveedor");
            $ca->bindValue(":codigo_resolucion", $codigoResolucion, true);
            $ca->bindValue(":descripcion", $p["descripcion"], true);
            $ca->bindValue(":nombre", $p["nombre"],true);
            $ca->bindValue(":fechahora_inicio", $fechaIni, true);
            $ca->bindValue(":fechahora_fin", $fechaFin, true);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false); 
        }
        else{ // es una actualizacion
            $codigoResolucion = $p["codigo_resolucion"];
            $sql = "select * from cu_resolucion_factura where 
                    ((fechahora_inicio < '$fechaFin' and fechahora_inicio > '$fechaIni') or
                    (fechahora_fin < '$fechaIni' and fechahora_fin > '$fechaFin') or
                    (fechahora_inicio < '$fechaIni' and fechahora_fin > '$fechaFin')) and
                    (codigo_proveedor = :codigo_proveedor and codigo_resolucion != :codigo_resolucion)";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":codigo_resolucion", $codigoResolucion, false);
            $ca->exec();
            if ($ca->size() > 0){ // verificar que no exista otro con esa fecha diferente al que se esta editando
                throw new JPublicException("Ya hay otras resoluciones en este rango de fechas");
            }
            
            $ca->prepareUpdate("cu_resolucion_factura", "descripcion, nombre, fechahora_inicio, fechahora_fin", "codigo_resolucion=:codigo_resolucion");
            $ca->bindValue(":codigo_resolucion", $codigoResolucion, true); 
            $ca->bindValue(":descripcion", $p["descripcion"], true);
            $ca->bindValue(":nombre", $p["nombre"],true);
            $ca->bindValue(":fechahora_inicio", $fechaIni, true);
            $ca->bindValue(":fechahora_fin", $fechaFin, true);
        }

        $db->transaction();
        $ca->exec();
        
        if(!empty($p["logo_resolucion"])) {
            modRs::save(rs::cuResoluciones, $codigoResolucion, rs::cuResolucionesLogo, $p["logo_resolucion"]);
        }
        
        $db->commit();
        return;
    }
    
    public static function autocompleteFormasDePago($p) {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $si = session::info();
        
        $like = $p != null ? "descripcion ilike '%{$p["filter"]}%' and" : "";

        $sql = "select descripcion as label, codigo_forma_pago as data
               from cu_formas_pago 
               where activo = '1' and $like codigo_forma_pago in(
                    select unnest(codigos_forma_pago) from cu_proveedores_pp where codigo_proveedor = :codigo_proveedor
               )";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();

        return $ca->fetchAll();
    }
    
    public static function autocompleteRecaudadores($p) {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $si = session::info();
        
        $like = $p != null ? "nombre ilike '%{$p["filter"]}%' and" : "";
        $formas_pago = isset($p["formas_pago"]) ? 'and codigo_formas_pago && (\'{'.implode(',',$p["formas_pago"]).'}\')' : '';

        $sql = "select codigo_recaudador as data, nombre as label
                from cu_recaudadores 
                where estado = 'activo' and $like flags->'visible' = '1' and flags->'fisico' = '1' 
                    $formas_pago
                ";
        $ca->prepare($sql);
        $ca->exec();
        return $ca->fetchAll();
    }
    
    public static function retornaDominioPrincipal() {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $si = session::info();
        
        $sql = "select url_domain
                from cu_domains_pp
                where codigo_proveedor_pp = :codigo_proveedor and flags->'principal' = '1'";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();
        if ($ca->size() == 0){ // verificar que no exista otro con esa fecha diferente al que se esta editando
            $resp = '';
        }
        else{
            $r = $ca->fetch();
            $resp = $r['url_domain'];
        }
        return $resp;
    }
    
    public static function aceptarContrato($p) {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $si = session::info();
        

		$empresa = $p["inboundEmpresa"];
        $empresa["session"] = $si;
        $empresa["codigo_proveedor"] = $si["codigo_proveedor"];
        $empresa["aceptacion_terminos_y_condisiones"] = $p['aceptacion_terminos_y_condisiones'];
        $empresa['codigo_contrato']=$p['codigo_contrato'];
        
    	if($p['aceptacion_terminos_y_condisiones'] !=1){
            throw new JPublicException("Por favor indique la aceptacion de terminos y condiciones");
        }

        /*Se dejan dos transacciones para asegurar con la primera que se guarde la información de la tienda independiente de si se puede aceptar el contrato*/
        $db->transaction();
        modVendesfacilVentas::guardarInboundEmpresaAdminTienda($empresa);
        $db->commit();

        $db->transaction();
        modProveedores::ValidarYGuardaFleteGratisCoordinadora(['codigo_proveedor' => $si["codigo_proveedor"]]);
        modProveedores::aceptarContrato($empresa);
        $db->commit();

        return;
    }
    
    public static function cargarTemas() {
        return modTemas::cargarTemas();
    }
    
    public static function aplicarTema($p) {
        $si = Session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        modTemas::aplicarTema($p);
        self::generarCacheTema($si["codigo_proveedor"]);
        //reseteo el cache de la info del proveedor
        global $manejadorCache;
    	$manejadorCache->set("PP_informacionProveedor_{$si["codigo_proveedor"]}",false,900);
    	$manejadorCache->set("version_personalizacion_{$si["codigo_proveedor"]}",false,900);
        return true;
    }
    
    public static function deshacerTema($p) {
        $si = Session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        modTemas::deshacerTema($p);
        self::generarCacheTema($si["codigo_proveedor"]);
        //reseteo el cache de la info del proveedor
        global $manejadorCache;
    	$manejadorCache->set("PP_informacionProveedor_{$si["codigo_proveedor"]}",false,900);
        $manejadorCache->set("version_personalizacion_{$si["codigo_proveedor"]}",false,900);
        return true;
    }
    
    public static function generarCacheTema($codigoProveedor) {
        global $cfg;
        $datosProveedor = modTemas::retornaDatosCacheTemaProveedor($codigoProveedor);
        
        $coloresNew = json_decode(file_get_contents("{$cfg["rutaArchivoJson"]}personalizacion.json"), true);
        foreach (array_keys($coloresNew) as $k) {
            foreach (array_keys($coloresNew[$k]) as $k1) {
                $coloresNew[$k][$k1]["default"] = $coloresNew[$k][$k1]["default"];

                if (isset($datosProveedor["colores_new"][$k1])) {
                    $coloresNew[$k][$k1]["default"] = $datosProveedor["colores_new"][$k1];
                }
            }
        }

        $pGenerarCache = array(
            "nombre_url" => $datosProveedor["nombre_url"],
            "json" => json_encode($coloresNew)
        );
        self::generarCacheEstiloTienda($pGenerarCache);
    
    }

    public static function obtenerFuentes(){
        return modFuentes::obtenerFuentes();
    }

    public static function aplicarFuente($p){
        $si = Session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modFuentes::aplicarFuente($p);
    }
    
    public static function crearTareaConfiguracionAdminGoogle(){
        $si = Session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modProveedores::crearTareaConfiguracionAdminGoogle($p);
    }
    
    // Notificar a servicio a el cliente que la tienda tiene interes */
    public static function notificarInteresMarketing($p){
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $db->transaction();
        $campos = "codigo_mensaje,asunto,cuerpo,de,para,para_oculto,tipo";
        $ca->prepareInsert("cu_mensajes", $campos);
        $codigoMensaje = $db->nextVal("cu_mensajes_codigo_mensaje");
        $ca->bindValue(":codigo_mensaje", $codigoMensaje, false);
        $ca->bindValue(":asunto","{$si['nombre']}  Marqueting ",true);
        $ca->bindValue(":cuerpo","La tienda {$si['nombre']} esta interesada en la  activo la opciónes  de Marqueting codigo_proveedor = {$si['codigo_proveedor']}",true);
        $ca->bindValue(":de", "notificaciones@coordiutil.com,sebastian.montoya@coordiutil.com", true );
        $ca->bindValue(":para", "{info@vendesfacil.com}", true);
        $ca->bindValue(":para_oculto", "null", false);
        $ca->bindValue(":tipo", "correo", true);
        $ca->exec();
        
        try {
            modMails::coordiutilEnviarCorreoGenerico(array("codigo_mensaje" => $codigoMensaje));
        }catch (Exception $e){
            throw new JPublicException ("<br><b>". $e->getMessage()."<b>");
        }
        $db->commit();
        return ;
    }
    
    public static function obtenerRutaConfiguradorGoogleAnalytics($p){
        global $cfg;
        return $cfg["urlMicrositioConfigGoogleAdmin"];
    }

    public static function obtenerMaestros(){
        $maestros = array(
            "tiposDocumentos" => modVendesfacilVentas::obtenerTiposDocumento()["coleccion"],
            "generos" => modVendesfacilVentas::obtenerGeneros()["coleccion"],
            "tiemposConstitucion" => modVendesfacilVentas::obtenerTiemposConstitucion()["coleccion"],
            "rangosNumeroEmpleados" => modVendesfacilVentas::obtenerColeccionNumeroEmpleados()["coleccion"],
            "tamanosEmpresas" => modVendesfacilVentas::obtenerTamanosEmpresa()["coleccion"],
            "tiposPersonas" => modVendesfacilVentas::obtenerTiposPersona()["coleccion"],
            "departamentosUbicacion" => modCiudades::obtenerDepartamentos()["coleccion"],
            "sectoresEconomicos" => modVendesfacilVentas::obtenerSectoresEconomicos()["coleccion"],
            "participacionMujer" => modVendesfacilVentas::obtenerColeccionParticipacionMujer()["coleccion"]
        );

        return $maestros;
    }

    public static function loadCiudades($codigo_estado){
        return modCiudades::obtenerCiudades($codigo_estado)["coleccion"];
    }

    public static function consultarDatosInboundEmpresa($p){
        return modVendesfacilVentas::consultarInboundAdminTienda($p);
    }

    public static function formasPagoPorDefectoTiendas(){
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_formas_pago", "codigo_forma_pago,descripcion", "activo =true and flags->'forma_pago_por_defecto'='1'");
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function formasPagoTienda(){
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_formas_pago", "codigo_forma_pago,descripcion", "activo = '1' and codigo_forma_pago in(
                    select unnest(codigos_forma_pago) from cu_proveedores_pp where codigo_proveedor = :codigo_proveedor
               )");
        $ca->bindValue(":codigo_proveedor",$si['codigo_proveedor'], false);
        $ca->exec();

        if($ca->size() >=1){
            return $ca->fetchAll();
        }
        return array();
    }
    public static function consultarCertificadoSSLTienda()
    {  
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        
        $ca->prepareSelect("mod_ssl", "primary_domain as url_domain,issued_date as fecha_emision_certificado,certificate_ca,certificate_crt,certificate_key,aliases as url_alias,status,error ", "codigo_proveedor_pp =:codigo_proveerdor");
        $ca->bindValue(":codigo_proveerdor", $si['codigo_proveedor'], false);
        $ca->exec();
        
        if ($ca->size() >= 1) {
             $r= $ca->fetch();
             $r['url_alias']= json_decode($r['url_alias']);
             $r['certificate_key']= self::sencrypt_decrypt('decrypt', $r['certificate_key']);
             return $r;
        } else {
            /*
             * Si en la tabla mod_ssl no trae registros se consulta cu_domains_pp para traer el dominio principal y los s dominios secundario si esta los trae
             * Fecha : 30 mayo 2018 Desarrollador: Sebastian Montoya
             */
            

            $ca->prepareSelect("cu_domains_pp", "url_domain,flags->'principal' as dominio_principal", "codigo_proveedor_pp=:codigo_proveerdor and url_domain ~ '^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$'");
            $ca->bindValue(":codigo_proveerdor", $si['codigo_proveedor'], false);
            $ca->exec();
            
            if ($ca->size() >= 1) {
                $infoDomains = array();
                $infoDomains['url_domain'] =[];
                $infoDomains['url_alias'] = []; /* Dominnios secuandarios */
                
                foreach ($ca->fetchAll() as $rDominio) {
                    if ($rDominio['dominio_principal'] == 1) {
                        $infoDomains['url_alias'][] = $rDominio['url_domain'];
                        $infoDomains['url_domain'] = $rDominio['url_domain'];
                    } else {
                        $infoDomains['url_alias'][] = $rDominio['url_domain'];
                    }
                }
                return $infoDomains;
            }
        }
        return array();
    }
    
    public static function  sencrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = '@garlic.sauce!';
        $secret_iv = 'Vu1(C0YKL8-34u';
        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
    public static function previewarchivo($p){
        if (strpos($p['name'], '.crt')==false){
            throw new JPublicException("formato de archivo incorrecto. tienen que ser .crt", 1);
        }
        $ruta = (JApp::privateTempPath() . "/{$p["tmp_name"]}");
        $contenido = file_get_contents($ruta);
       
        return $contenido;
        
        
    }
    public static function previewarchivoKey($p){
        if (strpos($p['name'], '.key')==false) {
            throw new JPublicException("formato de archivo incorrecto. tienen que ser .key", 1);
        }
        $ruta = (JApp::privateTempPath() . "/{$p["tmp_name"]}");
        $contenido = file_get_contents($ruta);
        
        return $contenido;
        
        
    }
    
    public static function validarInfoProveedor($p){
        
        if (empty($p['numero_documento_representante'])) {
            
            throw new JPublicException(" Por favor ingrese el documento del representante legal.", 1);
        }
        if (empty($p['telefono_representante'])) {
            
            throw new JPublicException(" Por favor ingrese el teléfono del representante legal.", 1);
        }
        if (empty($p['genero_representante']) || $p['genero_representante'] == -1) {
            
            throw new JPublicException(" Por favor ingrese el género del representante legal.", 1);
        }
        if (empty($p['tiempo_constitucion_empresa']) || $p['tiempo_constitucion_empresa'] == -1) {
            
            throw new JPublicException(" Por favor ingrese el tiempo de constitución de la empresa.", 1);
        }
        if (empty($p['numero_empleados_empresa'])) {
            
            throw new JPublicException(" Por favor ingrese el número de empleados de la empresa.", 1);
        }
        if (empty($p['tamano_empresa']) || $p['tamano_empresa'] == -1) {
            
            throw new JPublicException(" Por favor ingrese el tamaño de la empresa.", 1);
        }
        if (empty($p['tipo_persona']) || $p['tipo_persona'] == -1) {
            
            throw new JPublicException(" Por favor ingrese el tipo de persona.", 1);
        }
        if (empty($p['fecha_constitucion'])) {
            
            throw new JPublicException(" Por favor ingrese la fecha de constitución de la empresa valida.", 1);
        }
        
        $date = $p['fecha_constitucion'];
        $format = 'Y-m-d';
        $d = DateTime::createFromFormat($format, $date);
        
        if (!($d && $d->format($format) == $date))
        {
            throw new JPublicException(" Por favor ingrese la fecha de constitución de la empresa valida.", 1);
        }
        if (empty($p['ciudad_empresa']) || $p['ciudad_empresa'] == -1) {
            
            throw new JPublicException(" Por favor ingrese la ciudad de ubicación de la empresa.", 1);
        }
        if (empty($p['direccion_empresa'])) {
            
            throw new JPublicException(" Por favor ingrese la dirección de la empresa.", 1);
        }
        if (empty($p['actividad_economica_empresa']) || $p['actividad_economica_empresa'] == -1) {
            
            throw new JPublicException(" Por favor ingrese el sector económico.", 1);
        }
        if (empty($p['codigo_ciiu_empresa'])) {
            
            throw new JPublicException(" Por favor ingrese el código ciiu valido.", 1);
        }
        
        if (!is_numeric($p['codigo_ciiu_empresa']) || strlen($p['codigo_ciiu_empresa']) != 4){
            
            throw new JPublicException(" Por favor ingrese el código ciiu valido.", 1);
        }
    }
    
    public static function validarUrl($url, $regex){
        if(preg_match($regex, strtolower($url)) == 1) {
            return true;
        }else{
            return false;
        }
    }
    
    public static function convertirUrlHttps($url){
        $a=substr($url, 0, 7);
        $b=substr($url, 0, 8);
        if(substr($url, 0, 7) == "http://"){
            $url = str_ireplace("http://", "https://", $url);
        }elseif(substr($url, 0, 8) != "https://"){
            $url = "https://" . $url;
        }
        return $url;
    }
}