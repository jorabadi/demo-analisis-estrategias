<?php
require_once $cfg["rutaModeloLogico"] . "mod_templates_mk.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_campanas_mk.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_segmentos_mk.inc.php";

class Campanas{
    public static function listarCampana($p){
        $si = session::info();
        $p['codigo_proveedor']=$si['codigo_proveedor'];
        $listadoCampanas = modCampanasMarketing::listarCampana($p);
        return $listadoCampanas;
    }
    public static function CargarRs($p){
        $si = session::info();
        $flagsPp = JUtils::pgsqlHStoreToArray($si["flags_pp"]);
        $result = modCampanasMarketing::cargarRsCampana($flagsPp);
        return $result;
    }
    public static function cargarCampana($p)
    {
        $si = session::info();
        $p['codigo_proveedor'] = $si['codigo_proveedor'];
        
        $campana  = modCampanasMarketing::cargarCampana($p);
        
        return $campana;
    }
    public static function auntoCompletarSegmentos($p)
    
    {
        $si = session::info();
        $p['codigo_proveedor'] = $si['codigo_proveedor'];
        return modSegmentosMarketing::autoCompletarSegmentos($p);
        
    }
    public static function cargarVariablesPlantilla($p)
    {
        $rs = array();

        $si = session::info();
        $codigo_proveedor = $si['codigo_proveedor'];
        
        $rs["info_tienda"] = modPlantillasVariables::obtenerInformacionTienda($codigo_proveedor);
        
        $rs["variables_plantillas"] = modPlantillasVariables::obtenerVariablesPlantilla($p);
        return $rs;
        
    }
    public static function renderPlantilla($p)
    {
        $result    = array();
        $variables = array();
        foreach ($p['variables_plantillas'] as $r) {
            $variables[$r['nombre_variable']] = $r['valor_variable'];
        }
        
        
        $result = modTemplatesMarketing::renderPlantilla($p['plantilla'], $variables);
        
        
        return $result;
    }
    public static function guardarCampana($p)
    {
        $db = JDatabase::database();
        $si= session::info();
        $p['codigo_proveedor'] = $si['codigo_proveedor'];
        $p['codigo_plantilla'] = 0;
        if ($p['tipo_campana']==1){
            $p["codigo_plantilla"] = isset($p['variables_plantillas'][0]['codigo_plantilla']) ? $p["codigo_plantilla"] : 0;
        }
        try {
            return modCampanasMarketing::guardarCampana($p); 
            $db->commit();
            return $result;
        } catch(Exception $ex) {
            $db->rollback();
            throw new JPublicException($ex->getMessage());
        }
    }
    public static function inactivarCampana($p){
        
        $result = modCampanasMarketing::eliminarCampanaCm($p);
        
        return $result;
    }
    
    public static function testcrearCampañaWs($p)
    {
        $si = session::info();
        //llamar el de campaign monitor
        /*
         * correos
         * idCampana
         * nombreCliente => codigo_proveedor => si el cliente no existe modEmailMarketing cree el cliente
         * id_lista Cm
         * asunto
         * nombre_campana
         * nombre_remitente
         * correo_remitente
         * reply_to
         * correo_confirmacion
         * fecha_envio => immediately o 'YYYY-MM-DD 24HH:MM'
         *
         */
        
        
        $rCampana = modCampanasMarketing::listarCampana(array(
            "codigo_proveedor" => $si['codigo_proveedor']
        ));
        
        $rSegmento = modSegmentosMarketing::listarSegmentos(array(
            "codigo_proveedor" => $si['codigo_proveedor']
        ));
        $campana   = array();
        foreach ($rCampana as $r) {
            $r['id_lista'] = $rSegmento[0]['id_lista'];
            $campana = $r;
        }
        $result = modCampanasMarketing::crearCampana($campana);
        
        return $result;
    }
    
    public static function obtenerPlantillasCarritoAbandonado($p){
        return modPlantillas::obtenerPlantillasTipo('carritos_abandonados');
    }
    
    public static function obtenerPlantillasEmail($p){
        return modPlantillas::obtenerPlantillasTipo('campana_email_marketing');
    }
}