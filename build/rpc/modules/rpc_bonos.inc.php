<?php
require_once $cfg["rutaModeloLogico"] . "mod_bonos2.inc.php";
require_once $cfg["rutaModeloLogico"] . "mod_mails.inc.php";

class Bonos {

    public static function loadPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "b.identificacion_tercero,n.numero_nota,b.referencia";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if ($p["filters"]["tipo_bono"] != "todos") {
            $where .= " and b.codigo_tipo={$p["filters"]["tipo_bono"]} ";
        }
        if ($p["filters"]["estado"] != "todos") {
            $where .= " and b.estado='{$p["filters"]["estado"]}' ";
        }

        if ($p["filters"]["con_saldo"] != "todos") {
            if ($p["filters"]["con_saldo"] == "si") {
                $where .= " and b.saldo > 0 ";
            } 
	    else {
                $where .= " and b.saldo = 0 ";
            }
        }
        $whereh = "";
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $whereh .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $sql = "select * 
                from (  
                    select b.codigo_bono,b.fechahora,b.referencia,b.valor,b.saldo,bt.nombre as tipo_bono,
                        case when b.flag_requiere_tercero='1' then 'SI' else 'NO' END as requiere_tercero,
                        b.identificacion_tercero,coalesce(n.numero_nota,'') as numero_nota,b.estado, b.email_beneficiado,
                        case when b.flags->'acumulable' = '1' then 'Si' else 'No' end as acumulable
                    from cu_bonos b
                    left join cu_notas_credito_ventas_pp_enc n on b.codigo_nota = n.codigo_nota
                    join cu_bonos_tipos bt on bt.codigo_tipo = b.codigo_tipo
                    where b.codigo_proveedor_pp = :codigo_proveedor_pp and ({$where})
                ) tl 
                where 1=1 {$whereh}";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function inactivarBono($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareUpdate("cu_bonos", "estado", "codigo_bono=:codigo_bono");
        $ca->bindValue(":codigo_bono", $p["codigo_bono"], false);
        $ca->bindValue(":estado", "inactivo", true);
        $ca->exec();
        return;
    }

    public static function loadEditRs() {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();

        //Estados
        $result["tipo_bono"] = array(
            array("label" => "Promocional", "data" => "1"),
            array("label" => "Masivo con valor", "data" => "6"),
            array("label" => "Masivo con porcentaje", "data" => "9")
        );

        //Vendedores
        $ca->prepareSelect("cu_vendedores_pp", "codigo_vendedor as data, nombre as label", "codigo_proveedor_pp = :codigo_proveedor_pp");
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"]);
        $ca->exec();
        if ($ca->size() == 0) {
            $result["vendedores"] = array();
        } 
	else {
            $result["vendedores"] = $ca->fetchAll();
        }

        //Defaults
        $result["defaults"] = array("tipo_bono" => "1");

        $flagsPp = JUtils::pgsqlHStoreToArray($si["flags_pp"]);
        if ($flagsPp["usa_vendedores"] == '1') {
            $result["usa_vendedores"] = '1';
        } 
	else {
            $result["usa_vendedores"] = '0';
        }

        return $result;
    }

    public static function loadEditRsRedenciones() {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();
        /*

          $sql = "select a.codigo_categoria as data, a.nombre as label
          from cu_categorias_pp a
          where tipo=1
          and a.codigo_proveedor=:codigo_proveedor
          order by a.nombre";

          $ca->prepare($sql);


          $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
          $ca->exec();

          $result["negocios"] = $ca->fetchAll();


         */

        $result["entidades"] = array(
            array("label" => "Selecciones entidad:", "data" => "0"),
            array("label" => "Negocios", "data" => "1"),
            array("label" => "Categorias", "data" => "2"),
            array("label" => "Sub Categorias", "data" => "3"),
            array("label" => "Marcas", "data" => "4"),
            array("label" => "Categorias virtuales", "data" => "5")
        );



        //throw new Exception(print_r($result,1));
        $result["defaults"] = array("entidades" => "0");
        return $result;
    }

    public static function save($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["codigo_acuerdo"])) {
            throw new JPublicException("Debe seleccionar un codigo de acuerdo de redención");
        }

        if (($p["codigo_tipo"] == '1' || $p["codigo_tipo"] == '6') && !is_numeric($p["valor"])) {
            throw new JPublicException("Debes ingresar un valor válido para el bono");
        }

        if ($p["codigo_tipo"] == '9' && !is_numeric($p["valor"])) {
            throw new JPublicException("Debes ingresar un valor porcentual (entre 0 y 100) para el bono");
        }

        if (($p["codigo_tipo"] == '6' || $p["codigo_tipo"] == '9') && $p["clave"] == '') {
            throw new JPublicException("Debes ingresar la clave para el bono");
        }

        if (($p["codigo_tipo"] == '6' || $p["codigo_tipo"] == '9') && $p["referencia"] == '') {
            throw new JPublicException("Debes ingresar la referencia para el bono");
        }

        if ($p["codigo_tipo"] == '6' || $p["codigo_tipo"] == '9') {
            $p["clave_referencia_manual"] = true;
        }

        //genero el bono con los datos
        $p["codigo_proveedor_pp"] = $si["codigo_proveedor"];
        $p["codigo_cliente"] = "null";
        $p["valor_minimo_compra"] = 0;
        $p["codigo_acuerdo"] = $p["codigo_acuerdo"][0];

        $rBono = modBonos2::generar($p);
        $p["referencia"] = $rBono["referencia"];
        self::enviarBono($p); //envio el bono
    }

    public static function exportarBonos($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "b.identificacion_tercero,n.numero_nota,b.referencia";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if ($p["filters"]["tipo_bono"] != "todos") {
            $where .= " and b.codigo_tipo={$p["filters"]["tipo_bono"]} ";
        }
        if ($p["filters"]["estado"] != "todos") {
            $where .= " and b.estado='{$p["filters"]["estado"]}' ";
        }

        if ($p["filters"]["con_saldo"] != "todos") {
            if ($p["filters"]["con_saldo"] == "si") {
                $where .= " and b.saldo > 0 ";
            } else {
                $where .= " and b.saldo = 0 ";
            }
        }

        $sql = "select b.codigo_bono,b.fechahora,b.referencia,b.valor,b.saldo,bt.nombre as tipo_bono,
                case when b.flag_requiere_tercero='1' then 'SI' else 'NO' END as requiere_tercero,
                b.identificacion_tercero,coalesce(n.numero_nota,'') as numero_nota,b.estado,b.email_beneficiado
                from cu_bonos b
                left join cu_notas_credito_ventas_pp_enc n on b.codigo_nota=n.codigo_nota
                join cu_bonos_tipos bt on bt.codigo_tipo = b.codigo_tipo
                where b.codigo_proveedor_pp=:codigo_proveedor_pp and {$where}";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        $path = JApp::privateTempPath() . "/bonos_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = &$wb->addWorksheet('Hoja1');

        if ($ca->size() == 0){
            $ws->writeString(0, 0, "No hay informacion para descargar");
        } 
        else{
            $row = 0;
            foreach ($ca->fetchAll() as $r) {
                $col = 0;
                if ($row == 0) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {
                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }
                $row++;
            }
        }

        $wb->close();
        return basename($path);
    }

    public static function enviarBono($p) {
        $si = Session::info();
        $p["codigo_proveedor_pp"] = $si["codigo_proveedor"];
        modMails::beneficiarioNotificarBonoTienda($p);
    }

    public static function loadAcuerdosCreacionPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "r.nombre,re.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if (isset($p["filters"]["fecha_desde"]) && $p["filters"]["fecha_desde"] != "" && isset($p["filters"]["fecha_hasta"]) && $p["filters"]["fecha_hasta"] != "") {
            $where .= " and (cast(r.fechahora_inicial as date),cast(r.fechahora_final as date)) overlaps ('{$p["filters"]["fecha_desde"]}'::date,'{$p["filters"]["fecha_hasta"]}'::date) ";
        }

        if ($p["filters"]["accion"] != "todos") {
            $where .= " and r.codigo_accion_pp='{$p["filters"]["accion"]}' ";
        }

        $whereh = "";
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $whereh .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $sql = "select * 
                from (
                    select r.codigo_acuerdo,r.nombre,r.valor_minimo_compra,coalesce(re.nombre,'') as acuerdo_redencion,
                        r.fechahora_inicial,r.fechahora_final,coalesce(a.nombre,'') as accion, valor_bono
                    from cu_bonos_acuerdo_creacion r
                    left join cu_acciones_pp a on r.codigo_accion_pp= a.codigo_accion
                    left join cu_bonos_acuerdo_redencion re on r.codigo_acuerdo_redencion = re.codigo_acuerdo 
                    where r.codigo_proveedor = :codigo_proveedor_pp and ({$where})
                ) tl 
                where 1=1 {$whereh}";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function loadAcuerdosRedencionEditRs($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();

        //Estados
        $ca->prepareSelect("cu_acciones_pp", "nombre as label,codigo_accion as data", "true");
        $ca->exec();
        $result["acciones"] = $ca->fetchAll();
        array_unshift($result["acciones"], array("data" => "", "label" => "Seleccione.."));

        $result["hora_inicial"] = "00:00:00";
        $result["hora_final"] = "23:59:59";

        //Defaults
        $result["defaults"] = array(
            "tipo_acuerdo" => "registro",
            "acciones" => ""
        );
        return $result;
    }

    public static function autocompleteAcuerdosRedencion($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.codigo_acuerdo as data,a.nombre as label
        from cu_bonos_acuerdo_redencion a
        where 1=1 and a.codigo_proveedor=:codigo_proveedor and {$where}
        order by a.nombre";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        return $ca->fetchAll();
    }

    public static function saveAcuerdosCreacion($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["nombre"])) {
            throw new JPublicException("Debe seleccionar el nombre");
        }

        if (empty($p["codigo_accion_pp"])) {
            throw new JPublicException("Debe seleccionar la acción");
        }

        if (empty($p["valor_bono"])) {
            throw new JPublicException("Debe seleccionar el valor del bono");
        }

        if (empty($p["codigo_acuerdo_redencion"])) {
            throw new JPublicException("Debe seleccionar acuerdo redencion");
        }

        $db->transaction();

        $campos = "codigo_acuerdo,nombre,descripcion,codigo_proveedor,fechahora_inicial,fechahora_final,
        valor_minimo_compra,codigo_acuerdo_redencion,valor_bono,codigo_accion_pp,formas_pago_incluidos,recaudadores_incluidos";

        if ($p["codigo_acuerdo"] === "") {
            $ca->prepareSelect("cu_bonos_acuerdo_creacion", "codigo_acuerdo", "codigo_proveedor=:codigo_proveedor and codigo_accion_pp=:codigo_accion_pp
                and (cast(fechahora_inicial as timestamp),cast(fechahora_final as timestamp)) overlaps (cast(:fechahora_inicial as timestamp),cast(:fechahora_final as timestamp))");
            $ca->bindValue(":codigo_accion_pp", $p["codigo_accion_pp"], false);
            $ca->bindValue(":fechahora_inicial", "{$p["fecha_inicial"]} {$p["hora_inicial"]}", true);
            $ca->bindValue(":fechahora_final", "{$p["fecha_final"]} {$p["hora_final"]}", true);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();

            if ($ca->size() > 0) {
                $db->rollback();
                throw new JPublicException("Ya existe un acuerdo de creación en estas fechas para la acción dada.");
            }

            $ca->prepareInsert("cu_bonos_acuerdo_creacion", $campos);
            $codigoAcuerdo = $db->nextVal("cu_bonos_acuerdo_creacion_codigo_acuerdo");
        } 
	else {
            $ca->prepareUpdate("cu_bonos_acuerdo_creacion", $campos, "codigo_acuerdo=:codigo_acuerdo");
            $codigoAcuerdo = $p["codigo_acuerdo"];
        }

        $formasPago = JUtils::arrayToPgsqlArray($p["formas_pago"]);
        $recaudadores = JUtils::arrayToPgsqlArray($p["recaudadores"]);

        $ca->bindValue(":codigo_acuerdo", $codigoAcuerdo, false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":descripcion", $p["descripcion"], true);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":fechahora_inicial", "{$p["fecha_inicial"]} {$p["hora_inicial"]}", true);
        $ca->bindValue(":fechahora_final", "{$p["fecha_final"]} {$p["hora_final"]}", true);
        $ca->bindValue(":valor_minimo_compra", !empty($p["valor_minimo_compra"]) ? $p["valor_minimo_compra"] : 0, false);
        $ca->bindValue(":valor_bono", $p["valor_bono"], false);
        $ca->bindValue(":codigo_acuerdo_redencion", !empty($p["codigo_acuerdo_redencion"][0]) ? $p["codigo_acuerdo_redencion"][0] : 0, false);
        $ca->bindValue(":codigo_accion_pp", !empty($p["codigo_accion_pp"]) ? $p["codigo_accion_pp"] : 0, false);
        $ca->bindValue(":formas_pago_incluidos", $formasPago, true);
        $ca->bindValue(":recaudadores_incluidos", $recaudadores, true);

        $ca->exec();
        $db->commit();

        return;
    }

    public static function loadOneAcuerdosCreacion($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select r.codigo_acuerdo,r.nombre,r.descripcion,
                    round(r.valor_minimo_compra) as valor_minimo_compra,
                    r.codigo_acuerdo_redencion,r.valor_bono,
                    cast(r.fechahora_inicial as date) as fecha_inicial,
                    cast(r.fechahora_final as date) as fecha_final,
                    cast(r.fechahora_inicial as time) as hora_inicial,
                    cast(r.fechahora_final as time) as hora_final,
                    r.codigo_accion_pp, r.recaudadores_incluidos, r.formas_pago_incluidos
                 from cu_bonos_acuerdo_creacion r
                 where r.codigo_acuerdo = :codigo_acuerdo";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_acuerdo", $p["codigo_acuerdo"], false);
        $ca->exec();
        $result = $ca->fetch();

        $sql = "select a.codigo_acuerdo as data,a.nombre as label
        from cu_bonos_acuerdo_redencion a
        where a.codigo_acuerdo = :codigo_acuerdo
        order by a.nombre";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_acuerdo", $result["codigo_acuerdo_redencion"], false);
        $ca->exec();

        $result["codigo_acuerdo_redencion"] = $ca->fetchAll();

        return $result;
    }

    public static function loadAcuerdoRedencionPage($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "codigo_acuerdo";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if (isset($p["filters"]["fecha_desde"]) && $p["filters"]["fecha_desde"] != "" && isset($p["filters"]["fecha_hasta"]) && $p["filters"]["fecha_hasta"] != "") {
            $where .= " and (cast(fecha_inicial as date),cast(fecha_final as date)) overlaps ('{$p["filters"]["fecha_desde"]}'::date,'{$p["filters"]["fecha_hasta"]}'::date) ";
        }

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        $sql = "SELECT * from cu_bonos_acuerdo_redencion
                where 1=1 and ({$where}) and codigo_proveedor=:codigo_proveedor";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function loadOneAcuerdosRedencion($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $respuesta = array();
        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);

        $sql = "SELECT * from cu_bonos_acuerdo_redencion
                where 1=1 and codigo_acuerdo=:codigo_acuerdo and codigo_proveedor=:codigo_proveedor";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_acuerdo", $p["codigo_acuerdo"], false);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $respuesta = $ca->fetch();

        if ($respuesta["formas_pago_incluidas"] != '{}') {
            $eliminar = array('{', '}');
            $respuesta['formas_pago_incluidas'] = str_replace($eliminar, '', $respuesta['formas_pago_incluidas']);
            $codigoFormaPago = explode(',', $respuesta['formas_pago_incluidas']);

            foreach ($codigoFormaPago as $formaPago) {
                $ca->prepareSelect("cu_formas_pago", "descripcion as label , codigo_forma_pago as data", "codigo_forma_pago=:codigo_forma_pago");
                $ca->bindValue(":codigo_forma_pago", $formaPago, false);
                $ca->exec();
                $result['formas_pago_incluidas'][] = $ca->fetch();
            }
        }
        $respuesta["formas_pago_incluidas"] = empty($result['formas_pago_incluidas']) ? "" : $result['formas_pago_incluidas'];

        if ($respuesta["negocios_incluidos"] != '{}') {
            $eliminar = array('{', '}');
            $respuesta['negocios_incluidos'] = str_replace($eliminar, '', $respuesta['negocios_incluidos']);
            $codigosNegocios = explode(',', $respuesta['negocios_incluidos']);


            foreach ($codigosNegocios as $negocio) {
                if ($flags["flag_categorizacion"] == 1) {
                    $ca->prepareSelect("cu_categorias_pp a", "a.codigo_categoria as data,a.nombre as label", "codigo_categoria=:codigo_categoria and a.tipo=1");
                    $ca->bindValue(":codigo_categoria", $negocio, false);
                }if ($flags["flag_categorizacion"] == 0) {
                    $ca->prepareSelect("cu_categoriasn a", "a.codigo_categoria as data,a.nombre as label", "codigo_categoria=:codigo_categoria and a.tipo=1");
                    $ca->bindValue(":codigo_categoria", $negocio, false);
                }
                $ca->exec();
                $result['negocios_incluidos'][] = $ca->fetch();
            }
        }
        $respuesta["negocios_incluidos"] = empty($result['negocios_incluidos']) ? "" : $result['negocios_incluidos'];

        if ($respuesta["categorias_incluidas"] != '{}') {
            $eliminar = array('{', '}');
            $respuesta['categorias_incluidas'] = str_replace($eliminar, '', $respuesta['categorias_incluidas']);
            $codigosCategorias = explode(',', $respuesta['categorias_incluidas']);

            foreach ($codigosCategorias as $categorias) {
                if ($flags["flag_categorizacion"] == 1) {
                    $ca->prepareSelect("cu_categorias_pp a", "a.codigo_categoria as data,a.nombre as label", "codigo_categoria=:codigo_categoria and a.tipo=2");
                    $ca->bindValue(":codigo_categoria", $categorias, false);
                }
                if ($flags["flag_categorizacion"] == 0) {
                    $ca->prepareSelect("cu_categoriasn a", "a.codigo_categoria as data,a.nombre as label", "codigo_categoria=:codigo_categoria and a.tipo=2");
                    $ca->bindValue(":codigo_categoria", $categorias, false);
                }
                $ca->exec();
                $result['categorias_incluidas'][] = $ca->fetch();
            }
        }
        $respuesta["categorias_incluidas"] = empty($result['categorias_incluidas']) ? "" : $result['categorias_incluidas'];


        if ($respuesta["subcategorias_incluidas"] != '{}') {
            $eliminar = array('{', '}');
            $respuesta['subcategorias_incluidas'] = str_replace($eliminar, '', $respuesta['subcategorias_incluidas']);
            $codigosSubcategorias = explode(',', $respuesta['subcategorias_incluidas']);

            foreach ($codigosSubcategorias as $subcategorias) {
                if ($flags['flag_categorizacion'] == 1) {
                    $ca->prepareSelect("cu_categorias_pp a", "a.codigo_categoria as data,a.nombre as label", "codigo_categoria=:codigo_categoria and a.tipo=3");
                    $ca->bindValue(":codigo_categoria", $subcategorias, false);
                }
                if ($flags['flag_categorizacion'] == 0) {
                    $ca->prepareSelect("cu_categoriasn a", "a.codigo_categoria as data,a.nombre as label", "codigo_categoria=:codigo_categoria and a.tipo=3");
                    $ca->bindValue(":codigo_categoria", $subcategorias, false);
                }
                $ca->exec();
        		$sub_categorias = array();
        		if($ca->size() > 0){
        		    $sub_categorias = $ca->fetch();
        		}
        		$result['subcategorias_incluidas'][] = $sub_categorias;
            }
        }
        $respuesta["subcategorias_incluidas"] = empty($result['subcategorias_incluidas']) ? "" : $result['subcategorias_incluidas'];

        if ($respuesta["recaudadores_incluidos"] != '{}') {
            $eliminar = array('{', '}');
            $respuesta['recaudadores_incluidos'] = str_replace($eliminar, '', $respuesta['recaudadores_incluidos']);
            $codigosBancos = explode(',', $respuesta['recaudadores_incluidos']);
            foreach ($codigosBancos as $banco) {
                $ca->prepareSelect("cu_recaudadores", "codigo_recaudador as data,nombre as label", "codigo_recaudador=:codigo_recaudador");
                $ca->bindValue(":codigo_recaudador", $banco, false);
                $ca->exec();
                $result['recaudadores_incluidos'][] = $ca->fetch();
            }
        }
        $respuesta["recaudadores_incluidos"] = empty($result['recaudadores_incluidos']) ? "" : $result['recaudadores_incluidos'];

        if ($respuesta["idciudades_destino"] != '{}') {
            $eliminar = array('{', '}');
            $respuesta['idciudades_destino'] = str_replace($eliminar, '', $respuesta['idciudades_destino']);
            $codigosCiudades = explode(',', $respuesta['idciudades_destino']);
            foreach ($codigosCiudades as $ciudad) {
                $ca->prepareSelect("cu_ciudades a left join gen_sitios b on (a.codigo_pais=b.codigo_pais)", "a.idciudad as data,a.nombre as label", "idciudad=:idciudad and a.codigo_pais = 'CO'");
                $ca->bindValue(":idciudad", $ciudad, false);
                $ca->exec();
                $result['idciudades_destino'][] = $ca->fetch();
            }
        }
        $respuesta["idciudades_destino"] = empty($result['idciudades_destino']) ? "" : $result['idciudades_destino'];

        if ($respuesta["productos_incluidos"] != '{}') {
            $eliminar = array('{', '}');
            $codigosProductos = str_replace($eliminar, '', $respuesta['productos_incluidos']);
            $ca->prepareSelect("view_cu_productos_tienda a", "a.codigo_producto as data,'['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca as label,
        						a.inventario,a.estado_revision,a.estado", "codigo_producto in (:codigo_producto) and codigo_proveedor_pp=:codigo_proveedor_pp");
            $ca->bindValue(":codigo_producto", $codigosProductos, false);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();
            $result['productos_incluidos'] = $ca->fetchAll();
        }
        $respuesta["productos_incluidos"] = empty($result['productos_incluidos']) ? "" : $result['productos_incluidos'];

        return $respuesta;
    }

    public static function saveAcuerdoRedencion($p) {

        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $rProductos = array();
        $rNegocios = array();
        $rCategorias = array();
        $rSubCategorias = array();
        $rCiudades = array();
        $rFormasPagos = array();
        $rRecaudadores = array();


        foreach ($p['productos_incluidos'] as $r) {
            $rProductos[] = $r['data'];
        }
        foreach ($p['negocios_r'] as $r) {
            $rNegocios[] = $r;
        }
        foreach ($p['categorias_r'] as $r) {
            $rCategorias[] = $r;
        }
        foreach ($p['subcategorias_r'] as $r) {
            $rSubCategorias[] = $r;
        }
        foreach ($p['codigo_ciudades'] as $r) {
            $rCiudades[] = $r;
        }
        foreach ($p['formas_pago'] as $r) {
            $rFormasPagos[] = $r;
        }
        foreach ($p['recaudadores'] as $r) {
            $rRecaudadores[] = $r;
        }

        $codigosProductos = implode(",", $rProductos);
        $codigosNegocios = implode(",", $rNegocios);
        $codigosCategorias = implode(",", $rCategorias);
        $codigosSubCategoria = implode(",", $rSubCategorias);
        $codigosCiudades = implode(",", $rCiudades);
        $codigosFormasPago = implode(",", $rFormasPagos);
        $codigosRecaudadores = implode(",", $rRecaudadores);


        $campos = "nombre,valor_minimo_compra,fecha_inicial,fecha_final,descripcion,codigo_acuerdo,codigo_proveedor,flag_valido_oferta,dias_vigencia,
        		   productos_incluidos,negocios_incluidos,categorias_incluidas,subcategorias_incluidas,idciudades_destino,formas_pago_incluidas,
        		   recaudadores_incluidos,maximo_redenciones,maximo_redenciones_por_cliente";

        if ($p["codigo_acuerdo"] == "") {
            $ca->prepareInsert("cu_bonos_acuerdo_redencion", $campos);
            $codigoAcuerdo = $db->nextVal("cu_bonos_acuerdo_redencion_codigo_acuerdo");
        } 
	else {
            $ca->prepareUpdate("cu_bonos_acuerdo_redencion", $campos, "codigo_acuerdo=:codigo_acuerdo");
            $codigoAcuerdo = $p["codigo_acuerdo"];
        }



        $ca->bindValue(":codigo_acuerdo", $codigoAcuerdo, false);
        $ca->bindValue(":nombre", $p["nombre"], true);
        $ca->bindValue(":flag_valido_oferta", $p["flag_oferta"], true);
        $ca->bindValue(":valor_minimo_compra", $p["valor_minimo_compra"], false);
        $ca->bindValue(":fecha_inicial", $p["fecha_inicial"], true);
        $ca->bindValue(":fecha_final", $p["fecha_final"], true);
        $ca->bindValue(":descripcion", $p["descripcion"], true);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":dias_vigencia", empty($p["dias_vigencia"]) ? null : $p['dias_vigencia'], false);
        $ca->bindValue(":productos_incluidos", "{" . $codigosProductos . "}", true);
        $ca->bindValue(":negocios_incluidos", "{" . $codigosNegocios . "}", true);
        $ca->bindValue(":categorias_incluidas", "{" . $codigosCategorias . "}", true);
        $ca->bindValue(":subcategorias_incluidas", "{" . $codigosSubCategoria . "}", true);
        $ca->bindValue(":idciudades_destino", "{" . $codigosCiudades . "}", true);
        $ca->bindValue(":formas_pago_incluidas", "{" . $codigosFormasPago . "}", true);
        $ca->bindValue(":recaudadores_incluidos", "{" . $codigosRecaudadores . "}", true);
        $ca->bindValue(":maximo_redenciones", empty($p["maximo_redenciones"]) ? null : $p['maximo_redenciones'], false);
        $ca->bindValue(":maximo_redenciones_por_cliente", empty($p["maximo_redenciones_por_cliente"]) ? null : $p['maximo_redenciones_por_cliente'], false);
        $ca->exec();

        return $codigoAcuerdo;
    }

    public static function autoCompletarProducto($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre,a.referencia";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.codigo_producto as data,
            '['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca as label
            from view_cu_productos_tienda a
            where 1=1 and {$where} and codigo_proveedor_pp=:codigo_proveedor
            order by a.nombre";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function loadEntidades($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();
        if ($p == 1) {

            $sql = "select a.codigo_categoria as data, 
    						a.nombre as label,
    						'Negocio' as tipo,
    						1 as codigo_tipo
    				from cu_categorias_pp a 
    						where 1=1 
    				and a.codigo_proveedor=:codigo_proveedor
    				and a.tipo=1
    				order by a.nombre";

            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();

            $result["entidades"] = $ca->fetchAll();
        }
        if ($p == 2) {

            $sql = "select a.codigo_categoria as data,
    				a.nombre as label,
    				a.vpath,
    				'Categoria' as tipo,
    				2 as codigo_tipo
    				from cu_categorias_pp a
    					where 1=1
    				and a.codigo_proveedor=:codigo_proveedor
    				and a.tipo=2
    				order by a.nombre";

            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
            foreach ($ca->fetchAll() as $categoria) {

                $vpath = explode("/", $categoria["vpath"]);
                $categoria["arbol"] = $vpath[0] . "<b style=color:blue>Negocio</b> :<b>" . $vpath[1] . "</b>";
                $result["entidades"][] = $categoria;
            }
        }if ($p == 3) {

            $sql = "select a.codigo_categoria as data,
    				a.nombre as label,
    				a.vpath,
    				'Sub categoria' as tipo,
    				3 as codigo_tipo
    				from cu_categorias_pp a
    					where 1=1
    				and a.codigo_proveedor=:codigo_proveedor
    				and a.tipo=3
    				order by a.nombre";

            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();

            foreach ($ca->fetchAll() as $subCategoria) {

                $vpath = explode("/", $subCategoria["vpath"]);
                $subCategoria["arbol"] = $vpath[0] . "<b style=color:blue>N</b> :<b>" . $vpath[1] . "</b><b style=color:blue> C</b><b> : " . $vpath[2] . "</b>";
                $result["entidades"][] = $subCategoria;
            }
        }
        if ($p == 4) {

            $sql = "select 
					a.codigo_marca as data, 
					b.nombre as label, 
					'Marca' as tipo,
					4 as codigo_tipo
					from cu_proveedores_marcas a
					join cu_marcas b on (a.codigo_marca=b.codigo_marca)
    				and a.codigo_proveedor=:codigo_proveedor
    				order by b.nombre";

            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();

            $result["entidades"] = $ca->fetchAll();
        }if ($p == 5) {

            $sql = "select a.codigo_categoria as data,
						   a.nombre as label,
    					   'Categoria virtual' as tipo,
    					   5  as codigo_tipo
    				from  cu_categoriasv a
    				where 1=1
    				and a.codigo_proveedor=:codigo_proveedor
    				order by a.nombre";

            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();

            $result["entidades"] = $ca->fetchAll();
        }

        return $result;
    }

    public static function autocompleteNegocios($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);

        if ($flags["flag_categorizacion"] == 0) {
            $sql = "select a.codigo_categoria as data,a.nombre as label
        			from cu_categoriasn a
        			where 1=1 and a.tipo=1 and {$where}
        			order by a.nombre";
            $ca->prepare($sql);
        }
        if ($flags["flag_categorizacion"] == 1) {
            $sql = "select a.codigo_categoria as data,a.nombre as label
        			from cu_categorias_pp a
        			where 1=1 and a.tipo=1 and {$where}
        			and a.codigo_proveedor=:codigo_proveedor
        			order by a.nombre";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        }
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteCategorias($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);

        if (!empty($p["codigos_negocios"])) {
            $rNegocios = array();
            foreach ($p["codigos_negocios"] as $r) {
                $rNegocios[] = $r;
            }
            $codigosNegocios = implode(",", $rNegocios);
            $where .= " and a.categoria_superior  in ({$codigosNegocios})";
        }


        if ($flags["flag_categorizacion"] == 1) {



            $sql = "select a.codigo_categoria as data,a.nombre as label
    				from cu_categorias_pp a
    				where 1=1 and a.tipo=2 and {$where}
    				and a.codigo_proveedor=:codigo_proveedor
    				order by a.nombre";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        }
        if ($flags["flag_categorizacion"] == 0) {


            $sql = "select a.codigo_categoria as data,a.nombre as label
    				 from cu_categoriasn a
    				 where 1=1 and a.tipo=2 and {$where}
    				 order by a.nombre";
            $ca->prepare($sql);
        }
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteSubcategorias($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);

        if (!empty($p["codigos_categorias"])) {

            $rCategorias = array();
            foreach ($p["codigos_categorias"] as $r) {
                $rCategorias[] = $r;
            }

            $codigosCategorias = implode(",", $rCategorias);
            $where .= " and a.categoria_superior  in ({$codigosCategorias})";
        }
        if ($flags["flag_categorizacion"] == 1) {

            $sql = "select a.nombre as label,a.codigo_categoria as data
	    		from cu_categorias_pp a
	    		join cu_categorias_pp b on (a.categoria_superior=b.codigo_categoria)
	    		join cu_categorias_pp  c on (c.tipo=1 and  b.categoria_superior=c.codigo_categoria)
	    		where 1=1
	    		and  a.tipo=3
	    		and {$where}
	    		and a.codigo_proveedor=:codigo_proveedor
	    		order by a.nombre";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        }
        if ($flags["flag_categorizacion"] == 0) {

            $sql = "select a.nombre as label,
		    		a.codigo_categoria as data
		    		from cu_categoriasn a
		    		join cu_categoriasn b on (a.categoria_superior=b.codigo_categoria)
		    		join cu_categoriasn c on (c.codigo_categoria = any (a.negocios))
		    		where 1=1 and a.estado='activo' and b.estado='activo' and c.estado='activo' and a.tipo=3 and ({$where})
		    		order by c.nombre,b.nombre,a.nombre";
            $ca->prepare($sql);
        }


        $ca->exec();
        return $ca->fetchAll();
    }

    public static function loadProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);

        $p["codigo_proveedor"] = empty($p["codigo_proveedor"][0]) ? 0 : $p["codigo_proveedor"][0];

        $rMarcas = array();
        $rProductos = array();
        $rNegocios = array();
        $rCategorias = array();
        $rCategoriasvirtual = array();
        $rSubcategorias = array();
        $oferta = "";

        foreach ($p["productos"] as $r) {
            $rProductos[] = $r;
        }

        foreach ($p["negocios"] as $r) {
            $rNegocios[] = $r;
        }
        foreach ($p["categorias"] as $r) {
            $rCategorias[] = $r;
        }
        foreach ($p["subcategorias"] as $r) {
            $rSubcategorias[] = $r;
        }
        foreach ($p["marcas"] as $r) {
            $rMarcas[] = $r;
        }

        $codigosMarcas = implode(",", $rMarcas);
        $codigosProductos = implode(",", $rProductos);
        $codigosNegocios = implode(",", $rNegocios);
        $codigosCategorias = implode(",", $rCategorias);
        $codigosSubcategorias = implode(",", $rSubcategorias);



        $where = "";
        $where .= empty($codigosProductos) ? "" : " and codigo_producto in (:codigos_productos)";
        $where .= empty($p["codigo_proveedor"]) ? "" : " and codigo_proveedor=:codigo_proveedor";
        $where .= empty($codigosProductos) ? "" : " and codigo_producto in (:codigos_productos)";
        $where .= empty($codigosMarcas) ? "" : " and codigo_marca in (:codigos_marcas)";

        if ($flags["flag_categorizacion"] == 1) {
            $where .= empty($codigosCategorias) ? "" : " and codigo_categoria2_pp  in (:codigos_categorias)";
            $where .= empty($codigosSubcategorias) ? "" : " and codigo_categoria3_pp in (:codigos_subcategorias)";
            $where .= empty($codigosNegocios) ? "" : " and codigo_categoria1_pp = any (:codigos_negocios)";
        }
        if ($flags["flag_categorizacion"] == 0) {
            $where .= empty($codigosCategorias) ? "" : " and codigo_categoria2  in (:codigos_categorias)";
            $where .= empty($codigosNegocios) ? "" : " and negocios && :codigos_negocios";
            $where .= empty($codigosSubcategorias) ? "" : " and codigo_categoria3 in (:codigos_subcategorias)";
        }



        if (empty($where)) {
            throw new JPublicException("Falta indicar elemento de seleccion de productos");
        }

        $sql = "select a.codigo_producto as data,
    	'['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca as label,
    	a.inventario,
    	a.estado_revision,
    	a.estado
    	from view_cu_productos_tienda a
    	where 1=1 {$where} and a.codigo_proveedor_pp=:codigo_proveedor_pp
    	group by a.codigo_producto ,
    	'['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca ,
    	a.inventario,
    	a.estado_revision,
    	a.estado
    	order by '['||a.referencia||'] '||a.nombre||'<br/>'||a.nombre_proveedor||' --- '||a.nombre_marca ";
        $ca->prepare($sql);
        $ca->bindValue(":codigos_marcas", $codigosMarcas, false);
        $ca->bindValue(":codigos_productos", $codigosProductos, false);
        $ca->bindValue(":codigos_negocios", "{{$codigosNegocios}}", true);
        $ca->bindValue(":codigos_categorias", $codigosCategorias, false);
        $ca->bindValue(":codigos_subcategorias", $codigosSubcategorias, false);
        $ca->bindValue(":codigo_proveedor", $p["codigo_proveedor"], false);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->fetchAll();
    }

    public static function autocompleteFormasDePago($p) {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $si = session::info();

        $campos = "a.descripcion";

        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.descripcion as label,
    				   a.codigo_forma_pago as data
    			from cu_formas_pago a
     				join cu_proveedores_pp b on (a.codigo_forma_pago = any (b.codigos_forma_pago) and b.codigo_proveedor =:codigo_proveedor)
    			where 1=1 
    				and ($where)
    	";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();

        return $ca->fetchAll();
    }

    public static function autocompleteRecaudadores($p) {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $si = session::info();


        $campos = "a.nombre";

        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        if (!empty($p['codigos_forma_pago'])) {

            $rFormasPagos = array();
            foreach ($p["codigos_forma_pago"] as $r) {
                $rFormasPagos[] = $r;
            }

            $codigosFormasPagos = implode(",", $rFormasPagos);

            $where .= " and a.codigo_formas_pago && ARRAY[{$codigosFormasPagos}]";
        }

        $sql = "select codigo_recaudador as data,
    				   nombre as label
    			from cu_recaudadores a
					where estado = 'activo' 
					and flags -> 'visible' = '1'
					and flags->'fisico' = '1' 
					and {$where}
    	
    	";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"]);
        $ca->exec();

        return $ca->fetchAll();
    }

    public static function autocompleteCiudades($p) {

        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $ca->prepareSelect("cu_proveedores", "codigo_sitio", "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $r = $ca->fetch();


        $campos = "a.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select a.idciudad as data,a.nombre as label
    	from cu_ciudades a left join gen_sitios b on (a.codigo_pais=b.codigo_pais)
    	where {$where} and a.codigo_pais = 'CO' order by a.nombre";

        $ca->prepare($sql);
        $ca->exec();
        return $ca->fetchAll();
    }

}