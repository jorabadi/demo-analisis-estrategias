<?php
require_once $cfg["rutaModeloLogico"]."mod_archivos_plano.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_pedidos_pp.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_productos_precios.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_productos.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_aliados.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_pedidos_pp_pce.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_utils.inc.php";
require_once $cfg["rutaModeloLogico"]."mod_contenido.inc.php";
JLib::requireOnceModule("utils/jutils.inc.php");
JLib::requireOnceModule("fileformats/excel-2.0/reader.php");

class Importaciones {

    public static function importarInventarios($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $db->transaction();

        if (empty($p["archivo"]["tmp_name"])) {
            throw new JPublicException("Falta archivo");
        }
        if ($p["tipo"] == '1' && empty($p["separador"])) {
            throw new JPublicException("Falta seleccionar separador");
        }
        if (empty($p["campos"])) {
            throw new JPublicException("Falta seleccionar campos");
        }

        if ($p["campos"] == "1") {
            $rCampos = array("codigo_producto", "referencia", "talla", "color", "refinventario", "nuevo_inventario");
        }
        else {
            $rCampos = array("codigo_producto", "referencia", "nuevo_inventario");
        }

        $nombreArchivo = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");

        try {
            if($p["tipo"] == '1'){
                $rAjustes = modArchivosPlano::interpretarTxt($rCampos, $nombreArchivo, array("separador" => $p["separador"]));
            }
            else if($p["tipo"] == '2'){
                $rAjustes = modArchivosPlano::interpretarXls($rCampos, $nombreArchivo);
            }
            else{
                throw new JPublicException("No se seleccionó un tipo de archivo válido");
            }
        }
        catch(Exception $e) {
            $msg = "Error al interpretar el archivo: ".$e->getMessage();
            throw new JPublicException(substr($msg,0,500));
        }

        $ca->prepareSelect("cu_proveedores_usuarios", "clave", "usuario=:usuario");
        $ca->bindValue(":usuario", $si["usuario"], true);
        $ca->exec();

        $i = 0;
        foreach ($rAjustes as $r) {
            $i++;

            /*Validamos campos que se esperan*/
            $iv = array();
            $iv[] = array("name" => "nuevo_inventario", 'label' => 'Nuevo inventario', "filter" => FILTER_VALIDATE_INT);
            if ( $p["campos"] == "1" ) {
                $iv[] = array("name" => "codigo_producto", 'label' => 'Codigo producto', "filter" => FILTER_VALIDATE_INT);
                $iv[] = array('name' => 'talla', 'filter' => JFILTER_VALIDATE_STRING, 'min_length' => 1, 'max_length' => 100);
                $iv[] = array('name' => 'color', 'filter' => JFILTER_VALIDATE_STRING, 'min_length' => 1, 'max_length' => 100);

                if (empty($r["codigo_producto"])) {
                    $iv[] = array('name' => 'referencia', 'filter' => JFILTER_VALIDATE_STRING, 'min_length' => 3, 'max_length' => 100, 'error_msg' => "Debe indicarse Codigo producto o Referencia");
                }
            }
            else {
                $iv[] = array("name" => "codigo_producto", 'label' => 'Codigo producto', "filter" => FILTER_VALIDATE_INT);
                $iv[] = array('name' => 'referencia', 'filter' => JFILTER_VALIDATE_STRING, 'min_length' => 0, 'max_length' => 100);
            }

            $validationErrors = JInputValidator::validateVars($r, $iv, array('output' => 'string'));
            if ($validationErrors) {
                throw new Exception(substr("Error en linea $i: ".$validationErrors."<br/>Datos: ".print_r($r,1),0,500));
            }

            $pAsignarInventario = array(
                "codigo_producto" => empty($r["codigo_producto"])? 0 : $r["codigo_producto"],
                "codigo_proveedor" => $si["codigo_proveedor"],
                "referencia" => empty($r["referencia"]) ? "" : $r["referencia"],
                "talla" => empty($r["talla"]) ? "" : $r["talla"],
                "color" => empty($r["color"]) ? "" : $r["color"],
                "refinventario" => empty($r["refinventario"]) ? "" : $r["refinventario"],
                "nuevo_inventario" => $r["nuevo_inventario"],
                "codigo_ean" => empty($r["codigo_ean"]) ? "" : $r["codigo_ean"]
            );

            try{
            	modTransLog::logInput(__CLASS__, __METHOD__,$pAsignarInventario);
                modProductos::asignarInventario($pAsignarInventario);
            	$db->commit();
            }
            catch(JPublicException $e) {
            	modTransLog::log(__CLASS__, __METHOD__ . ".err",$e->getMessage());
            	$db->rollback();
            	throw new JPublicException("Error en linea {$i}: ".$e->getMessage());
            }
        }

        return;
    }

    function file_get_contents_utf8($fn) {
        $content = file_get_contents($fn);
        return mb_convert_encoding($content, 'UTF-8', mb_detect_encoding($content, 'ISO-8859-1, UTF-8', true));
    }

    public static function loadEditRs() {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        // para interactuar con la BD

        $result = array();

        //Separadores
        $result["separadores"] = array(
            array("label" => "Seleccione el separador", "data" => ""), 
            array("label" => "| - Pipe", "data" => "|"), 
            array("label" => "; - Punto y coma", "data" => ";"),
            array("label" => ", - Coma", "data" => ",")
        );

        //Campos
        $result["campos"] = array( array("label" => "Seleccione los campos", "data" => ""), array("label" => "Estructura con Referencia-Talla-Color-Inventario", "data" => "1"), array("label" => "Estructura con Referencia-Inventario (Sin Talla-Color)", "data" => "2"));
        $result["campos_pce"] =  array( array("label" => "Seleccione Separador", "data" => ""), array("label" => "| - Pipe", "data" => "|"), array("label" => "; - Punto y coma", "data" => ";"));
        $result["campos_con_Ean"] =  array( array("label" => "Estructura sin codigo EAN", "data" => "0"), array("label" => "Estructura con codigo EAN", "data" => "1"));
        $result["defaults"] = array("separador" => "", "campo" => "", "tipo" => "","campos_con_Ean"=>"0");

        //Tipo de archivo
        $result["tipos"] = array( array("label" => "Seleccione el tipo de archivo", "data" => ""), array("label" => "Archivo plano (.csv o .txt)", "data" => "1"), array("label" => "Archivo de excel (.xls)", "data" => "2"));

        return $result;
    }

    public static function importarProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["archivo"]["tmp_name"])) {
            throw new JPublicException("Falta archivo");
    	}
    	if ($p["tipo"] == '1' && empty($p["separador"])) {
            throw new JPublicException("Falta seleccionar separador");
    	}

        $rCampos = array (
            "codigo_marca",
            "codigo_categorian",
            "referencia",
            "nombre",
            "descripcion",
            "categoria_proveedor",
            "unidad_volumen",
            "alto",
            "ancho",
            "largo",
            "unidad_peso",
            "peso",
            "unidades_empaque",
            "dias_alistamiento_minimos",
            "dias_alistamiento_maximos",
            "importacion",
            "mayoria_edad",
            "moneda",
            "visibilidad",
            "por_iva",
            "precio_compra",
            "precio_coniva",
            "precio_compra_tienda",
            "precio_coniva_tienda",
            "inventario_minimo",
            "talla-color"
        );

        $ruta = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");

        try {
            if($p["tipo"] == '1'){
                $rProductos = modArchivosPlano::interpretarTxt($rCampos, $ruta, array("separador" => $p["separador"]));
            }
            else if($p["tipo"] == '2'){
                $rProductos = modArchivosPlano::interpretarXls($rCampos, $ruta);
            }
            else{
                throw new JPublicException("No se seleccionó un tipo de archivo válido");
            }
        }
        catch (Exception $e) {
            throw new JPublicException(substr($e->getMessage(), 0, 1000));
        }
        $ca->prepareSelect("cu_proveedores_usuarios", "clave", "usuario=:usuario");
        $ca->bindValue(":usuario", $si["usuario"], true);
        $ca->exec();
        $rUsuario = $ca->fetch();
        $i = 0;

        foreach ($rProductos as $r) {
            $i++;
            $rDetalle = array();
	    
	    $lineaTallaColor = json_decode($r["talla-color"], true);
	    if(! is_array($lineaTallaColor)){
		throw new JPublicException("No es un formato de talla y color válido en la fila $i");
	    }
	    
            foreach ($lineaTallaColor as $tmp) {
                $tmp["codigo_inventario"] = 0;
                $tmp["inventario_minimo"] = $r["inventario_minimo"];
                $tmp["estado"] = "activo";
                $rDetalle[] = $tmp;
            }

            $estadoRevision = "aceptado";
            if (strstr($r['visibilidad'], "vis_cu") == true) {
                $estadoRevision = "pendiente";
            }

            $psubirproducto = array(
                "usuario" => $si ["usuario"],
                "clave" => hash("sha256", $rUsuario ["clave"]),
                "producto" => array(
                    "codigo_producto" => 0,
                    "codigo_proveedor" => $si ["codigo_proveedor"],
                    "codigo_marca" => $r ["codigo_marca"],
                    "codigo_categorian" => $r ["codigo_categorian"],
                    "referencia" => $r ["referencia"],
                    "codigo_ean" => "",
                    "nombre" => $r ["nombre"],
                    "estado" => "activo",
                    "descripcion" => $r ["descripcion"],
                    "codigo_categoria_pp" => $r ["categoria_proveedor"],
                    "unidad_volumen" => $r ["unidad_volumen"],
                    "alto" => $r ["alto"],
                    "ancho" => $r ["ancho"],
                    "largo" => $r ["largo"],
                    "unidad_peso" => $r ["unidad_peso"],
                    "peso" => $r ["peso"],
                    "unidades_empaque" => $r ["unidades_empaque"],
                    "dias_alistamiento_minimos" => $r ["dias_alistamiento_minimos"],
                    "dias_alistamiento_maximos" => $r ["dias_alistamiento_maximos"],
                    "importacion" => $r ["importacion"],
                    "mayoria_edad" => $r ["mayoria_edad"],
                    "moneda" => strtolower($r["moneda"]),
                    "visibilidad" => $r ["visibilidad"],
                    "por_iva" => $r ["por_iva"],
                    "precio_compra" => $r ["precio_compra"],
                    "precio_coniva" => $r ["precio_coniva"],
                    "precio_compra_tienda" => $r ["precio_compra_tienda"],
                    "precio_coniva_tienda" => $r ["precio_coniva_tienda"],
                    "compuesto" => "0",
                    "imagen1" => "",
                    "imagen2" => "",
                    "imagen3" => "",
                    "imagen4" => "",
                    "imagen5" => "",
                    "imagen6" => "",
                    "imagen7" => "",
                    "imagen8" => "",
                    "imagen9" => "",
                    "imagen10" => "",
                    "url_producto" => "",
                    "val_manejo_inter" => "",
                    "detalle" => $rDetalle,
                    "precio_por_variaciones" => "0",
                    "estado_revision" => $estadoRevision
                )
            );

            $db->transaction();
            try {
                modProductos::save($psubirproducto["producto"]);
                $db->Commit();
            }
            catch (SoapFault $e) {
                $db->rollback();
                $msg = "Error en linea {$i}: " . $e->getMessage() . "<br/>Datos: " . print_r($r, 1);
                throw new JPublicException(substr($msg, 0, 100));
            }
        }

        return;
    }

    public static function importarPedidosPCE($p) {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["archivo"]["tmp_name"])) {
            throw new JPublicException("Falta archivo");
        }
        if (empty($p["correo"])) {
            throw new JPublicException("Falta correo");
        }
        if (!JInputValidator::isEmail($p["correo"])) {
            throw new JPublicException("Correo invalido");
        }
        if ($p["tipo"] == '1' && empty($p["campos"])) {
            throw new JPublicException("Falta separador");
        }

        $rCampos = array(
            "codigo_pedido_tercero",
            "codigo_proveedor_pp",
            "identificacion_cliente",
            "nombres_cliente",
            "apellidos_cliente",
            "codigo_pais_cliente",
            "codigo_ciudad_cliente",
            "direccion_cliente",
            "telefono_fijo_cliente",
            "telefono_celular_cliente",
            "email_cliente",
            "tipo_identificacion_cliente",
            "identificacion_destinatario",
            "nombres_destinatario",
            "apellidos_destinatario",
            "codigo_pais_destinatario",
            "codigo_ciudad_destinatario",
            "direccion_destinatario",
            "telefono_fijo_destinatario",
            "telefono_celular_destinatario",
            "email_destinatario",
            "tipo_identificacion_destinatario",
            "referencia",
            "unidades",
            "talla",
            "color"
        );

        $ruta = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");

        try {
            if($p["tipo"] == '1'){
                $rPedidosPce = modArchivosPlano::interpretarTxt($rCampos, $ruta, array("separador" => $p["campos"]));
            }
            else if($p["tipo"] == '2'){
                $rPedidosPce = modArchivosPlano::interpretarXls($rCampos, $ruta);
            }
            else{
                throw new JPublicException("No se seleccionó un tipo de archivo válido");
            }
        }
        catch (Exception $e) {
            $msg = "Error al interpretar el archivo: " . $e->getMessage();
            throw new JPublicException(substr($msg, 0, 1000));
        }

        $i = 1;
        $mensajes = "";
        $arrayParaExcel = array();
        $productos = array();
        $pedidos = array();
        foreach ($rPedidosPce as $pedido) {
            $productos[$pedido["codigo_pedido_tercero"]][] = array("referencia" => $pedido["referencia"],
                "unidades" => $pedido["unidades"],
                "talla" => $pedido["talla"],
                "color" => $pedido["color"]
            );
        }
        foreach ($rPedidosPce as $pedido) {
            $pedidos[$pedido["codigo_pedido_tercero"]] = $pedido;
        }
        foreach ($pedidos as $rPedidoPce) {
            $pedidoPce = array(
                "codigo_pedido_tercero" => $rPedidoPce["codigo_pedido_tercero"],
                "codigo_proveedor_pp" => $rPedidoPce["codigo_proveedor_pp"],
                "identificacion_cliente" => $rPedidoPce["identificacion_cliente"],
                "nombres_cliente" => $rPedidoPce["nombres_cliente"],
                "apellidos_cliente" => $rPedidoPce['apellidos_cliente'],
                "codigo_pais_cliente" => $rPedidoPce["codigo_pais_cliente"],
                "codigo_ciudad_cliente" => $rPedidoPce["codigo_ciudad_cliente"],
                "direccion_cliente" => $rPedidoPce["direccion_cliente"],
                "telefono_fijo_cliente" => $rPedidoPce["telefono_fijo_cliente"],
                "telefono_celular_cliente" => $rPedidoPce["telefono_celular_cliente"],
                "email_cliente" => $rPedidoPce["email_cliente"],
                "tipo_identificacion_cliente" => $rPedidoPce["tipo_identificacion_cliente"],
                "identificacion_destinatario" => $rPedidoPce["identificacion_destinatario"],
                "nombres_destinatario" => $rPedidoPce["nombres_destinatario"],
                "apellidos_destinatario" => $rPedidoPce['apellidos_destinatario'],
                "codigo_pais_destinatario" => $rPedidoPce["codigo_pais_destinatario"],
                "codigo_ciudad_destinatario" => $rPedidoPce["codigo_ciudad_destinatario"],
                "direccion_destinatario" => $rPedidoPce["direccion_destinatario"],
                "telefono_fijo_destinatario" => $rPedidoPce['telefono_fijo_destinatario'],
                "telefono_celular_destinatario" => $rPedidoPce['telefono_celular_destinatario'],
                "email_destinatario" => $rPedidoPce["email_destinatario"],
                "tipo_identificacion_destinatario" => $rPedidoPce["tipo_identificacion_destinatario"],
                "productos" => $productos[$rPedidoPce["codigo_pedido_tercero"]]
                /*
                  "referencia" =>$rPedidoPce['referencia'],
                  "unidades" =>$rPedidoPce['unidades'],
                  "talla" =>!empty($rPedidoPce['talla'])?$rPedidoPce['talla']:"",
                  "color" =>!empty($rPedidoPce['color'])?$rPedidoPce['color']:"",
                 */
            );

            $result = modPedidosPpPce::registrarPedido($pedidoPce);
            if (!empty($result["mensaje_error"])) {
                $mensajes .="Codigo pedido tercero : {$result['codigo_pedido_tercero']}, Error :{$result["mensaje_error"]} , linea del archivo : {$i} <br> ";
                $arrayParaExcel[] = ["codigo_pedido" => $result['codigo_pedido_tercero'], "estado" => "error", "pin" => "", "linea" => $i, "mensaje" => $result["mensaje_error"]];
            }
            else {
                $mensajes .="Codigo pedido tercero : {$result['codigo_pedido_tercero']}, pin : {$result["pin"]} pedido regristrado => ok {$i} <br>";
                $arrayParaExcel[] = ["codigo_pedido" => $result['codigo_pedido_tercero'], "estado" => "ok", "pin" => $result["pin"], "linea" => $i, "mensaje" => ""];
            }
            $i++;
        }
        
        //throw new JPublicException(print_r($p["correo"],1));
        $db->transaction();
        $campos = "codigo_mensaje,asunto,cuerpo,de,para,para_oculto,tipo,path_adjunto";
        $codigoMensaje = $db->nextVal("cu_mensajes_codigo_mensaje");
        $ca->prepareInsert("cu_mensajes", $campos);
        $ca->bindValue(":codigo_mensaje", $codigoMensaje, false);
        $ca->bindValue(":asunto", "Importacion pedido PCE", true);
        $ca->bindValue(":cuerpo", $mensajes, true);
        $ca->bindValue(":de", "notificaciones@coordiutil.com", true);
        $ca->bindValue(":para", "{" . $p["correo"] . "}", true);
        $ca->bindValue(":para_oculto", "{manuel.patino@coordiutil.com,jose.barraza@coordiutil.com,sebastian.montoya@coordiutil.com}", true);
        $ca->bindValue(":tipo", "correo", true);
        if(count($arrayParaExcel) > 0){
            $rutaArchivo = modUtils::exportarExcelDesdeArreglo($arrayParaExcel, "Reporte_Errores_Importacion_".date('Y-m-d-h_i_s'));
            $ca->bindValue(":path_adjunto", $rutaArchivo, true);
        }
        $ca->exec();

        modMails::coordiutilEnviarCorreoGenerico(array("codigo_mensaje" => $codigoMensaje));
        $db->commit();

        return;
    }

    public static function importarProductosTienda($p) {
    	$si = session::info();
    	$db = JDatabase::database();
    	$ca = new JDbQuery($db);

    	if (empty($p["archivo"]["tmp_name"])) {
            throw new JPublicException("Falta archivo");
    	}
    	if ($p["tipo"] == '1' && empty($p["separador"])) {
            throw new JPublicException("Falta seleccionar separador");
    	}
    	$rCampos = array (
            "codigo_marca",
            "referencia",
            "nombre",
            "descripcion",
            "descripcion_html_pp",
            "categoria_proveedor",
            "alto",
            "ancho",
            "largo",
            "unidad_peso",
            "peso",
            "unidades_empaque",
            "mayoria_edad",
            "moneda",
            "por_iva",
            "precio_coniva_tienda",
            "talla-color",
            "imagenes"
    	);

    	$ruta = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");

    	try {
            if($p["tipo"] == '1'){
                $rProductos = modArchivosPlano::interpretarTxt($rCampos, $ruta, array("separador" => $p["separador"]));
            }
            else if($p["tipo"] == '2'){
                $rProductos = modArchivosPlano::interpretarXls($rCampos, $ruta);
            }
            else{
                throw new JPublicException("No se seleccionó un tipo de archivo válido");
            }
    	}
        catch(Exception $e) {
            $msg = "Error al interpretar el archivo: ".$e->getMessage();
            throw new JPublicException(substr($msg,0,1000));
    	}

    	$ca->prepareSelect("cu_proveedores_usuarios", "clave", "usuario=:usuario");
    	$ca->bindValue(":usuario", $si["usuario"], true);
    	$ca->exec();
    	$rUsuario = $ca->fetch();

    	$i = 0;
        foreach ($rProductos as $r) {
            $i++;
            $rDatos = array();

            if ($r["talla-color"] != "0" && $r["talla-color"] != "" && $r["talla-color"] != " " && $r["talla-color"] != "-") {
                $rinventario = explode(",", $r["talla-color"]);

                foreach ($rinventario as $detalle) {
                    $dfinal = explode(":", $detalle);
                    $codigo_ean = isset($dfinal[2]) ? $dfinal[2] : "";
                    
                    $rDato = [
                        "codigo_inventario" => 0,
                        "estado" => "activo",
                        "refinventario" => "",
                        "talla" => $dfinal[0],
                        "color" => $dfinal[1],
                        "codigo_ean" => $codigo_ean
                    ];
                    $rDatos[] = $rDato;
                }
            }
            else {
                $rDato = array(
                    "codigo_inventario" => 0,
                    "estado" => "activo",
                    "refinventario" => "",
                    "talla" => "-",
                    "color" => "-",
                    "codigo_ean" => ""
                );
                $rDatos[] = $rDato;
            }

            $codigoProducto = 0;
            $ca->prepareSelect("cu_productos", "codigo_producto", "codigo_proveedor = :codigo_proveedor and referencia = :referencia");
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->bindValue(":referencia", $r["referencia"], true);
            $ca->exec();
            if ($ca->size() > 0) {
                $rCodigoProducto = $ca->fetch();
                $codigoProducto = $rCodigoProducto["codigo_producto"];
            }

            $psubirproducto = array(
                "usuario" => $si["usuario"],
                "clave" => hash("sha256", $rUsuario["clave"]),
                "producto" => array(
                    "codigo_producto" => $codigoProducto,
                    "codigo_proveedor" => $si["codigo_proveedor"],
                    "codigo_marca" => $r["codigo_marca"],
                    "codigo_categorian" => -1,
                    "referencia" => $r["referencia"],
                    "codigo_ean" => "",
                    "nombre" => $r["nombre"],
                    "estado" => "activo",
                    "descripcion" => $r["descripcion"],
                    "descripcion_html_pp" => $r["descripcion_html_pp"],
                    "codigo_categoria_pp" => $r["categoria_proveedor"],
                    "unidad_volumen" => "cm",
                    "alto" => $r["alto"],
                    "ancho" => $r["ancho"],
                    "largo" => $r["largo"],
                    "unidad_peso" => $r["unidad_peso"],
                    "peso" => $r["peso"],
                    "unidades_empaque" => $r["unidades_empaque"],
                    "dias_alistamiento_minimos" => 1,
                    "dias_alistamiento_maximos" => 1,
                    "importacion" => 0,
                    "mayoria_edad" => $r["mayoria_edad"],
                    "moneda" => strtolower($r["moneda"]),
                    "visibilidad" => "vis_ti",
                    "por_iva" => $r["por_iva"],
                    "precio_compra" => 0,
                    "precio_coniva" => 0,
                    "precio_compra_tienda" => 0,
                    "precio_coniva_tienda" => $r["precio_coniva_tienda"],
                    "compuesto" => "0",
                    "imagen1" => "",
                    "imagen2" => "",
                    "imagen3" => "",
                    "imagen4" => "",
                    "imagen5" => "",
                    "imagen6" => "",
                    "imagen7" => "",
                    "imagen8" => "",
                    "imagen9" => "",
                    "imagen10" => "",
                    "url_producto" => "",
                    "estado_revision" => "aceptado",
                    "val_manejo_inter" => "",
                    "detalle" => $rDatos,
                    "precio_por_variaciones" => "0"
                )
            );

            $db->transaction();
            try {
                $producto = modProductos::save($psubirproducto["producto"]);

                if ($r["imagenes"] != "" && $r["imagenes"] != "0") {
                    $rImagenes = explode(',', $r["imagenes"]);
                    $JsonImagenes = json_encode($rImagenes);

                    $ca->prepareUpdate("cu_productos", "imagenes_url", "codigo_producto = :codigo_producto");
                    $ca->bindValue(":codigo_producto", $producto["codigo_producto"], false);
                    $ca->bindValue(":imagenes_url", $JsonImagenes, true);
                    $ca->exec();
                    if ($ca->affectedRows() > 0) {
                        modTareasProgramdas::programar(__CLASS__, __METHOD__, "/tasks/cu_registrar_imagenes_prodoctos.php", array("codigo_producto" => $producto["codigo_producto"]),array("prioridad"=>4));
                    }
                }

                $db->Commit();
            }
            catch (SoapFault $e) {
                $db->rollback();
                $msg = "Error en linea {$i}: " . $e->getMessage() . "<br/>Datos: " . print_r($r, 1);
                throw new JPublicException(substr($msg, 0, 1000));
            }
        }
        return;
    }

    public static function importarPrecios($p) {
        $si = session::info();
        $db = JDatabase::database();

        if (empty($p["archivo"]["tmp_name"])) {
            throw new JPublicException("Falta archivo");
        }
        if ($p["tipo"] == '1' && empty($p["separador"])) {
            throw new JPublicException("Falta seleccionar separador");
        }

        $rCampos = array(
            "codigo_producto",
            "referencia",
            "precio_compra",
            "precio_coniva",
            "codigo_tipo",
            "fechahora_inicial",
            "fechahora_final",
            "descripcion",
            "talla",
            "color"
        );
        $ruta = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");

        if($p["tipo"] == '1'){
            $rData = modArchivosPlano::interpretarTxt($rCampos, $ruta, array("separador" => $p["separador"]));
        }
        else if($p["tipo"] == '2'){
            $rData = modArchivosPlano::interpretarXls($rCampos, $ruta);
        }
        else{
            throw new JPublicException("No se seleccionó un tipo de archivo válido");
        }

        $db->transaction();
        $i = 1;
        $pPrecios = array();
        foreach ($rData as $rPrecios) {
            $rPrecios["session"] = $si;
            $rPrecios["idprecio"] = 0;
            $rPrecios["precio_por_variaciones"] = "0";
            $rPrecios["estado_revision"] = "aprobado";
            if ($rPrecios["codigo_tipo"] == "2") {
                $rPrecios["estado_revision"] = "pendiente";
            }

            try {
                modPrecios::savePreciosProducto($rPrecios);
                $db->commit();
            }
            catch (Exception $ex) {
                utf8_decode($error = $ex->getMessage() . "Linea = " . $i);
                throw new JpublicException(print_r($error, 1));
                $db->rollback();
            }

            $i ++;
        }

        return;
    }

    public static function importarPedidos($p){
    	date_default_timezone_set('America/Bogota');
    	$si = session::info();
    	$db = JDatabase::database();
    	$ca = new JDbQuery($db);

    	if (empty($p["archivo"]["tmp_name"])) {
    		throw new JPublicException("Falta archivo");
    	}
    	$ruta = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");

    	/*
    	 * POR EL PROBLEMA QUE EXISTE EN EL METODO QUE TRANSFORMA EL XML EN UN ARRAY, EL CUAL ES QUE CUANDO EXISTE UN REGISTRO QUE SE REPITE MAS DE UNA VEZ , LOS AGREGA EN UN ARRAY CON EL NOMBRE DEL TAG, PERO CUANDO ES UN SOLO REGISTRO NO CREA EL ARRAY
    	 * SE CAMBIO ESO Y SE RECORRE EL XML CON DOM
    	 */

    	$doc = new DOMDocument();
    	$doc->load($ruta);

    	$rPedidos = $doc->getElementsByTagName("pedido");
    	foreach ($rPedidos as $infoPedido) {
    		$pedido = array();
    		//seteo la informacion
    		$pedido["codigo_pedido_tercero"] = $infoPedido->getElementsByTagName("codigo_pedido_tercero")->item(0)->nodeValue;

    		$fechaHora = $infoPedido->getElementsByTagName("fechahora_pedido")->item(0)->nodeValue;
    		$fechaHora= DateTime::createFromFormat('Y/m/d H:i:s', $fechaHora)->format('Y-m-d H:i:s');
    		$pedido["fecha_hora_pedido"] = $fechaHora;

    		$pedido["total_pedido"] = $infoPedido->getElementsByTagName("total_pedido")->item(0)->nodeValue;
    		$pedido["total_transporte"] = $infoPedido->getElementsByTagName("total_transporte")->item(0)->nodeValue;
    		$pedido["codigo_proveedor_pp"] = $si["codigo_proveedor"];



    		/*	LLENO LA INFORMACION DEL CLIENTE
    		 * --------------------------
    		 * --------------------------
    		*/

    		$rCliente= $infoPedido->getElementsByTagName("cliente")->item(0);
    		$infoCliente = array();
    		$identificacionCliente = $rCliente->getAttribute("identificacion");
    		$infoCliente["identificacion"]=!empty($identificacionCliente)?$identificacionCliente:"";

    		$nombreCliente = $rCliente->getAttribute("nombre");
    		$infoCliente["nombre"]=!empty($nombreCliente)?$nombreCliente:"";

    		$apellidoCliente = $rCliente->getAttribute("apellido");
    		$infoCliente["apellido"]=!empty($apellidoCliente)?$apellidoCliente:"";

    		$infoCliente["codigo_pais"]= "CO";

    		$codigoCiudadCliente = $rCliente->getAttribute("codigo_dane_ciudad");
    		$infoCliente["codigo_ciudad"]= !empty($codigoCiudadCliente)?$codigoCiudadCliente:"";

    		$direccionCliente = $rCliente->getAttribute("direccion");
    		$infoCliente["direccion"]= !empty($direccionCliente)?$direccionCliente:"";
    	//	$infoCliente["direccion"] = JUtils::stringSinTildes($infoCliente["direccion"]);
    		$infoCliente["direccion"] = preg_replace('/[^a-zA-Z0-9_.#\'-,\\s]+/', "", $infoCliente["direccion"]);

    		$telefonoFijoCliente = $rCliente->getAttribute("telefono_fijo");
    		$infoCliente["telefono_fijo"]= !empty($telefonoFijoCliente)?$telefonoFijoCliente:"";

    		$telefonoCelularCliente = $rCliente->getAttribute("telefono_celular");
    		$infoCliente["telefono_celular"]= !empty($telefonoCelularCliente)?$telefonoCelularCliente:"";


    		//en caso tal de que el fijo no exista coloca alli el telefono celular
    		if(empty($infoCliente["telefono_fijo"])) {
    			$infoCliente["telefono_fijo"] = $infoCliente["telefono_celular"];
    		}

    		$emailCliente = $rCliente->getAttribute("email");
    		$infoCliente["email"]= !empty($emailCliente)?$emailCliente:"";

    		/*if(!empty($cliente["email_cliente"]) ) {
    			$infoCliente["email"]= !empty($cliente["email_cliente"])?$cliente["email_cliente"]:"";
    		}*/
    		$tipoIdentificacionCliente = $rCliente->getAttribute("tipo_identificacion");
    		$infoCliente["tipo_identificacion"]= !empty($tipoIdentificacionCliente)?$tipoIdentificacionCliente:"c";
    		$pedido["cliente"] = $infoCliente;

    		/*			FIN DE LA INFORMACION DEL CLIENTE        */


    		/*	LLENO LA INFORMACION DEL DESTINATARIO
    		 * --------------------------
    		* --------------------------
    		*/
    		$rDestinatario= $infoPedido->getElementsByTagName("destinatario")->item(0);

    		$infoDestinatario = array();

    		$identificacionDestinatario = $rDestinatario->getAttribute("identificacion");
    		$infoDestinatario["identificacion"]=!empty($identificacionDestinatario)?$identificacionDestinatario:"";

    		$nombreDestinatario = $rDestinatario->getAttribute("nombre");
    		$infoDestinatario["nombre"]=!empty($nombreDestinatario)?$nombreDestinatario:"";

    		$apellidoDestinatario = $rDestinatario->getAttribute("apellido");
    		$infoDestinatario["apellido"]=!empty($apellidoDestinatario)?$apellidoDestinatario:"";
    		$infoDestinatario["codigo_pais"]= "CO";

    		$codigoCiudadDestinatario = $rDestinatario->getAttribute("codigo_dane_ciudad");
    		$infoDestinatario["codigo_ciudad"]= !empty($codigoCiudadDestinatario)?$codigoCiudadDestinatario:"";

    		$direccionDestinatario = $rDestinatario->getAttribute("direccion");
    		$infoDestinatario["direccion"]= !empty($direccionDestinatario)?$direccionDestinatario:"";
    		//$infoDestinatario["direccion"] = JUtils::stringSinTildes($infoDestinatario["direccion"]);
    		$infoDestinatario["direccion"] = preg_replace('/[^a-zA-Z0-9_.#\'-,\\s]+/', "", $infoDestinatario["direccion"]);

    		$telefonoFijoDestinatario = $rDestinatario->getAttribute("telefono_fijo");
    		$infoDestinatario["telefono_fijo"]= !empty($telefonoFijoDestinatario)?$telefonoFijoDestinatario:"";
    		$telefonoCelularDestinatario = $rDestinatario->getAttribute("telefono_celular");
    		$infoDestinatario["telefono_celular"]= !empty($telefonoCelularDestinatario)?$telefonoCelularDestinatario:"";

    		//en caso tal de que el fijo no exista coloca alli el telefono celular
    		if(empty($infoDestinatario["telefono_fijo"])) {
    			$infoDestinatario["telefono_fijo"] = $infoDestinatario["telefono_celular"];
    		}

    		$emailDestinatario = $rDestinatario->getAttribute("email");
    		$infoDestinatario["email"]= !empty($emailDestinatario)?$emailDestinatario:"";

    		$tipoIdentificacionDestinatario = $rDestinatario->getAttribute("tipo_identificacion");
    		$infoDestinatario["tipo_identificacion"]= !empty($tipoIdentificacionDestinatario)?$tipoIdentificacionDestinatario:"c";

    		$pedido["destinatario"] = $infoDestinatario;

    		/*			FIN DE LA INFORMACION DEL DESTINATARIO        */

    		/*	INFORMACION DE LOS PRODUCTOS */

    		/***
    		 * RECORRO TODOS LOS PRODUCTOS Y LLENO EL ARRAY CON LA INFO
    		 */

    		$rItems = $infoPedido->getElementsByTagName("item");
    		foreach ($rItems as $rItem){
    			//cuando son varios registros de items vienen en un array de @attributes, cuando es uno solo viene dentro del objeto directamente
    			$infoItem = array();
    			$codigoSkuCu = $rItem->getAttribute("codigo_sku_cu");
    			$infoItem["codigo_sku_cu"] = !empty($codigoSkuCu)?trim($codigoSkuCu):"";
    			$codigoSkuProv = $rItem->getAttribute("codigo_sku_prov");
    			$infoItem["codigo_sku_prov"] = !empty($codigoSkuProv)?trim($codigoSkuProv):"";
    			$referencia = $rItem->getAttribute("referencia");
    			$infoItem["referencia"] = !empty($referencia)?$referencia:"";
    			$unidades = $rItem->getAttribute("unidades");
    			$infoItem["unidades"] = !empty($unidades)?$unidades:"";
    			$precioVenta = $rItem->getAttribute("precio_venta");
    			$infoItem["precio_venta"] = !empty($precioVenta)?$precioVenta:"";
    			$pedido["items"][] = $infoItem;
    		}

    		/*	FIN DE LA INFORMACION DE LOS PRODUCTOS */

    		$registrarPedido = modPedidosPp::registrarPedido($pedido);

    		if(!empty($registrarPedido["mensaje_error"])) {
    			throw new JPublicException($registrarPedido["mensaje_error"]);
    		}
    	}

    	return;
    }

    public static function importarInventariosAliados($p) {
    	$si = session::info();
    	$db = JDatabase::database();
    	$ca = new JDbQuery($db);



    	if (empty($p["archivo"]["tmp_name"])) {
    		throw new JPublicException("Falta archivo");
    	}
    	if (empty($p["separador"])) {
    		throw new JPublicException("Falta seleccionar separador");
    	}
    	$rCampos = array("codigo_inventario","codigo_inventario_tercero","codigo_referencia_compra");

    	$ruta = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");

    	try {
    		$rData = modArchivosPlano::interpretarTxt($rCampos, $ruta, array("separador" => $p["separador"]));
    	}catch(Exception $e) {
    		$msg = "Error al interpretar el archivo: ".$e->getMessage();
    		throw new JPublicException(substr($msg,0,100));
    	}
    	$db->transaction();

    	$linea= 1;
    	foreach ($rData as $inventario){
    		$inventario["session"] =   $si;

    		$inventario = modAliados::importarInventarios($inventario);

    		$linea ++;

    	}

    	$db->commit();




    }

    public static function loadupdate() {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        // para interactura con la BD

        $result = array();

        $result["separadores"] = array( array("label" => "Seleccione Separador", "data" => ""), array("label" => "| - Pipe", "data" => "|"), array("label" => "; - Punto y coma", "data" => ";"));

        $result["defaults"] = array("separador" => "");

        return $result;

    }

    public static function importarPedidosPorPagar($p) {
      $si = session::info();
      $db = JDatabase::database();
      $ca = new JDbQuery($db);

      if (empty($p["archivo"]["tmp_name"])) {
          throw new JPublicException("Falta archivo");
      }
      if ($p["tipo"] == '1') {
          throw new JPublicException("Solo se permiten archivos .xls");
      }
      if ($p["tipo"] == '1' && empty($p["separador"])) {
          throw new JPublicException("Falta seleccionar separador");
      }

      $rCampos = array(
          "pin",
          "codigo_pedido",
          "punto_despacho",
          "contenido",
          "unidades_empaque",
          "factura_proveedor"
      );
      $ruta = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");

      if($p["tipo"] == '1'){
          $rData = modArchivosPlano::interpretarTxt($rCampos, $ruta, array("separador" => $p["separador"]));
      }
      else if($p["tipo"] == '2'){
          $rData = modArchivosPlano::interpretarXls($rCampos, $ruta);
      }
      else{
          throw new JPublicException("No se seleccionó un tipo de archivo válido");
      }

      $db->transaction();

      $codigoImportacion = $db->nextVal("cu_importaciones_archivos_codigo_importacion");
      $ca->prepareInsert("cu_importaciones_archivos", "codigo_importacion, nombre_archivo, tipo_importacion, fechahora_importacion, codigo_proveedor_pp");
      $ca->bindValue(":codigo_importacion", $codigoImportacion, false);
      $ca->bindValue(":nombre_archivo", $p["archivo"]["name"], true);
      $ca->bindValue(":tipo_importacion", "pedidos_por_despachar", true);
      $ca->bindValue(":fechahora_importacion", "current_timestamp", false);
      $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
      $ca->exec();

      $codigosPedidos = array();

      $i = 2;
      foreach ($rData as $rPedido) {
        $rPedido["codigo_importacion"] = $codigoImportacion;
        $rPedido["codigo_proveedor"] = $si["codigo_proveedor"];
        $rPedido["session"] = $si;
        $codigosPedidos[] = $rPedido["codigo_pedido"];

        try {
          modDespachos::imprimirDocumentos($rPedido);
        } catch (Exception $ex) {
          utf8_decode($error = $ex->getMessage() . " -> Fila = " . $i);
          throw new JpublicException(print_r($error, 1));
          $db->rollback();
        }
        $i++;
      }

      if(count($codigosPedidos) > 0){
          $strCodigosPedidos = implode(",", $codigosPedidos);
      }

      $db->commit();
      return $si["codigo_proveedor"]."|".$strCodigosPedidos;
 
  }
  
    public static function importarPedidosYDespachos($p) {
        if (empty($p["archivo"]["tmp_name"])) {
            throw new JPublicException("Falta el archivo");
        }
        if (empty($p["correo"])) {
            throw new JPublicException("Falta el correo");
        }
        if (!JInputValidator::isEmail($p["correo"])) {
            throw new JPublicException("Correo invalido");
        }
        if ($p["tipo"] == '1' && empty($p["campos"])) {
            throw new JPublicException("Falta separador");
        }

        $rCampos = array(
            "codigo_pedido_tercero",
            "codigo_proveedor_pp",
            "identificacion_cliente",
            "nombres_cliente",
            "apellidos_cliente",
            "codigo_pais_cliente",
            "codigo_ciudad_cliente",
            "direccion_cliente",
            "telefono_fijo_cliente",
            "telefono_celular_cliente",
            "email_cliente",
            "tipo_identificacion_cliente",
            "identificacion_destinatario",
            "nombres_destinatario",
            "apellidos_destinatario",
            "codigo_pais_destinatario",
            "codigo_ciudad_destinatario",
            "direccion_destinatario",
            "telefono_fijo_destinatario",
            "telefono_celular_destinatario",
            "email_destinatario",
            "tipo_identificacion_destinatario",
            "referencia",
            "unidades",
            "talla",
            "color",
            "punto_despacho",
            "contenido",
            "unidades_empaque",
            "factura_proveedor"
        );

        $ruta = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");

        try {
            if($p["tipo"] == '1'){
                $rPedidosPce = modArchivosPlano::interpretarTxt($rCampos, $ruta, array("separador" => $p["campos"]));
            }
            else if($p["tipo"] == '2'){
                $rPedidosPce = modArchivosPlano::interpretarXls($rCampos, $ruta);
            }
            else{
                throw new JPublicException("No se seleccionó un tipo de archivo válido");
            }
        }
        catch (Exception $e) {
            $msg = "Error al interpretar el archivo: " . $e->getMessage();
            throw new JPublicException(substr($msg, 0, 1000));
        }

        $si = session::info();
        $codigosPedidos = modPedidosPpPce::importarPedidosYDespachos($rPedidosPce, $p["archivo"]["name"], $si, $p["correo"]);
        return $si["codigo_proveedor"]."|".$codigosPedidos;
    }
    
    public static function importarContenidos($p) {
        $db = JDatabase::database();
        $db->transaction();
        try {
            $rCampos = ["nombre", "titulo", "html", "descripcion_seo"];
            
            if(empty($p["archivo"]["tmp_name"])){
                throw new JPublicException("Falta el archivo");
            }
            
            $ruta = (JApp::privateTempPath() . "/{$p["archivo"]["tmp_name"]}");
            if($p["tipo"] == '1'){
                $datosArchivo = modArchivosPlano::interpretarTxt($rCampos, $ruta, array("separador" => $p["separador"]));
            }
            else if($p["tipo"] == '2'){
                $datosArchivo = modArchivosPlano::interpretarXls($rCampos, $ruta);
            }
            else{
                throw new JPublicException("No se seleccionó un tipo de archivo válido");
            }
            
            $si = session::info();
            
            modContenidos::importarContenidos($datosArchivo, $si["codigo_proveedor"]);
            $db->commit();

            return true;
        }
        catch (Exception $e) {
            $db->rollback();
            $msg = "Error al interpretar el archivo: " . $e->getMessage();
            throw new JPublicException(substr($msg, 0, 1000));
        }
    }

}
