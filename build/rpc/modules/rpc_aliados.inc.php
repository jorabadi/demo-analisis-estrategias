<?php
JLib::requireOnceModule("templates/TwigLoader.inc.php");
require_once $cfg["rutaModeloLogico"] . "mod_productos.inc.php";

class Aliados {

    public static function loadEditRs() {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();

        $result["sesion"] = $si;

        $sql = "select c.codigo_categoria as data,c.nombre as label
                    from cu_categoriasn c
                    where c.codigo_categoria in
                    (select distinct unnest(a.negocios) as negocios
                    from cu_proveedores_asociados_pp a
                    where a.codigo_proveedor = :codigo_proveedor
                    and a.codigo_proveedor_asociado <> a.codigo_proveedor)";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $result["negocios_elegidos"] = $ca->fetchAll();

        array_unshift($result["negocios_elegidos"], array("data" => "", "label" => "Seleccione el negocio"));

        $result["defaults"] = array("negocio_elegido" => "");

        return $result;
    }

    public static function loadProductosAliados($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre,a.referencia,a.nombre_marca";
        $where = "";
        if (!empty($p["filter"])) {
            $where = " and " . $ca->sqlFieldsFilters($campos, $p["filter"]);
        }

        $sqlProductos = "select codigo_producto
        				 from cu_proveedores_asociados_pp_productos
        				 where codigo_proveedor = :codigo_proveedor";
        $ca->prepare($sqlProductos);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $rProductos = array();

        foreach ($ca->fetchAll() as $producto) {
            $rProductos[] = $producto["codigo_producto"];
        }

        $rProductos = implode(",", $rProductos);
        $sql = "select a.codigo_producto,
        	   a.nombre||'<br/>Ref: '||a.referencia||'<br/>Inventario: '||a.inventario::text as nombre,
        	   'P: '||a.nombre_proveedor||'<br/>M: '||a.nombre_marca as proveedor_marca,
        	   /* func_number_format(round((func_precio_compra_anexo(a.codigo_producto)::INTEGER*(func_descuento_proveedor_r(a.codigo_producto)/100)),2),2)*/  -1 as margen_neto_cu,
        	   /* func_number_format(round((func_precio_compra_anexo(a.codigo_producto)::INTEGER*(func_descuento_proveedor_r(a.codigo_producto)/100))*(b.por_margen_aliado/100),2),2) */  -1 as margen_neto_aliado
        	   FROM view_cu_productos a
        	   JOIN cu_proveedores_asociados_pp pa ON pa.codigo_proveedor_asociado=a.codigo_proveedor and pa.codigo_proveedor=:codigo_proveedor_aliado
			   join cu_proveedores_anexos_acuerdo b on (pa.codigo_proveedor=b.codigo_proveedor) 
			   left join cu_proveedores_asociados_pp_productos j on  j.codigo_proveedor=pa.codigo_proveedor and j.codigo_producto=a.codigo_producto
               where j.codigo_producto is null and pa.codigo_proveedor=:codigo_proveedor_aliado and pa.codigo_proveedor<>pa.codigo_proveedor_asociado
               and a.codigo_sitio='1' and a.estado_proveedor='activo' and a.estado_marca='activo'
               and a.estado='activo' and a.estado_revision='aceptado' and a.inventario > 0 and a.codigo_categoria3<>-1
               and (:negocio = 0 OR :negocio in (SELECT unnest(c.negocios) AS unnest FROM cu_categoriasn c WHERE c.codigo_categoria = a.codigo_categoria3))
               and (:negocio = 0 OR  :negocio=any(pa.negocios) ) and (:codigo_proveedor = 0 OR pa.codigo_proveedor_asociado =:codigo_proveedor)
        	   {$where} 
        	   group by a.codigo_producto,
				         a.nombre,
				         a.referencia,
				         a.inventario,
				         a.nombre_proveedor,
				         a.nombre_marca,
				         b.por_margen_aliado
        	   ";

        $ca->prepare($sql);
        $ca->bindValue(":negocio", !empty($p["negocio"]) ? $p["negocio"] : "0", false);
        $ca->bindValue(":codigo_proveedor_aliado", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_proveedor", !empty($p["codigo_proveedor"]) ? $p["codigo_proveedor"] : "0", false);
        $ca->exec();

        $result = $ca->fetchAll();
        for ($i = 0; $i < count($result); $i++) {
            $tmp = modRs::load(rs::cuProductos, $result[$i]["codigo_producto"], array(rs::cuProductosImagen1));
            if (isset($tmp["imagen_1"])) {
                $result[$i]["imagen_1"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }

        return $result;
    }

    public static function loadProductosIncluidosPage($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "p.nombre_proveedor_asociado";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $sql = "select p.* from ( select pa.codigo_proveedor,
					pa.codigo_proveedor_asociado,
					p.nombre as nombre_proveedor_asociado,
					array_to_string(array_agg(c.nombre),', ') as negocios
				from cu_proveedores_asociados_pp pa
				 	join cu_proveedores p on pa.codigo_proveedor_asociado = p.codigo_proveedor
			   left join cu_categoriasn c on c.codigo_categoria=any(pa.negocios)
                where pa.codigo_proveedor=:codigo_proveedor
				group by pa.codigo_proveedor,pa.codigo_proveedor_asociado,p.nombre ) p where 1=1 and {$where}";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->execPage($p);
    }

    public static function loadProductosIncluidosOne($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $result = array();

        $campos = "distinct codigo_proveedor as data, nombre as label";

        $ca->prepareSelect("cu_proveedores ", $campos, "codigo_proveedor=:codigo_proveedor");
        $ca->bindValue(":codigo_proveedor", $p["codigo_proveedor_asociado"], false);
        $ca->exec();
        $result["proveedor"] = $ca->fetchAll();

        $sql = "select c.codigo_categoria as data,c.nombre as label
               from cu_categoriasn c
               where c.codigo_categoria in
               (select distinct unnest(a.negocios) as negocios
               from cu_proveedores_asociados_pp a
               where a.codigo_proveedor = :codigo_proveedor
               and a.codigo_proveedor_asociado <> a.codigo_proveedor
               and a.codigo_proveedor_asociado=:codigo_proveedor_asociado)";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_proveedor_asociado", $p["codigo_proveedor_asociado"], false);
        $ca->exec();
        $result["negocios_elegidos"] = $ca->fetchAll();

        $sql = "SELECT a.codigo_producto,
					a.nombre || '<br/>Ref: ' || a.referencia || '<br/>Inventario: ' || a.inventario::TEXT AS nombre,
					'P: ' || a.nombre_proveedor || '<br/>M: ' || a.nombre_marca AS proveedor_marca,
					func_number_format(round((
								(
									SELECT func_precio_venta(CASE 
												WHEN pp.val_manejo_inter IS NOT NULL
													THEN pp.val_manejo_inter
												ELSE po.val_manejo_inter
												END + A.val_shipping_xt + A.por_tax_xt / 100::NUMERIC * CASE 
												WHEN pp.precio_compra IS NOT NULL
													THEN pp.precio_compra
												ELSE po.precio_compra
												END + A.por_garantia / 100::NUMERIC * CASE 
												WHEN pp.precio_compra IS NOT NULL
													THEN pp.precio_compra
												ELSE po.precio_compra
												END + CASE 
												WHEN pp.precio_compra IS NOT NULL
													THEN pp.precio_compra
												ELSE po.precio_compra
												END, A.moneda) AS func_precio_venta
									) * (
									(
										SELECT CASE 
												WHEN pp.descuento_proveedor IS NOT NULL
													THEN pp.descuento_proveedor
												ELSE po.descuento_proveedor
												END AS descuento_proveedor
										) / 100
									)
								), 2), 2) AS margen_neto_cu,
					func_number_format(round((
								(
									SELECT func_precio_venta(CASE 
												WHEN pp.val_manejo_inter IS NOT NULL
													THEN pp.val_manejo_inter
												ELSE po.val_manejo_inter
												END + A.val_shipping_xt + A.por_tax_xt / 100::NUMERIC * CASE 
												WHEN pp.precio_compra IS NOT NULL
													THEN pp.precio_compra
												ELSE po.precio_compra
												END + A.por_garantia / 100::NUMERIC * CASE 
												WHEN pp.precio_compra IS NOT NULL
													THEN pp.precio_compra
												ELSE po.precio_compra
												END + CASE 
												WHEN pp.precio_compra IS NOT NULL
													THEN pp.precio_compra
												ELSE po.precio_compra
												END, A.moneda) AS func_precio_venta
									) * (
									(
										SELECT CASE 
												WHEN pp.descuento_proveedor IS NOT NULL
													THEN pp.descuento_proveedor
												ELSE po.descuento_proveedor
												END AS descuento_proveedor
										) / 100
									)
								) * (b.por_margen_aliado / 100), 2), 2) AS margen_neto_aliado
				FROM view_cu_productos a
				LEFT JOIN cu_proveedores_anexos_acuerdo b ON (b.codigo_proveedor = :codigo_proveedor)
				INNER JOIN cu_proveedores_asociados_pp_productos c ON (a.codigo_producto = c.codigo_producto AND c.codigo_proveedor = :codigo_proveedor)
				INNER JOIN cu_productos_precios po ON a.codigo_producto = po.codigo_producto AND po.estado_revision = 'aprobado'::TEXT AND po.codigo_tipo = 1
				LEFT JOIN cu_productos_precios pp ON a.codigo_producto = pp.codigo_producto AND pp.estado_revision = 'aprobado'::TEXT AND pp.codigo_tipo = 2 AND now() >= pp.fechahora_inicial AND now() <= pp.fechahora_final AND pp.codigo_proveedor_pp = 0
				WHERE (A.flags -> 'vis_cu'::TEXT) = '1'::TEXT AND a.codigo_proveedor = :codigo_proveedor_asociado AND a.codigo_sitio = '1' AND a.estado_proveedor = 'activo' AND a.estado_marca = 'activo' AND a.estado = 'activo' AND a.estado_revision = 'aceptado' AND a.inventario > 0 AND a.codigo_categoria3 <> - 1
				GROUP BY a.codigo_producto,
					a.nombre,
					a.referencia,
					a.inventario,
					a.nombre_proveedor,
					a.nombre_marca,
					b.por_margen_aliado,
					pp.val_manejo_inter,
					po.val_manejo_inter,
					a.val_shipping_xt,
					a.por_tax_xt,
					pp.precio_compra,
					po.precio_compra,
					a.por_garantia,
					a.moneda,
					pp.descuento_proveedor,
					po.descuento_proveedor";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_proveedor_asociado", $p["codigo_proveedor_asociado"], false);
        $ca->exec();
        $result["productosIncluidos"] = $ca->fetchAll();



        if (!empty($result["productosIncluidos"])) {
            for ($i = 0; $i < count($result["productosIncluidos"]); $i++) {
                $tmp = modRs::load(rs::cuProductos, $result["productosIncluidos"][$i]["codigo_producto"], array(rs::cuProductosImagen1));
                if (isset($tmp["imagen_1"])) {
                    $result["productosIncluidos"][$i]["imagen_1"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
                }
            }
        }



        return $result;
    }

    public static function autocompleteProveedores($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "pr.nombre";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);

        $sql = "select distinct pr.codigo_proveedor as data,pr.nombre as label
                from cu_proveedores pr join cu_proveedores_asociados_pp pa on pr.codigo_proveedor = pa.codigo_proveedor_asociado 
                and pa.codigo_proveedor=:codigo_proveedor --aliado
                where 1=1 and {$where}
                order by pr.nombre";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        return $ca->assocAll();
    }

    public static function save($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);
        $db->transaction();

        $listaProductos = array();
        foreach ($p["productos_incluidos"] as $producto) {
            if (in_array($producto["codigo_producto"], array(15750, 15751, 15752, 16090, 16091))) {
                throw new JPublicException("Se encontro un error usted");
            }
            $listaProductos[] = $producto["codigo_producto"];
        }

        $productos = implode(",", $listaProductos);

        $db->transaction();

        $productosAliados = "";

        for ($i = 0; $i < count($listaProductos); $i++) {
            $productosAliados .= "(" . $si["codigo_proveedor"] . "," . $listaProductos[$i] . "," . $p["codigo_proveedor_asociado"] . "),";
        }

        $productosAliados = substr($productosAliados, 0, -1);
        if (!empty($listaProductos)) {
            $sqlnsert = "insert into cu_proveedores_asociados_pp_productos(codigo_proveedor,codigo_producto,codigo_proveedor_asociado)
            		 	select tbl.codigo_proveedor, tbl.codigo_producto,tbl.codigo_proveedor_asociado
        		 	 	from
        		 	 	(values $productosAliados) tbl(codigo_proveedor,codigo_producto,codigo_proveedor_asociado)
        		 	 	left join cu_proveedores_asociados_pp_productos a  on (tbl.codigo_proveedor=a.codigo_proveedor and tbl.codigo_producto=a.codigo_producto and tbl.codigo_proveedor_asociado=a.codigo_proveedor_asociado)
        		     	where a.codigo_producto is null";
            $ca->exec($sqlnsert);

            $sqlDelete = "delete from cu_proveedores_asociados_pp_productos 
                      	where codigo_proveedor in (:codigo_proveedor) and codigo_proveedor_asociado=:codigo_proveedor_asociado and codigo_producto in (select a.codigo_producto 
					  	from cu_proveedores_asociados_pp_productos a left join
                      	(values $productosAliados) tbl(codigo_proveedor,codigo_producto,codigo_proveedor_asociado) on (tbl.codigo_proveedor=a.codigo_proveedor and tbl.codigo_producto=a.codigo_producto and tbl.codigo_proveedor_asociado=a.codigo_proveedor_asociado)
                      	where tbl.codigo_producto is null and a.codigo_proveedor in (:codigo_proveedor) and a.codigo_proveedor_asociado=:codigo_proveedor_asociado)";
            $ca->prepare($sqlDelete);
            $ca->bindValue(":codigo_proveedor_asociado", $p["codigo_proveedor_asociado"], false);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();


            self::actualizarProductoPreciosMenoresAliados($productos);
            self::actualizarProductoPreciosMenoresPromocionesAliados($productos);
        } else {
            $sqlDelete = "delete from cu_proveedores_asociados_pp_productos where codigo_proveedor in (:codigo_proveedor) and codigo_proveedor_asociado=:codigo_proveedor_asociado";
            $ca->prepare($sqlDelete);
            $ca->bindValue(":codigo_proveedor_asociado", $p["codigo_proveedor_asociado"], false);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            $ca->exec();
        }




        $sql = "select func_productos_importados_cache_actualizar_pp()";
        $ca->prepare($sql);
        $ca->exec();


        $sql = "DELETE
					FROM matv_cu_productos_tienda p
					WHERE p.codigo_producto IN ({$productos});
					
					INSERT INTO MATV_CU_PRODUCTOS_TIENDA
					SELECT A.*
					FROM VIEW_CU_PRODUCTOS_TIENDA A
					     LEFT JOIN MATV_CU_PRODUCTOS_TIENDA B ON (A.codigo_producto =
					       B.codigo_producto AND A.codigo_proveedor_pp = B.codigo_proveedor_pp)
					WHERE 1 = 1
					 AND  ((A.*) <>(B.*) OR B.codigo_producto IS NULL) 
					 AND A.codigo_producto IN ({$productos});
        		";
        $ca->prepare($sql);
        $ca->exec();

        $db->commit();

        return;
    }

    public static function loadPageMargenesProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.nombre,a.referencia";

        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        $sql = "select a.codigo_producto,
                    a.nombre,
                    a.referencia,
                    a.nombre_marca,
                    func_number_format(a.precio_venta_coniva_r,0) as precio_venta,
                    func_number_format(func_precio_compra_anexo(a.codigo_producto)::integer,0) as precio_cu,
                    a.por_iva,
                    func_descuento_proveedor_r(a.codigo_producto) as descuento_proveedor,
                    func_number_format(round((func_descuento_proveedor_r(a.codigo_producto)*(func_descuento_proveedor_r(a.codigo_producto)/100)),2),2) as margen_total,
                    func_number_format(round((func_precio_siniva_r(a.codigo_producto)*(func_descuento_proveedor_r(a.codigo_producto)/100))*(b.por_margen_aliado/100),2),2) as margen_neto_aliado,
                    a.inventario,
                    coalesce((select sum(unidades) from cu_pedidos_det where codigo_inventario in 
                        (select codigo_inventario from cu_productos_inventario where codigo_producto=a.codigo_producto) 
                        and estado='reservado'),0) as inventario_reservado
                from cu_proveedores_asociados_pp_productos pa 
                    join view_cu_productos a ON (a.codigo_producto=pa.codigo_producto) 
					left join cu_proveedores_anexos_acuerdo b on (b.codigo_proveedor=pa.codigo_proveedor)
				where pa.codigo_proveedor=:codigo_proveedor and pa.codigo_proveedor<>pa.codigo_proveedor_asociado) a
                    where {$where}";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $result = $ca->execPage($p);

        for ($i = 0; $i < count($result["records"]); $i++) {
            $tmp = modRs::load(rs::cuProductos, $result["records"][$i]["codigo_producto"], array(rs::cuProductosImagen1));
            if (isset($tmp["imagen_1"])) {
                $result["records"][$i]["imagen_1"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }

        return $result;
    }

    public static function loadPagePinesAnexosFacturas($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.codigo_anexo_aliado,a.pin";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        /*
          $sql = "select a.codigo_anexo_aliado as codigo_anexo,
          a.pin,
          array_to_string(array_accum(a.unidades||' - '|| a.nombre),'<br/>') as detalle,
          a.fechahora_pago,
          func_number_format(a.total_coniva,0) as valor_pin,
          func_number_format(sum(a.precio_compra_anexo),0) as precio_coordiutil,
          func_number_format(sum(a.precio_venta_siniva*(a.descuento_proveedor/100)),0) as margen_coordiutil,
          func_number_format(a.total_coniva*((case when c.por_redrecaudo is null then b.por_redrecaudo else c.por_redrecaudo end)/100),0) as red_recaudo,
          func_number_format((sum(a.precio_venta_siniva*(a.descuento_proveedor/100))-a.total_coniva*
          ((case when c.por_redrecaudo is null then b.por_redrecaudo else c.por_redrecaudo end)/100))*
          ((case when c.por_margen_aliado is null then b.por_margen_aliado else c.por_margen_aliado end)/100),0) as total
          from view_cu_pedidos_det a
          join cu_proveedores_anexos_acuerdo b on (a.codigo_proveedor_pp=b.codigo_proveedor)
          left join cu_proveedores_anexos_factura c on (a.codigo_anexo_aliado=c.codigo_anexo)
          where a.codigo_proveedor_pp=:codigo_proveedor and
          a.estado_pin='pagado' and a.estado_autorizacion='aprobado' and ({$where})
          group by a.pin,a.fechahora_pago,a.total_coniva,a.codigo_anexo_aliado,c.por_redrecaudo,c.por_margen_aliado,b.por_redrecaudo,b.por_margen_aliado";
         */

        $sql = " select a.* from (select a.codigo_anexo_aliado ,
					a.pin,
					array_to_string(array_accum(a.unidades||' - '|| a.nombre),'<br/>') as detalle,
					a.fechahora_pago,
					func_number_format(a.total_coniva,0) as valor_pin,
					func_number_format(sum(a.precio_compra_anexo),0) as precio_coordiutil,
					func_number_format(case when sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades) < 0 then 0 else sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades) end,0) as margen_coordiutil,
					func_number_format(a.total_coniva*((case when c.por_redrecaudo is null then b.por_redrecaudo else c.por_redrecaudo end)/100),0) as red_recaudo,
					func_number_format(
							case when (sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)-a.total_coniva*(c.por_redrecaudo/100))*(c.por_margen_aliado/100) < 0 then 0 else
							(sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)-a.total_coniva*(c.por_redrecaudo/100))*(c.por_margen_aliado/100) end
						,0) as total
				from view_cu_pedidos_det a 
					join cu_proveedores_anexos_acuerdo b on (a.codigo_proveedor_pp=b.codigo_proveedor)
					left join cu_proveedores_anexos_factura c on (a.codigo_anexo_aliado=c.codigo_anexo)
				where a.codigo_proveedor_pp=:codigo_proveedor and
					a.estado_pin='pagado' and a.estado_autorizacion='aprobado' 
				group by a.pin,a.fechahora_pago,a.total_coniva,a.codigo_anexo_aliado,c.por_redrecaudo,c.por_margen_aliado,b.por_redrecaudo,b.por_margen_aliado ) a  where 1=1 and {$where}
				";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function loadPageAnexosFacturas($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.codigo_anexo";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);
        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        $sql = "select a.* from (select 
        		a.codigo_proveedor,
                a.codigo_anexo,
                substr(a.fechahora::text,1,19) as fechahora,
                a.fechahora_inicial,
                a.fechahora_final,
                0 as valor,
                coalesce(a.factura_proveedor,null,'') as factura_proveedor,
                coalesce(a.fecha_factura_proveedor,null,'') as fecha_factura_proveedor
            from cu_proveedores_anexos_factura a
            where a.codigo_proveedor=:codigo_proveedor  and a.agente='tienda') a where ({$where})";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function loadPromocionesAliadosPage($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "
			nombre,
			estado_revision,
			referencia,
			marca_proveedor,
			nombre_proveedor,
			origen
		";

        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if ($p["filters"]["estado_revision"] != "todos") {
            $where .= " and a.estado_revision='{$p["filters"]["estado_revision"]}' ";
        }
        if ($p["filters"]["vigente"] != "todas") {
            $where .= ($p["filters"]["vigente"] == "1" ? " and (current_timestamp between a.fechahora_inicial and a.fechahora_final or current_timestamp < a.fechahora_inicial)" : " and current_timestamp > a.fechahora_final ");
        }
        if (isset($p["filters"]["fecha_desde"]) && $p["filters"]["fecha_desde"] != "" && isset($p["filters"]["fecha_hasta"]) && $p["filters"]["fecha_hasta"] != "") {
            $where .= " and (cast(a.fechahora_inicial as date),cast(a.fechahora_final as date)) overlaps ('{$p["filters"]["fecha_desde"]}'::date,'{$p["filters"]["fecha_hasta"]}'::date) ";
        }

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }

        $sql = "
			select 
				* 
			from (-- trae las promociones del aliado de los productos que NO tienen precio por variacion
                    SELECT min(a.idprecio) AS idprecio,
                        a.codigo_producto,
                        a.estado_revision,
                        a.origen,
                        a.fechahora_inicial::TEXT || '<br>' || a.fechahora_final::TEXT AS fechahora_inicial_final,
                        func_numfmt(e.precio_coniva, 2) || '<br/>' || func_numfmt(a.precio_coniva, 2) AS precio_coniva,
                        func_numfmt(e.precio_compra, 2) || '<br/>' || func_numfmt(a.precio_compra, 2) AS precio_compra,
                        e.descuento_proveedor || '<br/>' || a.descuento_proveedor AS descuento_proveedor,
                        b.nombre,
                        b.nombre || '<br/>' || b.referencia AS nombre_referencia,
                        d.nombre || '<br/>' || c.nombre AS marca_proveedor,
                        b.orden_oferta AS orden,
                        sum(b.inventario) AS inventario,
                        b.referencia,
                        c.nombre AS nombre_proveedor,
                        a.fechahora_inicial,
                        a.fechahora_final,
                        CASE WHEN a.codigo_tipo = 2 THEN 'Coordiutil' WHEN a.codigo_tipo = 4 THEN 'P/Personalizado' WHEN a.codigo_tipo = 5 THEN 'Recuadador' END AS tipo
                    FROM cu_productos_precios a
                    INNER JOIN cu_productos b ON (a.codigo_producto = b.codigo_producto)
                    INNER JOIN cu_proveedores c ON (b.codigo_proveedor = c.codigo_proveedor)
                    INNER JOIN cu_marcas d ON (d.codigo_marca = b.codigo_marca)
                    INNER JOIN cu_productos_precios e ON (
                        a.codigo_producto = e.codigo_producto
                        AND e.codigo_inventario = a.codigo_inventario
                        AND e.codigo_tipo = (
                            SELECT pa.codigo_tipo_precio
                            FROM cu_proveedores_asociados_pp pa
                            WHERE a.codigo_proveedor_pp = pa.codigo_proveedor
                            AND c.codigo_proveedor = pa.codigo_proveedor_asociado
                        )
                    )
                    WHERE a.codigo_proveedor_pp <> 0
                        AND c.codigo_proveedor = b.codigo_proveedor
                        AND a.codigo_tipo IN (2, 4)
                        AND b.flags -> 'precio_por_variaciones' = '0'
                        AND a.codigo_proveedor_pp=:codigo_proveedor
                    GROUP BY a.codigo_producto,
                        a.estado_revision,
                        a.origen,
                        a.fechahora_inicial::TEXT || '<br>' || a.fechahora_final::TEXT,
                        e.precio_coniva,
                        a.precio_coniva,
                        e.precio_compra,
                        a.precio_compra,
                        e.descuento_proveedor || '<br/>' || a.descuento_proveedor,
                        b.nombre,
                        b.nombre || '<br/>' || b.referencia,
                        d.nombre || '<br/>' || c.nombre,
                        b.orden_oferta,
                        b.referencia,
                        c.nombre,
                        a.fechahora_inicial,
                        a.fechahora_final,
                        a.codigo_tipo
				
                    UNION ALL
				
                -- trae las promociones del aliado de los productos que tienen precio por variacion
                    SELECT a.idprecio,
                        a.codigo_producto,
                        a.estado_revision,
                        a.origen,
                        a.fechahora_inicial::TEXT || '<br>' || a.fechahora_final::TEXT AS fechahora_inicial_final,
                        func_numfmt(e.precio_coniva, 2) || '<br/>' || func_numfmt(a.precio_coniva, 2) AS precio_coniva,
                        func_numfmt(e.precio_compra, 2) || '<br/>' || func_numfmt(a.precio_compra, 2) AS precio_compra,
                        e.descuento_proveedor || '<br/>' || a.descuento_proveedor AS descuento_proveedor,
                        b.nombre,
                        b.nombre || '<br/>' || b.referencia || '<br/>' || 'Talla:' || b.talla || ' Color:' || b.color AS nombre_referencia,
                        d.nombre || '<br/>' || c.nombre AS marca_proveedor,
                        b.orden_oferta AS orden,
                        b.inventario,
                        b.referencia,
                        c.nombre AS nombre_proveedor,
                        a.fechahora_inicial,
                        a.fechahora_final,
                        CASE WHEN a.codigo_tipo = 2 THEN 'Coordiutil' WHEN a.codigo_tipo = 4 THEN 'P/Personalizado' WHEN a.codigo_tipo = 5 THEN 'Recuadador' END AS tipo
                    FROM cu_productos_precios a
                    INNER JOIN view_cu_productos_inventario b ON (
                        a.codigo_producto = b.codigo_producto
                        AND a.codigo_inventario = b.codigo_inventario
                    )
                    INNER JOIN cu_proveedores c ON (b.codigo_proveedor = c.codigo_proveedor)
                    INNER JOIN cu_marcas d ON (d.codigo_marca = b.codigo_marca)
                    INNER JOIN cu_productos_precios e ON (
                        a.codigo_producto = e.codigo_producto
                        AND e.codigo_inventario = b.codigo_inventario
                        AND e.codigo_tipo = (
                            SELECT pa.codigo_tipo_precio
                            FROM cu_proveedores_asociados_pp pa
                            WHERE a.codigo_proveedor_pp = pa.codigo_proveedor
                            AND c.codigo_proveedor = pa.codigo_proveedor_asociado
                        )
                    )
                    WHERE a.codigo_proveedor_pp <> 0
                        AND c.codigo_proveedor = b.codigo_proveedor
                        AND a.codigo_tipo IN (2, 4)
                        AND b.flags -> 'precio_por_variaciones' = '1'
                        AND a.codigo_proveedor_pp=:codigo_proveedor
                       
					) a
								where 
									1=1
				and ({$where}) 
		";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $result = $ca->execPage($p);
        for ($i = 0; $i < count($result["records"]); $i++) {
            $tmp = modRs::load(rs::cuProductos, $result["records"][$i]["codigo_producto"], array(rs::cuProductosImagen1));
            if (isset($tmp["imagen_1"])) {
                $result["records"][$i]["imagen_1"] = JDbFile::url($tmp["imagen_1"]["id"], array("w" => 60, "h" => 60, "frm" => 1));
            }
        }
        return $result;
    }

    public static function loadOne($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select a.*,cast(a.fechahora_inicial as date) as fecha_inicial,
             cast(a.fechahora_final as date) as fecha_final,
             cast(a.fechahora_inicial as time) as hora_inicial,
             cast(a.fechahora_final as time) as hora_final,
             c.nombre||', '||c.referencia||' ['||c.nombre_marca||', '||c.vpath||']' as nombre_producto,
             c.moneda as moneda_l,
             c.por_iva as por_iva_l,
             b.descripcion as descripcion_estado_revision,
             d.precio_compra as precio_compra_l,
             d.precio_coniva as precio_coniva_l,
             d.descuento_proveedor as descuento_proveedor_l,
             d.precio_coniva as precio_coniva_sinpromocion,
        	 coalesce(a.valor_transporte,0) as valor_transporte,
        	c.flags->'precio_por_variaciones' as precio_por_variaciones	
         from cu_productos_precios a
             join cu_estados_revision b on (a.codigo_estado_revision=b.codigo_estado)
             join view_cu_productos_base c on (a.codigo_producto=c.codigo_producto)
             join cu_productos_precios d on (d.codigo_producto=a.codigo_producto and d.codigo_tipo=1)
         where a.idprecio=:idprecio";
        $ca->prepare($sql);
        $ca->bindValue(":idprecio", $p["idprecio"], false);
        $ca->exec();

        $result = $ca->fetch();

        $result["codigo_producto"] = array(array("label" => $result["nombre_producto"], "data" => $result["codigo_producto"]));

        return $result;
    }

    public static function loadPromociones() {
        $si = Session::info();

        $result = array();
        $result["flag_tienda"] = $si["flag_tienda"];

        if ($si["flag_tienda"] == "1") {
            $result["codigos_tipo"] = array(array("data" => "", "label" => "Seleccione visibilidad"), array("data" => "4", "label" => "Portal personalizado"));
        } else {
            $result["codigos_tipo"] = array(array("data" => "2", "label" => "Coordiutil"));
        }

        $result["hora_inicial"] = "00:00:00";
        $result["hora_final"] = "23:59:59";

        $result["defaults"] = array("codigo_tipo" => "4");

        return $result;
    }

    public static function savePromocionAliados($p) {
        $si = Session::info();
        if ($si["tipo"] != "admin" && !in_array("FPromocionesNe_Save", $si["permisos"])) {
            throw new JPublicException("Usted no posee permisos para realizar esta acción");
        }

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $estadoRevision = "aprobado";
        $p["codigo_inventario"] = empty($p["codigo_inventario"]) ? 0 : $p["codigo_inventario"];
        $sqlCodigoInventario = "";
        if (!empty($p["codigo_inventario"])) {
            $sqlCodigoInventario = " and codigo_inventario=:codigo_inventario";
        }



        if ($p["codigo_tipo"] == 4) {
            $sql = "
				select 
					a.precio_coniva 
				from 
					cu_productos_precios a 
				where 
					a.codigo_producto=:codigo_producto 
					and :precio_coniva>=a.precio_coniva 
					and a.codigo_tipo= 1
					{$sqlCodigoInventario}
			";
            $ca->prepare($sql);
            $ca->bindValue(":precio_coniva", $p["precio_coniva"], false);
            $ca->bindValue(":codigo_producto", $p["codigo_producto"][0], false);
            $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
            $ca->exec();
            if ($ca->size() > 0) {
                throw new JPublicException("Error, el precio de la promocion debe de ser mas bajo que el precio corriente.");
            }
        }
        $db->transaction();
        $campos = "
			fechahora_inicial,
			fechahora_final,
			precio_coniva,
			descuento_proveedor,
			descripcion,
			codigo_estado_revision,
			estado_revision,
			origen,
			precio_compra,
			codigo_tipo,
			last_user,
        	valor_transporte,
        	codigo_proveedor_pp
		";

        if ($p["idprecio"] === "") {
            $ca->prepareSelect(
                    "cu_productos_precios", "codigo_producto", "codigo_producto=:codigo_producto 
				and codigo_tipo=:codigo_tipo 
				and codigo_proveedor_pp=:codogo_proveedor_pp
                and (cast(fechahora_inicial as timestamp),cast(fechahora_final as timestamp)) 
					overlaps (cast(:fechahora_inicial as timestamp),cast(:fechahora_final as timestamp))
				{$sqlCodigoInventario}
				"
            );
            $ca->bindValue(":codigo_producto", $p["codigo_producto"]["0"], false);
            $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
            $ca->bindValue(":fechahora_inicial", "{$p["fecha_inicial"]} {$p["hora_inicial"]}", true);
            $ca->bindValue(":fechahora_final", "{$p["fecha_final"]} {$p["hora_final"]}", true);
            $ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
            $ca->bindValue(":codogo_proveedor_pp", $si["codigo_proveedor"], false);

            $ca->exec();

            $campos .= ",codigo_producto,codigo_inventario";

            $p["valor_transporte"] = !empty($p["valor_transporte"]) ? $p["valor_transporte"] : "0";
            if ($ca->size() > 0) {
                throw new JPublicException("Error, ya existe una promoción en estas fechas para el producto seleccionado.");
            }

            if (!empty($p["codigo_inventario"])) {
                $ca->prepareInsert("cu_productos_precios", $campos);
            } else {
                $sqlCampos = "";
                $lCampos = explode(",", $campos);
                for ($i = 0; $i < count($lCampos); $i++) {
                    $lCampos[$i] = trim($lCampos[$i]);
                    /* No se incluye el codigo inventario en los campos */
                    if ($lCampos[$i] == "codigo_inventario")
                        continue;
                    $sqlCampos .= ":" . $lCampos[$i] . ",";
                }
                $sqlCampos = substr(trim($sqlCampos), 0, -1);
                $sql = "
            	insert into cu_productos_precios({$campos})
            	select
	            	{$sqlCampos},
	            	a.codigo_inventario
            	from
	            	cu_productos_inventario a
            	where
            		a.codigo_producto=:codigo_producto
            	";
                $ca->prepare($sql);
            }
        } else {
            $sqlCodigoInventarioValidacion = "";
            if (empty($p["codigo_inventario"])) {
                $sqlCodigoInventarioValidacion = "and codigo_inventario in (select codigo_inventario from cu_productos_precios where idprecio=:idprecio)";
            }
            $ca->prepareSelect(
                    "cu_productos_precios", "codigo_producto", "idprecio<>:idprecio 
				and codigo_producto=:codigo_producto 
				{$sqlCodigoInventarioValidacion}
				and codigo_tipo=:codigo_tipo 
				and codigo_proveedor_pp=:codigo_proveedor_pp
                and (cast(fechahora_inicial as timestamp),cast(fechahora_final as timestamp)) 
					overlaps (cast(:fechahora_inicial as timestamp),cast(:fechahora_final as timestamp))
				{$sqlCodigoInventario}
				"
            );
            $ca->bindValue(":codigo_producto", $p["codigo_producto"]["0"], false);
            $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
            $ca->bindValue(":fechahora_inicial", "{$p["fecha_inicial"]} {$p["hora_inicial"]}", true);
            $ca->bindValue(":fechahora_final", "{$p["fecha_final"]} {$p["hora_final"]}", true);
            $ca->bindValue(":idprecio", $p["idprecio"], false);
            $ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
            $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
            $ca->exec();

            if ($ca->size() > 0) {
                throw new JPublicException("Error, ya existe una promoción en estas fechas para el producto seleccionado.");
            }

            $p["valor_transporte"] = isset($p["valor_transporte"]) ? $p["valor_transporte"] : "valor_transporte";


            if (!empty($p["codigo_inventario"])) {
                $ca->prepareUpdate("cu_productos_precios", $campos, "idprecio=:idprecio {$sqlCodigoInventario}");
                $ca->bindValue(":idprecio", $p["idprecio"], false);
            } else {
                $sqlCampos = "";
                $lCampos = explode(",", $campos);
                for ($i = 0; $i < count($lCampos); $i++) {
                    $lCampos[$i] = trim($lCampos[$i]);
                    $sqlCampos .= $lCampos[$i] . " = :" . $lCampos[$i] . ",";
                }
                $sqlCampos = substr(trim($sqlCampos), 0, -1);

                /* Se debe actualizar todos los registros del producto para esa promocion (todos los codigos de inventario por igual) */
                $sql = "
					update 
						cu_productos_precios a
					set 
						{$sqlCampos}
					from 
						cu_productos_precios b
					where 
						b.idprecio=:idprecio 
						and b.codigo_producto=a.codigo_producto 
						and b.fechahora_inicial=a.fechahora_inicial
						and b.fechahora_final=a.fechahora_final 
						and b.codigo_tipo=a.codigo_tipo
						and b.codigo_proveedor_pp=a.codigo_proveedor_pp
				";
                $ca->prepare($sql);
                $ca->bindValue(":idprecio", $p["idprecio"], false);
            }
        }
        $ca->bindValue(":codigo_producto", $p["codigo_producto"]["0"], false);
        $ca->bindValue(":codigo_inventario", $p["codigo_inventario"], false);
        $ca->bindValue(":fechahora_inicial", "{$p["fecha_inicial"]} {$p["hora_inicial"]}", true);
        $ca->bindValue(":fechahora_final", "{$p["fecha_final"]} {$p["hora_final"]}", true);
        $ca->bindValue(":descuento_proveedor", $p["descuento_proveedor"], false);
        $ca->bindValue(":precio_coniva", $p["precio_coniva"], false);
        $ca->bindValue(":precio_compra", $p["precio_compra"], false);
        $ca->bindValue(":descripcion", $p["descripcion"], true);
        $ca->bindValue(":estado_revision", $estadoRevision, true);
        $ca->bindValue(":codigo_estado_revision", "0", false);
        $ca->bindValue(":origen", "proveedor", true);
        $ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
        $ca->bindValue(":last_user", $si["usuario"], true);
        $ca->bindValue(":valor_transporte", $p["valor_transporte"], false);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        //throw new JPublicException($ca->preparedQuery());
        $ca->exec();

        $flags = JUtils::pgsqlHStoreToArray($si["flags"]);

        if (!empty($flags["flag_tienda_externa"]) && $flags["flag_tienda_externa"] == "1") {
            $sql = "update cu_productos set fechahora_sync_proveedor=null where codigo_producto=:codigo_producto";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_producto", $p["codigo_producto"], false);
            $ca->exec();
        }

        if (!in_array($p["codigo_tipo"], array(1, 3))) {
            /* Se progrma la ejecucion de la tarea que va a actualizar la informacion de precios menores del producto una vez la promocion entre en vigencia (1 minuto despues de la fecha inicial) */
            $fechaProgramadaInicial = DateTime::createFromFormat('Y-m-d H:i:s', "{$p["fecha_inicial"]} {$p["hora_inicial"]}")->add(date_interval_create_from_date_string('1 minute'))->format('Y-m-d H:i:s');
            $fechaProgramadaFinal = DateTime::createFromFormat('Y-m-d H:i:s', "{$p["fecha_final"]} {$p["hora_final"]}")->add(date_interval_create_from_date_string('1 minute'))->format('Y-m-d H:i:s');
        }
        if ($fechaProgramadaInicial < date("Y-m-d H:i:s")) {
            $fechaProgramadaInicial = date("Y-m-d H:i:s");
        }
        modTareasProgramdas::programar(
                __CLASS__, __METHOD__, "/tasks/actualizar_productos_precios_menores.php", array("codigo_producto" => $p["codigo_producto"]["0"]), empty($fechaProgramadaInicial) ? array() : array("fechahora" => $fechaProgramadaInicial, "prioridad" => 1)
        );

        modTareasProgramdas::programar(
                __CLASS__, __METHOD__, "/tasks/actualizar_productos_precios_menores.php", array("codigo_producto" => $p["codigo_producto"]["0"]), empty($fechaProgramadaFinal) ? array() : array("fechahora" => $fechaProgramadaFinal, "prioridad" => 1)
        );

        $db->commit();
        return;
    }

    public static function autocompleteProductos($p) {
        $si = Session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (empty($p["codigo_tipo"])) {
            throw new JPublicException("Error, debe seleccionar Tipo promoción.");
        }

        $campos = "
			a.nombre,
			a.referencia,
			a.nombre_marca
		";
        $where = $ca->sqlFieldsFilters($campos, $p["filter"]);
        $sql = "
			select 
				a.codigo_producto as data,
    			a.nombre||', '||a.referencia||' ['||a.nombre_marca||', '||a.vpath||']' as label,
    			a.por_iva,
    			a.moneda,
				a.flags->'precio_por_variaciones' as precio_por_variaciones
			from 
				view_cu_productos_base a
				join cu_proveedores_asociados_pp_productos b on (b.codigo_producto=a.codigo_producto and a.codigo_proveedor=b.codigo_proveedor_asociado and  b.codigo_proveedor=:codigo_proveedor)
			where 
				1=1 
				and {$where}
			order by 
				a.nombre
			";
        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $rProductos = $ca->fetchAll();


        foreach ($rProductos as $k => $r) {

            $sql = "
				select 
					min(pp.descuento_proveedor) as descuento_proveedor,
					func_numfmt(ppm.precio,2) as precio_venta_coniva,
					func_numfmt(min(pp.precio_compra),2) as precio_cu_siniva,
					ppm.precio as precio_coniva_sinpromocion
				from cu_productos_precios_menores ppm
					join cu_productos_precios pp on (ppm.codigo_producto=pp.codigo_producto and pp.codigo_tipo = 1 )
				where 
					ppm.codigo_tipo=pp.codigo_tipo
					and ppm.codigo_producto=:codigo_producto
				group by 
					ppm.precio
			";
            $ca->prepare($sql);
            $ca->bindValue(":codigo_producto", $r["data"], false);
            $ca->bindValue(":codigo_tipo", $p["codigo_tipo"], false);
            $ca->exec();
            if ($ca->size() > 0) {
                $res = $ca->fetch();
                $rProductos[$k] = array_merge($r, $res);
            }
        }

        return $rProductos;
    }

    public static function exportarMargenesProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select a.codigo_producto,
				a.nombre,
				a.referencia,
				a.nombre_marca,
				a.precio_venta_coniva_r as precio_venta,
				a.precio_compra_anexo as precio_cu,
				a.por_iva,
				a.descuento_proveedor_r as descuento_proveedor,
				round((a.precio_venta_siniva_r*(a.descuento_proveedor_r/100)),2) as margen_total,
				round((a.precio_venta_siniva_r*(a.descuento_proveedor_r/100))*(b.por_margen_aliado/100),2) as margen_neto_aliado,
				a.inventario,
				coalesce((select sum(unidades) from cu_pedidos_det where codigo_inventario in 
					(select codigo_inventario from cu_productos_inventario where codigo_producto=a.codigo_producto) 
					and estado='reservado'),0) as inventario_reservado
			FROM view_cu_productos a
				left join cu_proveedores_anexos_acuerdo b on (b.codigo_proveedor=:codigo_proveedor)
				join cu_proveedores_asociados_pp_productos c on (a.codigo_producto = c.codigo_producto and 			   
        		c.codigo_proveedor=:codigo_proveedor)
			where a.codigo_proveedor=:codigo_proveedor_asociado";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->bindValue(":codigo_proveedor_asociado", $p["codigo_proveedor_asociado"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/margen_productos_aliados_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = & $wb->addWorksheet('Hoja1');

        $row = 0;
        foreach ($ca->fetchAll() as $r) {
            $col = 0;
            if ($row == 0) {
                foreach ($r as $k => $v) {
                    $k = str_replace("_", " ", $k);
                    $k = ucfirst($k);

                    $ws->writeString($row, $col, $k);
                    $col++;
                }
                $row += 1;
                $col = 0;
            }

            foreach ($r as $k => $v) {
                if (in_array($k, array("precio_venta", "precio_cu", "margen_total", "margen_neto_aliado", "descuento_proveedor", "por_iva", "inventario", "inventario_reservado"))) {
                    $ws->writeNumber($row, $col, $v);
                    $col++;
                    continue;
                }

                $ws->writeString($row, $col, utf8_decode($v));
                $col++;
            }
            $row++;
        }

        $wb->close();
        return basename($path);
    }

    public static function exportarPinesAnexosFacturasAliados($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        /*
          $sql = "select a.codigo_anexo_aliado as codigo_anexo,
          a.pin,
          array_to_string(array_accum(a.unidades||' - '|| a.nombre),'<br/>') as detalle,
          a.fechahora_pago,
          func_number_format(a.total_coniva,0) as valor_pin,
          func_number_format(sum(a.precio_compra_anexo),0) as precio_coordiutil,
          func_number_format(sum(a.precio_venta_siniva*(a.descuento_proveedor/100)),0) as margen_coordiutil,
          func_number_format(a.total_coniva*((case when c.por_redrecaudo is null then b.por_redrecaudo else c.por_redrecaudo end)/100),0) as red_recaudo,
          func_number_format((sum(a.precio_venta_siniva*(a.descuento_proveedor/100))-a.total_coniva*
          ((case when c.por_redrecaudo is null then b.por_redrecaudo else c.por_redrecaudo end)/100))*
          ((case when c.por_margen_aliado is null then b.por_margen_aliado else c.por_margen_aliado end)/100),0) as total
          from view_cu_pedidos_det a
          join cu_proveedores_anexos_acuerdo b on (a.codigo_proveedor_pp=b.codigo_proveedor)
          left join cu_proveedores_anexos_factura c on (a.codigo_anexo_aliado=c.codigo_anexo)
          where a.codigo_proveedor_pp=:codigo_proveedor and
          a.estado_pin='pagado' and a.estado_autorizacion='aprobado'
          group by a.pin,a.fechahora_pago,a.total_coniva,a.codigo_anexo_aliado,c.por_redrecaudo,c.por_margen_aliado,b.por_redrecaudo,b.por_margen_aliado";
         */

        $sql = "select a.codigo_anexo_aliado as codigo_anexo,
					a.pin,
					array_to_string(array_accum(a.unidades||' - '|| a.nombre),'<br/>') as detalle,
					a.fechahora_pago,
					a.total_coniva as valor_pin,
					sum(a.precio_compra_anexo) as precio_coordiutil,
					case when sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades) < 0 then 0 else sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades) end as margen_coordiutil,
					a.total_coniva*((case when c.por_redrecaudo is null then b.por_redrecaudo else c.por_redrecaudo end)/100) as red_recaudo,
					case when (sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)-a.total_coniva*(c.por_redrecaudo/100))*(c.por_margen_aliado/100) < 0 then 0 else
					(sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)-a.total_coniva*(c.por_redrecaudo/100))*(c.por_margen_aliado/100) end as total
				from view_cu_pedidos_det a 
					join cu_proveedores_anexos_acuerdo b on (a.codigo_proveedor_pp=b.codigo_proveedor)
					left join cu_proveedores_anexos_factura c on (a.codigo_anexo_aliado=c.codigo_anexo)
				where a.codigo_proveedor_pp=:codigo_proveedor and
					a.estado_pin='pagado' and a.estado_autorizacion='aprobado'
				group by a.pin,a.fechahora_pago,a.total_coniva,a.codigo_anexo_aliado,c.por_redrecaudo,c.por_margen_aliado,b.por_redrecaudo,b.por_margen_aliado
				";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();

        $path = JApp::privateTempPath() . "/pines_anexos_facturas_aliados_{$si["codigo_proveedor"]}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = & $wb->addWorksheet('Hoja1');

        $row = 0;
        foreach ($ca->fetchAll() as $r) {
            $col = 0;
            if ($row == 0) {
                foreach ($r as $k => $v) {
                    $k = str_replace("_", " ", $k);
                    $k = ucfirst($k);

                    $ws->writeString($row, $col, $k);
                    $col++;
                }
                $row += 1;
                $col = 0;
            }

            foreach ($r as $k => $v) {
                if (in_array($k, array("valor_pin", "precio_coordiutil", "margen_coordiutil", "red_recaudo", "total"))) {
                    $ws->writeNumber($row, $col, $v);
                    $col++;
                    continue;
                }

                $ws->writeString($row, $col, utf8_decode($v));
                $col++;
            }
            $row++;
        }

        $wb->close();
        return basename($path);
    }

    public static function exportarInventariosProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select case when pit.inventario<=5 then 1 else round(pit.inventario/2) end as inventario,pit.codigo_inventario ,ppp.codigo_inventario_tercero
				from view_cu_productos_inventario_tienda pit
				join cu_productos_inventario_pp ppp on pit.codigo_inventario = ppp.codigo_inventario and pit.codigo_proveedor_pp = ppp.codigo_proveedor_pp
				where pit.codigo_proveedor_pp =:codigo_proveedor_pp
				order by ppp.codigo_inventario_tercero asc";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();

        $rInventarios = $ca->fetchAll();

        $template = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
			<INVENTORY-UPDATE xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
				<CATALOGUE ID="34" FULL="false">
					<CHILD-CATALOGUE ID="34" />
					{% for item in inventarios %}
						<SKU ID="{{ item.codigo_inventario_tercero }}">
							<STOCK-LEVEL>{{ item.inventario }}</STOCK-LEVEL>
						</SKU>
					{% endfor %}
				</CATALOGUE>
			</INVENTORY-UPDATE>';

        $templateParser = new TwigLoader();
        $content = $templateParser->parseTemplate($template, array("inventarios" => $rInventarios));

        $fecha = date('Ymd');
        $path = JApp::privateTempPath() . "/inventario_CU{$fecha}.xml";
        file_put_contents($path, $content);

        return basename($path);
    }

    public static function exportarPreciosProductos($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $sql = "select pit.codigo_inventario,ppp.codigo_inventario_tercero,
				round(pit.precio_venta_coniva_r_transporte_incluido) as precio_venta_coniva_r,round(pit.precio_venta_coniva_transporte_incluido) as precio_venta_coniva
				from view_cu_productos_inventario_tienda pit
				join cu_productos_inventario_pp ppp on pit.codigo_inventario = ppp.codigo_inventario and pit.codigo_proveedor_pp = ppp.codigo_proveedor_pp
 				where pit.codigo_proveedor_pp =:codigo_proveedor_pp
				order by ppp.codigo_inventario_tercero asc";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        $ca->exec();

        $rPreciosInventarios = $ca->fetchAll();

        $template = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
			<PRICELIST-UPDATE xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
				<CATALOGUE ID="34">
					<CHILD-CATALOGUE ID="34"/>
					{% for item in productos %}
						<SKU ID="{{ item.codigo_inventario_tercero }}">
							<LIST-PRICE>{{ item.precio_venta_coniva }}</LIST-PRICE>
							<SALE-PRICE>{{ item.precio_venta_coniva_r }}</SALE-PRICE>
						</SKU>
					{% endfor %}
				</CATALOGUE>
			</PRICELIST-UPDATE>';

        $templateParser = new TwigLoader();
        $content = $templateParser->parseTemplate($template, array("productos" => $rPreciosInventarios));

        $fecha = date('Ymd');
        $path = JApp::privateTempPath() . "/precio_CU{$fecha}.xml";
        file_put_contents($path, $content);

        return basename($path);
    }

    public static function importarInventariosAliados($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        throw new JPublicException(print_r($p, 1));

        $sql = "select a.codigo_inventario, a.codigo_inventario_tercero,a.codigo_producto
				from  cu_productos_inventario_pp a
				where 1=1
				and.codigo_inventario_tercero:codigo_inventario_tercero
				and a.codigo_inventrio=:codigo_inventario 
				and a.codigo_proveedor_pp =:codigo_proveedor_pp";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor_pp", $si["codigo_proveedor"], false);
        //$ca->bindValue(":codigo_iventario",)
        $ca->exec();
    }

    public static function actualizarProductoPreciosMenoresAliados($codigoProductos) {

        /* actualizo informacion para cu_productos_precios_menores tipos precio 1 y 3 */

        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        /* Borrar los registros existentes del producto */
        $sql = " DELETE
					FROM cu_productos_precios_menores
				WHERE codigo_producto in ({$codigoProductos})
				AND codigo_tipo IN (1,3) ";
        $ca->prepare($sql);
        $ca->exec();

        /* Resgistra informacion del precio normal del proveedor (tipo 1) en Coordiutil */
        $sql = "INSERT INTO cu_productos_precios_menores (codigo_producto, codigo_tipo, codigo_proveedor_pp, precio, flags)
				SELECT a.codigo_producto, 1 AS codigo_tipo, 0 AS codigo_proveedor_pp,
				min(FUNC_REDONDEO100(FUNC_PRECIO_VENTA(PO.VAL_MANEJO_INTER + A.VAL_SHIPPING_XT + ((A.POR_TAX_XT / 100) * PO.PRECIO_COMPRA) + ((A.POR_GARANTIA / 100) * PO.PRECIO_COMPRA
					) + PO.PRECIO_CONIVA, A.MONEDA) + COALESCE(PO.VALOR_TRANSPORTE, 0))) AS precio, hstore('flag_transporte_incluido', CASE WHEN min(COALESCE(PO.VALOR_TRANSPORTE, 0)) > 0
				THEN '1'
				ELSE '0'
				END) AS flags
				FROM cu_productos a
				INNER JOIN CU_PRODUCTOS_PRECIOS PO ON ( A.CODIGO_PRODUCTO = PO.CODIGO_PRODUCTO AND PO.ESTADO_REVISION = 'aprobado' AND PO.CODIGO_TIPO = 1)
				LEFT JOIN cu_productos_precios_menores ppm ON ( a.codigo_producto = ppm.codigo_producto AND 0 = ppm.codigo_proveedor_pp AND ppm.codigo_tipo = 1)
				WHERE 1 = 1
				AND ppm.codigo_producto IS NULL
				AND a.codigo_producto  in ({$codigoProductos})
				GROUP BY a.codigo_producto
				--,PO.VALOR_TRANSPORTE ";
        $ca->prepare($sql);
        $ca->exec();

        /* Resgistra informacion del precio normal del proveedor en su tienda VendesFacil (tipo 3) */
        $sql = "INSERT INTO cu_productos_precios_menores (codigo_producto, codigo_tipo, codigo_proveedor_pp, precio, flags)
				SELECT a.codigo_producto, 3, PA.codigo_proveedor, --min(a.precio_venta_coniva_transporte_incluido),
				min(FUNC_REDONDEO100(FUNC_PRECIO_VENTA(PO.VAL_MANEJO_INTER + A.VAL_SHIPPING_XT + ((A.POR_TAX_XT / 100) * PO.PRECIO_COMPRA) +
				((A.POR_GARANTIA / 100) * PO.PRECIO_COMPRA) + PO.PRECIO_CONIVA, A.MONEDA) +
				(
					CASE
						WHEN (PPP.PRECIO_CONIVA IS NOT NULL)
						THEN COALESCE(PPP.VALOR_TRANSPORTE, 0)
					WHEN (PP.PRECIO_CONIVA IS NOT NULL)
						THEN COALESCE(PP.VALOR_TRANSPORTE, 0)
					ELSE COALESCE(PO.VALOR_TRANSPORTE, 0)
					END
			  	))), hstore('flag_transporte_incluido', CASE
				WHEN min(CASE
						WHEN (PPP.PRECIO_CONIVA IS NOT NULL)
							THEN COALESCE(PPP.VALOR_TRANSPORTE, 0)
						WHEN (PP.PRECIO_CONIVA IS NOT NULL)
							THEN COALESCE(PP.VALOR_TRANSPORTE, 0)
						ELSE COALESCE(PO.VALOR_TRANSPORTE, 0)
				END) > 0
				THEN '1'
				ELSE '0'
				END) AS flags
				FROM CU_PROVEEDORES P
				LEFT JOIN VIEW_CU_PROVEEDORES_ASOCIADOS_PP_PRODUCTOS PA ON (P.CODIGO_PROVEEDOR = PA.CODIGO_PROVEEDOR)
				INNER JOIN view_CU_PRODUCTOS_BASE A ON (A.CODIGO_PRODUCTO = PA.CODIGO_PRODUCTO)
				INNER JOIN CU_PRODUCTOS_PRECIOS PO ON (A.CODIGO_PRODUCTO = PO.CODIGO_PRODUCTO AND PO.ESTADO_REVISION = 'aprobado' AND PO.CODIGO_TIPO = PA.CODIGO_TIPO_PRECIO)
				LEFT JOIN CU_PRODUCTOS_PRECIOS PP ON (A.CODIGO_PRODUCTO = PP.CODIGO_PRODUCTO AND PP.ESTADO_REVISION = 'aprobado' AND PP.CODIGO_TIPO = PA.CODIGO_TIPO_PRECIO_PROMOCION AND CURRENT_TIMESTAMP BETWEEN PP.FECHAHORA_INICIAL AND PP.FECHAHORA_FINAL AND PP.CODIGO_PROVEEDOR_PP = 0)
				LEFT JOIN CU_PRODUCTOS_PRECIOS PPP ON (A.CODIGO_PRODUCTO = PPP.CODIGO_PRODUCTO AND PPP.ESTADO_REVISION = 'aprobado' AND CURRENT_TIMESTAMP BETWEEN PPP.FECHAHORA_INICIAL AND PPP.FECHAHORA_FINAL AND PPP.CODIGO_PROVEEDOR_PP = P.CODIGO_PROVEEDOR AND PPP.CODIGO_TIPO = 4)
				LEFT JOIN cu_productos_precios_menores ppm ON (a.codigo_producto = ppm.codigo_producto AND pa.codigo_proveedor = ppm.codigo_proveedor_pp AND ppm.codigo_tipo = 3)
				WHERE ((A.CODIGO_PROVEEDOR = P.CODIGO_PROVEEDOR AND A.FLAGS ->'vis_ti' = '1')
				OR P.CODIGO_PROVEEDOR <> PA.CODIGO_PROVEEDOR_ASOCIADO)
				AND CASE
				WHEN PA.NEGOCIOS <> '{}'
				THEN ARRAY(SELECT UNNEST(NEGOCIOS) FROM CU_CATEGORIASN WHERE CODIGO_CATEGORIA = A.CODIGO_CATEGORIA3) && PA.NEGOCIOS
				ELSE 1 = 1
				END
				----Para las tiendas que no son aliadas y manejan precios tipo 3
				AND A.CODIGO_PROVEEDOR = P.CODIGO_PROVEEDOR
				AND ppm.codigo_producto IS NULL
				AND a.codigo_producto  in ({$codigoProductos})
				GROUP BY a.codigo_producto,
				PA.codigo_proveedor
				--,PPP.PRECIO_CONIVA,PPP.VALOR_TRANSPORTE,PP.PRECIO_CONIVA,PP.VALOR_TRANSPORTE,PO.VALOR_TRANSPORTE";

        $ca->prepare($sql);
        $ca->exec();
        //throw new JPublicException(print_r($ca->fetchAll(),1));

        /* Registra información del precio normal para las tiendas aliadas (tipo 1) */

        $sql = "INSERT INTO cu_productos_precios_menores (
				codigo_producto,
				codigo_tipo,
				codigo_proveedor_pp,
				precio,
				flags
				)
			SELECT a.codigo_producto,
				1,
				PA.codigo_proveedor, --min(a.precio_venta_coniva_transporte_incluido),
				min(FUNC_REDONDEO100(FUNC_PRECIO_VENTA(PO.VAL_MANEJO_INTER + A.VAL_SHIPPING_XT + ((A.POR_TAX_XT / 100) * PO.PRECIO_COMPRA
								) + ((A.POR_GARANTIA / 100) * PO.PRECIO_COMPRA) + PO.
							PRECIO_CONIVA, A.MONEDA) + (
							CASE
								WHEN (PPP.PRECIO_CONIVA IS NOT NULL)
									THEN COALESCE(PPP.VALOR_TRANSPORTE, 0)
								WHEN (PP.PRECIO_CONIVA IS NOT NULL)
									THEN COALESCE(PP.VALOR_TRANSPORTE, 0)
								ELSE COALESCE(PO.VALOR_TRANSPORTE, 0)
								END
							))),
				hstore('flag_transporte_incluido', CASE
						WHEN min(CASE
									WHEN (PPP.PRECIO_CONIVA IS NOT NULL)
										THEN COALESCE(PPP.VALOR_TRANSPORTE, 0)
									WHEN (PP.PRECIO_CONIVA IS NOT NULL)
										THEN COALESCE(PP.VALOR_TRANSPORTE, 0)
									ELSE COALESCE(PO.VALOR_TRANSPORTE, 0)
									END) > 0
							THEN '1'
						ELSE '0'
						END) AS flags
			FROM CU_PROVEEDORES P
			LEFT JOIN VIEW_CU_PROVEEDORES_ASOCIADOS_PP_PRODUCTOS PA
				ON (P.CODIGO_PROVEEDOR = PA.CODIGO_PROVEEDOR)
			INNER JOIN view_CU_PRODUCTOS_BASE A
				ON (A.CODIGO_PRODUCTO = PA.CODIGO_PRODUCTO)
			INNER JOIN CU_PRODUCTOS_PRECIOS PO
				ON (
						A.CODIGO_PRODUCTO = PO.CODIGO_PRODUCTO
						AND PO.ESTADO_REVISION = 'aprobado'
						AND PO.CODIGO_TIPO = PA.CODIGO_TIPO_PRECIO
						)
			LEFT JOIN CU_PRODUCTOS_PRECIOS PP
				ON (
						A.CODIGO_PRODUCTO = PP.CODIGO_PRODUCTO
						AND PP.ESTADO_REVISION = 'aprobado'
						AND PP.CODIGO_TIPO = PA.CODIGO_TIPO_PRECIO_PROMOCION
						AND CURRENT_TIMESTAMP BETWEEN PP.FECHAHORA_INICIAL
							AND PP.FECHAHORA_FINAL
						AND PP.CODIGO_PROVEEDOR_PP = 0
						)
			LEFT JOIN CU_PRODUCTOS_PRECIOS PPP
				ON (
						A.CODIGO_PRODUCTO = PPP.CODIGO_PRODUCTO
						AND PPP.ESTADO_REVISION = 'aprobado'
						AND CURRENT_TIMESTAMP BETWEEN PPP.FECHAHORA_INICIAL
							AND PPP.FECHAHORA_FINAL
						AND PPP.CODIGO_PROVEEDOR_PP = P.CODIGO_PROVEEDOR
						AND PPP.CODIGO_TIPO = 4
						)
			LEFT JOIN cu_productos_precios_menores ppm
				ON (
						a.codigo_producto = ppm.codigo_producto
						AND pa.codigo_proveedor = ppm.codigo_proveedor_pp
						AND ppm.codigo_tipo = 1
						)
			WHERE (
					(
						A.CODIGO_PROVEEDOR = P.CODIGO_PROVEEDOR
						AND A.FLAGS -> 'vis_ti' = '1'
						)
					OR P.CODIGO_PROVEEDOR <> PA.CODIGO_PROVEEDOR_ASOCIADO
					)
				AND CASE
					WHEN PA.NEGOCIOS <> '{}'
						THEN ARRAY(SELECT UNNEST(NEGOCIOS) FROM CU_CATEGORIASN WHERE CODIGO_CATEGORIA = A.CODIGO_CATEGORIA3) && PA.NEGOCIOS
					ELSE 1 = 1
					END
				----Para las tiendas que son aliadas y manejan precios tipo 1
				AND P.CODIGO_PROVEEDOR <> PA.CODIGO_PROVEEDOR_ASOCIADO
				AND ppm.codigo_producto IS NULL
				AND a.codigo_producto  in ({$codigoProductos})
			GROUP BY a.codigo_producto,
				PA.codigo_proveedor
				--,PPP.PRECIO_CONIVA,PPP.VALOR_TRANSPORTE,PP.PRECIO_CONIVA,PP.VALOR_TRANSPORTE,PO.VALOR_TRANSPORTE
		";
        $ca->prepare($sql);
        $ca->exec();
    }

    public static function actualizarProductoPreciosMenoresPromocionesAliados($codigoProductos) {
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        /* actualizo informacion para cu_productos_precios_menores tipos precio 2 y 4 */

        /* Borrar los registros existentes del producto */
        $sql = " DELETE
					FROM cu_productos_precios_menores
				WHERE codigo_producto in ({$codigoProductos})
				AND codigo_tipo IN (2,4) ";
        $ca->prepare($sql);
        $ca->exec();

        /*  promociones tipo  2 Coordiutil */

        $sql = "INSERT INTO cu_productos_precios_menores (
				codigo_producto,
				codigo_tipo,
				codigo_proveedor_pp,
				precio,
				flags
				)
				SELECT a.codigo_producto,
					a.codigo_tipo,
					a.codigo_proveedor,
					min(a.precio),
					a.flags
				FROM (
					SELECT a.codigo_producto,
						2 AS codigo_tipo,
						0 AS codigo_proveedor, --min(a.precio_venta_coniva_transporte_incluido),
						min(FUNC_REDONDEO100(FUNC_PRECIO_VENTA(PO.VAL_MANEJO_INTER + A.VAL_SHIPPING_XT + ((A.POR_TAX_XT / 100) * PO.PRECIO_COMPRA) + ((A.POR_GARANTIA / 100) * PO.PRECIO_COMPRA) + PO.PRECIO_CONIVA, A.MONEDA) + (
									CASE 
										WHEN (PP.PRECIO_CONIVA IS NOT NULL)
											THEN COALESCE(PP.VALOR_TRANSPORTE, 0)
										ELSE COALESCE(PO.VALOR_TRANSPORTE, 0)
										END
									))) AS precio,
						hstore('flag_transporte_incluido', CASE 
								WHEN (
										CASE 
											WHEN (PP.PRECIO_CONIVA IS NOT NULL)
												THEN COALESCE(PP.VALOR_TRANSPORTE, 0)
											ELSE COALESCE(PO.VALOR_TRANSPORTE, 0)
											END
										) > 0
									THEN '1'
								ELSE '0'
								END) AS flags
					FROM cu_productos a
					INNER JOIN CU_PRODUCTOS_PRECIOS PO ON (
							A.CODIGO_PRODUCTO = PO.CODIGO_PRODUCTO
							AND PO.ESTADO_REVISION = 'aprobado'
							AND PO.CODIGO_TIPO = 1
							)
					INNER JOIN CU_PRODUCTOS_PRECIOS PP ON (
							A.CODIGO_PRODUCTO = PP.CODIGO_PRODUCTO
							AND PP.ESTADO_REVISION = 'aprobado'
							AND PP.CODIGO_TIPO = 2
							AND CURRENT_TIMESTAMP BETWEEN PP.FECHAHORA_INICIAL
								AND PP.FECHAHORA_FINAL
							AND PP.CODIGO_PROVEEDOR_PP = 0
							)
					LEFT JOIN cu_productos_precios_menores ppm ON (
							a.codigo_producto = ppm.codigo_producto
							AND 0 = ppm.codigo_proveedor_pp
							AND ppm.codigo_tipo = 2
							)
					WHERE 1 = 1
						AND ppm.codigo_producto IS NULL
						AND a.codigo_producto in ({$codigoProductos})
					GROUP BY a.codigo_producto,
						PO.VALOR_TRANSPORTE,
						PP.VALOR_TRANSPORTE,
						PP.PRECIO_CONIVA
					) a
				GROUP BY a.codigo_producto,
					a.codigo_tipo,
					a.codigo_proveedor,
					a.flags;";


        $ca->prepare($sql);
        $ca->exec();

        /* promociones tienda tipo 4  tienda  */

        $sql = "INSERT INTO cu_productos_precios_menores (
				codigo_producto,
				codigo_tipo,
				codigo_proveedor_pp,
				precio,
				flags
				)
				select  a.codigo_producto,
       		   a.codigo_tipo,
               a.codigo_proveedor,
             min  (a.precio),
               a.flags
    
        from (
	
	
                SELECT a.codigo_producto,
				4 as codigo_tipo,
				PA.codigo_proveedor, --min(a.precio_venta_coniva_transporte_incluido),
				min(FUNC_REDONDEO100(FUNC_PRECIO_VENTA((
								(
									CASE
										WHEN PPP.VAL_MANEJO_INTER IS NOT NULL
											THEN PPP.VAL_MANEJO_INTER
										WHEN PP.VAL_MANEJO_INTER IS NOT NULL
											THEN PP.VAL_MANEJO_INTER
										ELSE PO.VAL_MANEJO_INTER
										END
									) + A.VAL_SHIPPING_XT + (
									(A.POR_TAX_XT / 100) * (
										CASE
											WHEN PPP.PRECIO_COMPRA IS NOT NULL
												THEN PPP.PRECIO_COMPRA
											WHEN PP.PRECIO_COMPRA IS NOT NULL
												THEN PP.PRECIO_COMPRA
											ELSE PO.PRECIO_COMPRA
											END
										)
									) + (
									(A.POR_GARANTIA / 100) * (
										CASE
											WHEN PPP.PRECIO_COMPRA IS NOT NULL
												THEN PPP.PRECIO_COMPRA
											WHEN PP.PRECIO_COMPRA IS NOT NULL
												THEN PP.PRECIO_COMPRA
											ELSE PO.PRECIO_COMPRA
											END
										)
									) + (
									CASE
										WHEN PPP.PRECIO_CONIVA IS NOT NULL
											THEN PPP.PRECIO_CONIVA
										WHEN PP.PRECIO_CONIVA IS NOT NULL
											THEN PP.PRECIO_CONIVA
										ELSE PO.PRECIO_CONIVA
										END
									)
								), A.MONEDA) + (
							CASE
								WHEN (PPP.PRECIO_CONIVA IS NOT NULL)
									THEN COALESCE(PPP.VALOR_TRANSPORTE, 0)
								WHEN (PP.PRECIO_CONIVA IS NOT NULL)
									THEN COALESCE(PP.VALOR_TRANSPORTE, 0)
								ELSE COALESCE(PO.VALOR_TRANSPORTE, 0)
								END
							)))  as precio ,
				hstore('flag_transporte_incluido', CASE
						WHEN (
								CASE
									WHEN (PPP.PRECIO_CONIVA IS NOT NULL)
										THEN COALESCE(PPP.VALOR_TRANSPORTE, 0)
									WHEN (PP.PRECIO_CONIVA IS NOT NULL)
										THEN COALESCE(PP.VALOR_TRANSPORTE, 0)
									ELSE COALESCE(PO.VALOR_TRANSPORTE, 0)
									END
								) > 0
							THEN '1'
						ELSE '0'
						END) AS flags
			FROM CU_PROVEEDORES P
			LEFT JOIN VIEW_CU_PROVEEDORES_ASOCIADOS_PP_PRODUCTOS PA
				ON (P.CODIGO_PROVEEDOR = PA.CODIGO_PROVEEDOR)
			INNER JOIN view_CU_PRODUCTOS_BASE A
				ON (A.CODIGO_PRODUCTO = PA.CODIGO_PRODUCTO)
			INNER JOIN CU_PRODUCTOS_PRECIOS PO
				ON (
						A.CODIGO_PRODUCTO = PO.CODIGO_PRODUCTO
						AND PO.ESTADO_REVISION = 'aprobado'
						AND PO.CODIGO_TIPO = PA.CODIGO_TIPO_PRECIO
						)
			LEFT JOIN CU_PRODUCTOS_PRECIOS PP
				ON (
						A.CODIGO_PRODUCTO = PP.CODIGO_PRODUCTO
						AND PP.ESTADO_REVISION = 'aprobado'
						AND PP.CODIGO_TIPO = PA.CODIGO_TIPO_PRECIO_PROMOCION
						AND CURRENT_TIMESTAMP BETWEEN PP.FECHAHORA_INICIAL
							AND PP.FECHAHORA_FINAL
						AND PP.CODIGO_PROVEEDOR_PP = 0
						)
			LEFT JOIN CU_PRODUCTOS_PRECIOS PPP
				ON (
						A.CODIGO_PRODUCTO = PPP.CODIGO_PRODUCTO
						AND PPP.ESTADO_REVISION = 'aprobado'
						AND CURRENT_TIMESTAMP BETWEEN PPP.FECHAHORA_INICIAL
							AND PPP.FECHAHORA_FINAL
						AND PPP.CODIGO_PROVEEDOR_PP = P.CODIGO_PROVEEDOR
						AND PPP.CODIGO_TIPO = 4
						)
			LEFT JOIN cu_productos_precios_menores ppm
				ON (
						a.codigo_producto = ppm.codigo_producto
						AND pa.codigo_proveedor = ppm.codigo_proveedor_pp
						AND ppm.codigo_tipo = 4
						)
			WHERE (
					(
						A.CODIGO_PROVEEDOR = P.CODIGO_PROVEEDOR
						AND A.FLAGS -> 'vis_ti' = '1'
						)
					OR P.CODIGO_PROVEEDOR <> PA.CODIGO_PROVEEDOR_ASOCIADO
					)
				AND CASE
					WHEN PA.NEGOCIOS <> '{}'
						THEN ARRAY(SELECT UNNEST(NEGOCIOS) FROM CU_CATEGORIASN WHERE CODIGO_CATEGORIA = A.CODIGO_CATEGORIA3
) && PA.NEGOCIOS
					ELSE 1 = 1
					END
				AND (
					PP.CODIGO_PRODUCTO IS NOT NULL
					OR PPP.CODIGO_PRODUCTO IS NOT NULL
					)
				AND ppm.codigo_producto IS NULL
				AND a.codigo_producto in ({$codigoProductos})
			GROUP BY a.codigo_producto,
				PA.codigo_proveedor,
				PPP.PRECIO_CONIVA,
				PPP.VALOR_TRANSPORTE,
				PP.PRECIO_CONIVA,
				PP.VALOR_TRANSPORTE,
				PO.VALOR_TRANSPORTE
	
                ) a
                group by a.codigo_producto, a.codigo_tipo,a.codigo_proveedor,a.flags";
        $ca->prepare($sql);
        $ca->exec();
    }

}