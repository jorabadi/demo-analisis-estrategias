<?php
require_once $cfg["rutaModeloLogico"] . "mod_anexos_proveedores.inc.php";
JLib::requireOnceModule("fileformats/excel-2.0/jexcel2.0.inc.php");

class AnexosFacturas {

    public static function loadPage($p) {
        $si = session::info();

        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        $campos = "a.codigo_anexo,a.factura_proveedor";
        $where = $ca->sqlFieldsFilters($campos, $p["filters"]["filtro"]);

        if (!empty($p["filters"]["desde"]) && !empty($p["filters"]["hasta"])) {
            $where .= " and (cast(a.fechahora_inicial as date),cast(a.fechahora_final as date)) overlaps ('{$p["filters"]["desde"]}'::date,'{$p["filters"]["hasta"]}'::date) ";
        } else {
            $where .= " and (cast(a.fechahora_inicial as date),cast(a.fechahora_final as date)) overlaps (current_date-400,current_date) ";
        }

        foreach ($p["column_filters"] as $colum => $valorCulumn) {
            if ($valorCulumn != "") {
                $where .= " and " . $ca->sqlFieldsFilters($colum, $valorCulumn);
            }
        }


        $sql = " select * from (  select a.codigo_proveedor,a.codigo_anexo,substr(a.fechahora::text,1,19) as fechahora,a.fechahora_inicial,a.fechahora_final,
		            (select func_number_format(sum((precio_compra_anexo*unidades)*((100::numeric(12,2)+por_iva)/100::numeric(12,2))),0)
		            from cu_pedidos_det where codigo_anexo_proveedor=a.codigo_anexo) as valor,
		            coalesce(a.factura_proveedor,null,'') as factura_proveedor,
		            coalesce(a.fecha_factura_proveedor,null,'') as fecha_factura_proveedor
		        from cu_proveedores_anexos_factura a
		        where a.codigo_proveedor=:codigo_proveedor and a.agente='coordiutil') a where 1=1  and ({$where}) ";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        return $ca->execPage($p);
    }

    public static function exportarExcel($p) {
        $informe = modAnexosProveedores::exportarAnexoProveedores($p);
        return $informe;
    }

    public static function exportarExcelPp($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];

        if ($p["codigo_anexo"] > 6703) {
            $factura = modAnexosProveedores::exportarExcelPp($p);
            return $factura;
            exit;
        } 
	else {
            $db = JDatabase::database();
            $ca = new JDbQuery($db);

            if (isset($p["codigo_anexo"])) {
                $codigoAnexo = $p["codigo_anexo"];

                $sql = "select a.*,b.nombre,b.nit
					from cu_proveedores_anexos_factura a
						join cu_proveedores b on (a.codigo_proveedor=b.codigo_proveedor)
					where a.codigo_anexo=:codigo_anexo and a.codigo_proveedor=:codigo_proveedor";

                $ca->prepare($sql);
                $ca->bindValue(":codigo_anexo", $codigoAnexo, false);
                $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
            } 
	    else {
                JJSonRpcServer::error("Error, no se encontro el código del anexo");
            }

            $ca->exec();
            $enc = $ca->fetch();

            $sql = "(select
				a.pin as orden_compra,
				a.fechahora_autorizacion as fecha,
				a.referencia,
				a.nombre,
				a.factura_proveedor as factura,
				a.unidades,
				a.precio_compra_anexo*a.unidades as costo,
				((a.precio_compra_anexo*a.unidades)*((100+a.por_iva)/100)-(a.precio_compra_anexo*a.unidades)) as iva,
				(a.precio_compra_anexo*a.unidades)*((100+a.por_iva)/100) as total
	
	        from view_cu_pedidos_det a
	        where a.codigo_anexo_proveedor=:codigo_anexo_proveedor
	        order by factura)
			union all
			(
			select
				'' as orden_compra,'2014-06-30 00:00:00' as fecha,
				'MD199E/A' as referencia,
				'Apple Tv' as nombre,
				'' as factura,
				34 as unidades,
				(125000*34) as costo,
				((125000*34)*((100.0+16.0)/100)-(125000*34)) as iva,
				(125000*34)*((100.0+16.0)/100) as total
			where 5625=:codigo_anexo_proveedor
			)";

            $ca->prepare($sql);
            $ca->bindValue(":codigo_anexo_proveedor", $codigoAnexo, false);
            $ca->exec();

            $det = $ca->fetchAll();
            //JJSonRpcServer::error($ca->preparedQuery());

            $path = JApp::privateTempPath() . "/proveedores_anexo_factura_{$codigoAnexo}.xls";

            $wb = new Spreadsheet_Excel_Writer_Workbook($path);
            $ws = & $wb->addWorksheet('Hoja1');

            $bold = & $wb->addFormat(array(//'Size' => 10,
                        //'Align' => 'center',
                        'bold' => 1));
            $titulo = & $wb->addFormat(array('Size' => 10,
                        'Align' => 'center',
                        'bold' => 1));

            $ws->writeString(0, 0, "NIT", $bold);
            $ws->writeString(0, 1, $enc["nit"]);
            $ws->writeString(1, 0, "PROVEEDOR", $bold);
            $ws->writeString(1, 1, utf8_decode($enc["nombre"]));
            $ws->writeString(2, 0, "ANEXO", $bold);
            $ws->writeString(2, 1, $enc["codigo_anexo"]);
            $ws->writeString(3, 0, "FECHA ANEXO", $bold);
            $ws->writeString(3, 1, $enc["fechahora"]);
            $ws->writeString(4, 0, "FECHA INICIAL", $bold);
            $ws->writeString(4, 1, $enc["fechahora_inicial"]);
            $ws->writeString(5, 0, "FECHA FINAL", $bold);
            $ws->writeString(5, 1, $enc["fechahora_final"]);
            $ws->writeString(6, 0, "FECHA REPORTE", $bold);
            $ws->writeString(6, 1, date('Y-m-d h:i:s'));


            $row = 8;
            foreach ($det as $r) {
                $col = 0;
                if ($row == 8) {
                    foreach ($r as $k => $v) {
                        $k = str_replace("_", " ", $k);
                        $k = ucfirst($k);

                        $ws->writeString($row, $col, $k, $titulo);
                        $col++;
                    }
                    $row += 1;
                    $col = 0;
                }

                foreach ($r as $k => $v) {

                    if ($k == "unidades" || $k == "costo" || $k == "iva" || $k == "total") {
                        $ws->writeNumber($row, $col, $v);
                        $col++;
                        continue;
                    }

                    $ws->writeString($row, $col, utf8_decode($v));
                    $col++;
                }

                $row++;
            }

            $wb->close();

            return basename($path);
        }
    }

    public static function exportarExcelPpDetallado($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);


        if ($p["codigo_anexo"] > 6703) {
            $factura = modAnexosProveedores::exportarExcelPpDetallado($p);
            return $factura;
            exit;
        }

        throw new JPublicException("Informe valido solo para anexos superiores 6703 ");
    }

    public static function loadPageTienda($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modAnexosProveedores::cargarAnexosPaginadoTienda($p);
    }

    public static function exportarExcelAnexoAliados($p) {
        $informeProveedorresAliados = modAnexosProveedores::exportarAnexoProveedoresAliados($p);
        return $informeProveedorresAliados;
    }

    public static function exportarExcelAnexoAliadosDetalle($p) {
        $si = session::info();
        $db = JDatabase::database();
        $ca = new JDbQuery($db);

        if (isset($p["codigo_anexo"])) {
            $codigoAnexo = $p["codigo_anexo"];

            $sql = "select a.*,b.nombre,b.nit 
					from cu_proveedores_anexos_factura a 
						join cu_proveedores b on (a.codigo_proveedor=b.codigo_proveedor)
					where a.codigo_anexo=:codigo_anexo and a.codigo_proveedor=:codigo_proveedor";

            $ca->prepare($sql);
            $ca->bindValue(":codigo_anexo", $codigoAnexo, false);
            $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        } else {
            JJSonRpcServer::error("Error, no se encontro el código del anexo");
        }

        $ca->exec();
        $enc = $ca->fetch();

        $sql = "select b.pin,a.codigo_inventario,b.codigo_pedido,
					substr(a.nombre,0,100) as nombre,
					round(case when c.codigo_proveedor=310 then 
						sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)/(a.descuento_proveedor/100)
					else a.precio_venta_coniva end,2) as pvp,
					a.por_iva,
					a.descuento_proveedor as por_descuento,
					case when sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)<0 then 0 else
					sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades) end as valor_margen,
					sum(a.unidades) as unidades,
					c.flag_compuesto
				from cu_pedidos_det a 
					join cu_pedidos_enc b on (a.codigo_pedido=b.codigo_pedido)
					join view_cu_productos_inventario_base c on (a.codigo_inventario=c.codigo_inventario and b.codigo_proveedor_pp<>c.codigo_proveedor)
				where b.codigo_anexo_aliado=:codigo_anexo_aliado and b.codigo_proveedor_pp=:codigo_proveedor
				group by b.pin,b.fechahora_pago,a.nombre,a.precio_venta_coniva,a.por_iva,a.descuento_proveedor,c.codigo_proveedor,
					c.flag_compuesto,a.codigo_inventario,b.codigo_pedido
				order by b.fechahora_pago";

        $ca->prepare($sql);
        $ca->bindValue(":codigo_anexo_aliado", $codigoAnexo, false);
        $ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
        $ca->exec();
        $det = $ca->fetchAll();

        $path = JApp::privateTempPath() . "/proveedores_anexo_factura_aliado_detalle_{$codigoAnexo}.xls";

        $wb = new Spreadsheet_Excel_Writer_Workbook($path);
        $ws = & $wb->addWorksheet('Hoja1');
        $bold = & $wb->addFormat(array(//'Size' => 10,
                    //'Align' => 'center',
                    'bold' => 1));
        $titulo = & $wb->addFormat(array('Size' => 10,
                    'Align' => 'center',
                    'bold' => 1));

        $ws->writeString(0, 0, "NIT", $bold);
        $ws->writeString(0, 1, $enc["nit"]);
        $ws->writeString(1, 0, "PROVEEDOR", $bold);
        $ws->writeString(1, 1, utf8_decode($enc["nombre"]));
        $ws->writeString(2, 0, "ANEXO", $bold);
        $ws->writeString(2, 1, $enc["codigo_anexo"]);
        $ws->writeString(3, 0, "FECHA ANEXO", $bold);
        $ws->writeString(3, 1, $enc["fechahora"]);
        $ws->writeString(4, 0, "FECHA INICIAL", $bold);
        $ws->writeString(4, 1, $enc["fechahora_inicial"]);
        $ws->writeString(5, 0, "FECHA FINAL", $bold);
        $ws->writeString(5, 1, $enc["fechahora_final"]);
        $ws->writeString(6, 0, "FECHA REPORTE", $bold);
        $ws->writeString(6, 1, date('Y-m-d h:i:s'));


        $row = 8;
        foreach ($det as $r) {
            $col = 0;
            if ($row == 8) {
                foreach ($r as $k => $v) {
                    if (in_array($k, array("flag_compuesto", "codigo_inventario", "codigo_pedido"))) {
                        continue;
                    }
                    $k = str_replace("_", " ", $k);
                    $k = ucfirst($k);

                    $ws->writeString($row, $col, $k, $titulo);
                    $col++;
                }
                $row += 1;
                $col = 0;
            }

            foreach ($r as $k => $v) {
                if (in_array($k, array("flag_compuesto", "codigo_inventario", "codigo_pedido"))) {
                    continue;
                }

                if (in_array($k, array("pvp", "por_iva", "por_descuento", "valor_margen", "unidades"))) {
                    $ws->writeNumber($row, $col, $v);
                    $col++;
                    continue;
                }

                $ws->writeString($row, $col, utf8_decode($v));
                $col++;
            }

            if ($r["flag_compuesto"] == "1") {
                $sql = "select '' as pin,
						substr(nombre,0,100) as nombre,
						round(precio_coniva,2) as precio_coniva,
						por_iva,
						'' as por_descuento,
						'' as valor_margen,
						unidades
					from cu_pedidos_det_compuestos 
					where codigo_inventario=:codigo_inventario and codigo_pedido=:codigo_pedido";
                $ca->prepare($sql);
                $ca->bindValue(":codigo_pedido", $r["codigo_pedido"], false);
                $ca->bindValue(":codigo_inventario", $r["codigo_inventario"], false);
                $ca->exec();
                $rCompuesto = $ca->fetchAll();

                foreach ($rCompuesto as $compuesto) {
                    $col = 0;
                    $row++;
                    foreach ($compuesto as $k => $v) {
                        if (in_array($k, array("precio_coniva", "por_iva", "unidades"))) {
                            $ws->writeNumber($row, $col, $v);
                            $col++;
                            continue;
                        }

                        $ws->writeString($row, $col, utf8_decode($v));
                        $col++;
                    }
                }
            }

            $row++;
        }

        $wb->close();

        return basename($path);
    }

    public static function buscarPinAnexo($p) {
        $si = session::info();
        $p["codigo_proveedor"] = $si["codigo_proveedor"];
        return modAnexosProveedores::buscarPinAnexo($p);
    }

}