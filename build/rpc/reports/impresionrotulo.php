<?php
date_default_timezone_set('America/Bogota');
require_once '../_mod.inc.php';
require_once $cfg["rutaModeloLogico"]."mod_despachos.inc.php";
JLib::requireOnceModule("fileformats/fpdf/jfpdf.inc.php");

if (!empty($_GET['codigo_pedido'])) {
    $p["codigo_pedido"] = $_GET["codigo_pedido"];
    $p["codigo_proveedor"]=$_GET["codigo_proveedor"];
    $p["ms"] = $_GET["ms"];
    $p["mi"] = $_GET["mi"];
}
else {
    die("Falta código pedido");
}

if ($p['codigo_proveedor']==547){
	$pdf = modDespachos::reimprimirRotulo10x10($p);
}
else{
	$pdf = modDespachos::reimprimirRotulo($p);
}
header("Content-Type: application/pdf");
header("Cache-Control: no-cache");
header("Accept-Ranges: none");
header("Content-Disposition: inline; filename=\"rotulos_{$p["codigo_pedido"]}.pdf\"");

echo $pdf;
exit ;