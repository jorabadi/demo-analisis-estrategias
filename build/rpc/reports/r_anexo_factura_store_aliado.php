<?php
date_default_timezone_set('America/Bogota');
require_once '../_mod.inc.php';
require_once $cfg["rutaModeloLogico"]."mod_anexos_proveedores.inc.php";
JLib::requireOnceModule("fileformats/fpdf/jfpdf.inc.php");

if (!isset($_SESSION["info"])) {
	die("Sesión invalida");
}
$si = $_SESSION["info"];

$ca = new JDbQuery($db);


if ( isset($_GET["codigo_anexo"]) && isset($_GET["codigo_proveedor"]) ) {
    $p["codigo_anexo"] = $_GET["codigo_anexo"];
    $p["codigo_proveedor"]=$_GET["codigo_proveedor"];
    
	if ($p["codigo_anexo"]>6703) {
		$factura = modAnexosProveedores::generarAnexoFacturaAliados($p);
		echo $factura;
		exit;
	}
}

if ( isset($_GET["codigo_anexo"]) ) {
    $codigoAnexo = $_GET["codigo_anexo"];

    $sql = "select a.*,b.nombre,b.nit 
    from cu_proveedores_anexos_factura a 
    join cu_proveedores b on (a.codigo_proveedor=b.codigo_proveedor)
    where a.codigo_anexo=:codigo_anexo";

    $ca->prepare($sql);
    $ca->bindValue(":codigo_anexo", $codigoAnexo, false);
}else{
    die('No se encontro el código del anexo');
}

$ca->exec();

$enc = $ca->fetch();

$sql = "select 
            b.pin,
            b.fechahora_pago,
            func_number_format(b.total_coniva,0) as valor_pin,
            func_number_format(sum(a.precio_compra_anexo),0) as precio_coordiutil,
            func_number_format(case when sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades) < 0 then 0 else sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades) end,0) as margen_coordiutil,
            func_number_format(b.total_coniva*(d.por_redrecaudo/100),0) as red_recaudo,
            func_number_format(
				case when (sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)-b.total_coniva*(d.por_redrecaudo/100))*(d.por_margen_aliado/100) < 0 then 0 else
				(sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)-b.total_coniva*(d.por_redrecaudo/100))*(d.por_margen_aliado/100) end
			,0) as total
        from cu_pedidos_det a 
            join cu_pedidos_enc b on (a.codigo_pedido=b.codigo_pedido)
            join view_cu_productos_inventario_base c on (a.codigo_inventario=c.codigo_inventario and b.codigo_proveedor_pp<>c.codigo_proveedor)
			join cu_proveedores_anexos_factura d on (b.codigo_anexo_aliado=d.codigo_anexo)
        where b.codigo_anexo_aliado=:codigo_anexo_proveedor and b.codigo_proveedor_pp=:codigo_proveedor
        group by b.pin,b.fechahora_pago,b.total_coniva,b.codigo_anexo_aliado,d.por_redrecaudo,d.por_margen_aliado
        order by b.fechahora_pago";

$ca->prepare($sql);
$ca->bindValue(":codigo_anexo_proveedor", $codigoAnexo, false);
$ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
$ca->exec();

$det = $ca->fetchAll();



//216,280    array(300,140)
$pdf = new JFpdf('L','mm','Letter');
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5,10);
$pdf->AddPage();


$pdf->Cell(266,6,"ANEXO ALIADO",0,0,'C');
$pdf->Ln();

$pdf->SetFont('Arial','B',10);

$pdf->Cell(30,6,"NIT: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["nit"]);
$pdf->Cell(160,6,"");
$pdf->SetFont('Arial','',7);
$pdf->Cell(30,6,utf8_decode("FECHA IMPRESIÓN: ".date('Y-m-d h:i:s')));
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"ALIADO: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,utf8_decode($enc["nombre"]));
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"ANEXO: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["codigo_anexo"]);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"FECHA ANEXO: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["fechahora"]);
$pdf->Ln();

$pdf->SetFont('Arial','B',10);
$pdf->Cell(35,6,"PERIODO GENERADO ");
$pdf->Ln();
$pdf->SetFont('Arial','B',8);
$pdf->Cell(35,4,"FECHA INICIAL: ");$pdf->SetFont('Arial','',8);$pdf->Cell(20,4,$enc["fechahora_inicial"]);
$pdf->Ln();
$pdf->SetFont('Arial','B',8);
$pdf->Cell(35,4,"FECHA FINAL: ");$pdf->SetFont('Arial','',8);$pdf->Cell(20,4,$enc["fechahora_final"]);
$pdf->Ln();

$pdf->Cell(2,6,"");
$pdf->Ln();




//Orden compra	Fecha	Referencia	Nombre	Unidades	Total recaudo	% Servicio	Total Servicio



$pdf->SetFont('Arial','B',10);
$pdf->Cell(27,6,"PIN",1,0,'C');
$pdf->Cell(30,6,"Fecha pago",1,0,'C');
$pdf->Cell(30,6,"Total PIN",1,0,'C');
$pdf->Cell(85,6,"PVCU sin iva",1,0,'C');
$pdf->Cell(41,6,"Total margen CU",1,0,'C');
$pdf->Cell(28,6,"Red recaudo",1,0,'C');
$pdf->Cell(23,6,"Total",1,0,'C');
$pdf->SetFont('Arial','',10);
$pdf->Ln();


$redRecaudo = 0;
$margenCu = 0;
$total = 0;


foreach( $det as $r ) {
    $pdf->Cell(27,6,$r["pin"],'L',0,'C');
    $pdf->SetFont('Arial','',8);
    $pdf->Cell(30,6,$r["fechahora_pago"],'L');
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(30,6,$r["valor_pin"],'L',0,'R');
    $pdf->Cell(85,6,$r["precio_coordiutil"],'L',0,'R');
    $pdf->Cell(41,6,$r["margen_coordiutil"],'L',0,'R');
    $pdf->Cell(28,6,$r["red_recaudo"],'L',0,'R');
    $pdf->Cell(23,6,$r["total"],'LR',0,'R');

    $redRecaudo += str_replace(",","",$r["red_recaudo"]);
    $margenCu += str_replace(",","",$r["margen_coordiutil"]);
    $total += str_replace(",","",$r["total"]);
	
    $pdf->Ln();
}

$pdf->Cell(264,1,"",1);


$pdf->Ln();
$pdf->Ln();

$pdf->Cell(142,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"Totales",'',0,'C');
$pdf->SetFont('Arial','',10);
$pdf->Cell(41,6,number_format($margenCu),'',0,'R');
$pdf->Cell(28,6,number_format($redRecaudo),'',0,'R');
$pdf->Cell(23,6,number_format($total),'',0,'R');



$pdf->Cell(260,4,"");
$pdf->Ln();
$pdf->Ln();



//Detalle de productos
        
$sql = "select b.pin,a.codigo_inventario,b.codigo_pedido,
		substr(a.nombre,0,100) as nombre,
		func_number_format(case when c.codigo_proveedor=310 then 
	    	(sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)/(a.descuento_proveedor/100))/sum(a.unidades)
	        else a.precio_venta_coniva end
	    ,0) as pvp,
		a.por_iva,
		a.descuento_proveedor as por_descuento,
		func_number_format(
			case when sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades)<0 then 0 else
			sum((a.precio_venta_siniva-a.precio_compra_anexo)*a.unidades) end
		,0) as valor_margen,
		sum(a.unidades) as unidades,
		c.flag_compuesto
    from cu_pedidos_det a 
        join cu_pedidos_enc b on (a.codigo_pedido=b.codigo_pedido)
        join view_cu_productos_inventario_base c on (a.codigo_inventario=c.codigo_inventario and b.codigo_proveedor_pp<>c.codigo_proveedor)
    where b.codigo_anexo_aliado=:codigo_anexo_aliado and b.codigo_proveedor_pp=:codigo_proveedor
    group by b.pin,b.fechahora_pago,a.nombre,a.precio_venta_coniva,a.por_iva,a.descuento_proveedor,c.codigo_proveedor,
		c.flag_compuesto,a.codigo_inventario,b.codigo_pedido
    order by b.fechahora_pago";

$ca->prepare($sql);
$ca->bindValue(":codigo_anexo_aliado", $codigoAnexo, false);
$ca->bindValue(":codigo_proveedor", $si["codigo_proveedor"], false);
$ca->exec();

$rProductos = $ca->fetchAll();


$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('Arial','B',10);

$pdf->Cell(264,5,utf8_decode("DETALLE DE PRODUCTOS POR PIN"),'',0,'C');
$pdf->Ln();

$pdf->SetFont('Arial','B',10);
$pdf->Cell(27,5,"PIN",1,0,'C');
$pdf->Cell(90,5,"Producto",1,0,'C');
$pdf->Cell(25,5,"Unidades",1,0,'C');
$pdf->Cell(35,5,"PVP",1,0,'C');
$pdf->Cell(20,5,"% IVA",1,0,'C');
$pdf->Cell(30,5,"% margen CU",1,0,'C');
$pdf->Cell(37,5,"Total margen CU",1,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Ln();
foreach ($rProductos as $pr ) {
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(27,4,$pr["pin"],'L',0,'C');
    $pdf->Cell(90,4,substr(utf8_decode($pr["nombre"]),0,60),'L');
    $pdf->Cell(25,4,$pr["unidades"],'L');
    $pdf->Cell(35,4,$pr["pvp"],'L',0,'R');
    $pdf->Cell(20,4,$pr["por_iva"],'L',0,'R');
    $pdf->Cell(30,4,$pr["por_descuento"],'L',0,'R');
    $pdf->Cell(37,4,$pr["valor_margen"],'LR',0,'R');
    $pdf->Ln();
	
	if ( $pr["flag_compuesto"] == "1" ) {
		$sql = "select 
				substr(nombre,0,100) as nombre,
				sum(unidades) as unidades,
				por_iva,
				func_number_format(precio_coniva,0) as precio_coniva
			from cu_pedidos_det_compuestos 
			where codigo_inventario=:codigo_inventario and codigo_pedido=:codigo_pedido
			group by nombre,por_iva,precio_coniva
			";
		$ca->prepare($sql);
		$ca->bindValue(":codigo_pedido",$pr["codigo_pedido"], false);
		$ca->bindValue(":codigo_inventario", $pr["codigo_inventario"], false);
		$ca->exec();
		$rCompuesto = $ca->fetchAll();
		
		foreach ( $rCompuesto as $compuesto ) {
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(27,4,"",'L',0,'C');
			$pdf->Cell(90,4,substr(utf8_decode($compuesto["nombre"]),0,60),'L');
			$pdf->Cell(25,4,$compuesto["unidades"],'L');
			$pdf->Cell(35,4,$compuesto["precio_coniva"],'L',0,'R');
			$pdf->Cell(20,4,$compuesto["por_iva"],'L',0,'R');
			$pdf->Cell(30,4,"",'L',0,'R');
			$pdf->Cell(37,4,"",'LR',0,'R');
			$pdf->Ln();
		}
	}
}
$pdf->Cell(264,1,"",1);



/*
//Iva y retenciones
$pdf->Cell(223,4,"");
$pdf->Ln();
$pdf->Ln();


$servicio = 0;
$iva = 0;
$rtefte = 0;
$rteica = 0;
$rteiva = 0;
$subtotal = 0;

$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("V/r prestación Servicio"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6,number_format($servicio),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Iva"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6,number_format($iva),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Rte Fte"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6, "- ".number_format($rtefte),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Rte Iva"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6, "- ".number_format($rteiva),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Rte Ica"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6, "- ".number_format($rteica),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Subtotal servicio"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6,number_format($subtotal),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Total a pagar"));
$pdf->SetFont('Arial','',10);
$pagar = $subtotal;
$pdf->Cell(25,6,  number_format($pagar),'',0,'R');
$pdf->Ln();*/

$pdf->Output("store_anexo_factura_{$codigoAnexo}.pdf","I");

?>