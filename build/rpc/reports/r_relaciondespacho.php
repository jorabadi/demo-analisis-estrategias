<?php
date_default_timezone_set('America/Bogota');
require_once '../_mod.inc.php';
require_once $cfg["rutaModeloLogico"]."mod_despachos.inc.php";

if(!empty($_GET["codigo_pedido"])){
    $p["codigo_proveedor"] = $_GET["codigo_proveedor"];
    $p["codigo_pedido"] = $_GET["codigo_pedido"];
    
    $pdf = modDespachos::reimprimirRelacion($p);
	
    header("Content-Type: application/pdf");
    header("Cache-Control: no-cache");
    header("Accept-Ranges: none");
    header("Content-Disposition: inline; filename=\"relacion_{$p["codigo_pedido"]}.pdf\"");

    echo $pdf;
}
else {
    die("Falta código pedido");
}
exit;