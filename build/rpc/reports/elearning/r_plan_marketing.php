<?php
date_default_timezone_set('America/Bogota');

$codigoTienda = is_numeric($_GET['codigo_tienda']) ? $_GET['codigo_tienda'] : '';//JInputValidator::filterInputGet("codigo_tienda", true, FILTER_VALIDATE_INT);

if(!empty($codigoTienda)){
    
    $url = "https://www.coordiutil.com/vfventas/rpc/reports/r_plan_marketing.php?codigo_tienda={$codigoTienda}";
    $contenido = file_get_contents($url);
    
    header("Content-Type: application/pdf");
    header('Content-Length: ' . strlen($contenido));
    header("Cache-Control: no-cache");
    header("Accept-Ranges: none");
    header("Content-Disposition: inline; filename=\"certificado_componente_1_{$codigoTienda}.pdf\"");
    
    echo $contenido;
}
else {
    die("Falta código de la tienda");
}
exit;