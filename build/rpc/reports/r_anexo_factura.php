<?php
date_default_timezone_set('America/Bogota');
require_once '../_mod.inc.php';
require_once $cfg["rutaModeloLogico"]."mod_anexos_proveedores.inc.php";
JLib::requireOnceModule("fileformats/fpdf/jfpdf.inc.php");

$ca = new JDbQuery($db);


if ( isset($_GET["codigo_anexo"]) && isset($_GET["codigo_proveedor"]) ) {
    $p["codigo_anexo"] = $_GET["codigo_anexo"];
    $p["codigo_proveedor"]=$_GET["codigo_proveedor"];
    
	if ($p["codigo_anexo"]>6703) {
		$factura = modAnexosProveedores::generarAnexoFactura($p);
		echo $factura;
		exit;
	}
}



if ( isset($_GET["codigo_anexo"]) ) {
    $codigoAnexo = $_GET["codigo_anexo"];

    $sql = "select a.*,b.nombre,b.nit from cu_proveedores_anexos_factura a join cu_proveedores b on (a.codigo_proveedor=b.codigo_proveedor)
    where a.codigo_anexo=:codigo_anexo";

    $ca->prepare($sql);
    $ca->bindValue(":codigo_anexo", $codigoAnexo);
}else{
    die('No se encontro el código del anexo');
}

$ca->exec();

$enc = $ca->fetch();

$sql = "
(select
	a.pin as orden_compra,
	a.fechahora_autorizacion as fecha,
	a.referencia,
	a.nombre,
	a.unidades,
	func_number_format(a.precio_compra_anexo*a.unidades,0) as costo,
	func_number_format(((a.precio_compra_anexo*a.unidades)*((100+a.por_iva)/100)-(a.precio_compra_anexo*a.unidades)),0) as iva,
	func_number_format((a.precio_compra_anexo*a.unidades)*((100+a.por_iva)/100),0) as total,
	case when a.precio_compra_anexo<>a.precio_compra then 'SI' else 'NO' end as promocion

from view_cu_pedidos_det a
where a.codigo_anexo_proveedor=:codigo_anexo_proveedor
order by a.factura)
union all
(
select 
	'' as orden_compra,'2014-06-30 00:00:00' as fecha,
	'MD199E/A' as referencia,
	'Apple Tv' as nombre,
	34 as unidades,
	func_number_format(125000*34,0) as costo,
	func_number_format(((125000*34)*((100.0+16.0)/100)-(125000*34)),0) as iva,
	func_number_format((125000*34)*((100.0+16.0)/100),0) as total,
	'NO' as promocion
where 5625=:codigo_anexo_proveedor
)";

$ca->prepare($sql);
$ca->bindValue(":codigo_anexo_proveedor", $codigoAnexo);
$ca->exec();

$det = $ca->fetchAll();



//216,280    array(300,140)
$pdf = new JFpdf('L','mm','Letter');
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5,10);
$pdf->AddPage();


//$pdf->Cell(190,6,"RELACION DE DESPACHO COORDIUTIL.COM",0,0,'C');
//$pdf->Ln();

$pdf->SetFont('Arial','B',10);

$pdf->Cell(30,6,"NIT: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["nit"]);
$pdf->Cell(160,6,"");
$pdf->SetFont('Arial','',7);
$pdf->Cell(30,6,utf8_decode("FECHA IMPRESIÓN: ".date('Y-m-d h:i:s')));
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"PROVEEDOR: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,utf8_decode($enc["nombre"]));
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"ANEXO: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["codigo_anexo"]);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"FECHA ANEXO: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["fechahora"]);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"FECHA INICIAL: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["fechahora_inicial"]);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"FECHA FINAL: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["fechahora_final"]);

$pdf->Ln();

//Orden compra	Fecha	Referencia	Nombre	Unidades	Costo para coordiutil	Valor iva	Total a cobrar



$pdf->SetFont('Arial','B',10);
$pdf->Cell(27,6,"Orden compra",1,0,'C');
$pdf->Cell(30,6,"Fecha",1,0,'C');
$pdf->Cell(30,6,"Referencia",1,0,'C');
$pdf->Cell(90,6,"Nombre",1,0,'C');
$pdf->Cell(18,6,"Unidades",1,0,'C');
$pdf->Cell(23,6,"Costo",1,0,'C');
$pdf->Cell(23,6,"Iva",1,0,'C');
$pdf->Cell(23,6,"Total",1,0,'C');
$pdf->SetFont('Arial','',10);
$pdf->Ln();



$unidades = 0;
$costo = 0;
$iva = 0;
$total = 0;

foreach( $det as $r ) {
    $pdf->Cell(27,6,$r["orden_compra"],'L');
    $pdf->SetFont('Arial','',8);
    $pdf->Cell(30,6,$r["fecha"],'L');
    //$pdf->SetFont('Arial','',9);
    $pdf->Cell(30,6,$r["referencia"],'L');
    $pdf->Cell(90,6,utf8_decode($r["nombre"]),'L');
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(18,6,$r["unidades"],'L',0,'R');
    $pdf->Cell(23,6,(($r["promocion"]=="SI"?"***":"").$r["costo"]),'L',0,'R');
    $pdf->Cell(23,6,$r["iva"],'L',0,'R');
    $pdf->Cell(23,6,$r["total"],'LR',0,'R');

    $unidades += str_replace(",","",$r["unidades"]);
    $costo += str_replace(",","",$r["costo"]);
    $iva += str_replace(",","",$r["iva"]);
    $total += str_replace(",","",$r["total"]);

    $pdf->Ln();
}

$pdf->Cell(264,1,"",1);



$pdf->Ln();
$pdf->Ln();

$pdf->Cell(147,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"Totales",'',0,'C');
$pdf->SetFont('Arial','',10);
$pdf->Cell(18,6,number_format($unidades),'',0,'R');
$pdf->Cell(23,6,number_format($costo),'',0,'R');
$pdf->Cell(23,6,number_format($iva),'',0,'R');
$pdf->Cell(23,6,number_format($total),'',0,'R');


$pdf->Ln();
$pdf->Ln();

$pdf->Output("proveedores_anexo_factura_{$codigoAnexo}.pdf","I");

?>