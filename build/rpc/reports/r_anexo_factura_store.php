<?php
date_default_timezone_set('America/Bogota');
require_once '../_mod.inc.php';
require_once $cfg["rutaModeloLogico"]."mod_anexos_proveedores.inc.php";
JLib::requireOnceModule("fileformats/fpdf/jfpdf.inc.php");

if ( isset($_GET["codigo_anexo"]) && isset($_GET["codigo_proveedor"]) ) {
    $p["codigo_anexo"] = $_GET["codigo_anexo"];
    $p["codigo_proveedor"]=$_GET["codigo_proveedor"];
    $p["resumen"] = isset($_GET["resumen"]) ? $_GET["resumen"] : '0';
    
    if ($p["codigo_anexo"] > 6703) {
        echo modAnexosProveedores::generarAnexoFacturaPp($p);
        exit;
    }
}

$ca = new JDbQuery($db);
if ( isset($_GET["codigo_anexo"]) ) {
    $codigoAnexo = $_GET["codigo_anexo"];

    $sql = "select a.*,b.nombre,b.nit 
    from cu_proveedores_anexos_factura a 
    join cu_proveedores b on (a.codigo_proveedor=b.codigo_proveedor)
    where a.codigo_anexo=:codigo_anexo";

    $ca->prepare($sql);
    $ca->bindValue(":codigo_anexo", $codigoAnexo, false);
}else{
    die('No se encontro el código del anexo');
}

$ca->exec();

$enc = $ca->fetch();

$sql = "select
a.pin as orden_compra,
a.fechahora_autorizacion as fecha,
substr(a.referencia,0,20) as referencia,
substr(a.nombre,0,50) as nombre,
a.unidades,
a.factura_proveedor,
func_number_format(a.precio_venta_coniva*a.unidades,0) as total_recaudo,
a.descuento_proveedor as por_servicio,
func_number_format(a.precio_venta_coniva*a.unidades*a.descuento_proveedor/100,0) as total_servicio,
func_number_format((a.precio_venta_coniva*a.unidades*a.descuento_proveedor/100)*(C.POR_IVA/100),0) as iva_servicio,
func_number_format((a.precio_venta_coniva*a.unidades*a.descuento_proveedor/100)*(C.POR_RETEFUENTE/100),0) as rte_fte,
func_number_format(((a.precio_venta_coniva*a.unidades*a.descuento_proveedor/100)*(C.POR_IVA/100))*(C.POR_RETEIVA/100),0) as rte_iva,
func_number_format((a.precio_venta_coniva*a.unidades*a.descuento_proveedor/100)*(C.POR_RETEICA/100),0) as rte_ica,
case when c.fechahora::date<'2013-09-01' then func_number_format((a.precio_venta_coniva*a.unidades*a.descuento_proveedor/100)*(C.POR_RETECREE/100),0) else '0' end as rte_cree
from view_cu_pedidos_det a
join cu_proveedores_anexos_factura c on (a.codigo_anexo_proveedor=c.codigo_anexo)
where a.codigo_anexo_proveedor=:codigo_anexo_proveedor
order by a.factura";

$ca->prepare($sql);
$ca->bindValue(":codigo_anexo_proveedor", $codigoAnexo, false);
$ca->exec();

$det = $ca->fetchAll();


$rBono = array("total_bono"=>0);
$sql = "
select sum(valor) as total_bono
from cu_pedidos_bonos 
where codigo_pedido in (
select b.codigo_pedido
from cu_proveedores_anexos_factura a 
	join cu_pedidos_det b on (b.codigo_anexo_proveedor=a.codigo_anexo)
where a.codigo_anexo=:codigo_anexo_proveedor
)
";
$ca->prepare($sql);
$ca->bindValue(":codigo_anexo_proveedor", $codigoAnexo, false);
$ca->exec();

if ( $ca->size() > 0 ) {
	$rBono = $ca->fetch();
}


//216,280    array(300,140)
$pdf = new JFpdf('L','mm','Letter');
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5,10);
$pdf->AddPage();


$pdf->Cell(266,6,"ANEXO PORTAL PERSONALIZADO",0,0,'C');
$pdf->Ln();

$pdf->SetFont('Arial','B',10);

$pdf->Cell(30,6,"NIT: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["nit"]);
$pdf->Cell(160,6,"");
$pdf->SetFont('Arial','',7);
$pdf->Cell(30,6,utf8_decode("FECHA IMPRESIÓN: ".date('Y-m-d h:i:s')));
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"PROVEEDOR: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,utf8_decode($enc["nombre"]));
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"ANEXO: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["codigo_anexo"]);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"FECHA ANEXO: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["fechahora"]);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"FECHA INICIAL: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["fechahora_inicial"]);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"FECHA FINAL: ");$pdf->SetFont('Arial','',10);$pdf->Cell(20,6,$enc["fechahora_final"]);

$pdf->Ln();

//Orden compra	Fecha	Referencia	Nombre	Unidades	Total recaudo	% Servicio	Total Servicio



$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,"O. compra",1,0,'C');
$pdf->Cell(30,6,"Fecha",1,0,'C');
$pdf->Cell(30,6,"Referencia",1,0,'C');
$pdf->Cell(75,6,"Nombre",1,0,'C');
$pdf->Cell(22,6,"Factura",1,0,'C');
$pdf->Cell(13,6,"Unids",1,0,'C');
$pdf->Cell(28,6,"V/r Recaudado",1,0,'C');
$pdf->Cell(23,6,"% Servicio",1,0,'C');
$pdf->Cell(23,6,"V/r Servicio",1,0,'C');
$pdf->SetFont('Arial','',10);
$pdf->Ln();



$unidades = 0;
$recaudado = 0;
$servicio = 0;
$iva = 0;
$rteica = 0;
$rteiva = 0;
$rtefte = 0;
$rtecree = 0;


foreach( $det as $r ) {
    $pdf->Cell(20,6,$r["orden_compra"],'L');
    $pdf->SetFont('Arial','',8);
    $pdf->Cell(30,6,$r["fecha"],'L');
    $pdf->Cell(30,6,utf8_decode($r["referencia"]),'L');
    $pdf->Cell(75,6,utf8_decode($r["nombre"]),'L');
    $pdf->Cell(22,6,utf8_decode($r["factura_proveedor"]),'L');
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(13,6,$r["unidades"],'L',0,'R');
    $pdf->Cell(28,6,$r["total_recaudo"],'L',0,'R');
    $pdf->Cell(23,6,$r["por_servicio"],'L',0,'R');
    $pdf->Cell(23,6,$r["total_servicio"],'LR',0,'R');

    $unidades += str_replace(",","",$r["unidades"]);
    $recaudado += str_replace(",","",$r["total_recaudo"]);
    $servicio += str_replace(",","",$r["total_servicio"]);

	$iva += str_replace(",","",$r["iva_servicio"]);
	$rtefte += str_replace(",","",$r["rte_fte"]);
	$rteiva += str_replace(",","",$r["rte_iva"]);
	$rteica += str_replace(",","",$r["rte_ica"]);
    $rtecree += str_replace(",","",$r["rte_cree"]);
	
    $pdf->Ln();
}

$pdf->Cell(264,1,"",1);



$pdf->Ln();
$pdf->Ln();

$pdf->Cell(147,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,"Totales",'',0,'C');
$pdf->SetFont('Arial','',10);
$pdf->Cell(13,6,number_format($unidades),'',0,'R');
$pdf->Cell(28,6,number_format($recaudado),'',0,'R');
$pdf->Cell(23,6,"",'',0,'R');
$pdf->Cell(23,6,number_format($servicio),'',0,'R');


//Iva y retenciones
$pdf->Cell(223,4,"");
$pdf->Ln();
$pdf->Ln();


$anoDocumento = substr($r["fecha"], 0, 4);
if ( $anoDocumento == "2012" ) {
    $enc["val_base_retefuente"] = 101400;
    $enc["val_base_reteica"] = 101400;
    $enc["val_base_reteiva"] = 101400;
    $enc["val_base_retecree"] = 0;
}
if ( $anoDocumento == "2013" ) {
    $enc["val_base_retefuente"] = 107000;
    $enc["val_base_reteica"] = 107000;
    $enc["val_base_reteiva"] = 107000;
    $enc["val_base_retecree"] = 0;
}

if ( $anoDocumento == "2014" ) {
    $enc["val_base_retefuente"] = 110000;
    $enc["val_base_reteica"] = 110000;
    $enc["val_base_reteiva"] = 110000;
    $enc["val_base_retecree"] = 0;
}
if ( $anoDocumento == "2015" ) {
    $enc["val_base_retefuente"] = 113000;
    $enc["val_base_reteica"] = 113000;
    $enc["val_base_reteiva"] = 113000;
    $enc["val_base_retecree"] = 0;
}


$rtefte = ($servicio>=$enc["val_base_retefuente"]?$rtefte:0);
$rtecree = ($servicio>=$enc["val_base_retecree"]?$rtecree:0);
$rteica = ($servicio>=$enc["val_base_reteica"]?$rteica:0);
$rteiva = ($servicio>=$enc["val_base_reteiva"]?$rteiva:0);
$subtotal = $servicio+$iva-$rtefte-$rteica-$rteiva-$rtecree;

$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("V/r prestación Servicio"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6,number_format($servicio),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Iva"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6,number_format($iva),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Rte Fte"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6, "- ".number_format($rtefte),'',0,'R');
$pdf->Ln();
if ( date($enc["fechahora"]) < date('2013-09-01') ) {
    $pdf->Cell(199,6,"");
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(40,6,  utf8_decode("Rte Cree"));
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(25,6, "- ".number_format($rtecree),'',0,'R');
    $pdf->Ln();
}
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Rte Iva"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6, "- ".number_format($rteiva),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Rte Ica"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6, "- ".number_format($rteica),'',0,'R');
$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Subtotal servicio"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6,number_format($subtotal),'',0,'R');

$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Total bonos"));
$pdf->SetFont('Arial','',10);
$pdf->Cell(25,6,number_format($rBono["total_bono"]),'',0,'R');

$pdf->Ln();
$pdf->Cell(199,6,"");
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,  utf8_decode("Total a pagar"));
$pdf->SetFont('Arial','',10);
$pagar = $recaudado-$subtotal-$rBono["total_bono"];
$pdf->Cell(25,6,  number_format($pagar),'',0,'R');
$pdf->Ln();

$pdf->Output("store_anexo_factura_{$codigoAnexo}.pdf","I");

?>