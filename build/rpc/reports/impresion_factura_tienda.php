<?php
date_default_timezone_set('America/Bogota');
require_once '../_mod.inc.php';
require_once $cfg["rutaModeloLogico"]."mod_factura_tienda_vf.inc.php";
JLib::requireOnceModule("utils/jutils.inc.php");

if ( isset($_REQUEST["pin"]) && isset($_REQUEST["proveedor"])) {
    $p["pin"] = "'".implode("','",explode(",",$_REQUEST["pin"]))."'";
    $p["proveedor"] = $_REQUEST["proveedor"];
    
    header("Content-type: application/pdf");
    header('Content-Disposition: attachment; filename="factura_tienda_vf.pdf"');
    echo modFacturaTiendaVf::generar($p);
}
else{
    echo 'Por favor especifique el Pin y el Proveedor';
}

exit;