<?php

require_once __DIR__ . '/_mod.inc.php';

header('Access-Control-Allow-Origin: * ');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');

ini_set("soap.wsdl_cache_enabled", "0");
JLib::requireOnceModule("rpc/jrpcserver.inc.php");
JLib::requireOnceModule('utils/jinputvalidator.inc.php');

require_once $cfg["rutaModeloLogico"].'mod_resources.inc.php';


$server = new JRpcServer();

foreach(glob("modules/*") as $moduleFile){
	require_once __DIR__."/{$moduleFile}";
}

$classes=array(
	"Application",
	"Session",
	"Marcas",
	"Productos",
	"Despachos",
	"Proveedor",
	"Pedidos",
    "Promociones",
    "Usuarios",
    "Garantias",
    "GruposCiudades",
	"AnexosFacturas",
	"Clientes",
	"Mensajes",
	"CategoriasPp",
	"Aliados",
	"Reclamaciones",
	"Importaciones",
	"Transacciones",
	"Categoriasv",
	"Banners",
	"Devoluciones",
	"Bonos",
	"Canastas",
	"Contenidos",
	"Opiniones",
	"Popups",
    "Plantillas",
	"Facebook1",
    "Campanas",
    "Reportes",
	"Vendedores",
	"Segmentos",
	"ProductosRelacionados"
);

$server->addClass($classes);


/*
$server->addClass("Session", dirname(__FILE__)."/modules/rpc_session.inc.php");
$server->addClass("Marcas", dirname(__FILE__)."/modules/rpc_marcas.inc.php");
$server->addClass("Productos", dirname(__FILE__)."/modules/rpc_productos.inc.php");
$server->addClass("Despachos", dirname(__FILE__)."/modules/rpc_despachos.inc.php");
$server->addClass("Proveedor", dirname(__FILE__)."/modules/rpc_proveedor.inc.php");
*/


$server->setDebug(false);
$server->setPublicWsdl(true);
$server->setOnPrivateExceptionMessage("Error interno, por favor comuniquese con nuestro centro de servicio al cliente");
$server->setMode(JRpcServer::MODE_JSON);
$server->handle();