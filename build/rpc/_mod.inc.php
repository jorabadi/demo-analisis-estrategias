<?php
require_once __DIR__ . '/../../config.inc.php';
require_once __DIR__ . "/../../inicializador.php";

$milExpiracion = 60 * 60 * 8;
session_set_cookie_params($milExpiracion);

session_name("admin_tiendas_vf");
if (!session_id()){
    session_start();
}

if (isset($_SESSION["info"]["codigo_proveedor"]) ) {
    $pp = $_SESSION["info"]["codigo_proveedor"];
    $exp=time()+($milExpiracion);
    setcookie("admin_tiendas_vf_{$pp}",$pp,$exp,"/");
}

JLib::requireOnceModule("database/jdb.inc.php");
JLib::requireOnceModule("database/jdbfile.inc.php");

$db = new JDatabase($cfg["dbDriver"]);
$db->setHostName($cfg["dbHost"]);
$db->setPort($cfg["dbPort"]);
$db->setDatabaseName($cfg["dbName"]);
$db->setUserName($cfg["dbUser"]);
$db->setPassword($cfg["dbPassword"]);
$db->open_();