<?php
class UploadFile {

    public static function progress() {
        $result = array(
            "error" => false,
            "message" => "",
            "status" => 100
        );

        if (!isset($_GET["uid"])) {
            $result["error"] = true;
            $result["message"] = "Invalid upload identifier";
            echo json_encode($result);
            exit;
        }

        $status = false;
        if (function_exists("uploadprogress_get_info")) {
            $status = uploadprogress_get_info($_GET['uid']);
        }

        if ($status) {
            $result["status"] = round($status['bytes_uploaded'] / $status['bytes_total'] * 100);
        } else {
            $result["status"] = 80;
        }

        echo json_encode($result);
        exit;
    }

    public static function upload() {

        if (isset($_FILES["file"]) && is_uploaded_file($_FILES["file"]["tmp_name"])) {
            $file = $_FILES["file"];
            $src = $file["tmp_name"];
            $dst = __DIR__ . "/../../private_tmp/" . basename($file["tmp_name"]);
//                        Cambiar para que funcione la ruta por config
//                        global $cfg;
//			  $dst = $cfg["rutaPrivateTmp"] . basename($file["tmp_name"]);

            sleep(5);
            @ $status = move_uploaded_file($src, $dst);

            if ($status) {
                $result = array(
                    "error" => false,
                    "name" => $file["name"],
                    "tmp_name" => basename($file["tmp_name"]),
                    "type" => $file["type"],
                    "size" => $file["size"],
                    "uid" => (isset($_POST["UPLOAD_IDENTIFIER"]) ? $_POST["UPLOAD_IDENTIFIER"] : -1)
                );
            } else {
                $error = error_get_last();
                $errorMessage = strip_tags($error["message"]);
                $result = array(
                    "error" => true,
                    "message" => "Error moving file, {$errorMessage}"
                );
            }
        } else {
            $result = array(
                "error" => true,
                "message" => "Invalid uploaded file"
            );
        }

        echo json_encode($result);
        exit;
    }

    public static function process() {
        if (isset($_GET["uid"])) {
            self::progress();
            return;
        }

        self::upload();
    }

}

UploadFile::process();