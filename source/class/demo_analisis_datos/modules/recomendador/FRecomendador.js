/**
 *
 #asset(qx/icon/${qx.icontheme}/64/status/image-missing.png)
 #asset(qx/icon/${qx.icontheme}/128/status/image-missing.png)
 #ignore(app)
 */

qx.Class.define("demo_analisis_datos.modules.recomendador.FRecomendador", {
    extend:itx.ui.window.Dialog,

    construct: function() {
        this.base(arguments);
        var ui = this.ui = demo_analisis_datos.modules.recomendador._ui.FRecomendador.setupUi(this);
        var that = this;
        this.setCaption("Recomendador");
        this.slotNuevo();
        return;
    },

    members: {
        slotNuevo: function() {
            this.clearPr();
            var ui = this.ui;
            return;
        }
      
    }

});
