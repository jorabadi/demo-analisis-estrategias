qx.Class.define("demo_analisis_datos.modules.recomendador._ui.FRecomendador",{
	type:"static",
	statics:{

		ui:{
			/** @memberOf demo_analisis_datos.modules.recomendador._ui.FRecomendador */
			__undefined:null,
			__disabled:true
		},
		buildUi:function(){
			var ui = {};
			ui.valCode="0x17";
			ui.valSignature="UHJvcGllZGFkIGRlIEp1YW4gRmVybmFuZG8gRXN0cmFkYSAyMDEz";
			ui.tableWidget = new qx.ui.container.Composite();
			//QDialog
			ui.tableWidget.setUserData('objectName','tableWidget');
			ui.tableWidget.setWidth(780);
			ui.tableWidget.setHeight(440);
			ui.tableWidget.setHeight(null);
			ui.gridLayout_7 = new qx.ui.layout.Grid(4,4);
			ui.tableWidget.setLayout( ui.gridLayout_7 );
			ui.gridLayout_5 = new qx.ui.layout.Grid(4,4);
			ui.gridLayout_5Container = new qx.ui.container.Composite(ui.gridLayout_5);
			ui.label_3 = new qx.ui.basic.Label("...");
			ui.label_3.setRich(false);
			ui.label_3.setValue("Productos incluidos");
			//QLabel
			ui.label_3.setUserData('objectName','label_3');
			ui.label_3.setEnabled(true);
			ui.gridLayout_5Container.add(ui.label_3,{row: 0, column: 0, rowSpan: 0, colSpan: 0});
			ui.tableWidgetProductosIncluidos = new itx.ui.table.TableWidget().set({columnVisibilityButtonVisible:false});
			//itx_ui_table_TableWidget
			ui.tableWidgetProductosIncluidos.setUserData('objectName','tableWidgetProductosIncluidos');
			ui.tableWidgetProductosIncluidos.setMaxWidth(16777215);
			ui.tableWidgetProductosIncluidos.setMaxHeight(16777215);
			ui.tableWidgetProductosIncluidos.setAllowGrowX(true);
			ui.tableWidgetProductosIncluidos.setAllowGrowY(true);
			ui.gridLayout_5Container.add(ui.tableWidgetProductosIncluidos,{row: 1, column: 0, rowSpan: 0, colSpan: 0});
			ui.gridLayout_5.setColumnFlex(0,1);
			ui.gridLayout_5.setRowFlex(1,1);
			ui.tableWidget.add(ui.gridLayout_5Container,{row: 1, column: 2, rowSpan: 0, colSpan: 0});
			ui.gridLayout_7.setColumnFlex(2,1);//from layout
			ui.gridLayout_7.setRowFlex(1,1);//from layout
			ui.gridLayout_2 = new qx.ui.layout.Grid(4,4);
			ui.gridLayout_2Container = new qx.ui.container.Composite(ui.gridLayout_2);
			ui.pushButtonGuardar = new qx.ui.form.Button("Guardar");
			ui.buttonGuardar = ui.pushButtonGuardar;
			//QPushButton
			ui.pushButtonGuardar.setUserData('objectName','pushButtonGuardar');
			ui.gridLayout_2Container.add(ui.pushButtonGuardar,{row: 0, column: 0, rowSpan: 0, colSpan: 0});
			ui.horizontalSpacer = new qx.ui.basic.Label();
			ui.horizontalSpacer.set({allowGrowX:true,allowGrowY:false});
			ui.gridLayout_2Container.add(ui.horizontalSpacer,{row: 0, column: 1, rowSpan: 0, colSpan: 0});
			ui.gridLayout_2.setColumnFlex(1,1);
			ui.tableWidget.add(ui.gridLayout_2Container,{row: 2, column: 0, rowSpan: 0, colSpan: 3});
			ui.gridLayout_7.setColumnFlex(0,1);//from layout
			ui.gridLayout_4 = new qx.ui.layout.Grid(4,4);
			ui.gridLayout_4Container = new qx.ui.container.Composite(ui.gridLayout_4);
			ui.label_2 = new qx.ui.basic.Label("...");
			ui.label_2.setRich(false);
			ui.label_2.setValue("Productos");
			//QLabel
			ui.label_2.setUserData('objectName','label_2');
			ui.gridLayout_4Container.add(ui.label_2,{row: 0, column: 0, rowSpan: 0, colSpan: 0});
			ui.tableWidgetProductos = new itx.ui.table.TableWidget().set({columnVisibilityButtonVisible:false});
			//itx_ui_table_TableWidget
			ui.tableWidgetProductos.setUserData('objectName','tableWidgetProductos');
			ui.tableWidgetProductos.setMaxWidth(16777215);
			ui.tableWidgetProductos.setMaxHeight(16777215);
			ui.tableWidgetProductos.setAllowGrowX(true);
			ui.tableWidgetProductos.setAllowGrowY(true);
			ui.gridLayout_4Container.add(ui.tableWidgetProductos,{row: 1, column: 0, rowSpan: 0, colSpan: 0});
			ui.gridLayout_4.setColumnFlex(0,1);
			ui.gridLayout_4.setRowFlex(1,1);
			ui.tableWidget.add(ui.gridLayout_4Container,{row: 1, column: 0, rowSpan: 0, colSpan: 0});
			ui.gridLayout_7.setColumnFlex(0,1);//from layout
			ui.gridLayout_7.setRowFlex(1,1);//from layout
			ui.gridLayout = new qx.ui.layout.Grid(4,4);
			ui.gridLayoutContainer = new qx.ui.container.Composite(ui.gridLayout);
			ui.verticalSpacer_2 = new qx.ui.basic.Label();
			ui.verticalSpacer_2.set({allowGrowX:false,allowGrowY:true});
			ui.gridLayoutContainer.add(ui.verticalSpacer_2,{row: 0, column: 0, rowSpan: 0, colSpan: 0});
			ui.gridLayout.setRowFlex(0,1);
			ui.pushButtonIncluirProducto = new qx.ui.form.Button(">>");
			ui.buttonIncluirProducto = ui.pushButtonIncluirProducto;
			//QPushButton
			ui.pushButtonIncluirProducto.setUserData('objectName','pushButtonIncluirProducto');
			ui.gridLayoutContainer.add(ui.pushButtonIncluirProducto,{row: 1, column: 0, rowSpan: 0, colSpan: 0});
			ui.verticalSpacer = new qx.ui.basic.Label();
			ui.verticalSpacer.set({allowGrowX:false,allowGrowY:true});
			ui.gridLayoutContainer.add(ui.verticalSpacer,{row: 2, column: 0, rowSpan: 0, colSpan: 0});
			ui.gridLayout.setRowFlex(2,1);
			ui.tableWidget.add(ui.gridLayoutContainer,{row: 1, column: 1, rowSpan: 0, colSpan: 0});
			ui.gridLayout_7.setRowFlex(1,1);//from layout
			ui.gridLayout_3 = new qx.ui.layout.Grid(4,4);
			ui.gridLayout_3Container = new qx.ui.container.Composite(ui.gridLayout_3);
			ui.label_4 = new qx.ui.basic.Label("...");
			ui.label_4.setRich(false);
			ui.label_4.setValue("Proveedor");
			//QLabel
			ui.label_4.setUserData('objectName','label_4');
			ui.gridLayout_3Container.add(ui.label_4,{row: 0, column: 1, rowSpan: 0, colSpan: 0});
			ui.tokenFieldProveedor = new itx.ui.form.TokenField();
			//itx_ui_form_TokenField
			ui.tokenFieldProveedor.setUserData('objectName','tokenFieldProveedor');
			ui.tokenFieldProveedor.setMinWidth(0);
			ui.tokenFieldProveedor.setMinHeight(0);
			ui.tokenFieldProveedor.setAllowGrowX(true);
			ui.gridLayout_3Container.add(ui.tokenFieldProveedor,{row: 1, column: 1, rowSpan: 0, colSpan: 0});
			ui.gridLayout_3.setColumnFlex(1,1);
			ui.label = new qx.ui.basic.Label("...");
			ui.label.setRich(false);
			ui.label.setValue("Negocios");
			//QLabel
			ui.label.setUserData('objectName','label');
			ui.gridLayout_3Container.add(ui.label,{row: 0, column: 0, rowSpan: 0, colSpan: 0});
			ui.label_5 = new qx.ui.basic.Label("...");
			ui.label_5.setRich(false);
			ui.label_5.setValue("Filtro");
			//QLabel
			ui.label_5.setUserData('objectName','label_5');
			ui.gridLayout_3Container.add(ui.label_5,{row: 0, column: 2, rowSpan: 0, colSpan: 0});
			ui.comboBoxNegocios = new qx.ui.form.SelectBox();
			//QComboBox
			ui.comboBoxNegocios.setUserData('objectName','comboBoxNegocios');
			ui.comboBoxNegocios.setMinWidth(0);
			ui.comboBoxNegocios.setMinHeight(0);
			var tempItem = new qx.ui.form.ListItem("...","",null);
			ui.comboBoxNegocios.add(tempItem);
			ui.gridLayout_3Container.add(ui.comboBoxNegocios,{row: 1, column: 0, rowSpan: 0, colSpan: 0});
			ui.lineEditFiltro = new qx.ui.form.TextField();
			ui.textFieldFiltro = ui.lineEditFiltro;
			//QLineEdit
			ui.lineEditFiltro.setUserData('objectName','lineEditFiltro');
			ui.lineEditFiltro.setMaxWidth(16777206);
			ui.lineEditFiltro.setMaxHeight(16777215);
			ui.lineEditFiltro.setAllowGrowX(true);
			ui.lineEditFiltro.set({allowGrowX:true});
			ui.gridLayout_3Container.add(ui.lineEditFiltro,{row: 1, column: 2, rowSpan: 0, colSpan: 0});
			ui.gridLayout_3.setColumnFlex(2,1);
			ui.pushButtonAplicar = new qx.ui.form.Button("Aplicar");
			ui.buttonAplicar = ui.pushButtonAplicar;
			//QPushButton
			ui.pushButtonAplicar.setUserData('objectName','pushButtonAplicar');
			ui.gridLayout_3Container.add(ui.pushButtonAplicar,{row: 1, column: 3, rowSpan: 0, colSpan: 0});
			ui.tableWidget.add(ui.gridLayout_3Container,{row: 0, column: 0, rowSpan: 0, colSpan: 0});
			ui.gridLayout_7.setColumnFlex(0,1);//from layout
			
			ui.mainWidget = null;ui.mainWidget =ui.tableWidget;
			return ui;
			
		},
		setupUi:function(parent,autoSize){
			autoSize = typeof autoSize !=="undefined" ? autoSize:true;

			var ui = this.buildUi();
			if( parent !== undefined ){
				ui.mainScroll =new qx.ui.container.Scroll();
				ui.mainScroll.add(ui.mainWidget);
				parent.setLayout(new qx.ui.layout.VBox());
				parent.add(ui.mainScroll,{flex:1});

				ui.mainScroll.setMargin(0);
				ui.mainScroll.setContentPadding(0,0,0,0);
			}
			return ui;
		}
	}
});
