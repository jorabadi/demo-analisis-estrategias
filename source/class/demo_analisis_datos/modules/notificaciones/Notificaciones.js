qx.Class.define("demo_analisis_datos.modules.notificaciones.Notificaciones", {
    extend: qx.core.Object,
    
    construct: function () {
        if(! document.getElementById("contenedor_notificaciones")){
            var elemDiv = document.createElement('div');
            elemDiv.id = "contenedor_notificaciones";
            elemDiv.innerHTML = '<div id="notificacion_mal" class="notificacion"><span class="titulo_notificacion" onclick="cerrarNotificacion(\'mal\')">X</span><span id="texto_notificacion_mal"></span></div><div id="notificacion_atencion" class="notificacion"><span class="titulo_notificacion" onclick="cerrarNotificacion(\'atencion\')">X</span><span id="texto_notificacion_atencion"></span></div><div id="notificacion_bien" class="notificacion"><span class="titulo_notificacion" onclick="cerrarNotificacion(\'bien\')">X</span><span id="texto_notificacion_bien"></span></div>';
            document.body.insertBefore(elemDiv, document.body.firstChild);
            
            var res = "com/coordiutil/proveedores/notificaciones/style_notificaciones.css";
            var uri = qx.util.ResourceManager.getInstance().toUri(res);
            var head = document.getElementsByTagName("head")[0];
            var el = document.createElement("link");
            el.type = "text/css";
            el.rel = "stylesheet";
            el.href = uri;
            head.appendChild(el);
            
            var el2 = document.createElement("script");
            el2.type = "text/javascript";
            el2.href = uri;
            el2.textContent = 'function cerrarNotificacion(tipo) {var elemento = "notificacion_"+tipo; if(document.getElementById(elemento)){document.getElementById(elemento).style.display = "none";}}';
            head.appendChild(el2);
        }
    },

    statics: {
        estado: 'ok'
    },
    
    members:{
        ocultar: function (idElemento) {
            if(document.getElementById(idElemento)){
                document.getElementById(idElemento).style.display = 'none';
            }
        },
        
        mostrar: function (idElemento) {
            if(document.getElementById(idElemento)){
                document.getElementById(idElemento).style.display = 'block';
            }
        },
        
        setHtml: function (idElemento, html) {
            if(document.getElementById(idElemento)){
                document.getElementById(idElemento).innerHTML = html;
            }
        },
        
        __mensajeRespuesta: function (mensaje, tipo) {
            var contexto = this;
            if (contexto.estado == 'bien' | contexto.estado == 'atencion' | contexto.estado == 'mal') {
                contexto.ocultar("notificacion_" + contexto.estado);
                contexto.estado = 'ok';
            }

            contexto.setHtml("texto_notificacion_" + tipo, mensaje);
            contexto.mostrar("notificacion_" + tipo);
            contexto.estado = tipo;
            setTimeout(function () {
                contexto.ocultar("notificacion_" + tipo);
                contexto.estado = 'ok';
            }, 30000);

            document.body.onkeydown = function (e) {
                contexto.ocultar("notificacion_" + tipo);
                contexto.estado = 'ok';
            };
            
//            document.body.onmouseup = function (e) {
//                contexto.ocultar("notificacion_" + tipo);
//                contexto.estado = 'ok';
//            };

	    app.cargando(false);
        },
        
        mensajeOk: function (mensaje) {
            this.__mensajeRespuesta(mensaje, 'bien');
        },
        
        mensajeAtencion: function (mensaje) {
            this.__mensajeRespuesta(mensaje, 'atencion');
        },
        
        mensajeError: function (mensaje) {
            this.__mensajeRespuesta(mensaje, 'mal');
        }
    }
});