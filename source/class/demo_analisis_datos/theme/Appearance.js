/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("demo_analisis_datos.theme.Appearance", {
    extend: itx.theme.modern.Appearance,

    appearances: {
        "menubar": {
            style: function (states) {
                return {
                    backgroundColor: "barramenu-background",
                    padding: [2]
                };
            }
        },
        "menubar-button": {
            style: function (states) {
                var decorator;
                var padding = [2, 4];
                var textColor = "barramenu-texto";
                if (!states.disabled) {
                    if (states.pressed) {
                        decorator = "menubar-button-pressed";
                        padding = [2, 4];
                        textColor = "barramenu-texto-selected";
                    } else if (states.hovered) {
                        decorator = "menubar-button-hovered";
                        padding = [2, 4];
                        textColor = "barramenu-texto-selected";
                    }
                }

                return {
                    padding: padding,
                    margin: [0, 5],
                    textColor: textColor
                };
            }
        },
        "menubar-button/icon": {
            style: function (states) {
                return {
                    padding: [0, 5, 0, 5]
                };
            }
        },
        "menu": {
            style: function (states) {
                var result = {
                    backgroundColor: "background",
                    spacingX: 0,
                    spacingY: 0,
                    iconColumnWidth: 16,
                    arrowColumnWidth: 8,
                    position: "bottom-left",
                    offset: [0, 0, 0, -4]
                };
                return result;
            }
        },

        "menu-button": {
            alias: "atom",

            style: function (states) {
                return {
                    backgroundColor: states.selected ? "menu-item-background-selected" : "menu-item-background",
                    textColor: states.selected ? "menu-item-texto-selected" : "menu-item-texto",
                    font: states.selected ? "bold" : "default",
                    padding: [8, 2]
                };
            }
        },
        "menu-button/icon": {
            style: function (states) {
                return {
                    backgroundColor: states.selected ? "menu-item-selected" : "menu-item"
                };
            }
        },
        "menu-button-header/icon": {
            include: "menu-button/icon",

            style: function (states) {
                return {
                    marginLeft: 6
                };
            }
        },
        "tabview-page/button": {
            style: function (states) {
                var decorator;

                // default padding
                if (states.barTop || states.barBottom) {
                    //var padding = [8, 16, 8, 13];
                    var padding = [5, 10, 5, 10];
                } else {
                    //var padding = [8, 4, 8, 4];
                    var padding = [5, 10, 5, 10];
                }

                // decorator
                if (states.checked) {
                    if (states.barTop) {
                        decorator = "tabview-page-button-top";
                    }
                } else {
                    if (states.barTop) {
                        decorator = "tabview-page-button-top-inactive";
                    }
                }
                return {
                    zIndex: states.checked ? 10 : 5,
                    decorator: decorator,
                    textColor: states.disabled ? "tabview-pane-button-inactive-texto" : states.checked ? null : "tabview-pane-button-active-texto",

                    padding: padding,
                    cursor: "pointer"
                };
            }
        },
        "tabview-page": {
            style: function (states) {
                return {
                    backgroundColor: "tabview-pane-page-background"
                };
            }
        },
        "tabview/pane": {
            style: function (states) {
                return {
                    padding: 10,
                    backgroundColor: "tabview-pane-background",
                    decorator: "tabview-pane"
                };
            }
        },
        "tabview": {
            style: function (states) {
                return {
                    padding: 0
                };
            }
        },
        "actionMenuHorizontal": {
            style: function (states) {
                return {
                    //backgroundColor : "actionMenuHorizontal-background",
                    decorator: "actionMenuHorizontal"
                };
            }
        },
        "querydialogTabla": {
            style: function (states) {
                return {
                    padding: [5, 10, 5, 20]
                };
            }
        },
        "button": {
            style: function (states) {
                var decorator = "boton-fondo-normal";
                var color = "boton-texto";
                if (!states.disabled) {
                    if (states.hovered && !states.pressed && !states.checked) {
                        decorator = "boton-fondo-selected";
                        color = "boton-texto-selected";
                    } else if (states.hovered && (states.pressed || states.checked)) {
                        decorator = "boton-fondo-selected";
                        color = "boton-texto-selected";
                    } else if (states.pressed || states.checked) {
                        decorator = "boton-fondo-selected";
                        color = "boton-texto-selected";
                    }
                }
                return {
                    decorator: decorator,
                    center: true,
                    padding: [2, 10],
                    textColor: color,
                    cursor: states.disabled ? undefined : "pointer",
                    font: "button"
                };
            }
        },
        "table-scroller/header": {
            style: function (states) {
                return {
                    textColor: "tabla-encabezado-texto-color",
                    backgroundColor: "tabla-encabezado-background",
                    padding: 5
                };
            }
        },
        "table-header-cell": {
            style: function (states) {
                return {
                    minWidth: 13,
                    minHeight: 20,
                    padding: states.hovered ? [3, 4, 2, 4] : [3, 4],
                    decorator: "table-header-cell",
                    sortIcon: states.sorted ?
                        (states.sortedAscending ? "decoration/table/ascending.png" : "decoration/table/descending.png") : undefined
                };
            }
        },
        "groupbox/legend": {
            style: function (states) {
                return {
                    textColor: "black",
                    font: "bold"
                };
            }
        },
        "groupbox/frame": {
            style: function (states) {
                return {
                    backgroundColor: "groupbox-background",
                    padding: [6, 9],
                    margin: [5, 2, 2, 2],
                    decorator: "groupbox-borde"
                };
            }
        },
        "tooltip": {
            style: function (states) {
                return {
                    backgroundColor: "tooltip-background",
                    padding: [10],
                    offset: [10, 5, 5, 5]
                };

            }

        },
        "botonLogin": {
            style: function (states) {
                return {
                    backgroundColor: "boton-login-background",
                    textColor: "boton-login-color-texto",
                    center: true,
                    padding: [2, 10],
                    cursor: states.disabled ? undefined : "pointer",
                    font: "button",
                    decorator: "boton-login"
                };
            }
        },
        "cardTemplate": {
            style: function (states) {
                return {
                    width: 300,
                    height: 300,
                    margin: 15,
                    backgroundColor: "card-template-backgroundColor",
                    decorator: "cardShadow"
                };
            }
        },
        "lineaSeparadora":{
            style: function (states) {
                return {                   
                    decorator: "groupbox-borde",
                    margin: [10]
                };
            }
        }
    }
});