/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("demo_analisis_datos.theme.Decoration", {
    extend: itx.theme.modern.Decoration,
    decorations: {
        "tabview-page-button-top": {
            style: {
                width: [1, 1, 0, 1],
                backgroundColor: "tabview-pane-button-active-background",
                color: "tabview-pane-button-active-borde",
                radius: [3, 3, 0, 0]
            }
        },
        "tabview-page-button-top-inactive": {
            include: "tabview-page-button-top",
            style: {
                backgroundColor: "tabview-pane-button-inactive-background",
                color: "tabview-pane-button-inactive-borde"
            }
        },

        "tabview-page-button-bottom": {
            include: "tabview-page-button-top",

            style: {
                radius: [0, 0, 3, 3],
                width: [0, 1, 1, 1]
            }
        },

        "tabview-page-button-left": {
            include: "tabview-page-button-top",

            style: {
                radius: [3, 0, 0, 3],
                width: [1, 0, 1, 1]
            }
        },

        "tabview-page-button-right": {
            include: "tabview-page-button-top",

            style: {
                radius: [0, 3, 3, 0],
                width: [1, 1, 1, 0]
            }
        },
        "tabview-page-view": {
            style: {
                color: "tabview-page-view-borde"
            }
        },
        "boton-fondo-normal": {
            style: {
                radius: 2,
                width: 1,
                color: "boton-borde-color",
                backgroundColor: "boton-background-normal"
            }
        },
        "boton-fondo-selected": {
            style: {
                radius: 1,
                width: 1,
                color: "boton-borde-color",
                backgroundColor: "boton-background-selected"
            }
        },
        "table-header-cell": {
            include: "main",
            style: {
                width: 0
            }
        },
        "groupbox-borde": {
            style: {
                width: 1,
                color: "groupbox-borde-color",
                shadowBlurRadius: 0,
                shadowColor: "#999999",
                radius: 1,
                backgroundColor: "white",
                shadowLength: 0
            }
        },
        "tabview-pane": {
            style: {
                width: 2,
                color: "tabview-pane-borde-color",
                radius: 1
            }
        },
        "boton-login": {
            style: {
                radius: 1,
                width: 0,
                color: "boton-login-background",
                backgroundColor: "boton-login-background"
            }

        },
        "actionMenuHorizontal": {
            style: {
                radius: 1,
                width: 1,
                color: "actionMenuHorizontal-background"
            }
        },
        "cardShadow": {
            style: {
                shadowBlurRadius: 5,
                shadowLength: 4,
                shadowColor: "#999999"
            }
        }
    }
});