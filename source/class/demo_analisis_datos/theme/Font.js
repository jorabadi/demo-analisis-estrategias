/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("demo_analisis_datos.theme.Font",
{
  extend : itx.theme.modern.Font,

  fonts :
  {
    "button" :
    {
      size : 13,
      family : ["Lato", "Helvetica Neue", "arial", "Helvetica", "sans-serif"],
      lineHeight: 1.33
    }
  }
});
