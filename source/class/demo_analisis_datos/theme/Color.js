/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("demo_analisis_datos.theme.Color", {
    extend: itx.theme.modern.Color,

    colors: {
        // main
        "background": "white",
        "barramenu-background": "#282828",
        "barramenu-texto": "white",
        "barramenu-texto-selected": "#5bc0de",
        "menu-item-background-selected": "#286090",
        "menu-item-background": "#7ba9d0",
        "menu-item-texto-selected": "white",
        "menu-item-texto": "white",
        "tabview-pane-button-active-background": "#eeeeee",
        "tabview-pane-button-active-borde": "#eeeeee",
        "tabview-pane-button-inactive-background": "#ffffff",
        "tabview-pane-button-inactive-borde": "#f9f9f9",
        "tabview-pane-button-active-texto": "black",
        "tabview-pane-button-inactive-texto": "black",
        "tabview-pane-page-background": "white",
        "tabview-pane-background": "white",
        "tabview-page-view-borde": "red",
        "actionMenuHorizontal-background": "#EEEEEE",
        "boton-background-normal": "#f9f9f9",
        "boton-background-selected": "#286090",
        "boton-texto": "rgb(26, 26, 26)",
        "boton-texto-selected": "white",
        "boton-borde": "#bbbbbb",
        "table-row-background-odd": "#F5F5F5",
        "table-row-background-even": "white",
        "table-row-background-selected": "#5bc0de",
        "table-row-background-focused-selected": "#5bc0de",
        "table-row-background-focused": "#5bc0de",
        "tabla-encabezado-background": "#EEEEEE",
        "tabla-encabezado-texto-color": "#286090",
        "groupbox-background": "#EEEEEE",
        "groupbox-borde-color": "#bbbbbb",
        "boton-borde-color": "#bbbbbb",
        "tabview-pane-borde-color": "#EEEEEE",
        "tooltip-background": "#d9edf7",
        "boton-login-background": "#337ab7",
        "boton-login-color-texto": "white",
        "text": qx.core.Environment.get("css.rgba") ? "rgba(0, 0, 0, 0.82)" : "#2E2E2E",
        "card-template-backgroundColor": "#f5f5f5"
    }
});