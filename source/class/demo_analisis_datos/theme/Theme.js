/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("demo_analisis_datos.theme.Theme",
{
  meta :
  {
    color : demo_analisis_datos.theme.Color,
    decoration : demo_analisis_datos.theme.Decoration,
    font : demo_analisis_datos.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : demo_analisis_datos.theme.Appearance
  }
});