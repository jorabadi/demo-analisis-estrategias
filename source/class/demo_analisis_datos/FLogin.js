/**
 *
 #ignore(app)
 */

qx.Class.define("demo_analisis_datos.FLogin",
{
    extend: qx.ui.groupbox.GroupBox,
    events:{
        "authenticated":"qx.event.type.Data"
    },
	
    construct: function () {
        //inicializo las notificaciones de validacion
        notificaciones = new demo_analisis_datos.modules.notificaciones.Notificaciones();

        var that = this;
        var root = this.getApplicationRoot();

        this.base(arguments);
        var groupBox = this.set({width: 320});

        groupBox.setAlignX("center");


        var gl = new qx.ui.layout.Grid(1, 1);
        var row = 0;

        var labelTitle = new qx.ui.basic.Label('<span style="font-size:17px ;">Demo</strong>').set({
            rich:true,
            textAlign:"center",
            allowGrowX:true
        });

        this.add(labelTitle, {row: row, column: 1});

        row++;

        this.textFieldUsuario = new qx.ui.form.TextField();
        this.add(this.textFieldUsuario, {row: row, column: 1});
        this.textFieldUsuario.setHeight(45);
        this.textFieldUsuario.setMaxWidth(230);
        this.textFieldUsuario.setAlignX("center");
        this.textFieldUsuario.setTextAlign("center");
        this.textFieldUsuario.setPlaceholder("Usuario");

        var element = this.textFieldUsuario.getContentElement();
        element.setStyles({
            borderRadius: "6px"
        });

        var font = new qx.bom.Font.fromString('14px');
        this.textFieldUsuario.setFont(font);

        row++;

        this.textFieldClave = new qx.ui.form.PasswordField();
        this.add(this.textFieldClave, {row: row, column: 1});
        this.textFieldClave.setHeight(45);
        this.textFieldClave.setMaxWidth(230);
        this.textFieldClave.setAlignX("center");
        this.textFieldClave.setPlaceholder("Clave");
        this.textFieldClave.setTextAlign("center");
        this.textFieldClave.setFont(font);

        var element = this.textFieldClave.getContentElement();

        element.setStyles({
            borderRadius: "6px"
        });
        row++;

        this.buttonEntrar = new qx.ui.form.Button("Ingresar");
        this.add(this.buttonEntrar, {row: row, column: 1});
        this.buttonEntrar.setHeight(45);
        this.buttonEntrar.setMaxWidth(230);
        this.buttonEntrar.setAlignX("center");


        var font = new qx.bom.Font(14, ["Tahoma", "Liberation Sans", "Arial"]);
        this.buttonEntrar.setFont(font);

        var element = this.buttonEntrar.getContentElement();

        element.setStyles({
            borderRadius: "6px",
            background: 'rgba(226,226,226,1)'
        });
        row++;

        gl.setColumnFlex(1, 1);
        this.setLayout(gl);
        gl.setSpacing(8);


        this.buttonEntrar.addListener("execute", function (e) {
        	var mainWindow = new demo_analisis_datos.FMain();
        }, this);
        this.buttonEntrar.setAppearance("botonLogin");
        return;
    },
    members: {
    }
});
