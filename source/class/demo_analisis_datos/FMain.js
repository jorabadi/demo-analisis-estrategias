/**
 *
 * #ignore(app)
 */

qx.Class.define("demo_analisis_datos.FMain", {
    extend: qx.ui.core.Widget,
    construct: function () {
        var that = this;

        this.ui = {};
        var root = this.getApplicationRoot();

        this.ui.tabView = new qx.ui.tabview.TabView();
        root.add(this.ui.tabView, {
            left: 0,
            top: 40,
            width: '100%',
            height: '90%'
        });
        
        this.actionRecomendador();
        return;
    },
    members: {
    	actionRecomendador: function () {
            var d = new demo_analisis_datos.modules.recomendador.FRecomendador();
            d.showCenteredModal();
            d.maximize();
        }
    }
});
