/* ************************************************************************
 Copyright:
 License:
 Authors:
 ************************************************************************ */

/*************************************************************************
 #asset(demo_analisis_datos/*)
 #ignore(app)
 ************************************************************************ */

/**
 * This is the main application class of your custom application "admin_tiendas"
 */
qx.Class
		.define(
				"demo_analisis_datos.Application",
				{
					extend : itx.application.Standalone,
					members : {
						// Instancio el objeto de rpc del standalone (antes se hacia en cada archivo L o R) y sobre este llamo el metodo callsync con la funcionalidad del cargando
						callSync : function(description,action,params,data,callback) {
							var service = new qx.bom.rest.Resource(description);
							service.setBaseUrl('http://127.0.0.1:8000/recomendacion');
							
							service.configureRequest(function(req) {
								req.setRequestHeader("Content-Type", "application/json");
							});
							
							service.addListener("success",function(res){
								var tmp = qx.lang.Json.parse(res.response);
								qx.lang.Function.bind( callback,{})(tmp,null);
							},context);
							
							service[ action ](params, qx.lang.Json.stringify(data) );
						},
						main : function() {
							// Call super class
							this.base(arguments);
							qx.Class.include(qx.ui.treevirtual.TreeVirtual,
									qx.ui.treevirtual.MNode);

							// Enable logging in debug variant
							if (qx.core.Environment.get("qx.debug")) {
								// support native logging capabilities, e.g. Firebug for Firefox
								qx.log.appender.Native;
								// support additional cross-browser console. Press F7 to toggle visibility
								qx.log.appender.Console;
							}

							/*
							 -------------------------------------------------------------------------
							 Below is your actual application code...
							 -------------------------------------------------------------------------
							 */

							itx.Bootstrap.init();
							window.app = this;

							this.setAppName("Demo");

							this.setAppVersion(3.0);

							document.title = this.getAppName() + " v"
									+ this.getAppVersion();

							var root = this.getRoot();
							var dLogin = new demo_analisis_datos.FLogin();

							root.add(dLogin, {
								left : "40%",
								top : "30%"
							});

							return;
						}
					}
				});