qx.Class.define("itx.ui.table.TableWidget", {
    extend:qx.ui.table.Table,

	//validation
	implement : [
		qx.ui.form.IForm
	],
	include : [
		qx.ui.form.MForm
	],
	//----

    events:{

		//ojo cambiar por returnPressed
        "activateNextCell":"qx.event.type.Data",
        "returnPressed":"qx.event.type.Data",

        "cellFocusout":"qx.event.type.Data",
		"changeValue":"qx.event.type.Data" //validation
    },

    construct:function () {
        var that = this;
        this.base(arguments);

        this.__columns = [];
        this.__cellOptions = {};


        this.addListener("keydown", function (e) {
            var keyCode = e.getKeyCode();

            //tab
            if (keyCode == 9) {
                return;
            }

            //arrows,return,escape
            if (!that.isEditing() && ( [37, 38, 39, 40,13,27].indexOf(keyCode) == -1 )) {
                that.startEditing();
            }
        });

		this.addListener("cellTap",function(e){
			console.log('TableWidget.cellTap');
            //var colId = ui.tableWidget.getColumnId(e.getColumn());
			var col = e.getColumn();
            var row = e.getRow();
			var colSt = that.__columns[col];

			if( colSt.type == itx.core.ColumnTypes.CheckBox && colSt.oneClick!==undefined && colSt.oneClick==true ){
				var value = that.getValue(col,row);
				that.setValue(col,row, !value );
			}
		});

		/*
		this.addListener("focusout",function(e){
			console.log("focusout",e.getTarget().classname);

			if( e.getTarget() !== this ){
				//this.stopEditing();
				//this.focus();

				var focusedRow = this.getFocusedRow();
				var focusedCol = this.getFocusedColumn();
				var columnId = this.getColumnId(focusedCol);

                var data = {
                    row:focusedRow,
                    col:focusedCol,
                    columnId:columnId,
                    value:this.getTableModel().getValue(focusedCol, focusedRow)
                };

                //this.fireDataEvent("activateNextCell", data);
                this.fireDataEvent("cellFocusout", data);
			}
		},this);
		*/

		//change valid or invalid decorator
		this.addListener("changeValid",this.__changeValidDecorator,this);
		this.__changeValidDecorator({ getData:function(){ return true;} });

		//this.setRowHeight(26);
        return;
    },

    statics:{

		actionColumn:function(name,label,width){
			if( name == "action.remove" ){
				return {
					name:"action.remove",
					label:(label || ""),
					type:itx.core.ColumnTypes.Image,
					editable:false,
					width:( width || 30 ),
					defaultValue:itx.ui.Icons.s16.actions.listRemove,
					exclude:true
				};
			} else if( name == "action.edit" ){
				return {
					name:"action.edit",
					label:(label || ""),
					type:itx.core.ColumnTypes.Image,
					editable:false,
					width:( width || 30 ),
					defaultValue:itx.ui.Icons.s16.actions.listEdit,
					exclude:true
				};
			}

			return null;
		}
    },

    members:{
		__changeValidDecorator:function(e){
			var value = e.getData();

			if( !value ){
				this.setDecorator("border-invalid");
				return;
			}

			this.setDecorator("main");
			return;



			var params={
				width: 1,
				style: "solid",
				color: "border-main"
			};
			var decorator = new qx.ui.decoration.Decorator().set(params);
			this.setDecorator(decorator);
		},
		/*
		setInvalidMessage:function(message){
			var toolTip=new qx.ui.tooltip.ToolTip(message);
			//toolTip.setDecorator("tooltip-error");
			toolTip.setAppearance("tooltip-error");
			this.setToolTip(toolTip);
		},
		*/

        __columns:null,
        //__tableModel:null,
        __cellOptions:null,

        columnNameToIndex:function (columnName) {
            for (var i = 0; i < this.__columns.length; i++) {
                if (this.__columns[i].name == columnName) {
                    return i;
                }
            }

            return -1;

        },

        columnIndexToName:function (columnIndex) {
            return this.__columns[columnIndex].name;
        },

        getColumnId:function (columnIndex) {
            return this.getTableModel().getColumnId(columnIndex);
        },

        getColumnIndexById:function (columnId) {
            return this.getTableModel().getColumnIndexById(columnId);
        },

        setColumnEditable:function (columnName,editable) {
            var columnIndex = this.columnNameToIndex(columnName);
            if (columnIndex == -1) {
                return false;
            }

            editable = typeof editable!=='undefined' ? editable:true;
            this.getTableModel().setColumnEditable(columnIndex, editable);
            return true;
        },

        setColumnVisible:function(columnId,visible){
            var columnIndex = this.columnNameToIndex(columnId);
            if (columnIndex == -1) {
                return false;
            }

            var tcm = this.getTableColumnModel();
            tcm.setColumnVisible(columnIndex,visible);
            return true;
        },

        setColumnWidthById:function(columnId,newWith){

            var columnIndex = this.columnNameToIndex(columnId);
            if (columnIndex == -1) {
                return false;
            }

            this.setColumnWidth(columnIndex,newWith);
            return;
        },

		setColumnNamesById:function(columnNames){
			this.getTableModel().setColumnNamesById(columnNames);

		},
        setColumns:function (columns) {
            var that = this;

            /*
            columns.push({
            	name:"__editrs__",
            	visible:true
            });
            */

            this.__columns = [];
            var columnNames = [];
            var columnIds = [];
            var col = null;

            for (var i = 0; i < columns.length; i++) {
                col={
                    editable:true,
                    visible:true,
                    defaultValue:"",
                    type:itx.core.ColumnTypes.Text,
                    width:null,
                    height:'auto',
                    exclude:false
                };
                qx.lang.Object.mergeWith(col, columns[i],true);


				if( col.name === undefined ){
					throw "Undefined column name on tableWidget";
				}


                col.label = typeof col.label =="undefined" ? col.name:col.label;


                this.__columns.push(col);
                columnIds.push(this.__columns[i].name);
                columnNames.push(this.__columns[i].label);
            }

            //itx.dev.logObject(this.__columns);


            //var isEnabled = this.getEnabled();
            //this.setEnabled(false);
            //this.removeAllRows();

            //this.__tableModel = null;

            var tableModel = new itx.ui.table.model.Simple();
            //var tableModel = new qx.ui.table.model.Simple();
            tableModel.setColumns(columnNames, columnIds);


            this.setTableModel(tableModel);
            this.getSelectionModel().setSelectionMode(qx.ui.table.selection.Model.NO_SELECTION);


            var tcm = this.getTableColumnModel();


            var cellEditorFactory = new qx.ui.table.celleditor.Dynamic( qx.lang.Function.bind(function(cellInfo) {
                var colInfo = that.__columns[cellInfo.col];
                var columnType = colInfo.type;
                var cellEditor = null;

				//console.log( this.classname );

                switch (columnType) {
                    case itx.core.ColumnTypes.SelectBox:
                        cellEditor = new qx.ui.table.celleditor.SelectBox();
                        var listData = itx.data.Conversion.toListData(that.getCellOptionsById(cellInfo.row, colInfo.name));
                        cellEditor.setListData(listData);
                        break;

                    case itx.core.ColumnTypes.CheckBox:
						cellEditor = new qx.ui.table.celleditor.CheckBox();
						//cellEditor = new itx.ui.table.celleditor.Void();
						//cellEditor = new itx.ui.table.celleditor.CheckBox();
                        break;

                    case itx.core.ColumnTypes.TokenField:
                    	cellEditor = new itx.ui.table.celleditor.TokenField();
                    	cellEditor.setRpcInitFunction( colInfo.rpcInitFunction );
                    	break;

                    case itx.core.ColumnTypes.DateField:
						cellEditor = new itx.ui.table.celleditor.DateField();
						break;

                    default:
                        cellEditor = new qx.ui.table.celleditor.TextField();
                        break;
                }

                return cellEditor;

            },this) );


            var cellRendererFactory = new qx.ui.table.cellrenderer.Dynamic(function (cellInfo) {
                var colInfo = that.__columns[cellInfo.col];
                var columnType = colInfo.type;
                var cellRender = null;

                //console.log( columnType+"="+itx.core.ColumnTypes.CheckBox);

                switch (columnType) {
                	case itx.core.ColumnTypes.Integer:
						var nf = new qx.util.format.NumberFormat();
						nf.set({
							maximumFractionDigits:0,
							minimumFractionDigits:0,
							groupingUsed:true
						});

                        cellRender = new qx.ui.table.cellrenderer.Number();
						cellRender.setNumberFormat(nf);
						break;

                	case itx.core.ColumnTypes.Numeric:
						var nf = new qx.util.format.NumberFormat();
						nf.set({
							maximumFractionDigits:4,
							minimumFractionDigits:0,
							groupingUsed:true
						});

                        cellRender = new qx.ui.table.cellrenderer.Number();
						cellRender.setNumberFormat(nf);
						break;

                    case itx.core.ColumnTypes.SelectBox:
                        cellRender = new qx.ui.table.cellrenderer.Replace;
                        var replaceMap = {};
                        var cellOptions = qx.lang.Array.clone(that.getCellOptionsById(cellInfo.row, colInfo.name));

                        for (var i = 0; i < cellOptions.length; i++) {
                            replaceMap[ cellOptions[i].data ] = cellOptions[i].label;
                        }

                        cellOptions = null; //free nmemory
                        cellRender.setReplaceMap(replaceMap);
                        cellRender.addReversedReplaceMap();
                        break;

                    case itx.core.ColumnTypes.Image:
                        var height = colInfo.height;
                        if( height == 'auto'){
                            height = that.getRowHeight();
                        }

                        cellRender = new qx.ui.table.cellrenderer.Image(colInfo.width, height);
                        break;

                    case itx.core.ColumnTypes.TokenField:
                    	cellRender = new itx.ui.table.cellrenderer.TokenField();
                    	break;


                    case itx.core.ColumnTypes.DateField:
						cellRender = new qx.ui.table.cellrenderer.Date();
						break;


                    case itx.core.ColumnTypes.Html:
                        cellRender = new qx.ui.table.cellrenderer.Html();
                        break;

                    case itx.core.ColumnTypes.CheckBox:
                        //cellRender = new qx.ui.table.cellrenderer.Boolean();
                        cellRender = new itx.ui.table.cellrenderer.CheckBox();
                        break;


                    default:
                        cellRender = new qx.ui.table.cellrenderer.Default();
                        break;
                }

                return cellRender;
            });



            var col = null;
            for (var i = 0; i < this.__columns.length; i++) {
                col = this.__columns[i];
                if (col.editable) {
                    this.setColumnEditable(col.name);
                }

                if( !col.visible ){
                    tcm.setColumnVisible(i,false);
                }

                if (col.width !== null) {
                    this.setColumnWidth(i, col.width);
                }

                tcm.setCellEditorFactory(i, cellEditorFactory);
                tcm.setDataCellRenderer(i, cellRendererFactory);

				//tcm.setHeaderCellRenderer(0,itx.ui.table.headerrenderer.Default);

				//custom sort functions

				//model.setSortMethods(Integer columnIndex, (Function | Map) compare)
				/*if( col.type == itx.core.ColumnTypes.Integer ){

					tableModel.setSortMethods(i,function(){
						console.log("sort");
					});
				}*/
            }

            //this.setEnabled(isEnabled);

            return;
        },

        setCellOptionsById:function (columnId, row, options) {
            this.__cellOptions[row + "_" + columnId] = options;
            /*var editRs = this.__getRowEditRs(row);
            if( typeof editRs[columnId] === "undefined"){
            	editRs[columnId]={};
            }

            editRs[columnId].options = options;
            */

        },

        getCellOptionsById:function (row, columnId) {

        	/*var editRs = this.__getRowEditRs(row);
        	if( typeof editRs[columnId] !=="undefined" && typeof editRs[columnId].options !=="undefined"){
        		return editRs[columnId].options;
        	}*/


            var optionsIndex = row + "_" + columnId;
            if (typeof(this.__cellOptions[optionsIndex]) != "undefined") {
                return this.__cellOptions[optionsIndex];
            }

			var colIndex = this.getTableModel().getColumnIndexById(columnId);
            var colInfo = this.__columns[colIndex];
            if (typeof(colInfo.options) != "undefined") {
                return colInfo.options;
            }

            return [];
        },


        customCellEditorFactory:function (cellInfo) {
            var table = cellInfo.table;
            var tableModel = table.getTableModel();
            var rowData = tableModel.getRowData(cellInfo.row);
            //console.log(rowData);
            //var metaData = rowData[3];
            var cellEditor = new qx.ui.table.celleditor.TextField;
            //var validationFunc = null;

            return cellEditor;
        },

        addRow:function (index) {
            var r = [];
            for (var i = 0; i < this.__columns.length; i++) {
                r.push(this.__columns[i].defaultValue);
            }

			if( index === undefined ){
				index = this.getRowCount();
			}

            this.getTableModel().addRows([r]);

            return index;
        },

		addRows:function(n){
			for(var i=0;i<n;i++){
				this.addRow();
			}
		},


		addRowIfLast:function(){
			var currRow=this.getFocusedRow();
			var lastRow=this.getRowCount()-1;
			if( lastRow < 0 ){
				this.addRow();
				return;
			}

			if( currRow < lastRow ){
				this.addRow();
				return;
			}
		},

        addRowAsMapArray:function(mapArr,startIndex,rememberMaps,clearSorting){
            this.getTableModel().addRowsAsMapArray([mapArr],startIndex,rememberMaps,clearSorting);
        },

        getCurrentRow:function () {
            var that = this;
			if( this.getFocusedRow() !== null ){
				return this.getFocusedRow();
			}

            var selectionModel = this.getSelectionModel();
            if (selectionModel.isSelectionEmpty()) {
                return null;
            }

            var selectedRanges = selectionModel.getSelectedRanges();

            return selectedRanges[0].minIndex;
        },




        getCurrentColumnName:function () {
            return "";
        },

		getFocusedColumnId:function(){
			return this.getColumnId( this.getFocusedColumn() );
		},


        setFocusedColumnById:function (columnId, startEditing) {
        	if( this.isEditing()){
        		this.stopEditing();
        	}

            startEditing = startEditing!==undefined?startEditing:true;
            var index = this.getTableModel().getColumnIndexById(columnId);
            this.setFocusedCell(index, this.getFocusedRow());

            if (!this.isEditing() && startEditing ) {
                this.startEditing();
            }
            return;
        },

        setFocusedCellById:function(columnId,row,startEditing){
        	if( this.isEditing()){
        		this.stopEditing();
        	}

        	startEditing = startEditing!==undefined?startEditing:true;

        	var col = this.getColumnIndexById(columnId);
        	this.setFocusedCell(col,row,true);

            if (!this.isEditing() && startEditing ) {
                this.startEditing();
            }
        },

        setSortable:function(sortable){
        	var model = this.getTableModel();
        	for(var i=0;i<model.getColumnCount();i++){
        		model.setColumnSortable(i,sortable);
        	}
        },

        moveFocusedCellByColumnId:function (columnId, deltaY, addRow) {
        	if( this.isEditing()){
        		this.stopEditing();
        	}

            addRow = typeof addRow ==="undefined" ? true: addRow;
            deltaY = typeof deltaY ==="undefined" ? 0:deltaY;


            var focusedRow = this.getFocusedRow();
            var rowCount = this.getTableModel().getRowCount();

            if (addRow === true && rowCount == ( focusedRow + 1 )) {
                this.addRow();
            }

            this.moveFocusedCell(0, deltaY);
            this.setFocusedColumnById(columnId);
            return;
        },

        _onKeyPressNative : function(evt)
        {
          if (!this.getEnabled()) {
            return;
          }

          // No editing mode
          var oldFocusedRow = this.__focusedRow;
          var consumed = true;

          // Handle keys that are independent from the modifiers
          var identifier = evt.getKeyIdentifier();

          if (this.isEditing())
          {
            // Editing mode
            if (evt.getModifiers() == 0)
            {
              switch(identifier)
              {
                case "Enter":
                  this.stopEditing();
                  //var oldFocusedRow = this.__focusedRow;
                  //this.moveFocusedCell(0, 1);

                  //if (this.__focusedRow != oldFocusedRow) {
                  //  consumed = this.startEditing();
                  //}

                  break;

                case "Escape":
                  this.cancelEditing();
                  this.focus();
                  break;

                default:
                  consumed = false;
                  break;
              }
            }

          }
          else
          {
            // No editing mode
            if (evt.isCtrlPressed())
            {
              // Handle keys that depend on modifiers
              consumed = true;

              switch(identifier)
              {
                case "A": // Ctrl + A
                  var rowCount = this.getTableModel().getRowCount();

                  if (rowCount > 0) {
                    this.getSelectionModel().setSelectionInterval(0, rowCount - 1);
                  }

                  break;

                default:
                  consumed = false;
                  break;
              }
            }
            else
            {
              // Handle keys that are independent from the modifiers
              switch(identifier)
              {
                case "Space":
                  this.__selectionManager.handleSelectKeyDown(this.__focusedRow, evt);
                  break;

                case "F2":
                case "Enter":
                  this.startEditing();
                  consumed = true;
                  break;

                case "Home":
                  this.setFocusedCell(this.__focusedCol, 0, true);
                  break;

                case "End":
                  var rowCount = this.getTableModel().getRowCount();
                  this.setFocusedCell(this.__focusedCol, rowCount - 1, true);
                  break;

                case "Left":
                  this.moveFocusedCell(-1, 0);
                  break;

                case "Right":
                  this.moveFocusedCell(1, 0);
                  break;

                case "Up":
                  this.moveFocusedCell(0, -1);
                  break;

                case "Down":
                  this.moveFocusedCell(0, 1);
                  break;

                case "PageUp":
                case "PageDown":
                  var scroller = this.getPaneScroller(0);
                  var pane = scroller.getTablePane();
                  var rowHeight = this.getRowHeight();
                  var direction = (identifier == "PageUp") ? -1 : 1;
                  rowCount = pane.getVisibleRowCount() - 1;
                  scroller.setScrollY(scroller.getScrollY() + direction * rowCount * rowHeight);
                  this.moveFocusedCell(0, direction * rowCount);
                  break;

                default:
                  consumed = false;
              }
            }
          }

          if (oldFocusedRow != this.__focusedRow &&
              this.getRowFocusChangeModifiesSelection())
          {
            // The focus moved -> Let the selection manager handle this event
            this.__selectionManager.handleMoveKeyDown(this.__focusedRow, evt);
          }

          if (consumed)
          {
            evt.preventDefault();
            evt.stopPropagation();
          }
        },


        _onKeyPress:function (evt) {
            var that = this;
            var identifier = evt.getKeyIdentifier();

            var focusedRow = this.getFocusedRow();
            var focusedCol = this.getFocusedColumn();
            var columnId = this.getColumnId(focusedCol);

            this._onKeyPressNative(evt);
            //this.base(arguments, evt);

            if (identifier == "Enter") {
                var data = {
                    row:focusedRow,
                    col:focusedCol,
                    columnId:columnId,
                    value:this.getTableModel().getValue(focusedCol, focusedRow)
                };

                this.fireDataEvent("activateNextCell", data);
            }

            return;
        },

        getRowCount:function () {
            return this.getTableModel().getRowCount();
        },

        getColumnCount:function(){
            return this.getTableModel().getColumnCount();
        },

        setValueById:function (columnId, row, value) {
            var columnIndex = this.getColumnIndexById(columnId);
            this.setValue(columnIndex, row, value);
        },

        setValue:function(col,row,value){

        	//if( typeof value !=="string" && typeof value!=="boolean"){
        	if( typeof value === "object" ){
        		this.getTableModel().setValue(col, row, qx.lang.Json.stringify(value) );
        		return;
        	}

            this.getTableModel().setValue(col, row, value);
        },

        setRecord:function(row,data){
        	for(var k in data){
        		this.setValueById(k,row,data[k]);
        	}
        },

		setRecords:function(rl){
			this.removeAllRows();
			this.addRows(rl.length);

			for(var i=0;i<rl.length;i++){
				this.setRecord(i,rl[i]);
			}
		},

		updateRecords:function(rl){
			var r=null;
			for(var i=0;i<rl.length;i++){
				r = rl[i];

				for(var k in r){
					this.setValueById(k,i,r[k]);
				}
			}
			return;
		},

		getValueById:function(columnId,row){
			var columnIndex = this.getColumnIndexById(columnId);
			return this.getTableModel().getValue(columnIndex,row);
		},
		getValue:function(col,row){
			return this.getTableModel().getValue(col,row);
		},
        getRecord:function (row) {
            var r = this.getTableModel().getRowDataAsMap(row);
            for (var i = 0; i < this.__columns.length; i++) {

                if (typeof(r[this.__columns[i].name]) == "undefined") {
                    r[ this.__columns[i].name ] = "";
                }

				if( this.__columns[i].name=="action.remove" ) continue;

				if( this.__columns[i].type== itx.core.ColumnTypes.TokenField && itx.Util.empty(r[ this.__columns[i].name ])===false){
					r[ this.__columns[i].name ]= qx.lang.Json.parse( r[ this.__columns[i].name ] );
				}

            }

            return r;
        },
        getCurrentRecord:function(){
        	var row = this.getFocusedRow(); //this.getCurrentRow(); este da null
			if( row === null || row === -1 ){
					return null;
			}

        	return this.getRecord(row);
        },

        getRecords:function () {
			this.stopEditing();

            var result = [];
            for (var i = 0; i < this.getRowCount(); i++) {
                result.push(this.getRecord(i));
            }
            return result;
            //return this.__tableModel.getDataAsMap();
        },

   		__getRowEditRs:function(row){
			var columnIndex = this.getColumnIndexById("__editrs__");
			var tmp = this.getTableModel().getValue(columnIndex,row);
			if( typeof tmp==="undefined" || tmp==="" || tmp === null ){
				return {};
			}

			return qx.lang.Json.parse(tmp);
		},
		__setRowEditRs:function(row,editRs){
			var columnIndex = this.getColumnIndexById("__editrs__");
			var tmp = qx.lang.Json.stringify(editRs);
			this.getTableModel().setValue(columnIndex,row);
			return;
		},
        removeRow:function (row) {
            this.getTableModel().removeRows(row, 1);
        },

        removeFocusedRow:function () {
			this.stopEditing();
            this.removeRow(this.getFocusedRow());
        },

        removeAllRows:function(){
        	if( this.getTableModel() == null ){
        		return;
        	}

			this.stopEditing();
            this.getTableModel().removeRows(0,this.getRowCount());
        },

        cellTapRemoveRow:function (row) {
            this.setEnabled(false);
            this.removeRow(row);
            if (this.getRowCount() == 0) {
                this.addRow();
            }
            this.setEnabled(true);
            return;
        },

        setSingleSelection:function(){
        	this.getSelectionModel().setSelectionMode(qx.ui.table.selection.Model.SINGLE_SELECTION);
        },


		getColumnWidthsAsMap:function(){
			var columnWidths={};
			var columnModel=this.getTableColumnModel();
			for(var i=0;i<this.__columns.length;i++){
				columnWidths[ this.__columns[i].name ] = columnModel.getColumnWidth(i);
			}

			return columnWidths;
		},

		setColumnWidthsAsMap:function(columnWidths){
			var columnModel=this.getTableColumnModel();
			var index=null;

			for(var k in columnWidths){
				index = this.columnNameToIndex(k);
				if( index==-1 ) continue;

				columnModel.setColumnWidth(index, columnWidths[k] );
			}
		}

    }
});
/**
 *
 */

qx.Class.define("itx.core.ColumnTypes", {
    type:"static",

    statics: {
        Integer:"integerfield",
        IntegerField:"integerfield",
        Numeric:"numericfield",
        NumericField:"numericfield",
        Text:"textfield",
        TextField:"textfield",
        Image:"image",
        CheckBox:"checkbox",
        DateField:"datefield",
        ComboBox:"combobox",
        SelectBox:"selectbox",
        TokenField:"tokenfield",
        PasswordField:"passwordfield",
        Html:"html", //deprecated do not use
        Combo:"selectbox" //deprecated do not use
    }
});

/**
 *
 */


/* ************************************************************************
* @asset(qx/icon/${qx.icontheme}/16/actions/*)
************************************************************************ */


qx.Class.define("itx.ui.Icons", {
    type:"static",
    statics:{
        test:1,
        s16:{
            actions:{
                listAdd:"icon/16/actions/list-add.png",
                listNew:"icon/16/actions/document-new.png",
                listEdit:"icon/16/actions/edit-find.png",
                listRemove:"icon/16/actions/dialog-close.png",
				listCancel:"icon/16/actions/edit-delete.png",
				listPrint:"icon/16/actions/document-print.png",
				listPreview:"icon/16/actions/document-print-preview.png",
				
                documentSave:"icon/16/actions/document-save.png",
                ok:"icon/16/actions/dialog-ok.png",
                dialogOk:"icon/16/actions/dialog-ok.png",
                dialogClose:"icon/16/actions/dialog-close.png",
				dialogApply:"icon/16/actions/dialog-apply.png"
            }
        }

    }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2006 Christian Boulanger

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Christian Boulanger (cboulanger)

************************************************************************ */

/**
 * A cell editor factory which can dynamically exchange the cell editor
 * based on information retrieved at runtime. This is useful when different
 * rows in a column should have different cellEditors based on cell content
 * or row meta data. A typical example would be a spreadsheet that has different
 * kind of data in one column.
 *
 */
qx.Class.define("qx.ui.table.celleditor.Dynamic",
{
  extend : qx.core.Object,
  implement : qx.ui.table.ICellEditorFactory,


  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  /**
   * @param cellEditorFactoryFunction {Function?null} the factory function
   *    {@link #cellEditorFactoryFunction}.
   */
  construct : function(cellEditorFactoryFunction)
  {
    this.base(arguments);
    if (cellEditorFactoryFunction)
    {
      this.setCellEditorFactoryFunction(cellEditorFactoryFunction);
    }

    this.__infos = {};
  },


  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

  properties :
  {

    /**
     * Function that returns a cellEditorFactory instance which will be
     * used for the row that is currently being edited. The function is
     * defined like this:
     * <pre class="javascript">
     * myTable.getTableColumnModel().setCellEditorFactory(function(cellInfo){
     *   // based on the cellInfo map or other information, return the
     *   // appropriate cellEditorFactory
     *   if (cellInfo.row == 5)
     *     return new qx.ui.table.celleditor.CheckBox;
     *   else
     *     return new qx.ui.table.celleditor.TextField;
     * });
     * </pre>
     **/
    cellEditorFactoryFunction :
    {
      check : "Function",
      nullable : true,
      init : null
    }
  },

  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    __cellEditorFactory : null,
    __infos : null,


    /**
     * Creates the cell editor based on the cellEditorFactory instance
     * returned by the function stored in the cellEditorFactoryFunction
     * property. Passes the cellInfo map to the function.
     *
     * @param cellInfo {Map} A map containing the information about the cell to
     *      create.
     * @return {qx.ui.core.Widget}
     */
    createCellEditor : function(cellInfo)
    {
      var cellEditorFactoryFunction = this.getCellEditorFactoryFunction();

      if (qx.core.Environment.get("qx.debug")) {
        this.assertFunction(cellEditorFactoryFunction, "No function provided! Aborting.");
      }

      this.__cellEditorFactory = cellEditorFactoryFunction(cellInfo);
      var cellEditor = this.__cellEditorFactory.createCellEditor(cellInfo);

      // save the cell info to the editor (needed for getting teh value)
      this.__infos[cellEditor.toHashCode()] = cellInfo;

      return cellEditor;
    },


    // interface implementation
    getCellEditorValue : function(cellEditor)
    {
      var cellEditorFactoryFunction = this.getCellEditorFactoryFunction();

      if (qx.core.Environment.get("qx.debug")) {
        this.assertFunction(cellEditorFactoryFunction, "No function provided! Aborting.");
      }

      var cellInfo = this.__infos[cellEditor.toHashCode()];
      // update the propper factory
      this.__cellEditorFactory = cellEditorFactoryFunction(cellInfo);
      var value = this.__cellEditorFactory.getCellEditorValue(cellEditor);
      return value;
    }
  },

  /*
  *****************************************************************************
     DESTRUCTOR
  *****************************************************************************
  */

  destruct : function() {
    this.__cellEditorFactory = null;
  }
});
/* ************************************************************************

    qooxdoo - the new era of web development

    http://qooxdoo.org

    Copyright:
      2007 by Christian Boulanger

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

    Authors:
      * Christian Boulanger (cboulanger)

************************************************************************ */

/**
 * A cell renderer factory which can dynamically exchange the cell renderer
 * based on information retrieved at runtime. This is useful when different
 * rows in a column should have different cell renderer based on cell content
 * or row metadata. A typical example would be a spreadsheet that has different
 * kind of data in one column.
 *
 */
qx.Class.define("qx.ui.table.cellrenderer.Dynamic",
{
  extend : qx.ui.table.cellrenderer.Default,


  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  /**
   * @param cellRendererFactoryFunction {Function?null} The initial value for
   *    the property {@link #cellRendererFactoryFunction}.
   */
  construct : function(cellRendererFactoryFunction)
  {
    this.base(arguments);
    if (cellRendererFactoryFunction)
    {
      this.setCellRendererFactoryFunction(cellRendererFactoryFunction);
    }
  },


  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

  properties :
  {

    /**
     * Function that returns a cellRenderer instance which will be
     * used for the row that is currently being edited. The function is
     * defined like this:
     *
     * <pre class="javascript">
     * myTable.getTableColumnModel().setCellRenderer(function(cellInfo){
     *   // based on the cellInfo map or other information, return the
     *   // appropriate cell renderer
     *   if (cellInfo.row == 5)
     *     return new qx.ui.table.cellrenderer.Boolean;
     *   else
     *     return new qx.ui.table.cellrenderer.Default;
     * });
     * </pre>
     *
     * the function MUST return at least a qx.ui.table.cellrenderer.Default
     **/
    cellRendererFactoryFunction :
    {
      check : "Function",
      nullable : true,
      init : null
    }
  },


  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    /**
     * Overridden; called whenever the cell updates. The cell will call the
     * function stored in the cellRendererFactoryFunction to retrieve the
     * cell renderer which should be used for this particular cell
     *
     * @param cellInfo {Map} A map containing the information about the cell to
     *     create.
     * @param htmlArr {String[]} Target string container. The HTML of the data
     *     cell should be appended to this array.
     * @return {String} Data cell HTML
     */
    createDataCellHtml : function(cellInfo, htmlArr)
    {
      var cellRendererFactoryFunction = this.getCellRendererFactoryFunction();
      if ( ! cellRendererFactoryFunction ) {
        throw new Error("No function provided! Aborting.");
      }
      var cellRenderer = cellRendererFactoryFunction(cellInfo);

      return cellRenderer.createDataCellHtml(cellInfo, htmlArr);
    }
  }
});
qx.Class.define("itx.ui.table.model.Simple", {
    extend: qx.ui.table.model.Simple,

    construct: function() {
        this.base(arguments);

        this.__rowArr = [];
        this.__sortColumnIndex = -1;

        // Array of objects, each with property "ascending" and "descending"
        this.__sortMethods = [];

        this.__editableColArr = null;
    },
    members: {

        // overridden
        /*
        setValue: function(columnIndex, rowIndex, value) {

            if (this.__rowArr[rowIndex][columnIndex] !== value) {
                this.__rowArr[rowIndex][columnIndex] = value;

                // Inform the listeners
                if (this.hasListener("dataChanged")) {
                    var data = {
                        firstRow: rowIndex,
                        lastRow: rowIndex,
                        firstColumn: columnIndex,
                        lastColumn: columnIndex
                    };

                    this.fireDataEvent("dataChanged", data);
                }

                if (columnIndex == this.__sortColumnIndex) {
                    this.clearSorting();
                }
            }
        }
        */
    }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2006 Christian Boulanger

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Fabian Jakobs (fjakobs)

************************************************************************ */

/**
 * A cell editor factory creating select boxes.
 */
qx.Class.define("qx.ui.table.celleditor.SelectBox",
{
  extend : qx.core.Object,
  implement : qx.ui.table.ICellEditorFactory,


  properties :
  {
    /**
     * function that validates the result
     * the function will be called with the new value and the old value and is
     * supposed to return the value that is set as the table value.
     **/
    validationFunction :
    {
      check : "Function",
      nullable : true,
      init : null
    },

    /** array of data to construct ListItem widgets with */
    listData :
    {
      check : "Array",
      init : null,
      nullable : true
    }

  },


  members :
  {
    // interface implementation
    createCellEditor : function(cellInfo)
    {
      var cellEditor = new qx.ui.form.SelectBox().set({
        appearance: "table-editor-selectbox"
      });

      var value = cellInfo.value;
      cellEditor.originalValue = value;

      // check if renderer does something with value
      var cellRenderer = cellInfo.table.getTableColumnModel().getDataCellRenderer(cellInfo.col);
      var label = cellRenderer._getContentHtml(cellInfo);
      if ( value != label ) {
        value = label;
      }

      // replace null values
      if ( value === null ) {
        value = "";
      }

      var list = this.getListData();
      if (list)
      {
        var item;

        for (var i=0,l=list.length; i<l; i++)
        {
          var row = list[i];
          if ( row instanceof Array ) {
            item = new qx.ui.form.ListItem(row[0], row[1]);
            item.setUserData("row", row[2]);
            if (value == row[2]) {
              label = row[0];
            }
          } else {
            item = new qx.ui.form.ListItem(row, null);
            item.setUserData("row", row);
          }
          cellEditor.add(item);
        };
      }

      if (label != null) {
        var itemToSelect = cellEditor.getChildrenContainer().findItem(label + "");
      }

      if (itemToSelect) {
        cellEditor.setSelection([itemToSelect]);
      } else {
        cellEditor.resetSelection();
      }
      cellEditor.addListener("appear", function() {
        cellEditor.open();
      });

      return cellEditor;
    },


    // interface implementation
    getCellEditorValue : function(cellEditor)
    {
      var selection = cellEditor.getSelection();
      var value = "";

      if (selection && selection[0]) {
        var userValue = selection[0].getUserData("row")
        value = userValue === undefined ? selection[0].getLabel() : userValue;
      }

      // validation function will be called with new and old value
      var validationFunc = this.getValidationFunction();
      if (validationFunc ) {
         value = validationFunc( value, cellEditor.originalValue );
      }

      if (typeof cellEditor.originalValue == "number") {
        value = parseFloat(value);
      }

      return value;
    }
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2006 David Perez

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * David Perez (david-perez)

************************************************************************ */

/**
 * For editing boolean data in a checkbox. It is advisable to use this in
 * conjunction with {@link qx.ui.table.cellrenderer.Boolean}.
 */
qx.Class.define("qx.ui.table.celleditor.CheckBox",
{
  extend : qx.core.Object,
  implement : qx.ui.table.ICellEditorFactory,


  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    // interface implementation
    createCellEditor : function(cellInfo)
    {
      var editor = new qx.ui.container.Composite(new qx.ui.layout.HBox().set({
        alignX: "center",
        alignY: "middle"
      })).set({
        focusable: true
      });

      var checkbox = new qx.ui.form.CheckBox().set({
        value: cellInfo.value
      });
      editor.add(checkbox);

      // propagate focus
      editor.addListener("focus", function() {
        checkbox.focus();
      });

      // propagate active state
      editor.addListener("activate", function() {
        checkbox.activate();
      });

      return editor;
    },

    // interface implementation
    getCellEditorValue : function(cellEditor) {
      return cellEditor.getChildren()[0].getValue();
    }
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2004-2008 1&1 Internet AG, Germany, http://www.1und1.de

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Martin Wittemann (martinwittemann)

************************************************************************ */

/**
 * A toggle Button widget
 *
 * If the user presses the button by tapping on it pressing the enter or
 * space key, the button toggles between the pressed an not pressed states.
 * There is no execute event, only a {@link qx.ui.form.ToggleButton#changeValue}
 * event.
 */
qx.Class.define("qx.ui.form.ToggleButton",
{
  extend : qx.ui.basic.Atom,
  include : [
    qx.ui.core.MExecutable
  ],
  implement : [
    qx.ui.form.IBooleanForm,
    qx.ui.form.IExecutable,
    qx.ui.form.IRadioItem
  ],



  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  /**
   * Creates a ToggleButton.
   *
   * @param label {String} The text on the button.
   * @param icon {String} An URI to the icon of the button.
   */
  construct : function(label, icon)
  {
    this.base(arguments, label, icon);

    // register pointer events
    this.addListener("pointerover", this._onPointerOver);
    this.addListener("pointerout", this._onPointerOut);
    this.addListener("pointerdown", this._onPointerDown);
    this.addListener("pointerup", this._onPointerUp);

    // register keyboard events
    this.addListener("keydown", this._onKeyDown);
    this.addListener("keyup", this._onKeyUp);

    // register execute event
    this.addListener("execute", this._onExecute, this);

  },



  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

  properties:
  {
    // overridden
    appearance:
    {
      refine: true,
      init: "button"
    },

    // overridden
    focusable :
    {
      refine : true,
      init : true
    },

    /** The value of the widget. True, if the widget is checked. */
    value :
    {
      check : "Boolean",
      nullable : true,
      event : "changeValue",
      apply : "_applyValue",
      init : false
    },

    /** The assigned qx.ui.form.RadioGroup which handles the switching between registered buttons. */
    group :
    {
      check  : "qx.ui.form.RadioGroup",
      nullable : true,
      apply : "_applyGroup"
    },

    /**
    * Whether the button has a third state. Use this for tri-state checkboxes.
    *
    * When enabled, the value null of the property value stands for "undetermined",
    * while true is mapped to "enabled" and false to "disabled" as usual. Note
    * that the value property is set to false initially.
    *
    */
    triState :
    {
      check : "Boolean",
      apply : "_applyTriState",
      nullable : true,
      init : null
    }
  },




  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    /** The assigned {@link qx.ui.form.RadioGroup} which handles the switching between registered buttons */
    _applyGroup : function(value, old)
    {
      if (old) {
        old.remove(this);
      }

      if (value) {
        value.add(this);
      }
    },


    /**
     * Changes the state of the button dependent on the checked value.
     *
     * @param value {Boolean} Current value
     * @param old {Boolean} Previous value
     */
    _applyValue : function(value, old) {
      value ? this.addState("checked") : this.removeState("checked");

      if (this.isTriState()) {
        if (value === null) {
          this.addState("undetermined");
        } else if (old === null) {
          this.removeState("undetermined");
        }
      }
    },

    /**
    * Apply value property when triState property is modified.
    *
    * @param value {Boolean} Current value
    * @param old {Boolean} Previous value
    */
    _applyTriState : function(value, old) {
      this._applyValue(this.getValue());
    },


    /**
     * Handler for the execute event.
     *
     * @param e {qx.event.type.Event} The execute event.
     */
    _onExecute : function(e) {
      this.toggleValue();
    },


    /**
     * Listener method for "pointerover" event.
     * <ul>
     * <li>Adds state "hovered"</li>
     * <li>Removes "abandoned" and adds "pressed" state (if "abandoned" state is set)</li>
     * </ul>
     *
     * @param e {qx.event.type.Pointer} Pointer event
     */
    _onPointerOver : function(e)
    {
      if (e.getTarget() !== this) {
        return;
      }

      this.addState("hovered");

      if (this.hasState("abandoned"))
      {
        this.removeState("abandoned");
        this.addState("pressed");
      }
    },


    /**
     * Listener method for "pointerout" event.
     * <ul>
     * <li>Removes "hovered" state</li>
     * <li>Adds "abandoned" state (if "pressed" state is set)</li>
     * <li>Removes "pressed" state (if "pressed" state is set and button is not checked)
     * </ul>
     *
     * @param e {qx.event.type.Pointer} pointer event
     */
    _onPointerOut : function(e)
    {
      if (e.getTarget() !== this) {
        return;
      }

      this.removeState("hovered");

      if (this.hasState("pressed"))
      {
        if (!this.getValue()) {
          this.removeState("pressed");
        }

        this.addState("abandoned");
      }
    },


    /**
     * Listener method for "pointerdown" event.
     * <ul>
     * <li>Activates capturing</li>
     * <li>Removes "abandoned" state</li>
     * <li>Adds "pressed" state</li>
     * </ul>
     *
     * @param e {qx.event.type.Pointer} pointer event
     */
    _onPointerDown : function(e)
    {
      if (!e.isLeftPressed()) {
        return;
      }

      // Activate capturing if the button get a pointerout while
      // the button is pressed.
      this.capture();

      this.removeState("abandoned");
      this.addState("pressed");
      e.stopPropagation();
    },


    /**
     * Listener method for "pointerup" event.
     * <ul>
     * <li>Releases capturing</li>
     * <li>Removes "pressed" state (if not "abandoned" state is set and "pressed" state is set)</li>
     * <li>Removes "abandoned" state (if set)</li>
     * <li>Toggles {@link #value} (if state "abandoned" is not set and state "pressed" is set)</li>
     * </ul>
     *
     * @param e {qx.event.type.Pointer} pointer event
     */
    _onPointerUp : function(e)
    {
      this.releaseCapture();

      if (this.hasState("abandoned")) {
        this.removeState("abandoned");
      } else if (this.hasState("pressed")) {
        this.execute();
      }

      this.removeState("pressed");
      e.stopPropagation();
    },


    /**
     * Listener method for "keydown" event.<br/>
     * Removes "abandoned" and adds "pressed" state
     * for the keys "Enter" or "Space"
     *
     * @param e {Event} Key event
     */
    _onKeyDown : function(e)
    {
      switch(e.getKeyIdentifier())
      {
        case "Enter":
        case "Space":
          this.removeState("abandoned");
          this.addState("pressed");

          e.stopPropagation();
      }
    },


    /**
     * Listener method for "keyup" event.<br/>
     * Removes "abandoned" and "pressed" state (if "pressed" state is set)
     * for the keys "Enter" or "Space". It also toggles the {@link #value} property.
     *
     * @param e {Event} Key event
     */
    _onKeyUp : function(e)
    {
      if (!this.hasState("pressed")) {
        return;
      }

      switch(e.getKeyIdentifier())
      {
        case "Enter":
        case "Space":
          this.removeState("abandoned");
          this.execute();

          this.removeState("pressed");
          e.stopPropagation();
      }
    }
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2004-2008 1&1 Internet AG, Germany, http://www.1und1.de

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Sebastian Werner (wpbasti)
     * Fabian Jakobs (fjakobs)
     * Andreas Ecker (ecker)

************************************************************************ */

/**
 * A check box widget with an optional label.
 */
qx.Class.define("qx.ui.form.CheckBox",
{
  extend : qx.ui.form.ToggleButton,
  include : [
    qx.ui.form.MForm,
    qx.ui.form.MModelProperty
  ],
  implement : [
    qx.ui.form.IForm,
    qx.ui.form.IModel
  ],

  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  /**
   * @param label {String?null} An optional label for the check box.
   */
  construct : function(label)
  {
    if (qx.core.Environment.get("qx.debug")) {
      this.assertArgumentsCount(arguments, 0, 1);
    }

    this.base(arguments, label);

    // Initialize the checkbox to a valid value (the default is null which
    // is invalid)
    this.setValue(false);
  },




  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

  properties :
  {
    // overridden
    appearance :
    {
      refine : true,
      init : "checkbox"
    },

    // overridden
    allowGrowX :
    {
      refine : true,
      init : false
    }
  },

  members :
  {
    /**
     * @lint ignoreReferenceField(_forwardStates)
     */
    _forwardStates :
    {
      invalid : true,
      focused : true,
      undetermined : true,
      checked : true,
      hovered : true
    },

    /**
     * overridden (from MExecutable to keep the icon out of the binding)
     * @lint ignoreReferenceField(_bindableProperties)
     */
    _bindableProperties :
    [
      "enabled",
      "label",
      "toolTipText",
      "value",
      "menu"
    ]
  }
});
/**
 *
 */


/**
 *
 */

qx.Class.define("itx.data.Conversion", {
    type:"static",

    statics: {
        toListData:function (value) {
            var result = [];
            var r = null;
            for (var i = 0; i < value.length; i++) {
                r = value[i];
                result.push([ r.label, null, r.data ]);
            }

            return result;
        }
    }
});
qx.Class.define("itx.ui.table.celleditor.TokenField", {
	extend : qx.core.Object,
	implement : qx.ui.table.ICellEditorFactory,

	properties : {
		/**
		 * function that validates the result
		 * the function will be called with the new value and the old value and is
		 * supposed to return the value that is set as the table value.
		 **/
		validationFunction : {
			check : "Function",
			nullable : true,
			init : null
		},

		rpcInitFunction : {
			check : "Function",
			nullable : true,
			init : null
		},
		
		autocompleteCallback:{
			check:"Function",
			nullable:true,
			init:null
		},
		
		tokenLimit:{
			check:"Integer",
			nullable:false,
			init:1
		},

		tokenCreation : {
			check : "Boolean",
			nullable : false,
			init :false
		}

	},

	members : {
		// interface implementation
		createCellEditor : function(cellInfo) {

			var cellEditor = new itx.ui.form.TokenField();
			cellEditor.setTokenLimit( this.getTokenLimit() );
			cellEditor.setTokenCreation( this.getTokenCreation() );

			
			
			if (this.getAutocompleteCallback() !== null) {
				var callback = this.getAutocompleteCallback();
				cellEditor.setCompletitionCallback(callback);
			}
			
			//old way, deprecated
			var func = this.getRpcInitFunction();
			if( func!==null ){
				func(cellEditor);
			}


			cellEditor.originalValue = cellInfo.value;
			if (cellInfo.value === null || cellInfo.value === undefined) {
				cellInfo.value = "";
			}

			if (cellInfo.value !== "") {
				cellEditor.setValue(qx.lang.Json.parse(cellInfo.value));
			}

			return cellEditor;
		},

		// interface iplementations
		getCellEditorValue : function(cellEditor) {
			var value = cellEditor.getValueAsMap() || "";
			var newValue = qx.lang.Json.stringify(value);

			// validation function will be called with new and old value
			/*var validationFunc = this.getValidationFunction();
			 if (validationFunc) {
			 value = validationFunc( value, cellEditor.originalValue );
			 }
			 */
			return newValue;
		}
	}

});
qx.Class.define("itx.ui.form.AbstractTokenField",{
	extend:qx.ui.core.Widget,
	include : [
		//qx.ui.core.MRemoteChildrenHandling,
		qx.ui.form.MForm
	],
	implement : [
		qx.ui.form.IForm
	],
	type:"abstract",
	
	construct:function(){
		this.base(arguments);
		this.setAppearance("tokenfield");
		this.setBackgroundColor('white');
		
        var layout = new qx.ui.layout.Flow(4, 4, "left");
        this._setLayout(layout);
		
		
		
		/*
		this._dummy = new qx.ui.form.ListItem();
		this._dummy.setEnabled(false);
		this._dummy.setLabel("Buscando...");
		*/
		
		//this.addListener("focusout",function(){ this.cancel();},this);


		
		return;
	},


    events:{
        "loadData":"qx.event.type.Data",
        "tokenAdded":"qx.event.type.Data",
        "tokenRemoved":"qx.event.type.Data",
		"tokenDblclick":"qx.event.type.Data"
    },
	
    properties:{
        appearance:{
            refine:true,
            init:"tokenfield"
        },

        // overridden
        allowGrowX:{
            refine:true,
            init:true
        },
        
	    // overridden
	    focusable :{
	      refine : true,
	      init : true
	    },

        minChars:{
            init:2,
			check:"Integer",
			nullable:false
        },
		
		searchDelay:{
            init:600,
			check:"Integer",
			nullable:false
        },
		
		searchTrigger:{
			init:"auto",
			check:"String",
			nullable:false,
			apply:"_applySearchTrigger"
		},

        tokenLimit:{
            init:1,
			check:"Integer",
			nullable:false
        },

        tokenCreation:{
            init:false,
			check:"Boolean",
			nullable:false
        }
    },
	
	members:{
		_createChildControlImpl : function(id, hash){
			var that=this;
			var control;
			
			switch(id){
				/*case "add-widget":
					control = new qx.ui.form.TextField();
					control.setWidth(100);
					control.setFocusable(false);
					//control.setAnonymous(true);

					this.addListener("keyup",this._onTextFieldKeyUp,this);
					this._add(control);
					break;
				*/
				
				case "list":
					control = this._createListControl();
					/*control.set({
						focusable: false,
						keepFocus: true
					});*/
					
					control.addListener("keyup",this._onListKeyUp,this);
					
					//control.addListener("click",this._addSelectedToken,this);
					//control.setAnonymous(true);
					control.setDecorator(null);
					break;
				
				case "status-label":
					control = new qx.ui.basic.Label('<span style="color:white">Buscando...</span>');
					control.set({
						rich:true,
						padding:[4,4,4,4],
						backgroundColor:'#4A4A4A'
					});
					break;
					
					
				case "popup":
					control = new qx.ui.popup.Popup(new qx.ui.layout.VBox);
					control.setAutoHide(true);
					control.setKeepActive(true);
					
					control.add(this.getChildControl("status-label"));
					control.add(this.getChildControl("list"));
				
					
					//control.setAnonymous(true);
					break;
			}
			
			return control || this.base(arguments, id);
		},
		
		
		/*
		// overridden
		tabFocus : function(){
			var field = this.getChildControl("add-widget");
			field.getFocusElement().focus();
			field.selectAllText();
		},

		// overridden
		focus : function(){
			console.log("focus:");
			this.base(arguments);
			this.getChildControl("add-widget").getFocusElement().focus();
		},
		*/
		
		_applySearchTrigger:function(value){
			this.setSearchDelay(0);
		},
		
        setTokenLimitInfinite:function(){
            this.setTokenLimit(-1);
            this._updateAddWidget();
        },
		
		_updateAddWidget:function(){
			var textField = this.getChildControl("add-widget");
            if ( (this.getTokenCount() < this.getTokenLimit()) || this.getTokenLimit()==-1 ) {
                textField.show();
                textField.setWidth(100);
            }
            else {
                textField.hide();
                textField.setWidth(0);
            }

        },
		_onTextFieldKeyUp:function(e){
			if( !this.getEnabled() ) return;
				
			var that=this;
			
			if( this._timerId===undefined){
				this._timerId=null;
			}
			
            var keyCode = e.getKeyCode();
            var keyIdentifier=e.getKeyIdentifier();

			var textField = this.getChildControl("add-widget");

			if( (this.getSearchTrigger()=="manual" && keyIdentifier=="Enter") || this.getSearchTrigger()=="auto" ){
				clearTimeout(this._timerId);
				this._timerId = setTimeout(function(){
					try {
						var data = textField.getValue();
					}
					catch(e){
						return;
					}
					
					data = typeof(data) == "string" ? data : "";
		
					if (data.length < that.getMinChars() ) {
						that.close();
						return;
					}
					
					that.fireDataEvent("loadData", textField.getValue());
				},that.getSearchDelay());
				
			}
			
            //escape
			if(keyIdentifier=="Escape"){
            //if (keyCode == 27) {
				clearTimeout(this._timerId);
				
				if( this.getChildControl("popup").isVisible() ){
					this.close();
					e.stopPropagation();
					return;
				}
				
				textField.setValue("");
                e.stopPropagation();
                return;
            }

            //return
            if (e.getKeyCode() == 13  && this.getTokenCreation()==true ) {
				clearTimeout(this._timerId);
                this.addToken(textField.getValue());
                this.close();
				textField.setValue("");
                e.stopPropagation();
                return;
            }
			
			if( keyIdentifier=="Enter" && this.getTokenCreation()==false && this.getChildControl("popup").isVisible() ){
				this._setListSelectedRow(0);
				this._addSelectedToken();
				
				e.stopPropagation();
				return;
			}

            //down arrow
            if (e.getKeyCode() == 40) {
				clearTimeout(this._timerId);
				if( this._getListRowCount() > 0 ){
					this._setListSelectedRow(0);
				}
				
				this.getChildControl("list").focus();
                e.stopPropagation();
                return;
            }
			
			//backspace
			if( keyIdentifier=="Backspace"){
				this.removeLastToken();
				this.getChildControl("add-widget").getFocusElement().focus();
			}
			
		},
		
		_onListKeyUp:function(e){
			if( !this.getEnabled() ) return;
				
			var list = this.getChildControl("list");
			var textField = this.getChildControl("add-widget");
			var keyCode = e.getKeyCode();
			
			//escape
			if( keyCode == 27 ){
				this.close();
				textField.setValue("");
				e.stopPropagation();
				this.focus();
				return;
			}
			
            //return
            if (e.getKeyCode() == 13 ) {
				this._addSelectedToken();
                e.stopPropagation();
				this.focus();
                return;
            }
			

		},
		
		open:function(){
			var textField=this.getChildControl("add-widget");
			
			if( !this.getEnabled() ) return;
			
			var popup = this.getChildControl("popup");
			var list = this.getChildControl("list");
			
			var cloc=this.getContentLocation("box");
			var width = cloc.right - cloc.left;
			popup.setWidth( width > list.getWidth() ? width : list.getWidth() );

			var fieldLeft = textField.getContentLocation("box").left;
			popup.setOffsetLeft(cloc.left-fieldLeft);
			
			popup.placeToWidget(textField,true);
			popup.show();
		
		},
		close:function(){
			this.getChildControl("popup").hide();
		},
		
		cancel:function(){
			this.getChildControl("add-widget").setValue("");
			this.removeAll();
			this.close();
		},
		

		
		addToken:function(label, data, extra){
			var textField=this.getChildControl("add-widget");

            var item = new qx.ui.container.Composite();
            item.setAppearance("tokenfield/token-item");

            item.setUserData("data", data);
            item.setUserData("label", label);
            item.setUserData("extra",extra);

            item.setLayout(new qx.ui.layout.HBox(4));
			
			var label=new qx.ui.basic.Label(label);
			//label.setAppearance("tokenfield/token-label");
            item.add( label );
			item.setUserData("labelWidget",label);

            var closeButton = new qx.ui.basic.Label("X");
            closeButton.setAppearance("tokenfield/close-button");
			
			//var closeButton = new  qx.ui.form.Button("x");
			//closeButton.setAppearance("tokenfield/close-button");
			
            closeButton.addListener("click", function (e) {
                this.fireDataEvent("tokenRemoved", item.getUserData("extra") );
                item.destroy();
                e.stopPropagation();
				
                this._updateAddWidget();
				//textField.focus();
				//this.focus();
				this.getChildControl("add-widget").getFocusElement().focus();
            },this);

            item.add(closeButton);
			
			var toolTipText="";
			for(var k in extra){
				if( k.match(/^_/) ) continue;
				toolTipText+="<b>"+k+"</b>: "+extra[k]+"<br/>";
			}
			
			var toolTip = new qx.ui.tooltip.ToolTip(toolTipText);
			toolTip.setRich(true);
			item.setToolTip( toolTip );
			
			var id = Math.random();
			item.setUserData("id",id);
			
			
			item.addListener("dblclick",function(e){
				this.fireDataEvent("tokenDblclick",item.getUserData("extra"));
			},this);
			
            this._addAt(item, this._getChildren().length - 1);

            this._updateAddWidget();
			
			return id;
		},
		
		getTokenById:function(id){
			var childrens = this._getChildren();
			for(var i=0;i<childrens.length;i++){
				if( childrens[i].getUserData("id") == id ) return childrens[i];
			}
			return null;
		},
		updateTokenById:function(id,label,data,extra){
			var token = this.getTokenById(id);
			token.setUserData("data",data);
			token.setUserData("label",label);
			token.setUserData("extra",extra);
			
			token.getUserData("labelWidget").setValue( label );
		},
		
        getTokenCount:function () {
            return this._getChildren().length - 1;
        },
		
		
        removeAll:function () {
            var childrens = this._getChildren();
            var child = null;

            for (var i = childrens.length; i > 0; i--) {
                child = childrens[i - 1];
                if (child.classname == "qx.ui.container.Composite") {
                    child.destroy();
                }
            }

            this._updateAddWidget();
        },
		
		removeLastToken:function(){
            var childrens = this._getChildren();
            var child = null;

            for (var i = childrens.length; i > 0; i--) {
                child = childrens[i - 1];
                if (child.classname == "qx.ui.container.Composite") {
					this.fireDataEvent("tokenRemoved", child.getUserData("extra") );
                    child.destroy();
					break;
                }
            }
			
            this._updateAddWidget();
		},
		
		
		getValue:function(){
            var result = [];
            var childrens = this._getChildren();
            var child = null;

            for (var i = 0; i < childrens.length; i++) {
                child = childrens[i];
                if (child.classname == "qx.ui.container.Composite") {
                    result.push(child.getUserData("data"));
                }
            }
            return result;
		},
        getValueAsMap: function(){
            var result = [];
            var childrens = this._getChildren();
            var child = null;

            for (var i = 0; i < childrens.length; i++) {
                child = childrens[i];
                if (child.classname == "qx.ui.container.Composite") {
                    result.push({
                    	data:child.getUserData("data"),
                    	label:child.getUserData("label")
                    });
                }
            }
            
            return result;
        },
        getValueAsString:function () {
            return this.getValue().join(",");
        },
		
		getLabels:function(){
            var result = [];
            var childrens = this._getChildren();
            var child = null;

            for (var i = 0; i < childrens.length; i++) {
                child = childrens[i];
                if (child.classname == "qx.ui.container.Composite") {
                    result.push(child.getUserData("label"));
                }
            }
            return result;
		},
		
		getText:function(){
			return this.getChildControl("add-widget").getValue();
		},
		
		setValue:function(values){
			this.removeAll();
			if( values===undefined || values===null ) return;
			
			if( !qx.lang.Type.isArray(values) ){
				values = [ values ];
			}
			
			var r = null;
			for(var i=0;i<values.length;i++){
				r=values[i];
				if( r.data===undefined || r.label===undefined ){
					continue;
				}
				
				this.addToken(r.label,r.data,r);
			}
		},
		
		setRecords:function(records,extra){
			var list = this.getChildControl("list");
			this._setListRecords(records,extra);
		},
		
		_addSelectedToken:function(e){
			//console.log("_addSelectedToken",e);
			//e.stopPropagation();
			//e.preventDefault();
			/*
			if(e){
				console.log( e.getTarget().classname );
				console.log( e.getCurrentTarget().classname );

				e.stop();
				e.preventDefault();
				e.stopPropagation();
				return;
				
			}
			*/
			
			var list=this.getChildControl("list");
			var textField=this.getChildControl("add-widget");
			
			var r = this._getListSelectedRecord();
			this.addToken(r.label, r.data,r.extra);
			this.close();
			textField.setValue("");
			this.fireDataEvent("tokenAdded",r);
		},

		setAutocompleteFromRpc:function(rpc,methodName,extra){
			this.autocompleteFromRpc(rpc,methodName,extra);
		},
		
        autocompleteFromRpc:function (rpc, methodName, extra) {
			//extra = extra!==undefined ? extra:{};
			extra = extra || {};
			
			qx.lang.Object.mergeWith(extra,{
				beforeComplete:function (data) {
					return;
				},
				afterComplete:function(result){
					return;
				}
			},false);
			

			
			
            this.addListener("loadData", function (e) {
				this.open();
				
				var statusLabel=this.getChildControl("status-label");
				statusLabel.setValue("Buscando...");
				statusLabel.show();

                var data = e.getData();
                var p = {
                    filter:data
                };

                extra.beforeComplete(p);
                qx.lang.Object.mergeWith(p, extra.vars,false);

                
                
                var rpcObject = null;
                if( typeof rpc ==="string"){
                	rpcObject = new qx.io.remote.Rpc(rpc);
                }
                else {
                	rpcObject = rpc;
                }
                
				var res=null;
                
				try {
					res = rpcObject.callSync(methodName, p);
				}
				catch(e){
					
					this.setRecords([]);
					
					var message="Fallo consultando fuente de datos";
					/*
					try {
						message=e.rpcdetails.message;
					}
					catch(e){
						message="Failed loading data";
					}
					*/
					
					statusLabel.hide();
					itx.ui.Util.critical(message);
					return;
				}
                
				
				
				
				extra.afterComplete(res);
				
				if( res.length > 0 ){
					statusLabel.exclude();
				}
				else {
					statusLabel.setValue('<span style="color:white">No se encontró información</span>');
				}
				
				if( res.records !== undefined ){
					this.setRecords(res.records,res.extra);
				}
				else {
					this.setRecords(res);
				}
				
				
				//this.open();

            },this);
			
        },
        setCompletitionCallback:function(callback){
        	
        	this.addListener("loadData",function(e){
        		var res = qx.lang.Array.clone( callback(e.getData()) );
				this.setRecords(res);
				this.open();
        	},this);
        	
        }
	},
	
	destruct:function(){
		//console.log("destruct");
		//this.getChildControl("list").destroy();
		/*
		this._data = this._moreData = null;
		this._disposeObjects("_buttonOk", "_buttonCancel");
		this._disposeArray("_children");
		this._disposeMap("_registry");
		*/
	}
});
/* ************************************************************************

   qooxdoo - the new era of web development
   http://qooxdoo.org

   Copyright:
     2008 Dihedrals.com, http://www.dihedrals.com

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Chris Banford (zermattchris)
     * Fabian Jakobs (fjakobs)

************************************************************************ */

/**
 * A basic layout, which supports positioning of child widgets in a 'flowing'
 * manner, starting at the container's top/left position, placing children left to right
 * (like a HBox) until the there's no remaining room for the next child. When
 * out of room on the current line of elements, a new line is started, cleared
 * below the tallest child of the preceding line -- a bit like using 'float'
 * in CSS, except that a new line wraps all the way back to the left.
 *
 * *Features*
 *
 * <ul>
 * <li> Reversing children order </li>
 * <li> Manual line breaks </li>
 * <li> Horizontal alignment of lines </li>
 * <li> Vertical alignment of individual widgets within a line </li>
 * <li> Margins with horizontal margin collapsing </li>
 * <li> Horizontal and vertical spacing </li>
 * <li> Height for width calculations </li>
 * <li> Auto-sizing </li>
 * </ul>
 *
 * *Item Properties*
 *
 * <ul>
 * <li><strong>lineBreak</strong> <em>(Boolean)</em>: If set to <code>true</code>
 *   a forced line break will happen after this child widget.
 * </li>
 * <li><strong>stretch</strong> <em>(Boolean)</em>: If set to <code>true</code>
 *   the widget will be stretched to the remaining line width. This requires
 *   lineBreak to be true.
 * </li>

 * </ul>
 *
 * *Example*
 *
 * Here is a little example of how to use the Flow layout.
 *
 * <pre class="javascript">
 *  var flowlayout = new qx.ui.layout.Flow();
 *
 *  flowlayout.setAlignX( "center" );  // Align children to the X axis of the container (left|center|right)
 *
 *  var container = new qx.ui.container.Composite(flowlayout);
 *  this.getRoot().add(container, {edge: 0});
 *
 *  var button1 = new qx.ui.form.Button("1. First Button", "flowlayout/test.png");
 *  container.add(button1);
 *
 *  var button2 = new qx.ui.form.Button("2. Second longer Button...", "flowlayout/test.png");
 *  // Have this child create a break in the current Line (next child will always start a new Line)
 *  container.add(button2, {lineBreak: true});
 *
 *  var button3 = new qx.ui.form.Button("3rd really, really, really long Button", "flowlayout/test.png");
 *  button3.setHeight(100);  // tall button
 *  container.add(button3);
 *
 *  var button4 = new qx.ui.form.Button("Number 4", "flowlayout/test.png");
 *  button4.setAlignY("bottom");
 *  container.add(button4);
 *
 *  var button5 = new qx.ui.form.Button("20px Margins around the great big 5th button!");
 *  button5.setHeight(100);  // tall button
 *  button5.setMargin(20);
 *  container.add(button5, {lineBreak: true});    // Line break after this button.
 *
 *  var button6 = new qx.ui.form.Button("Number 6", "flowlayout/test.png");
 *  button6.setAlignY("middle");  // Align this child to the vertical center of this line.
 *  container.add(button6);
 *
 *  var button7 = new qx.ui.form.Button("7th a wide, short button", "flowlayout/test.png");
 *  button7.setMaxHeight(20);  // short button
 *  container.add(button7);
 * </pre>
 *
 * *External Documentation*
 *
 * <a href='http://manual.qooxdoo.org/${qxversion}/pages/layout/flow.html'>
 * Extended documentation</a> and links to demos of this layout in the qooxdoo manual.
 */
qx.Class.define("qx.ui.layout.Flow",
{
  extend : qx.ui.layout.Abstract,


  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  /**
   * @param spacingX {Integer?0} The spacing between child widgets {@link #spacingX}.
   * @param spacingY {Integer?0} The spacing between the lines {@link #spacingY}.
   * @param alignX {String?"left"} Horizontal alignment of the whole children
   *     block {@link #alignX}.
   */
  construct : function(spacingX, spacingY, alignX)
  {
    this.base(arguments);

    if (spacingX) {
      this.setSpacingX(spacingX);
    }

    if (spacingY) {
      this.setSpacingY(spacingY);
    }

    if (alignX) {
      this.setAlignX(alignX);
    }
  },



  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

  properties :
  {
    /**
     * Horizontal alignment of the whole children block. The horizontal
     * alignment of the child is completely ignored in HBoxes (
     * {@link qx.ui.core.LayoutItem#alignX}).
     */
    alignX :
    {
      check : [ "left", "center", "right" ],
      init : "left",
      apply : "_applyLayoutChange"
    },

    /**
     * Vertical alignment of each child. Can be overridden through
     * {@link qx.ui.core.LayoutItem#alignY}.
     */
    alignY :
    {
      check : [ "top", "middle", "bottom"],
      init : "top",
      apply : "_applyLayoutChange"
    },

    /** Horizontal spacing between two children */
    spacingX :
    {
      check : "Integer",
      init : 0,
      apply : "_applyLayoutChange"
    },

    /**
     * The vertical spacing between the lines.
     */
    spacingY :
    {
      check : "Integer",
      init : 0,
      apply : "_applyLayoutChange"
    },

    /** Whether the actual children list should be laid out in reversed order. */
    reversed :
    {
      check : "Boolean",
      init : false,
      apply : "_applyLayoutChange"
    }

  },



  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    /*
    ---------------------------------------------------------------------------
      LAYOUT INTERFACE
    ---------------------------------------------------------------------------
    */

    // overridden
    verifyLayoutProperty : qx.core.Environment.select("qx.debug",
    {
      "true" : function(item, name, value) {
        var validProperties = ["lineBreak", "stretch"];
        this.assertInArray(name, validProperties, "The property '"+name+"' is not supported by the flow layout!" );
      },

      "false" : null
    }),


    // overridden
    connectToWidget : function(widget)
    {
      this.base(arguments, widget);

      // Necessary to be able to calculate the lines for the flow layout.
      // Otherwise the layout calculates the needed width and height by using
      // only one line of items which is leading to the wrong height. This
      // wrong height does e.g. surpress scrolling since the scroll pane does
      // not know about the correct needed height.
      if (widget) {
        widget.setAllowShrinkY(false);
      }
    },


    /**
     * The FlowLayout tries to add as many Children as possible to the current 'Line'
     * and when it sees that the next Child won't fit, it starts on a new Line, continuing
     * until all the Children have been added.
     * To enable alignX "left", "center", "right" renderLayout has to calculate the positions
     * of all a Line's children before it draws them.
     *
     * @param availWidth {Integer} Final width available for the content (in pixel)
     * @param availHeight {Integer} Final height available for the content (in pixel)
     * @param padding {Map} Map containing the padding values. Keys:
     * <code>top</code>, <code>bottom</code>, <code>left</code>, <code>right</code>
     */
    renderLayout : function(availWidth, availHeight, padding)
    {
      var children = this._getLayoutChildren();

      if (this.getReversed()) {
        children = children.concat().reverse();
      }

      var lineCalculator = new qx.ui.layout.LineSizeIterator(
        children,
        this.getSpacingX()
      );

      var lineTop = padding.top;
      while (lineCalculator.hasMoreLines())
      {
        var line = lineCalculator.computeNextLine(availWidth);
        this.__renderLine(line, lineTop, availWidth, padding);
        lineTop += line.height + this.getSpacingY();
      }
    },


    /**
     * Render a line in the flow layout
     *
     * @param line {Map} A line configuration as returned by
     *    {@link LineSizeIterator#computeNextLine}.
     * @param lineTop {Integer} The line's top position
     * @param availWidth {Integer} The available line width
     * @param padding {Map} Map containing the padding values. Keys:
     * <code>top</code>, <code>bottom</code>, <code>left</code>, <code>right</code>
     */
    __renderLine : function(line, lineTop, availWidth, padding)
    {
      var util = qx.ui.layout.Util;

      var left = padding.left;
      if (this.getAlignX() != "left") {
        left = padding.left + availWidth - line.width;
        if (this.getAlignX() == "center") {
          left = padding.left + Math.round((availWidth - line.width) / 2);
        }
      }

      for (var i=0; i<line.children.length; i++)
      {
        var child = line.children[i];
        var size = child.getSizeHint();
        var marginTop = child.getMarginTop();
        var marginBottom = child.getMarginBottom();

        var top = util.computeVerticalAlignOffset(
          child.getAlignY() || this.getAlignY(),
          marginTop + size.height + marginBottom,
          line.height,
          marginTop, marginBottom
        );

        var layoutProps = child.getLayoutProperties();
        if (layoutProps.stretch && layoutProps.stretch) {
          size.width += availWidth - line.width;
        }

        child.renderLayout(
          left + line.gapsBefore[i],
          lineTop + top,
          size.width,
          size.height
        );

        left += line.gapsBefore[i] + size.width;
      }
    },


    // overridden
    _computeSizeHint : function() {
      return this.__computeSize(Infinity);
    },


    // overridden
    hasHeightForWidth : function() {
      return true;
    },


    // overridden
    getHeightForWidth : function(width) {
      return this.__computeSize(width).height;
    },


    /**
     * Returns the list of children fitting in the last row of the given width.
     * @param width {Number} The width to use for the calculation.
     * @return {Array} List of children in the first row.
     */
    getLastLineChildren : function(width) {
      var lineCalculator = new qx.ui.layout.LineSizeIterator(
        this._getLayoutChildren(),
        this.getSpacingX()
      );

      var lineData = [];
      while (lineCalculator.hasMoreLines()) {
        lineData = lineCalculator.computeNextLine(width).children;
      }

      return lineData;
    },


    /**
     * Compute the preferred size optionally constrained by the available width
     *
     * @param availWidth {Integer} The available width
     * @return {Map} Map containing the preferred height and width of the layout
     */
    __computeSize : function(availWidth)
    {
      var lineCalculator = new qx.ui.layout.LineSizeIterator(
        this._getLayoutChildren(),
        this.getSpacingX()
      );

      var height = 0;
      var width = 0;
      var lineCount = 0;

      while (lineCalculator.hasMoreLines())
      {
        var line = lineCalculator.computeNextLine(availWidth);
        lineCount += 1;
        width = Math.max(width, line.width);
        height += line.height;
      }

      return {
        width : width,
        height : height + this.getSpacingY() * (lineCount-1)
      };
    }
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development
   http://qooxdoo.org

   Copyright:
     2008 Dihedrals.com, http://www.dihedrals.com

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Chris Banford (zermattchris)
     * Fabian Jakobs (fjakobs)

************************************************************************ */

/**
 * This class iterates over the lines in a flow layout.
 *
 * @internal
 */
qx.Class.define("qx.ui.layout.LineSizeIterator",
{
  extend : Object,

  /**
   * @param children {qx.ui.core.Widget[]} The children of the flow layout to
   *    compute the lines from
   * @param spacing {Integer} The horizontal spacing between the children
   */
  construct : function(children, spacing)
  {
    this.__children = children;
    this.__spacing = spacing;

    this.__hasMoreLines = children.length > 0;
    this.__childIndex = 0;
  },

  members :
  {
    __children : null,
    __spacing : null,
    __hasMoreLines : null,
    __childIndex : null,


    /**
     * Computes the properties of the next line taking the available width into
     * account
     *
     * @param availWidth {Integer} The available width for the next line
     * @return {Map} A map containing the line's properties.
     */
    computeNextLine : function(availWidth)
    {
      var availWidth = availWidth || Infinity;

      if (!this.__hasMoreLines) {
        throw new Error("No more lines to compute");
      }

      var children = this.__children;
      var lineHeight = 0;
      var lineWidth = 0;
      var lineChildren = [];
      var gapsBefore = [];

      for (var i=this.__childIndex; i<children.length; i++)
      {
        var child = children[i];
        var size = child.getSizeHint();

        var gapBefore = this.__computeGapBeforeChild(i);
        var childWidth = size.width + gapBefore;
        var isFirstChild = i == this.__childIndex;
        if (!isFirstChild && lineWidth + childWidth > availWidth)
        {
          this.__childIndex = i;
          break;
        }

        var childHeight = size.height + child.getMarginTop() + child.getMarginBottom();
        lineChildren.push(child);
        gapsBefore.push(gapBefore);
        lineWidth += childWidth;
        lineHeight = Math.max(lineHeight, childHeight);

        if (child.getLayoutProperties().lineBreak) {
          this.__childIndex = i+1;
          break;
        }
      }

      if (i >= children.length) {
        this.__hasMoreLines = false;
      }

      return {
        height: lineHeight,
        width: lineWidth,
        children: lineChildren,
        gapsBefore : gapsBefore
      }
    },


    /**
     * Computes the gap before the child at the given index
     *
     * @param childIndex {Integer} The index of the child widget
     * @return {Integer} The gap before the given child
     */
    __computeGapBeforeChild : function(childIndex)
    {
      var isFirstInLine = childIndex == this.__childIndex
      if (isFirstInLine) {
        return this.__children[childIndex].getMarginLeft();
      } else {
        return Math.max(
          this.__children[childIndex-1].getMarginRight(),
          this.__children[childIndex].getMarginLeft(),
          this.__spacing
        )
      }
    },


    /**
     * Whether there are more lines
     *
     * @return {Boolean} Whether there are more lines
     */
    hasMoreLines : function() {
      return this.__hasMoreLines;
    }
  }
})
qx.Class.define("itx.ui.form.TokenField",{
	extend:itx.ui.form.AbstractTokenField,
	
	construct:function(){
		this.base(arguments);
		
		this._createChildControl("add-widget");
		var textField=this.getChildControl("add-widget");
		
		this.addListener("focusin", function(e) {
			this.getChildControl("add-widget").getFocusElement().focus();
		}, this);
		
		return;
	},
	members:{
		_createChildControlImpl : function(id, hash){
			var control;
			
			switch(id){
				case "add-widget":
					control = new qx.ui.form.TextField();
					control.setWidth(100);
					control.setFocusable(false);
					control.setAppearance("tokenfield/text-field");
				
					this.addListener("keyup",this._onTextFieldKeyUp,this);
					this._add(control);
					break;
			}
			
			return control || this.base(arguments, id);
		},
			
		_createListControl:function(){
			var control = new qx.ui.form.List();
			control.set({
				maxHeight:300
			});
			
			control.addListener("click",this._addSelectedToken,this);
			return control;
		},
		
		_getListRowCount:function(){
			var list = this.getChildControl("list");
			return list.getChildren().length;
		},
		
		_setListSelectedRow:function(row){
			var list = this.getChildControl("list");
			var childList=list.getChildren();
			list.setSelection([childList[0]]);
		},
		
		_getListSelectedRecord:function(){
			var list = this.getChildControl("list");
			var item = list.getSelection()[0];
			if( item === undefined ){
				return;
			}
			
			return {
				data:item.getModel(),
				label:item.getLabel(),
				extra:item.getUserData("extra")
			};
		},
		
		_setListRecords:function(records){
			var list = this.getChildControl("list");
			list.removeAll();
			
			if( records===undefined || records.length==0 ){
				return;
			}

			var listItem=null;
			var r=null;
            for (var i = 0; i < records.length; i++) {
                var r = records[i];

                listItem = new qx.ui.form.ListItem(r.label, null, r.data);
                listItem.setUserData("extra",r);
                list.add(listItem);
            }
		}
	}
});
qx.Class.define("itx.ui.table.celleditor.DateField", {
	extend : qx.core.Object,
	implement : qx.ui.table.ICellEditorFactory,

	properties : {
		validationFunction : {
			check : "Function",
			nullable : true,
			init : null
		}

	},

	members : {
		// interface implementation
		createCellEditor : function(cellInfo) {
			var cellEditor = new qx.ui.form.DateField();
			
			cellEditor.originalValue = cellInfo.value;
			if (cellInfo.value === null || cellInfo.value === undefined) {
				cellInfo.value = "";
			}

			if (cellInfo.value !== "") {
                var tmp = cellInfo.value.split("-");
                tmp[1] = parseInt(tmp[1],10)-1; //javascript 0 index months
				cellEditor.setValue(  new Date( tmp[0],tmp[1],tmp[2]) );
			}
			
			cellEditor.setDateFormat( new qx.util.format.DateFormat("y-MM-d") );
			
			cellEditor.addListener("focus",function(){
				cellEditor.open();
			});
			return cellEditor;
		},

		// interface iplementations
		getCellEditorValue : function(cellEditor) {
			var dateObject = cellEditor.getValue();
        	if( dateObject === null ){
        		return "";
        	}
        	
        	var month = String( dateObject.getMonth()+1 );
        	while( month.length < 2 ){
        		month = "0"+month;
        	}
        	
        	return dateObject.getFullYear()+"-"+month+"-"+dateObject.getDate();
		}
	}

});
/* ************************************************************************

    qooxdoo - the new era of web development

    http://qooxdoo.org

    Copyright:
      2007 by Tartan Solutions, Inc, http://www.tartansolutions.com

    License:
      LGPL 2.1: http://www.gnu.org/licenses/lgpl.html
      EPL: http://www.eclipse.org/org/documents/epl-v10.php

    Authors:
      * Dan Hummon

************************************************************************ */

/**
 * The conditional cell renderer allows special per cell formatting based on
 * conditions on the cell's value.
 *
 * @require(qx.util.format.NumberFormat)
 */
qx.Class.define("qx.ui.table.cellrenderer.Conditional",
{
  extend : qx.ui.table.cellrenderer.Default,




  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  /**
   * @param align {String|null}
   *   The default text alignment to format the cell with by default.
   *
   * @param color {String|null}
   *   The default font color to format the cell with by default.
   *
   * @param style {String|null}
   *   The default font style to format the cell with by default.
   *
   * @param weight {String|null}
   *   The default font weight to format the cell with by default.
   */
  construct : function(align, color, style, weight)
  {
    this.base(arguments);

    this.numericAllowed = ["==", "!=", ">", "<", ">=", "<="];
    this.betweenAllowed = ["between", "!between"];
    this.conditions = [];

    this.__defaultTextAlign = align || "";
    this.__defaultColor = color || "";
    this.__defaultFontStyle = style || "";
    this.__defaultFontWeight = weight || "";
  },




  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    __defaultTextAlign : null,
    __defaultColor : null,
    __defaultFontStyle : null,
    __defaultFontWeight : null,


    /**
     * Applies the cell styles to the style map.
     * @param condition {Array} The matched condition
     * @param style {Map} map of already applied styles.
     */
    __applyFormatting : function(condition, style)
    {
      if (condition[1] != null) {
        style["text-align"] = condition[1];
      }

      if (condition[2] != null) {
        style["color"] = condition[2];
      }

      if (condition[3] != null) {
        style["font-style"] = condition[3];
      }

      if (condition[4] != null) {
        style["font-weight"] = condition[4];
      }
    },


    /**
     * The addNumericCondition method is used to add a basic numeric condition to
     * the cell renderer.
     *
     * Note: Passing null is different from passing an empty string in the align,
     * color, style and weight arguments. Null will allow pre-existing formatting
     * to pass through, where an empty string will clear it back to the default
     * formatting set in the constructor.
     *
     *
     * @param condition {String} The type of condition. Accepted strings are "==", "!=", ">", "<", ">=",
     *     and "<=".
     * @param value1 {Integer} The value to compare against.
     * @param align {String|null} The text alignment to format the cell with if the condition matches.
     * @param color {String|null} The font color to format the cell with if the condition matches.
     * @param style {String|null} The font style to format the cell with if the condition matches.
     * @param weight {String|null} The font weight to format the cell with if the condition matches.
     * @param target {String|null} The text value of the column to compare against. If this is null,
     *     comparisons will be against the contents of this cell.
     * @throws {Error} If the condition can not be recognized or value is null.
     */
    addNumericCondition : function(condition, value1, align, color, style, weight, target)
    {
      var temp = null;

      if (qx.lang.Array.contains(this.numericAllowed, condition))
      {
        if (value1 != null) {
          temp = [condition, align, color, style, weight, value1, target];
        }
      }

      if (temp != null) {
        this.conditions.push(temp);
      } else {
        throw new Error("Condition not recognized or value is null!");
      }
    },


    /**
     * The addBetweenCondition method is used to add a between condition to the
     * cell renderer.
     *
     * Note: Passing null is different from passing an empty string in the align,
     * color, style and weight arguments. Null will allow pre-existing formatting
     * to pass through, where an empty string will clear it back to the default
     * formatting set in the constructor.
     *
     *
     * @param condition {String} The type of condition. Accepted strings are "between" and "!between".
     * @param value1 {Integer} The first value to compare against.
     * @param value2 {Integer} The second value to compare against.
     * @param align {String|null} The text alignment to format the cell with if the condition matches.
     * @param color {String|null} The font color to format the cell with if the condition matches.
     * @param style {String|null} The font style to format the cell with if the condition matches.
     * @param weight {String|null} The font weight to format the cell with if the condition matches.
     * @param target {String|null} The text value of the column to compare against. If this is null,
     *     comparisons will be against the contents of this cell.
     * @throws {Error} If the condition can not be recognized or value is null.
     */
    addBetweenCondition : function(condition, value1, value2, align, color, style, weight, target)
    {
      if (qx.lang.Array.contains(this.betweenAllowed, condition))
      {
        if (value1 != null && value2 != null) {
          var temp = [condition, align, color, style, weight, value1, value2, target];
        }
      }

      if (temp != null) {
        this.conditions.push(temp);
      } else {
        throw new Error("Condition not recognized or value1/value2 is null!");
      }
    },


    /**
     * The addRegex method is used to add a regular expression condition to the
     * cell renderer.
     *
     * Note: Passing null is different from passing an empty string in the align,
     * color, style and weight arguments. Null will allow pre-existing formatting
     * to pass through, where an empty string will clear it back to the default
     * formatting set in the constructor.
     *
     *
     * @param regex {String} The regular expression to match against.
     * @param align {String|null} The text alignment to format the cell with if the condition matches.
     * @param color {String|null} The font color to format the cell with if the condition matches.
     * @param style {String|null} The font style to format the cell with if the condition matches.
     * @param weight {String|null} The font weight to format the cell with if the condition matches.
     * @param target {String|null} The text value of the column to compare against. If this is null,
     *     comparisons will be against the contents of this cell.
     * @throws {Error} If the regex is null.
     */
    addRegex : function(regex, align, color, style, weight, target)
    {
      if (regex != null) {
        var temp = ["regex", align, color, style, weight, regex, target];
      }

      if (temp != null) {
        this.conditions.push(temp);
      } else {
        throw new Error("regex cannot be null!");
      }
    },


    /**
     * Overridden; called whenever the cell updates. The cell will iterate through
     * each available condition and apply formatting for those that
     * match. Multiple conditions can match, but later conditions will override
     * earlier ones. Conditions with null values will stack with other conditions
     * that apply to that value.
     *
     * @param cellInfo {Map} The information about the cell.
     *          See {@link qx.ui.table.cellrenderer.Abstract#createDataCellHtml}.
     * @return {Map}
     */
    _getCellStyle : function(cellInfo)
    {
      var tableModel = cellInfo.table.getTableModel();
      var i;
      var cond_test;
      var compareValue;

      var style =
      {
        "text-align": this.__defaultTextAlign,
        "color": this.__defaultColor,
        "font-style": this.__defaultFontStyle,
        "font-weight": this.__defaultFontWeight
      };

      for (i in this.conditions)
      {
        cond_test = false;

        if (qx.lang.Array.contains(this.numericAllowed, this.conditions[i][0]))
        {
          if (this.conditions[i][6] == null) {
            compareValue = cellInfo.value;
          } else {
            compareValue = tableModel.getValueById(this.conditions[i][6], cellInfo.row);
          }

          switch(this.conditions[i][0])
          {
            case "==":
              if (compareValue == this.conditions[i][5]) {
                cond_test = true;
              }

              break;

            case "!=":
              if (compareValue != this.conditions[i][5]) {
                cond_test = true;
              }

              break;

            case ">":
              if (compareValue > this.conditions[i][5]) {
                cond_test = true;
              }

              break;

            case "<":
              if (compareValue < this.conditions[i][5]) {
                cond_test = true;
              }

              break;

            case ">=":
              if (compareValue >= this.conditions[i][5]) {
                cond_test = true;
              }

              break;

            case "<=":
              if (compareValue <= this.conditions[i][5]) {
                cond_test = true;
              }

              break;
          }
        }
        else if (qx.lang.Array.contains(this.betweenAllowed, this.conditions[i][0]))
        {
          if (this.conditions[i][7] == null) {
            compareValue = cellInfo.value;
          } else {
            compareValue = tableModel.getValueById(this.conditions[i][7], cellInfo.row);
          }

          switch(this.conditions[i][0])
          {
            case "between":
              if (compareValue >= this.conditions[i][5] && compareValue <= this.conditions[i][6]) {
                cond_test = true;
              }

              break;

            case "!between":
              if (compareValue < this.conditions[i][5] || compareValue > this.conditions[i][6]) {
                cond_test = true;
              }

              break;
          }
        }
        else if (this.conditions[i][0] == "regex")
        {
          if (this.conditions[i][6] == null) {
            compareValue = cellInfo.value;
          } else {
            compareValue = tableModel.getValueById(this.conditions[i][6], cellInfo.row);
          }

          var the_pattern = new RegExp(this.conditions[i][5], 'g');
          cond_test = the_pattern.test(compareValue);
        }

        // Apply formatting, if any.
        if (cond_test == true) {
          this.__applyFormatting(this.conditions[i], style);
        }
      }

      var styleString = [];
      for(var key in style) {
        if (style[key]) {
          styleString.push(key, ":", style[key], ";");
        }
      }
      return styleString.join("");
    }
  },

  /*
  *****************************************************************************
     DESTRUCTOR
  *****************************************************************************
  */

  destruct : function() {
    this.numericAllowed = this.betweenAllowed = this.conditions = null;
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2007 OpenHex SPRL, http://www.openhex.org

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Gaetan de Menten (ged)

************************************************************************ */

/**
 * Specific data cell renderer for numbers.
 */
qx.Class.define("qx.ui.table.cellrenderer.Number",
{
  extend : qx.ui.table.cellrenderer.Conditional,


  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

  properties :
  {
    /**
     * NumberFormat used to format data. If the numberFormat contains a
     * prefix and/or postfix containing characters which needs to be escaped,
     * those need to be given to the numberFormat in their escaped form
     * because no escaping happens at the cellrenderer level.
     */
    numberFormat :
    {
      check : "qx.util.format.NumberFormat",
      init : null,
      nullable : true
    }
  },


  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    _getContentHtml : function(cellInfo)
    {
      var nf = this.getNumberFormat();

      if (nf)
      {
        if (cellInfo.value || cellInfo.value == 0) {
          // I don't think we need to escape the resulting string, as I
          // don't know of any decimal or separator which use a character
          // which needs escaping. It is much more plausible to have a
          // prefix, postfix containing such characters but those can be
          // (should be) added in their escaped form to the number format.
          return nf.format(cellInfo.value);
        } else {
          return "";
        }
      }
      else
      {
        return cellInfo.value == 0 ? "0" : (cellInfo.value || "");
      }
    },


    // overridden
    _getCellClass : function(cellInfo) {
      return "qooxdoo-table-cell qooxdoo-table-cell-right";
    }
  }
});
/* ************************************************************************

    qooxdoo - the new era of web development

    http://qooxdoo.org

    Copyright:
      2007 by Christian Boulanger

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

    Authors:
      * Christian Boulanger (cboulanger)

************************************************************************ */


/**
 * The cell will use, if given, the
 * replaceMap property and/or the replaceFunction to look up labels for a
 * specific cell value. if the replaceMap, which does not need to be used but
 * takes precedence if given, has no entry for a specific value, you can implement
 * a fallback lookup in the replacementFunction, or use the replacementFunction exclusively.
 *
 * In editable cells, you need to make sure that the method returning the data
 * to the data storage (for example, a database backend) translates the replaced
 * cell value (the label) back into the corresponding value. Thus, both map and
 * function MUST also take care of the reverse translation of labels into
 * values. Example: if you have a field that should display "Active" on a "1"
 * value and "Inactive" on a "0" value, you must use the following map:
 *
 * <pre class='javascript'>
 * {
 *   0 : "Inactive",
 *   1 : "Active",
 *   "Inactive" : 0,
 *   "Active" : 1
 * }
 * </pre>
 *
 * You can use the addReversedReplaceMap() method to do this for you:
 * <pre class='javascript'>
 * var propertyCellRenderer = new qx.ui.table.cellrenderer.Replace;
 * propertyCellRenderer.setReplaceMap({
 *    1 : "Active",
 *   0 : "Inactive",
 *   2  : "Waiting",
 *   'admin' : "System Administrator",
 *   'manager' : "User Manager",
 *   'user' : "Website User"
 * });
 * propertyCellRenderer.addReversedReplaceMap();
 * </pre>
 *
 * @param cellInfo {Map} The information about the cell.
 *          See {@link qx.ui.table.cellrenderer.Abstract#createDataCellHtml}.
 * @return {String}
 */
qx.Class.define("qx.ui.table.cellrenderer.Replace",
{
  extend : qx.ui.table.cellrenderer.Default,

  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

  properties :
  {

    /** a hashmap which is used to replace values by labels */
    replaceMap :
    {
      check : "Object",
      nullable : true,
      init : null
    },

    /**
     * function that provides the label for a specific value
     **/
    replaceFunction :
    {
      check : "Function",
      nullable : true,
      init : null
    }

  },


  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    // overridden
    _getContentHtml : function(cellInfo)
    {
      var value         = cellInfo.value;
      var replaceMap    = this.getReplaceMap();
      var replaceFunc   = this.getReplaceFunction();
      var label;

      // use map
      if ( replaceMap  )
      {
        label = replaceMap[value];
        if ( typeof label != "undefined" )
        {
          cellInfo.value = label;
          return qx.bom.String.escape(this._formatValue(cellInfo));
        }
      }

      // use function
      if ( replaceFunc )
      {
        cellInfo.value = replaceFunc (value);
      }
      return qx.bom.String.escape(this._formatValue(cellInfo));
    },

    /**
     * adds a reversed replaceMap to itself to translate labels back to the original values
     * @return {Boolean} <code>true</code>
     */
    addReversedReplaceMap : function()
    {
       var map = this.getReplaceMap();
       for (var key in map )
       {
         var value = map[key];
         map[value] = key;
       }
       return true;
    }
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2006 STZ-IDA, Germany, http://www.stz-ida.de

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Til Schneider (til132)
     * Carsten Lergenmueller (carstenl)

************************************************************************ */

/**
 * A template class for cell renderer, which display images. Concrete
 * implementations must implement the method {@link #_identifyImage}.
 */
qx.Class.define("qx.ui.table.cellrenderer.AbstractImage",
{
  extend : qx.ui.table.cellrenderer.Abstract,
  type : "abstract",



  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  construct : function()
  {
    this.base(arguments);

    var clazz = this.self(arguments);
    if (!clazz.stylesheet)
    {
      clazz.stylesheet = qx.bom.Stylesheet.createElement(
        ".qooxdoo-table-cell-icon {" +
        "  text-align:center;" +
        "  padding-top:1px;" +
        "}"
      );
    }
  },


  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */
  properties :
  {
    /**
     * Whether to repeat or scale the image.
     *
     * @param repeat {String}
     *   One of
     *     <code>scale</code>,
     *     <code>scale-x</code>,
     *     <code>scale-y</code>,
     *     <code>repeat</code>,
     *     <code>repeat-x</code>,
     *     <code>repeat-y</code>,
     *     <code>no-repeat</code>
    */
    repeat :
    {
      check : function(value)
      {
        var valid =
          [
            "scale",
            "scale-x",
            "scale-y",
            "repeat",
            "repeat-x",
            "repeat-y",
            "no-repeat"
          ];
        return qx.lang.Array.contains(valid, value);
      },
      init  : "no-repeat"
    }
  },


  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    __defaultWidth : 16,
    __defaultHeight : 16,
    __imageData : null,

    // overridden
    _insetY : 2,

    /**
     * Identifies the Image to show. This is a template method, which must be
     * implemented by sub classes.
     *
     * @abstract
     * @param cellInfo {Map} The information about the cell.
     *          See {@link qx.ui.table.cellrenderer.Abstract#createDataCellHtml}.
     * @return {Map} A map having the following attributes:
     *           <ul>
     *           <li>
     *             "url": (type string) must be the URL of the image to show.
     *             The url given must either be managed by the {@link qx.util.ResourceManager}
     *             or pre-loaded with {@link qx.io.ImageLoader}. This is to make sure that
     *             the renderer knows the dimensions and the format of the image.
     *           </li>
     *           <li>"imageWidth": (type int) the width of the image in pixels.</li>
     *           <li>"imageHeight": (type int) the height of the image in pixels.</li>
     *           <li>"tooltip": (type string) must be the image tooltip text.</li>
     *           </ul>
     * @throws {Error} the abstract function warning.
     */
    _identifyImage : function(cellInfo) {
      throw new Error("_identifyImage is abstract");
    },


    /**
     * Retrieves the image infos.
     *
     * @param cellInfo {Map} The information about the cell.
     *          See {@link qx.ui.table.cellrenderer.Abstract#createDataCellHtml}.
     * @return {Map} Map with an "url" attribute (type string)
     *                 holding the URL of the image to show
     *                 and a "tooltip" attribute
     *                 (type string) being the tooltip text (or null if none was specified)
     */
    _getImageInfos : function(cellInfo)
    {
      // Query the subclass about image and tooltip
      var imageData = this._identifyImage(cellInfo);

      // If subclass refuses to give map, construct it with required properties
      // If no map is given, but instead a string, assume that this string is
      // the URL of the image [BUG #4289]
      if (imageData == null || typeof imageData == "string")
      {
        imageData =
        {
          url : imageData,
          tooltip : null
        };
      }

      // If sizes are not included in map given by subclass,
      // fall-back to calculated image size
      if (!imageData.imageWidth || !imageData.imageHeight)
      {
        var sizes = this.__getImageSize(imageData.url);

        imageData.imageWidth = sizes.width;
        imageData.imageHeight = sizes.height;
      }

      // Add width and height keys to map [BUG #4289]
      // - [width|height] is read by _getContentHtml()
      // - [imageWidth|imageHeight] is possibly read in legacy applications
      imageData.width = imageData.imageWidth;
      imageData.height = imageData.imageHeight;

      return imageData;
    },


    /**
     * Compute the size of the given image
     *
     * @param source {String} the image URL
     * @return {Map} A map containing the image's <code>width</code> and
     *    <code>height</code>
     */
    __getImageSize : function(source)
    {
      var ResourceManager = qx.util.ResourceManager.getInstance();
      var ImageLoader = qx.io.ImageLoader;
      var width, height;

      // Detect if the image registry knows this image
      if (ResourceManager.has(source))
      {
        width = ResourceManager.getImageWidth(source);
        height = ResourceManager.getImageHeight(source);
      }
      else if (ImageLoader.isLoaded(source))
      {
        width = ImageLoader.getWidth(source);
        height = ImageLoader.getHeight(source);
      }
      else
      {
        width = this.__defaultWidth;
        height = this.__defaultHeight;
      }

      return {width : width, height : height};
    },


    // overridden
    createDataCellHtml : function(cellInfo, htmlArr)
    {
      this.__imageData = this._getImageInfos(cellInfo);
      return this.base(arguments, cellInfo, htmlArr);
    },


    // overridden
    _getCellClass : function(cellInfo) {
      return this.base(arguments) + " qooxdoo-table-cell-icon";
    },


    // overridden
    _getContentHtml : function(cellInfo)
    {
      var content = "<div></div>";

      // set image
      if (this.__imageData.url) {
        content = qx.bom.element.Decoration.create(
          this.__imageData.url,
          this.getRepeat(),
          {
          width: this.__imageData.width + "px",
          height: this.__imageData.height + "px",
          display: qx.core.Environment.get("css.inlineblock"),
          verticalAlign: "top",
          position: "static"
        });
      };

      return content;
    },


    // overridden
    _getCellAttributes : function(cellInfo)
    {
      var tooltip = this.__imageData.tooltip;

      if (tooltip) {
        return "title='" + tooltip + "'";
      } else {
        return "";
      }
    }
  },

  /*
  *****************************************************************************
     DESTRUCTOR
  *****************************************************************************
  */

  destruct : function()
  {
    this.__imageData = null;
  }
});
/* ************************************************************************

    qooxdoo - the new era of web development

    http://qooxdoo.org

    Copyright:
      2007 by Tartan Solutions, Inc, http://www.tartansolutions.com

    License:
      LGPL 2.1: http://www.gnu.org/licenses/lgpl.html

    Authors:
      * Dan Hummon

************************************************************************ */

/**
 * The image cell renderer renders image into table cells.
 */
qx.Class.define("qx.ui.table.cellrenderer.Image",
{
  extend : qx.ui.table.cellrenderer.AbstractImage,




  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */


  /**
   * @param height {Integer?16} The height of the image. The default is 16.
   * @param width {Integer?16} The width of the image. The default is 16.
   */
  construct : function(width, height)
  {
    this.base(arguments);

    if (width) {
      this.__imageWidth = width;
    }

    if (height) {
      this.__imageHeight = height;
    }

    this.__am = qx.util.AliasManager.getInstance();
  },




  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    __am : null,
    __imageHeight : 16,
    __imageWidth : 16,


    // overridden
    _identifyImage : function(cellInfo)
    {
      var imageHints =
      {
        imageWidth  : this.__imageWidth,
        imageHeight : this.__imageHeight
      };

      if (cellInfo.value == "") {
        imageHints.url = null;
      } else {
        imageHints.url = this.__am.resolve(cellInfo.value);
      }

      imageHints.tooltip = cellInfo.tooltip;

      return imageHints;
    }
  },

  /*
  *****************************************************************************
     DESTRUCTOR
  *****************************************************************************
  */

  destruct : function() {
    this.__am = null;
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2007 OpenHex SPRL, http://www.openhex.org

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Gaetan de Menten (ged)

************************************************************************ */

/**
 * Specific data cell renderer for dates.
 */
qx.Class.define("qx.ui.table.cellrenderer.Date",
{
  extend : qx.ui.table.cellrenderer.Conditional,

  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

  properties :
  {
    /**
     * DateFormat used to format the data.
     */
    dateFormat :
    {
      check : "qx.util.format.DateFormat",
      init : null,
      nullable : true
    }
  },


  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    _getContentHtml : function(cellInfo)
    {
      var df = this.getDateFormat();

      if (df)
      {
        if (cellInfo.value) {
          return qx.bom.String.escape(df.format(cellInfo.value));
        } else {
          return "";
        }
      }
      else
      {
        return cellInfo.value || "";
      }
    },


    // overridden
    _getCellClass : function(cellInfo) {
      return "qooxdoo-table-cell";
    }
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2007 OpenHex SPRL, http://www.openhex.org

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Dirk Wellmann (dw(at)piponline.net)

************************************************************************ */

/**
 * This Cellrender is for transparent use, without escaping! Use this Cellrender
 * to output plain HTML content.
 */
qx.Class.define("qx.ui.table.cellrenderer.Html",
{
  extend : qx.ui.table.cellrenderer.Conditional,

  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    // overridden
    _getContentHtml : function(cellInfo) {
      return (cellInfo.value || "");
    },

    // overridden
    _getCellClass : function(cellInfo) {
      return "qooxdoo-table-cell";
    }
  }
});
qx.Class.define("itx.ui.table.cellrenderer.TokenField",{
	extend : qx.ui.table.cellrenderer.Conditional,

	/*
	*****************************************************************************
	MEMBERS
	*****************************************************************************
	*/

	members :{
		// overridden
		_getContentHtml : function(cellInfo) {

			var text = [];
			var options = qx.lang.Json.parse( cellInfo.value || "[]" );
			
			if( !qx.lang.Type.isArray(options) ){
				//options = [ options ];
				alert("Invalid data in tableWidget.TokenField cell");
				return;
			}

			for( var i=0;i<options.length;i++){
				text.push(options[i].label);
			}

			return ( text.join(", ") || "" );
		},

		// overridden
		_getCellClass : function(cellInfo) {
			return "qooxdoo-table-cell";
		}
	}
});

/*************************************************************************
* @asset(itx/icons/*)
************************************************************************ */

qx.Class.define("itx.ui.table.cellrenderer.CheckBox",
{
  extend : qx.ui.table.cellrenderer.Conditional,

  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    // overridden
    _getContentHtml : function(cellInfo) {

		//var width = cellInfo.styleWidth-12;
		var width=cellInfo.styleWidth-12;
		var height = cellInfo.styleHeight-8;
		var icon='itx/icons/checkbox-unchecked.png'; //http://icons.iconarchive.com/icons/visualpharm/icons8-metro-style/32/Very-Basic-Unchecked-checkbox-icon.png';
		
		if( cellInfo.value ){
			//icon='http://icons.iconarchive.com/icons/visualpharm/icons8-metro-style/32/Very-Basic-Checked-checkbox-icon.png';
			icon='itx/icons/checkbox-checked.png';
		}
		
		icon=qx.util.ResourceManager.getInstance().toUri(icon);
		return '<img src="'+icon+'" width="'+width+'" height="'+height+'"/>';
    },

    // overridden
    _getCellClass : function(cellInfo) {
      return "qooxdoo-table-cell";
    }
  }
});
qx.Class.define("itx.Util", {
    type:"static",

    statics:{
		
		//deprecated
        empty:function(value){
			if( value === undefined ) return true;
			if( value === null ) return true;
			if( value === false ) return true;
			if( value === 0 ) return true;
			if( value==0 ) return true;
				
			return false;
        }
    }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2004-2008 1&1 Internet AG, Germany, http://www.1und1.de

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's left-level directory for details.

   Authors:
     * Sebastian Werner (wpbasti)
     * Fabian Jakobs (fjakobs)

************************************************************************ */

/**
 * Container, which allows vertical and horizontal scrolling if the contents is
 * larger than the container.
 *
 * Note that this class can only have one child widget. This container has a
 * fixed layout, which cannot be changed.
 *
 * *Example*
 *
 * Here is a little example of how to use the widget.
 *
 * <pre class='javascript'>
 *   // create scroll container
 *   var scroll = new qx.ui.container.Scroll().set({
 *     width: 300,
 *     height: 200
 *   });
 *
 *   // add a widget which is larger than the container
 *   scroll.add(new qx.ui.core.Widget().set({
 *     width: 600,
 *     minWidth: 600,
 *     height: 400,
 *     minHeight: 400
 *   }));
 *
 *   this.getRoot().add(scroll);
 * </pre>
 *
 * This example creates a scroll container and adds a widget, which is larger
 * than the container. This will cause the container to display vertical
 * and horizontal toolbars.
 *
 * *External Documentation*
 *
 * <a href='http://manual.qooxdoo.org/${qxversion}/pages/widget/scroll.html' target='_blank'>
 * Documentation of this widget in the qooxdoo manual.</a>
 */
qx.Class.define("qx.ui.container.Scroll",
{
  extend : qx.ui.core.scroll.AbstractScrollArea,
  include : [qx.ui.core.MContentPadding],



  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  /**
   * @param content {qx.ui.core.LayoutItem?null} The content widget of the scroll
   *    container.
   */
  construct : function(content)
  {
    this.base(arguments);

    if (content) {
      this.add(content);
    }
  },




  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    /**
     * Sets the content of the scroll container. Scroll containers
     * may only have one child, so it always replaces the current
     * child with the given one.
     *
     * @param widget {qx.ui.core.Widget} Widget to insert
     */
    add : function(widget) {
      this.getChildControl("pane").add(widget);
    },


    /**
     * Returns the content of the scroll area.
     *
     * @param widget {qx.ui.core.Widget} Widget to remove
     */
    remove : function(widget) {
      this.getChildControl("pane").remove(widget);
    },


    /**
     * Returns the content of the scroll container.
     *
     * Scroll containers may only have one child. This
     * method returns an array containing the child or an empty array.
     *
     * @return {Object[]} The child array
     */
    getChildren : function() {
      return this.getChildControl("pane").getChildren();
    },


    /**
     * Returns the element, to which the content padding should be applied.
     *
     * @return {qx.ui.core.Widget} The content padding target.
     */
    _getContentPaddingTarget : function() {
      return this.getChildControl("pane");
    }
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2004-2008 1&1 Internet AG, Germany, http://www.1und1.de

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Sebastian Werner (wpbasti)
     * Andreas Ecker (ecker)

************************************************************************ */
/**
 * Tango icons
 */
qx.Theme.define("qx.theme.icon.Tango",
{
  title : "Tango",
  aliases : {
    "icon" : "qx/icon/Tango"
  }
});
/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

/**
 * 
 *
 * @asset(itx/decoration/modern/font/Roboto-Regular.ttf)
 */
 
qx.Theme.define("itx.theme.modern.Font",
{
  fonts :
  {
    "default" :
    {
      size : (qx.core.Environment.get("os.name") == "win" &&
        (qx.core.Environment.get("os.version") == "7" ||
        qx.core.Environment.get("os.version") == "vista")) ? 12 : 12,
      lineHeight : 1.4,
      family : qx.core.Environment.get("os.name") == "osx" ?
        [ "Roboto","Lucida Grande" ] :
        ((qx.core.Environment.get("os.name") == "win" &&
          (qx.core.Environment.get("os.version") == "7" ||
          qx.core.Environment.get("os.version") == "vista"))) ?
        [ "Roboto","Segoe UI", "Candara" ] :
        [ "Roboto","Tahoma", "Liberation Sans", "Arial", "sans-serif" ]
		
      ,sources:
      [
        {
          family : "Roboto",
          source: [
            "itx/decoration/modern/font/Roboto-Regular.ttf"
          ]
        }
      ]
	
    },

    "bold" :
    {
      size : (qx.core.Environment.get("os.name") == "win" &&
        (qx.core.Environment.get("os.version") == "7" ||
        qx.core.Environment.get("os.version") == "vista")) ? 12 : 12,
      lineHeight : 1.4,
      family : qx.core.Environment.get("os.name") == "osx" ?
        [  "Roboto","Lucida Grande" ] :
        ((qx.core.Environment.get("os.name") == "win" &&
          (qx.core.Environment.get("os.version") == "7" ||
          qx.core.Environment.get("os.version") == "vista"))) ?
        [ "Roboto","Segoe UI", "Candara" ] :
        [ "Roboto","Tahoma", "Liberation Sans", "Arial", "sans-serif" ],
      bold : true
    },
	

    "small" :
    {
      size : (qx.core.Environment.get("os.name") == "win" &&
        (qx.core.Environment.get("os.version") == "7" ||
        qx.core.Environment.get("os.version") == "vista")) ? 11 : 10,
      lineHeight : 1.4,
      family : qx.core.Environment.get("os.name") == "osx" ?
        [  "Roboto","Lucida Grande" ] :
        ((qx.core.Environment.get("os.name") == "win" &&
          (qx.core.Environment.get("os.version") == "7" ||
          qx.core.Environment.get("os.version") == "vista"))) ?
        [ "Roboto","Segoe UI", "Candara" ] :
        [ "Roboto","Tahoma", "Liberation Sans", "Arial", "sans-serif" ]
    },

    "monospace" :
    {
      size: 11,
      lineHeight : 1.4,
      family : qx.core.Environment.get("os.name") == "osx" ?
        [ "Lucida Console", "Monaco" ] :
        ((qx.core.Environment.get("os.name") == "win" &&
          (qx.core.Environment.get("os.version") == "7" ||
          qx.core.Environment.get("os.version") == "vista"))) ?
        [ "Consolas" ] :
        [ "Consolas", "DejaVu Sans Mono", "Courier New", "monospace" ]
    }
  }
});
