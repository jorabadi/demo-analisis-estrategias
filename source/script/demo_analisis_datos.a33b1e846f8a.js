/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("itx.theme.modern.Color",
{
  colors :
  {
		/*
		---------------------------------------------------------------------------
		  BACKGROUND COLORS
		---------------------------------------------------------------------------
		*/

		// application, desktop, ...
		"background-application" : "white",

		// pane color for windows, splitpanes, ...
		"background-pane" : "white",

		// textfields, ...
		"background-light" : "#FCFCFC",

		// headers, ...
		"background-medium" : "#EEEEEE",

		// splitpane
		"background-splitpane" : "#AFAFAF",

		// tooltip, ...
		"background-tip" : "#ffffdd",

		// error tooltip
		"background-tip-error": "#C72B2B",

		// tables, ...
		"background-odd" : "#E4E4E4",

		// progress bar
		"progressbar-background" : "white",




		/*
		---------------------------------------------------------------------------
		  TEXT COLORS
		---------------------------------------------------------------------------
		*/

		// other types
		"text-light" : "#909090",
		"text-gray" : "#4a4a4a",

		// labels
		"text-label" : "#1a1a1a",

		// group boxes
		"text-title" : "#314a6e",

		// text fields
		"text-input" : "#000000",

		// states
		"text-hovered"  : "#001533",
		"text-disabled" : "#7B7A7E",
		"text-selected" : "#fffefe",
		"text-active"   : "#26364D",
		"text-inactive" : "#404955",
		"text-placeholder" : "#CBC8CD",






		/*
		---------------------------------------------------------------------------
		  BORDER COLORS
		---------------------------------------------------------------------------
		*/

		"border-inner-scrollbar" : "white",

		// menus, tables, scrollbars, list, etc.
		"border-main" : "#d3d3d3",
		"menu-separator-top" : "#C5C5C5",
		"menu-separator-bottom" : "#FAFAFA",

		// between toolbars
		"border-separator" : "#ddd",
		"border-toolbar-button-outer" : "#b6b6b6",
		"border-toolbar-border-inner" : "#f8f8f8",
		"border-toolbar-separator-right" : "#f4f4f4",
		"border-toolbar-separator-left" : "#b8b8b8",

		// text fields
		"border-input" : "#d3d3d3",
		"border-inner-input" : "white",

		// disabled text fields
		"border-disabled" : "#B6B6B6",

		// tab view, window
		"border-pane" : "#00204D",

		// buttons
		"border-button" : "#efefef",

		// tables (vertical line)
		"border-column" : "#CCCCCC",

		// focus state of text fields
		"border-focused" : "#99C3FE",

		// invalid form widgets
		"invalid" : "#990000",
		"border-focused-invalid" : "#FF9999",

		// drag & drop
		"border-dragover" : "#33508D",

		"keyboard-focus" : "black",


		/*
		---------------------------------------------------------------------------
		  TABLE COLORS
		---------------------------------------------------------------------------
		*/

		// equal to "background-pane"
		"table-pane" : "white",

		// own table colors
		// "table-row-background-selected" and "table-row-background-focused-selected"
		// are inspired by the colors of the selection decorator
		"table-focus-indicator" : "#4A90E2",
		"table-row-background-focused-selected" : "#4A90E2",
		"table-row-background-focused" : "#80BBFF",
		"table-row-background-selected" : "#80BBFF",

		// equal to "background-pane" and "background-odd"
		"table-row-background-even" : "#F5F5F5",
		"table-row-background-odd" : "white",

		// equal to "text-selected" and "text-label"
		"table-row-selected" : "#fffefe",
		"table-row" : "#1a1a1a",

		// equal to "border-collumn"
		"table-row-line" : "#eee",
		"table-column-line" : "transparent",

		//"table-header" : [ 242, 242, 242 ],
		"table-header" : "#ddd",
		"table-header-hovered" : "white",
		
		

		/*
		---------------------------------------------------------------------------
		  PROGRESSIVE TABLE COLORS
		---------------------------------------------------------------------------
		*/

		/*
		"progressive-table-header"              : "#AAAAAA",
		"progressive-table-header-border-right" : "#F2F2F2",


		"progressive-table-row-background-even" : "#F4F4F4",
		"progressive-table-row-background-odd"  : "#E4E4E4",

		"progressive-progressbar-background"         : "gray",
		"progressive-progressbar-indicator-done"     : "#CCCCCC",
		"progressive-progressbar-indicator-undone"   : "white",
		"progressive-progressbar-percent-background" : "gray",
		"progressive-progressbar-percent-text"       : "white",
		*/

		/*
		---------------------------------------------------------------------------
		  CSS ONLY COLORS
		---------------------------------------------------------------------------
		*/
		
		"selected-start" : "#4A90E2",
		"selected-end" : "#4A90E2",
		"background-selected" : "#00368A",
		

		"tabview-background" : "#dddddd",

		"shadow" : qx.core.Environment.get("css.rgba") ? "rgba(0, 0, 0, 0.4)" : "#999999",

		"pane-start" : "#FBFBFB",
		"pane-end" : "#F0F0F0",

		"group-background" : "#E8E8E8",
		"group-border" : "#B4B4B4",

		"radiobutton-background" : "#EFEFEF",

		"checkbox-border" : "#314A6E",
		"checkbox-focus" : "#87AFE7",
		"checkbox-hovered" : "#B2D2FF",
		"checkbox-hovered-inner" : "#D1E4FF",
		"checkbox-inner" : "#EEEEEE",
		"checkbox-start" : "#E4E4E4",
		"checkbox-end" : "#F3F3F3",
		"checkbox-disabled-border" : "#787878",
		"checkbox-disabled-inner" : "#CACACA",
		"checkbox-disabled-start" : "#D0D0D0",
		"checkbox-disabled-end" : "#D8D8D8",
		"checkbox-hovered-inner-invalid" : "#FAF2F2",
		"checkbox-hovered-invalid" : "#F7E9E9",

		"radiobutton-checked" : "#005BC3",
		"radiobutton-disabled" : "#D5D5D5",
		"radiobutton-checked-disabled" : "#7B7B7B",
		"radiobutton-hovered-invalid" : "#F7EAEA",

		"tooltip-error" : "#C82C2C",

		"scrollbar-start" : "#CCCCCC",
		"scrollbar-end" : "#F1F1F1",
		"scrollbar-slider-start" : "#EEEEEE",
		"scrollbar-slider-end" : "#C3C3C3",

		"button-border-disabled" : "#959595",
		//"button-start" : "#F0F0F0",
		//"button-end" : "#AFAFAF",
		"button-start" : "#F0F0F0",
		"button-end" : "#dddddd",
		"button-disabled-start" : "#F4F4F4",
		"button-disabled-end" : "#BABABA",
		"button-hovered-start" : "#F0F9FE",
		"button-hovered-end" : "#8EB8D6",
		"button-focused" : "#83BAEA",

		"border-invalbuid" : "#930000",

		"input-start" : "#F0F0F0",
		"input-end" : "#FBFCFB",
		"input-focused-start" : "#D7E7F4",
		"input-focused-end" : "#5CB0FD",
		"input-focused-inner-invalid" : "#FF6B78",
		"input-border-disabled" : "#9B9B9B",
		"input-border-inner" : "white",

		"toolbar-start" : "#EFEFEF",
		"toolbar-end" : "#DDDDDD",

		
		"window-border" : "#dddddd",
		"window-border-caption" : "#727272",
		"window-caption-active-text" : "#000",
		"window-caption-active-start" : "#f2f2f2",
		"window-caption-active-end" : "#ddd",
		"window-caption-inactive-start" : "#F2F2F2",
		"window-caption-inactive-end" : "#DBDBDB",
		"window-statusbar-background" : "#EFEFEF",
		
		
		/*
		"window-border" : "#00A2C1",
		"window-border-caption" : "#000",
		"window-caption-active-text" : "#000",
		"window-caption-active-end" : "#00A2C1",
		"window-caption-active-start" : "#00A2C1",
		"window-caption-inactive-start" : "#F2F2F2",
		"window-caption-inactive-end" : "#DBDBDB",
		"window-statusbar-background" : "#EFEFEF",		
		*/


		"tabview-start" : "white",
		"tabview-end" : "white",
		"tabview-inactive" : "white",
		"tabview-inactive-start" : "#EAEAEA",
		"tabview-inactive-end" : "#dddddd",

		//"table-header-start" : "#E8E8E8",
		//"table-header-end" : "#B3B3B3",
		"table-header-start" : "#eee",
		"table-header-end" : "#ccc",

		"menu-start" : "#fff",
		"menu-end" : "#ddd",
		"menubar-start" : "#fff",

		"groupitem-start" : "#A7A7A7",
		"groupitem-end" : "#949494",
		"groupitem-text" : "white",
		"virtual-row-layer-background-even" : "white",
		"virtual-row-layer-background-odd" : "white"
  }
});
